(function(A,w){function ma(){if(!c.isReady){try{s.documentElement.doScroll("left")}catch(a){setTimeout(ma,1);return}c.ready()}}function Qa(a,b){b.src?c.ajax({url:b.src,async:false,dataType:"script"}):c.globalEval(b.text||b.textContent||b.innerHTML||"");b.parentNode&&b.parentNode.removeChild(b)}function X(a,b,d,f,e,j){var i=a.length;if(typeof b==="object"){for(var o in b)X(a,o,b[o],f,e,d);return a}if(d!==w){f=!j&&f&&c.isFunction(d);for(o=0;o<i;o++)e(a[o],b,f?d.call(a[o],o,e(a[o],b)):d,j);return a}return i?
e(a[0],b):w}function J(){return(new Date).getTime()}function Y(){return false}function Z(){return true}function na(a,b,d){d[0].type=a;return c.event.handle.apply(b,d)}function oa(a){var b,d=[],f=[],e=arguments,j,i,o,k,n,r;i=c.data(this,"events");if(!(a.liveFired===this||!i||!i.live||a.button&&a.type==="click")){a.liveFired=this;var u=i.live.slice(0);for(k=0;k<u.length;k++){i=u[k];i.origType.replace(O,"")===a.type?f.push(i.selector):u.splice(k--,1)}j=c(a.target).closest(f,a.currentTarget);n=0;for(r=
j.length;n<r;n++)for(k=0;k<u.length;k++){i=u[k];if(j[n].selector===i.selector){o=j[n].elem;f=null;if(i.preType==="mouseenter"||i.preType==="mouseleave")f=c(a.relatedTarget).closest(i.selector)[0];if(!f||f!==o)d.push({elem:o,handleObj:i})}}n=0;for(r=d.length;n<r;n++){j=d[n];a.currentTarget=j.elem;a.data=j.handleObj.data;a.handleObj=j.handleObj;if(j.handleObj.origHandler.apply(j.elem,e)===false){b=false;break}}return b}}function pa(a,b){return"live."+(a&&a!=="*"?a+".":"")+b.replace(/\./g,"`").replace(/ /g,
"&")}function qa(a){return!a||!a.parentNode||a.parentNode.nodeType===11}function ra(a,b){var d=0;b.each(function(){if(this.nodeName===(a[d]&&a[d].nodeName)){var f=c.data(a[d++]),e=c.data(this,f);if(f=f&&f.events){delete e.handle;e.events={};for(var j in f)for(var i in f[j])c.event.add(this,j,f[j][i],f[j][i].data)}}})}function sa(a,b,d){var f,e,j;b=b&&b[0]?b[0].ownerDocument||b[0]:s;if(a.length===1&&typeof a[0]==="string"&&a[0].length<512&&b===s&&!ta.test(a[0])&&(c.support.checkClone||!ua.test(a[0]))){e=
true;if(j=c.fragments[a[0]])if(j!==1)f=j}if(!f){f=b.createDocumentFragment();c.clean(a,b,f,d)}if(e)c.fragments[a[0]]=j?f:1;return{fragment:f,cacheable:e}}function K(a,b){var d={};c.each(va.concat.apply([],va.slice(0,b)),function(){d[this]=a});return d}function wa(a){return"scrollTo"in a&&a.document?a:a.nodeType===9?a.defaultView||a.parentWindow:false}var c=function(a,b){return new c.fn.init(a,b)},Ra=A.jQuery,Sa=A.$,s=A.document,T,Ta=/^[^<]*(<[\w\W]+>)[^>]*$|^#([\w-]+)$/,Ua=/^.[^:#\[\.,]*$/,Va=/\S/,
Wa=/^(\s|\u00A0)+|(\s|\u00A0)+$/g,Xa=/^<(\w+)\s*\/?>(?:<\/\1>)?$/,P=navigator.userAgent,xa=false,Q=[],L,$=Object.prototype.toString,aa=Object.prototype.hasOwnProperty,ba=Array.prototype.push,R=Array.prototype.slice,ya=Array.prototype.indexOf;c.fn=c.prototype={init:function(a,b){var d,f;if(!a)return this;if(a.nodeType){this.context=this[0]=a;this.length=1;return this}if(a==="body"&&!b){this.context=s;this[0]=s.body;this.selector="body";this.length=1;return this}if(typeof a==="string")if((d=Ta.exec(a))&&
(d[1]||!b))if(d[1]){f=b?b.ownerDocument||b:s;if(a=Xa.exec(a))if(c.isPlainObject(b)){a=[s.createElement(a[1])];c.fn.attr.call(a,b,true)}else a=[f.createElement(a[1])];else{a=sa([d[1]],[f]);a=(a.cacheable?a.fragment.cloneNode(true):a.fragment).childNodes}return c.merge(this,a)}else{if(b=s.getElementById(d[2])){if(b.id!==d[2])return T.find(a);this.length=1;this[0]=b}this.context=s;this.selector=a;return this}else if(!b&&/^\w+$/.test(a)){this.selector=a;this.context=s;a=s.getElementsByTagName(a);return c.merge(this,
a)}else return!b||b.jquery?(b||T).find(a):c(b).find(a);else if(c.isFunction(a))return T.ready(a);if(a.selector!==w){this.selector=a.selector;this.context=a.context}return c.makeArray(a,this)},selector:"",jquery:"1.4.2",length:0,size:function(){return this.length},toArray:function(){return R.call(this,0)},get:function(a){return a==null?this.toArray():a<0?this.slice(a)[0]:this[a]},pushStack:function(a,b,d){var f=c();c.isArray(a)?ba.apply(f,a):c.merge(f,a);f.prevObject=this;f.context=this.context;if(b===
"find")f.selector=this.selector+(this.selector?" ":"")+d;else if(b)f.selector=this.selector+"."+b+"("+d+")";return f},each:function(a,b){return c.each(this,a,b)},ready:function(a){c.bindReady();if(c.isReady)a.call(s,c);else Q&&Q.push(a);return this},eq:function(a){return a===-1?this.slice(a):this.slice(a,+a+1)},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},slice:function(){return this.pushStack(R.apply(this,arguments),"slice",R.call(arguments).join(","))},map:function(a){return this.pushStack(c.map(this,
function(b,d){return a.call(b,d,b)}))},end:function(){return this.prevObject||c(null)},push:ba,sort:[].sort,splice:[].splice};c.fn.init.prototype=c.fn;c.extend=c.fn.extend=function(){var a=arguments[0]||{},b=1,d=arguments.length,f=false,e,j,i,o;if(typeof a==="boolean"){f=a;a=arguments[1]||{};b=2}if(typeof a!=="object"&&!c.isFunction(a))a={};if(d===b){a=this;--b}for(;b<d;b++)if((e=arguments[b])!=null)for(j in e){i=a[j];o=e[j];if(a!==o)if(f&&o&&(c.isPlainObject(o)||c.isArray(o))){i=i&&(c.isPlainObject(i)||
c.isArray(i))?i:c.isArray(o)?[]:{};a[j]=c.extend(f,i,o)}else if(o!==w)a[j]=o}return a};c.extend({noConflict:function(a){A.$=Sa;if(a)A.jQuery=Ra;return c},isReady:false,ready:function(){if(!c.isReady){if(!s.body)return setTimeout(c.ready,13);c.isReady=true;if(Q){for(var a,b=0;a=Q[b++];)a.call(s,c);Q=null}c.fn.triggerHandler&&c(s).triggerHandler("ready")}},bindReady:function(){if(!xa){xa=true;if(s.readyState==="complete")return c.ready();if(s.addEventListener){s.addEventListener("DOMContentLoaded",
L,false);A.addEventListener("load",c.ready,false)}else if(s.attachEvent){s.attachEvent("onreadystatechange",L);A.attachEvent("onload",c.ready);var a=false;try{a=A.frameElement==null}catch(b){}s.documentElement.doScroll&&a&&ma()}}},isFunction:function(a){return $.call(a)==="[object Function]"},isArray:function(a){return $.call(a)==="[object Array]"},isPlainObject:function(a){if(!a||$.call(a)!=="[object Object]"||a.nodeType||a.setInterval)return false;if(a.constructor&&!aa.call(a,"constructor")&&!aa.call(a.constructor.prototype,
"isPrototypeOf"))return false;var b;for(b in a);return b===w||aa.call(a,b)},isEmptyObject:function(a){for(var b in a)return false;return true},error:function(a){throw a;},parseJSON:function(a){if(typeof a!=="string"||!a)return null;a=c.trim(a);if(/^[\],:{}\s]*$/.test(a.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return A.JSON&&A.JSON.parse?A.JSON.parse(a):(new Function("return "+
a))();else c.error("Invalid JSON: "+a)},noop:function(){},globalEval:function(a){if(a&&Va.test(a)){var b=s.getElementsByTagName("head")[0]||s.documentElement,d=s.createElement("script");d.type="text/javascript";if(c.support.scriptEval)d.appendChild(s.createTextNode(a));else d.text=a;b.insertBefore(d,b.firstChild);b.removeChild(d)}},nodeName:function(a,b){return a.nodeName&&a.nodeName.toUpperCase()===b.toUpperCase()},each:function(a,b,d){var f,e=0,j=a.length,i=j===w||c.isFunction(a);if(d)if(i)for(f in a){if(b.apply(a[f],
d)===false)break}else for(;e<j;){if(b.apply(a[e++],d)===false)break}else if(i)for(f in a){if(b.call(a[f],f,a[f])===false)break}else for(d=a[0];e<j&&b.call(d,e,d)!==false;d=a[++e]);return a},trim:function(a){return(a||"").replace(Wa,"")},makeArray:function(a,b){b=b||[];if(a!=null)a.length==null||typeof a==="string"||c.isFunction(a)||typeof a!=="function"&&a.setInterval?ba.call(b,a):c.merge(b,a);return b},inArray:function(a,b){if(b.indexOf)return b.indexOf(a);for(var d=0,f=b.length;d<f;d++)if(b[d]===
a)return d;return-1},merge:function(a,b){var d=a.length,f=0;if(typeof b.length==="number")for(var e=b.length;f<e;f++)a[d++]=b[f];else for(;b[f]!==w;)a[d++]=b[f++];a.length=d;return a},grep:function(a,b,d){for(var f=[],e=0,j=a.length;e<j;e++)!d!==!b(a[e],e)&&f.push(a[e]);return f},map:function(a,b,d){for(var f=[],e,j=0,i=a.length;j<i;j++){e=b(a[j],j,d);if(e!=null)f[f.length]=e}return f.concat.apply([],f)},guid:1,proxy:function(a,b,d){if(arguments.length===2)if(typeof b==="string"){d=a;a=d[b];b=w}else if(b&&
!c.isFunction(b)){d=b;b=w}if(!b&&a)b=function(){return a.apply(d||this,arguments)};if(a)b.guid=a.guid=a.guid||b.guid||c.guid++;return b},uaMatch:function(a){a=a.toLowerCase();a=/(webkit)[ \/]([\w.]+)/.exec(a)||/(opera)(?:.*version)?[ \/]([\w.]+)/.exec(a)||/(msie) ([\w.]+)/.exec(a)||!/compatible/.test(a)&&/(mozilla)(?:.*? rv:([\w.]+))?/.exec(a)||[];return{browser:a[1]||"",version:a[2]||"0"}},browser:{}});P=c.uaMatch(P);if(P.browser){c.browser[P.browser]=true;c.browser.version=P.version}if(c.browser.webkit)c.browser.safari=
true;if(ya)c.inArray=function(a,b){return ya.call(b,a)};T=c(s);if(s.addEventListener)L=function(){s.removeEventListener("DOMContentLoaded",L,false);c.ready()};else if(s.attachEvent)L=function(){if(s.readyState==="complete"){s.detachEvent("onreadystatechange",L);c.ready()}};(function(){c.support={};var a=s.documentElement,b=s.createElement("script"),d=s.createElement("div"),f="script"+J();d.style.display="none";d.innerHTML="   <link/><table></table><a href='/a' style='color:red;float:left;opacity:.55;'>a</a><input type='checkbox'/>";
var e=d.getElementsByTagName("*"),j=d.getElementsByTagName("a")[0];if(!(!e||!e.length||!j)){c.support={leadingWhitespace:d.firstChild.nodeType===3,tbody:!d.getElementsByTagName("tbody").length,htmlSerialize:!!d.getElementsByTagName("link").length,style:/red/.test(j.getAttribute("style")),hrefNormalized:j.getAttribute("href")==="/a",opacity:/^0.55$/.test(j.style.opacity),cssFloat:!!j.style.cssFloat,checkOn:d.getElementsByTagName("input")[0].value==="on",optSelected:s.createElement("select").appendChild(s.createElement("option")).selected,
parentNode:d.removeChild(d.appendChild(s.createElement("div"))).parentNode===null,deleteExpando:true,checkClone:false,scriptEval:false,noCloneEvent:true,boxModel:null};b.type="text/javascript";try{b.appendChild(s.createTextNode("window."+f+"=1;"))}catch(i){}a.insertBefore(b,a.firstChild);if(A[f]){c.support.scriptEval=true;delete A[f]}try{delete b.test}catch(o){c.support.deleteExpando=false}a.removeChild(b);if(d.attachEvent&&d.fireEvent){d.attachEvent("onclick",function k(){c.support.noCloneEvent=
false;d.detachEvent("onclick",k)});d.cloneNode(true).fireEvent("onclick")}d=s.createElement("div");d.innerHTML="<input type='radio' name='radiotest' checked='checked'/>";a=s.createDocumentFragment();a.appendChild(d.firstChild);c.support.checkClone=a.cloneNode(true).cloneNode(true).lastChild.checked;c(function(){var k=s.createElement("div");k.style.width=k.style.paddingLeft="1px";s.body.appendChild(k);c.boxModel=c.support.boxModel=k.offsetWidth===2;s.body.removeChild(k).style.display="none"});a=function(k){var n=
s.createElement("div");k="on"+k;var r=k in n;if(!r){n.setAttribute(k,"return;");r=typeof n[k]==="function"}return r};c.support.submitBubbles=a("submit");c.support.changeBubbles=a("change");a=b=d=e=j=null}})();c.props={"for":"htmlFor","class":"className",readonly:"readOnly",maxlength:"maxLength",cellspacing:"cellSpacing",rowspan:"rowSpan",colspan:"colSpan",tabindex:"tabIndex",usemap:"useMap",frameborder:"frameBorder"};var G="jQuery"+J(),Ya=0,za={};c.extend({cache:{},expando:G,noData:{embed:true,object:true,
applet:true},data:function(a,b,d){if(!(a.nodeName&&c.noData[a.nodeName.toLowerCase()])){a=a==A?za:a;var f=a[G],e=c.cache;if(!f&&typeof b==="string"&&d===w)return null;f||(f=++Ya);if(typeof b==="object"){a[G]=f;e[f]=c.extend(true,{},b)}else if(!e[f]){a[G]=f;e[f]={}}a=e[f];if(d!==w)a[b]=d;return typeof b==="string"?a[b]:a}},removeData:function(a,b){if(!(a.nodeName&&c.noData[a.nodeName.toLowerCase()])){a=a==A?za:a;var d=a[G],f=c.cache,e=f[d];if(b){if(e){delete e[b];c.isEmptyObject(e)&&c.removeData(a)}}else{if(c.support.deleteExpando)delete a[c.expando];
else a.removeAttribute&&a.removeAttribute(c.expando);delete f[d]}}}});c.fn.extend({data:function(a,b){if(typeof a==="undefined"&&this.length)return c.data(this[0]);else if(typeof a==="object")return this.each(function(){c.data(this,a)});var d=a.split(".");d[1]=d[1]?"."+d[1]:"";if(b===w){var f=this.triggerHandler("getData"+d[1]+"!",[d[0]]);if(f===w&&this.length)f=c.data(this[0],a);return f===w&&d[1]?this.data(d[0]):f}else return this.trigger("setData"+d[1]+"!",[d[0],b]).each(function(){c.data(this,
a,b)})},removeData:function(a){return this.each(function(){c.removeData(this,a)})}});c.extend({queue:function(a,b,d){if(a){b=(b||"fx")+"queue";var f=c.data(a,b);if(!d)return f||[];if(!f||c.isArray(d))f=c.data(a,b,c.makeArray(d));else f.push(d);return f}},dequeue:function(a,b){b=b||"fx";var d=c.queue(a,b),f=d.shift();if(f==="inprogress")f=d.shift();if(f){b==="fx"&&d.unshift("inprogress");f.call(a,function(){c.dequeue(a,b)})}}});c.fn.extend({queue:function(a,b){if(typeof a!=="string"){b=a;a="fx"}if(b===
w)return c.queue(this[0],a);return this.each(function(){var d=c.queue(this,a,b);a==="fx"&&d[0]!=="inprogress"&&c.dequeue(this,a)})},dequeue:function(a){return this.each(function(){c.dequeue(this,a)})},delay:function(a,b){a=c.fx?c.fx.speeds[a]||a:a;b=b||"fx";return this.queue(b,function(){var d=this;setTimeout(function(){c.dequeue(d,b)},a)})},clearQueue:function(a){return this.queue(a||"fx",[])}});var Aa=/[\n\t]/g,ca=/\s+/,Za=/\r/g,$a=/href|src|style/,ab=/(button|input)/i,bb=/(button|input|object|select|textarea)/i,
cb=/^(a|area)$/i,Ba=/radio|checkbox/;c.fn.extend({attr:function(a,b){return X(this,a,b,true,c.attr)},removeAttr:function(a){return this.each(function(){c.attr(this,a,"");this.nodeType===1&&this.removeAttribute(a)})},addClass:function(a){if(c.isFunction(a))return this.each(function(n){var r=c(this);r.addClass(a.call(this,n,r.attr("class")))});if(a&&typeof a==="string")for(var b=(a||"").split(ca),d=0,f=this.length;d<f;d++){var e=this[d];if(e.nodeType===1)if(e.className){for(var j=" "+e.className+" ",
i=e.className,o=0,k=b.length;o<k;o++)if(j.indexOf(" "+b[o]+" ")<0)i+=" "+b[o];e.className=c.trim(i)}else e.className=a}return this},removeClass:function(a){if(c.isFunction(a))return this.each(function(k){var n=c(this);n.removeClass(a.call(this,k,n.attr("class")))});if(a&&typeof a==="string"||a===w)for(var b=(a||"").split(ca),d=0,f=this.length;d<f;d++){var e=this[d];if(e.nodeType===1&&e.className)if(a){for(var j=(" "+e.className+" ").replace(Aa," "),i=0,o=b.length;i<o;i++)j=j.replace(" "+b[i]+" ",
" ");e.className=c.trim(j)}else e.className=""}return this},toggleClass:function(a,b){var d=typeof a,f=typeof b==="boolean";if(c.isFunction(a))return this.each(function(e){var j=c(this);j.toggleClass(a.call(this,e,j.attr("class"),b),b)});return this.each(function(){if(d==="string")for(var e,j=0,i=c(this),o=b,k=a.split(ca);e=k[j++];){o=f?o:!i.hasClass(e);i[o?"addClass":"removeClass"](e)}else if(d==="undefined"||d==="boolean"){this.className&&c.data(this,"__className__",this.className);this.className=
this.className||a===false?"":c.data(this,"__className__")||""}})},hasClass:function(a){a=" "+a+" ";for(var b=0,d=this.length;b<d;b++)if((" "+this[b].className+" ").replace(Aa," ").indexOf(a)>-1)return true;return false},val:function(a){if(a===w){var b=this[0];if(b){if(c.nodeName(b,"option"))return(b.attributes.value||{}).specified?b.value:b.text;if(c.nodeName(b,"select")){var d=b.selectedIndex,f=[],e=b.options;b=b.type==="select-one";if(d<0)return null;var j=b?d:0;for(d=b?d+1:e.length;j<d;j++){var i=
e[j];if(i.selected){a=c(i).val();if(b)return a;f.push(a)}}return f}if(Ba.test(b.type)&&!c.support.checkOn)return b.getAttribute("value")===null?"on":b.value;return(b.value||"").replace(Za,"")}return w}var o=c.isFunction(a);return this.each(function(k){var n=c(this),r=a;if(this.nodeType===1){if(o)r=a.call(this,k,n.val());if(typeof r==="number")r+="";if(c.isArray(r)&&Ba.test(this.type))this.checked=c.inArray(n.val(),r)>=0;else if(c.nodeName(this,"select")){var u=c.makeArray(r);c("option",this).each(function(){this.selected=
c.inArray(c(this).val(),u)>=0});if(!u.length)this.selectedIndex=-1}else this.value=r}})}});c.extend({attrFn:{val:true,css:true,html:true,text:true,data:true,width:true,height:true,offset:true},attr:function(a,b,d,f){if(!a||a.nodeType===3||a.nodeType===8)return w;if(f&&b in c.attrFn)return c(a)[b](d);f=a.nodeType!==1||!c.isXMLDoc(a);var e=d!==w;b=f&&c.props[b]||b;if(a.nodeType===1){var j=$a.test(b);if(b in a&&f&&!j){if(e){b==="type"&&ab.test(a.nodeName)&&a.parentNode&&c.error("type property can't be changed");
a[b]=d}if(c.nodeName(a,"form")&&a.getAttributeNode(b))return a.getAttributeNode(b).nodeValue;if(b==="tabIndex")return(b=a.getAttributeNode("tabIndex"))&&b.specified?b.value:bb.test(a.nodeName)||cb.test(a.nodeName)&&a.href?0:w;return a[b]}if(!c.support.style&&f&&b==="style"){if(e)a.style.cssText=""+d;return a.style.cssText}e&&a.setAttribute(b,""+d);a=!c.support.hrefNormalized&&f&&j?a.getAttribute(b,2):a.getAttribute(b);return a===null?w:a}return c.style(a,b,d)}});var O=/\.(.*)$/,db=function(a){return a.replace(/[^\w\s\.\|`]/g,
function(b){return"\\"+b})};c.event={add:function(a,b,d,f){if(!(a.nodeType===3||a.nodeType===8)){if(a.setInterval&&a!==A&&!a.frameElement)a=A;var e,j;if(d.handler){e=d;d=e.handler}if(!d.guid)d.guid=c.guid++;if(j=c.data(a)){var i=j.events=j.events||{},o=j.handle;if(!o)j.handle=o=function(){return typeof c!=="undefined"&&!c.event.triggered?c.event.handle.apply(o.elem,arguments):w};o.elem=a;b=b.split(" ");for(var k,n=0,r;k=b[n++];){j=e?c.extend({},e):{handler:d,data:f};if(k.indexOf(".")>-1){r=k.split(".");
k=r.shift();j.namespace=r.slice(0).sort().join(".")}else{r=[];j.namespace=""}j.type=k;j.guid=d.guid;var u=i[k],z=c.event.special[k]||{};if(!u){u=i[k]=[];if(!z.setup||z.setup.call(a,f,r,o)===false)if(a.addEventListener)a.addEventListener(k,o,false);else a.attachEvent&&a.attachEvent("on"+k,o)}if(z.add){z.add.call(a,j);if(!j.handler.guid)j.handler.guid=d.guid}u.push(j);c.event.global[k]=true}a=null}}},global:{},remove:function(a,b,d,f){if(!(a.nodeType===3||a.nodeType===8)){var e,j=0,i,o,k,n,r,u,z=c.data(a),
C=z&&z.events;if(z&&C){if(b&&b.type){d=b.handler;b=b.type}if(!b||typeof b==="string"&&b.charAt(0)==="."){b=b||"";for(e in C)c.event.remove(a,e+b)}else{for(b=b.split(" ");e=b[j++];){n=e;i=e.indexOf(".")<0;o=[];if(!i){o=e.split(".");e=o.shift();k=new RegExp("(^|\\.)"+c.map(o.slice(0).sort(),db).join("\\.(?:.*\\.)?")+"(\\.|$)")}if(r=C[e])if(d){n=c.event.special[e]||{};for(B=f||0;B<r.length;B++){u=r[B];if(d.guid===u.guid){if(i||k.test(u.namespace)){f==null&&r.splice(B--,1);n.remove&&n.remove.call(a,u)}if(f!=
null)break}}if(r.length===0||f!=null&&r.length===1){if(!n.teardown||n.teardown.call(a,o)===false)Ca(a,e,z.handle);delete C[e]}}else for(var B=0;B<r.length;B++){u=r[B];if(i||k.test(u.namespace)){c.event.remove(a,n,u.handler,B);r.splice(B--,1)}}}if(c.isEmptyObject(C)){if(b=z.handle)b.elem=null;delete z.events;delete z.handle;c.isEmptyObject(z)&&c.removeData(a)}}}}},trigger:function(a,b,d,f){var e=a.type||a;if(!f){a=typeof a==="object"?a[G]?a:c.extend(c.Event(e),a):c.Event(e);if(e.indexOf("!")>=0){a.type=
e=e.slice(0,-1);a.exclusive=true}if(!d){a.stopPropagation();c.event.global[e]&&c.each(c.cache,function(){this.events&&this.events[e]&&c.event.trigger(a,b,this.handle.elem)})}if(!d||d.nodeType===3||d.nodeType===8)return w;a.result=w;a.target=d;b=c.makeArray(b);b.unshift(a)}a.currentTarget=d;(f=c.data(d,"handle"))&&f.apply(d,b);f=d.parentNode||d.ownerDocument;try{if(!(d&&d.nodeName&&c.noData[d.nodeName.toLowerCase()]))if(d["on"+e]&&d["on"+e].apply(d,b)===false)a.result=false}catch(j){}if(!a.isPropagationStopped()&&
f)c.event.trigger(a,b,f,true);else if(!a.isDefaultPrevented()){f=a.target;var i,o=c.nodeName(f,"a")&&e==="click",k=c.event.special[e]||{};if((!k._default||k._default.call(d,a)===false)&&!o&&!(f&&f.nodeName&&c.noData[f.nodeName.toLowerCase()])){try{if(f[e]){if(i=f["on"+e])f["on"+e]=null;c.event.triggered=true;f[e]()}}catch(n){}if(i)f["on"+e]=i;c.event.triggered=false}}},handle:function(a){var b,d,f,e;a=arguments[0]=c.event.fix(a||A.event);a.currentTarget=this;b=a.type.indexOf(".")<0&&!a.exclusive;
if(!b){d=a.type.split(".");a.type=d.shift();f=new RegExp("(^|\\.)"+d.slice(0).sort().join("\\.(?:.*\\.)?")+"(\\.|$)")}e=c.data(this,"events");d=e[a.type];if(e&&d){d=d.slice(0);e=0;for(var j=d.length;e<j;e++){var i=d[e];if(b||f.test(i.namespace)){a.handler=i.handler;a.data=i.data;a.handleObj=i;i=i.handler.apply(this,arguments);if(i!==w){a.result=i;if(i===false){a.preventDefault();a.stopPropagation()}}if(a.isImmediatePropagationStopped())break}}}return a.result},props:"altKey attrChange attrName bubbles button cancelable charCode clientX clientY ctrlKey currentTarget data detail eventPhase fromElement handler keyCode layerX layerY metaKey newValue offsetX offsetY originalTarget pageX pageY prevValue relatedNode relatedTarget screenX screenY shiftKey srcElement target toElement view wheelDelta which".split(" "),
fix:function(a){if(a[G])return a;var b=a;a=c.Event(b);for(var d=this.props.length,f;d;){f=this.props[--d];a[f]=b[f]}if(!a.target)a.target=a.srcElement||s;if(a.target.nodeType===3)a.target=a.target.parentNode;if(!a.relatedTarget&&a.fromElement)a.relatedTarget=a.fromElement===a.target?a.toElement:a.fromElement;if(a.pageX==null&&a.clientX!=null){b=s.documentElement;d=s.body;a.pageX=a.clientX+(b&&b.scrollLeft||d&&d.scrollLeft||0)-(b&&b.clientLeft||d&&d.clientLeft||0);a.pageY=a.clientY+(b&&b.scrollTop||
d&&d.scrollTop||0)-(b&&b.clientTop||d&&d.clientTop||0)}if(!a.which&&(a.charCode||a.charCode===0?a.charCode:a.keyCode))a.which=a.charCode||a.keyCode;if(!a.metaKey&&a.ctrlKey)a.metaKey=a.ctrlKey;if(!a.which&&a.button!==w)a.which=a.button&1?1:a.button&2?3:a.button&4?2:0;return a},guid:1E8,proxy:c.proxy,special:{ready:{setup:c.bindReady,teardown:c.noop},live:{add:function(a){c.event.add(this,a.origType,c.extend({},a,{handler:oa}))},remove:function(a){var b=true,d=a.origType.replace(O,"");c.each(c.data(this,
"events").live||[],function(){if(d===this.origType.replace(O,""))return b=false});b&&c.event.remove(this,a.origType,oa)}},beforeunload:{setup:function(a,b,d){if(this.setInterval)this.onbeforeunload=d;return false},teardown:function(a,b){if(this.onbeforeunload===b)this.onbeforeunload=null}}}};var Ca=s.removeEventListener?function(a,b,d){a.removeEventListener(b,d,false)}:function(a,b,d){a.detachEvent("on"+b,d)};c.Event=function(a){if(!this.preventDefault)return new c.Event(a);if(a&&a.type){this.originalEvent=
a;this.type=a.type}else this.type=a;this.timeStamp=J();this[G]=true};c.Event.prototype={preventDefault:function(){this.isDefaultPrevented=Z;var a=this.originalEvent;if(a){a.preventDefault&&a.preventDefault();a.returnValue=false}},stopPropagation:function(){this.isPropagationStopped=Z;var a=this.originalEvent;if(a){a.stopPropagation&&a.stopPropagation();a.cancelBubble=true}},stopImmediatePropagation:function(){this.isImmediatePropagationStopped=Z;this.stopPropagation()},isDefaultPrevented:Y,isPropagationStopped:Y,
isImmediatePropagationStopped:Y};var Da=function(a){var b=a.relatedTarget;try{for(;b&&b!==this;)b=b.parentNode;if(b!==this){a.type=a.data;c.event.handle.apply(this,arguments)}}catch(d){}},Ea=function(a){a.type=a.data;c.event.handle.apply(this,arguments)};c.each({mouseenter:"mouseover",mouseleave:"mouseout"},function(a,b){c.event.special[a]={setup:function(d){c.event.add(this,b,d&&d.selector?Ea:Da,a)},teardown:function(d){c.event.remove(this,b,d&&d.selector?Ea:Da)}}});if(!c.support.submitBubbles)c.event.special.submit=
{setup:function(){if(this.nodeName.toLowerCase()!=="form"){c.event.add(this,"click.specialSubmit",function(a){var b=a.target,d=b.type;if((d==="submit"||d==="image")&&c(b).closest("form").length)return na("submit",this,arguments)});c.event.add(this,"keypress.specialSubmit",function(a){var b=a.target,d=b.type;if((d==="text"||d==="password")&&c(b).closest("form").length&&a.keyCode===13)return na("submit",this,arguments)})}else return false},teardown:function(){c.event.remove(this,".specialSubmit")}};
if(!c.support.changeBubbles){var da=/textarea|input|select/i,ea,Fa=function(a){var b=a.type,d=a.value;if(b==="radio"||b==="checkbox")d=a.checked;else if(b==="select-multiple")d=a.selectedIndex>-1?c.map(a.options,function(f){return f.selected}).join("-"):"";else if(a.nodeName.toLowerCase()==="select")d=a.selectedIndex;return d},fa=function(a,b){var d=a.target,f,e;if(!(!da.test(d.nodeName)||d.readOnly)){f=c.data(d,"_change_data");e=Fa(d);if(a.type!=="focusout"||d.type!=="radio")c.data(d,"_change_data",
e);if(!(f===w||e===f))if(f!=null||e){a.type="change";return c.event.trigger(a,b,d)}}};c.event.special.change={filters:{focusout:fa,click:function(a){var b=a.target,d=b.type;if(d==="radio"||d==="checkbox"||b.nodeName.toLowerCase()==="select")return fa.call(this,a)},keydown:function(a){var b=a.target,d=b.type;if(a.keyCode===13&&b.nodeName.toLowerCase()!=="textarea"||a.keyCode===32&&(d==="checkbox"||d==="radio")||d==="select-multiple")return fa.call(this,a)},beforeactivate:function(a){a=a.target;c.data(a,
"_change_data",Fa(a))}},setup:function(){if(this.type==="file")return false;for(var a in ea)c.event.add(this,a+".specialChange",ea[a]);return da.test(this.nodeName)},teardown:function(){c.event.remove(this,".specialChange");return da.test(this.nodeName)}};ea=c.event.special.change.filters}s.addEventListener&&c.each({focus:"focusin",blur:"focusout"},function(a,b){function d(f){f=c.event.fix(f);f.type=b;return c.event.handle.call(this,f)}c.event.special[b]={setup:function(){this.addEventListener(a,
d,true)},teardown:function(){this.removeEventListener(a,d,true)}}});c.each(["bind","one"],function(a,b){c.fn[b]=function(d,f,e){if(typeof d==="object"){for(var j in d)this[b](j,f,d[j],e);return this}if(c.isFunction(f)){e=f;f=w}var i=b==="one"?c.proxy(e,function(k){c(this).unbind(k,i);return e.apply(this,arguments)}):e;if(d==="unload"&&b!=="one")this.one(d,f,e);else{j=0;for(var o=this.length;j<o;j++)c.event.add(this[j],d,i,f)}return this}});c.fn.extend({unbind:function(a,b){if(typeof a==="object"&&
!a.preventDefault)for(var d in a)this.unbind(d,a[d]);else{d=0;for(var f=this.length;d<f;d++)c.event.remove(this[d],a,b)}return this},delegate:function(a,b,d,f){return this.live(b,d,f,a)},undelegate:function(a,b,d){return arguments.length===0?this.unbind("live"):this.die(b,null,d,a)},trigger:function(a,b){return this.each(function(){c.event.trigger(a,b,this)})},triggerHandler:function(a,b){if(this[0]){a=c.Event(a);a.preventDefault();a.stopPropagation();c.event.trigger(a,b,this[0]);return a.result}},
toggle:function(a){for(var b=arguments,d=1;d<b.length;)c.proxy(a,b[d++]);return this.click(c.proxy(a,function(f){var e=(c.data(this,"lastToggle"+a.guid)||0)%d;c.data(this,"lastToggle"+a.guid,e+1);f.preventDefault();return b[e].apply(this,arguments)||false}))},hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}});var Ga={focus:"focusin",blur:"focusout",mouseenter:"mouseover",mouseleave:"mouseout"};c.each(["live","die"],function(a,b){c.fn[b]=function(d,f,e,j){var i,o=0,k,n,r=j||this.selector,
u=j?this:c(this.context);if(c.isFunction(f)){e=f;f=w}for(d=(d||"").split(" ");(i=d[o++])!=null;){j=O.exec(i);k="";if(j){k=j[0];i=i.replace(O,"")}if(i==="hover")d.push("mouseenter"+k,"mouseleave"+k);else{n=i;if(i==="focus"||i==="blur"){d.push(Ga[i]+k);i+=k}else i=(Ga[i]||i)+k;b==="live"?u.each(function(){c.event.add(this,pa(i,r),{data:f,selector:r,handler:e,origType:i,origHandler:e,preType:n})}):u.unbind(pa(i,r),e)}}return this}});c.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error".split(" "),
function(a,b){c.fn[b]=function(d){return d?this.bind(b,d):this.trigger(b)};if(c.attrFn)c.attrFn[b]=true});A.attachEvent&&!A.addEventListener&&A.attachEvent("onunload",function(){for(var a in c.cache)if(c.cache[a].handle)try{c.event.remove(c.cache[a].handle.elem)}catch(b){}});(function(){function a(g){for(var h="",l,m=0;g[m];m++){l=g[m];if(l.nodeType===3||l.nodeType===4)h+=l.nodeValue;else if(l.nodeType!==8)h+=a(l.childNodes)}return h}function b(g,h,l,m,q,p){q=0;for(var v=m.length;q<v;q++){var t=m[q];
if(t){t=t[g];for(var y=false;t;){if(t.sizcache===l){y=m[t.sizset];break}if(t.nodeType===1&&!p){t.sizcache=l;t.sizset=q}if(t.nodeName.toLowerCase()===h){y=t;break}t=t[g]}m[q]=y}}}function d(g,h,l,m,q,p){q=0;for(var v=m.length;q<v;q++){var t=m[q];if(t){t=t[g];for(var y=false;t;){if(t.sizcache===l){y=m[t.sizset];break}if(t.nodeType===1){if(!p){t.sizcache=l;t.sizset=q}if(typeof h!=="string"){if(t===h){y=true;break}}else if(k.filter(h,[t]).length>0){y=t;break}}t=t[g]}m[q]=y}}}var f=/((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^[\]]*\]|['"][^'"]*['"]|[^[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,
e=0,j=Object.prototype.toString,i=false,o=true;[0,0].sort(function(){o=false;return 0});var k=function(g,h,l,m){l=l||[];var q=h=h||s;if(h.nodeType!==1&&h.nodeType!==9)return[];if(!g||typeof g!=="string")return l;for(var p=[],v,t,y,S,H=true,M=x(h),I=g;(f.exec(""),v=f.exec(I))!==null;){I=v[3];p.push(v[1]);if(v[2]){S=v[3];break}}if(p.length>1&&r.exec(g))if(p.length===2&&n.relative[p[0]])t=ga(p[0]+p[1],h);else for(t=n.relative[p[0]]?[h]:k(p.shift(),h);p.length;){g=p.shift();if(n.relative[g])g+=p.shift();
t=ga(g,t)}else{if(!m&&p.length>1&&h.nodeType===9&&!M&&n.match.ID.test(p[0])&&!n.match.ID.test(p[p.length-1])){v=k.find(p.shift(),h,M);h=v.expr?k.filter(v.expr,v.set)[0]:v.set[0]}if(h){v=m?{expr:p.pop(),set:z(m)}:k.find(p.pop(),p.length===1&&(p[0]==="~"||p[0]==="+")&&h.parentNode?h.parentNode:h,M);t=v.expr?k.filter(v.expr,v.set):v.set;if(p.length>0)y=z(t);else H=false;for(;p.length;){var D=p.pop();v=D;if(n.relative[D])v=p.pop();else D="";if(v==null)v=h;n.relative[D](y,v,M)}}else y=[]}y||(y=t);y||k.error(D||
g);if(j.call(y)==="[object Array]")if(H)if(h&&h.nodeType===1)for(g=0;y[g]!=null;g++){if(y[g]&&(y[g]===true||y[g].nodeType===1&&E(h,y[g])))l.push(t[g])}else for(g=0;y[g]!=null;g++)y[g]&&y[g].nodeType===1&&l.push(t[g]);else l.push.apply(l,y);else z(y,l);if(S){k(S,q,l,m);k.uniqueSort(l)}return l};k.uniqueSort=function(g){if(B){i=o;g.sort(B);if(i)for(var h=1;h<g.length;h++)g[h]===g[h-1]&&g.splice(h--,1)}return g};k.matches=function(g,h){return k(g,null,null,h)};k.find=function(g,h,l){var m,q;if(!g)return[];
for(var p=0,v=n.order.length;p<v;p++){var t=n.order[p];if(q=n.leftMatch[t].exec(g)){var y=q[1];q.splice(1,1);if(y.substr(y.length-1)!=="\\"){q[1]=(q[1]||"").replace(/\\/g,"");m=n.find[t](q,h,l);if(m!=null){g=g.replace(n.match[t],"");break}}}}m||(m=h.getElementsByTagName("*"));return{set:m,expr:g}};k.filter=function(g,h,l,m){for(var q=g,p=[],v=h,t,y,S=h&&h[0]&&x(h[0]);g&&h.length;){for(var H in n.filter)if((t=n.leftMatch[H].exec(g))!=null&&t[2]){var M=n.filter[H],I,D;D=t[1];y=false;t.splice(1,1);if(D.substr(D.length-
1)!=="\\"){if(v===p)p=[];if(n.preFilter[H])if(t=n.preFilter[H](t,v,l,p,m,S)){if(t===true)continue}else y=I=true;if(t)for(var U=0;(D=v[U])!=null;U++)if(D){I=M(D,t,U,v);var Ha=m^!!I;if(l&&I!=null)if(Ha)y=true;else v[U]=false;else if(Ha){p.push(D);y=true}}if(I!==w){l||(v=p);g=g.replace(n.match[H],"");if(!y)return[];break}}}if(g===q)if(y==null)k.error(g);else break;q=g}return v};k.error=function(g){throw"Syntax error, unrecognized expression: "+g;};var n=k.selectors={order:["ID","NAME","TAG"],match:{ID:/#((?:[\w\u00c0-\uFFFF-]|\\.)+)/,
CLASS:/\.((?:[\w\u00c0-\uFFFF-]|\\.)+)/,NAME:/\[name=['"]*((?:[\w\u00c0-\uFFFF-]|\\.)+)['"]*\]/,ATTR:/\[\s*((?:[\w\u00c0-\uFFFF-]|\\.)+)\s*(?:(\S?=)\s*(['"]*)(.*?)\3|)\s*\]/,TAG:/^((?:[\w\u00c0-\uFFFF\*-]|\\.)+)/,CHILD:/:(only|nth|last|first)-child(?:\((even|odd|[\dn+-]*)\))?/,POS:/:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^-]|$)/,PSEUDO:/:((?:[\w\u00c0-\uFFFF-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/},leftMatch:{},attrMap:{"class":"className","for":"htmlFor"},attrHandle:{href:function(g){return g.getAttribute("href")}},
relative:{"+":function(g,h){var l=typeof h==="string",m=l&&!/\W/.test(h);l=l&&!m;if(m)h=h.toLowerCase();m=0;for(var q=g.length,p;m<q;m++)if(p=g[m]){for(;(p=p.previousSibling)&&p.nodeType!==1;);g[m]=l||p&&p.nodeName.toLowerCase()===h?p||false:p===h}l&&k.filter(h,g,true)},">":function(g,h){var l=typeof h==="string";if(l&&!/\W/.test(h)){h=h.toLowerCase();for(var m=0,q=g.length;m<q;m++){var p=g[m];if(p){l=p.parentNode;g[m]=l.nodeName.toLowerCase()===h?l:false}}}else{m=0;for(q=g.length;m<q;m++)if(p=g[m])g[m]=
l?p.parentNode:p.parentNode===h;l&&k.filter(h,g,true)}},"":function(g,h,l){var m=e++,q=d;if(typeof h==="string"&&!/\W/.test(h)){var p=h=h.toLowerCase();q=b}q("parentNode",h,m,g,p,l)},"~":function(g,h,l){var m=e++,q=d;if(typeof h==="string"&&!/\W/.test(h)){var p=h=h.toLowerCase();q=b}q("previousSibling",h,m,g,p,l)}},find:{ID:function(g,h,l){if(typeof h.getElementById!=="undefined"&&!l)return(g=h.getElementById(g[1]))?[g]:[]},NAME:function(g,h){if(typeof h.getElementsByName!=="undefined"){var l=[];
h=h.getElementsByName(g[1]);for(var m=0,q=h.length;m<q;m++)h[m].getAttribute("name")===g[1]&&l.push(h[m]);return l.length===0?null:l}},TAG:function(g,h){return h.getElementsByTagName(g[1])}},preFilter:{CLASS:function(g,h,l,m,q,p){g=" "+g[1].replace(/\\/g,"")+" ";if(p)return g;p=0;for(var v;(v=h[p])!=null;p++)if(v)if(q^(v.className&&(" "+v.className+" ").replace(/[\t\n]/g," ").indexOf(g)>=0))l||m.push(v);else if(l)h[p]=false;return false},ID:function(g){return g[1].replace(/\\/g,"")},TAG:function(g){return g[1].toLowerCase()},
CHILD:function(g){if(g[1]==="nth"){var h=/(-?)(\d*)n((?:\+|-)?\d*)/.exec(g[2]==="even"&&"2n"||g[2]==="odd"&&"2n+1"||!/\D/.test(g[2])&&"0n+"+g[2]||g[2]);g[2]=h[1]+(h[2]||1)-0;g[3]=h[3]-0}g[0]=e++;return g},ATTR:function(g,h,l,m,q,p){h=g[1].replace(/\\/g,"");if(!p&&n.attrMap[h])g[1]=n.attrMap[h];if(g[2]==="~=")g[4]=" "+g[4]+" ";return g},PSEUDO:function(g,h,l,m,q){if(g[1]==="not")if((f.exec(g[3])||"").length>1||/^\w/.test(g[3]))g[3]=k(g[3],null,null,h);else{g=k.filter(g[3],h,l,true^q);l||m.push.apply(m,
g);return false}else if(n.match.POS.test(g[0])||n.match.CHILD.test(g[0]))return true;return g},POS:function(g){g.unshift(true);return g}},filters:{enabled:function(g){return g.disabled===false&&g.type!=="hidden"},disabled:function(g){return g.disabled===true},checked:function(g){return g.checked===true},selected:function(g){return g.selected===true},parent:function(g){return!!g.firstChild},empty:function(g){return!g.firstChild},has:function(g,h,l){return!!k(l[3],g).length},header:function(g){return/h\d/i.test(g.nodeName)},
text:function(g){return"text"===g.type},radio:function(g){return"radio"===g.type},checkbox:function(g){return"checkbox"===g.type},file:function(g){return"file"===g.type},password:function(g){return"password"===g.type},submit:function(g){return"submit"===g.type},image:function(g){return"image"===g.type},reset:function(g){return"reset"===g.type},button:function(g){return"button"===g.type||g.nodeName.toLowerCase()==="button"},input:function(g){return/input|select|textarea|button/i.test(g.nodeName)}},
setFilters:{first:function(g,h){return h===0},last:function(g,h,l,m){return h===m.length-1},even:function(g,h){return h%2===0},odd:function(g,h){return h%2===1},lt:function(g,h,l){return h<l[3]-0},gt:function(g,h,l){return h>l[3]-0},nth:function(g,h,l){return l[3]-0===h},eq:function(g,h,l){return l[3]-0===h}},filter:{PSEUDO:function(g,h,l,m){var q=h[1],p=n.filters[q];if(p)return p(g,l,h,m);else if(q==="contains")return(g.textContent||g.innerText||a([g])||"").indexOf(h[3])>=0;else if(q==="not"){h=
h[3];l=0;for(m=h.length;l<m;l++)if(h[l]===g)return false;return true}else k.error("Syntax error, unrecognized expression: "+q)},CHILD:function(g,h){var l=h[1],m=g;switch(l){case "only":case "first":for(;m=m.previousSibling;)if(m.nodeType===1)return false;if(l==="first")return true;m=g;case "last":for(;m=m.nextSibling;)if(m.nodeType===1)return false;return true;case "nth":l=h[2];var q=h[3];if(l===1&&q===0)return true;h=h[0];var p=g.parentNode;if(p&&(p.sizcache!==h||!g.nodeIndex)){var v=0;for(m=p.firstChild;m;m=
m.nextSibling)if(m.nodeType===1)m.nodeIndex=++v;p.sizcache=h}g=g.nodeIndex-q;return l===0?g===0:g%l===0&&g/l>=0}},ID:function(g,h){return g.nodeType===1&&g.getAttribute("id")===h},TAG:function(g,h){return h==="*"&&g.nodeType===1||g.nodeName.toLowerCase()===h},CLASS:function(g,h){return(" "+(g.className||g.getAttribute("class"))+" ").indexOf(h)>-1},ATTR:function(g,h){var l=h[1];g=n.attrHandle[l]?n.attrHandle[l](g):g[l]!=null?g[l]:g.getAttribute(l);l=g+"";var m=h[2];h=h[4];return g==null?m==="!=":m===
"="?l===h:m==="*="?l.indexOf(h)>=0:m==="~="?(" "+l+" ").indexOf(h)>=0:!h?l&&g!==false:m==="!="?l!==h:m==="^="?l.indexOf(h)===0:m==="$="?l.substr(l.length-h.length)===h:m==="|="?l===h||l.substr(0,h.length+1)===h+"-":false},POS:function(g,h,l,m){var q=n.setFilters[h[2]];if(q)return q(g,l,h,m)}}},r=n.match.POS;for(var u in n.match){n.match[u]=new RegExp(n.match[u].source+/(?![^\[]*\])(?![^\(]*\))/.source);n.leftMatch[u]=new RegExp(/(^(?:.|\r|\n)*?)/.source+n.match[u].source.replace(/\\(\d+)/g,function(g,
h){return"\\"+(h-0+1)}))}var z=function(g,h){g=Array.prototype.slice.call(g,0);if(h){h.push.apply(h,g);return h}return g};try{Array.prototype.slice.call(s.documentElement.childNodes,0)}catch(C){z=function(g,h){h=h||[];if(j.call(g)==="[object Array]")Array.prototype.push.apply(h,g);else if(typeof g.length==="number")for(var l=0,m=g.length;l<m;l++)h.push(g[l]);else for(l=0;g[l];l++)h.push(g[l]);return h}}var B;if(s.documentElement.compareDocumentPosition)B=function(g,h){if(!g.compareDocumentPosition||
!h.compareDocumentPosition){if(g==h)i=true;return g.compareDocumentPosition?-1:1}g=g.compareDocumentPosition(h)&4?-1:g===h?0:1;if(g===0)i=true;return g};else if("sourceIndex"in s.documentElement)B=function(g,h){if(!g.sourceIndex||!h.sourceIndex){if(g==h)i=true;return g.sourceIndex?-1:1}g=g.sourceIndex-h.sourceIndex;if(g===0)i=true;return g};else if(s.createRange)B=function(g,h){if(!g.ownerDocument||!h.ownerDocument){if(g==h)i=true;return g.ownerDocument?-1:1}var l=g.ownerDocument.createRange(),m=
h.ownerDocument.createRange();l.setStart(g,0);l.setEnd(g,0);m.setStart(h,0);m.setEnd(h,0);g=l.compareBoundaryPoints(Range.START_TO_END,m);if(g===0)i=true;return g};(function(){var g=s.createElement("div"),h="script"+(new Date).getTime();g.innerHTML="<a name='"+h+"'/>";var l=s.documentElement;l.insertBefore(g,l.firstChild);if(s.getElementById(h)){n.find.ID=function(m,q,p){if(typeof q.getElementById!=="undefined"&&!p)return(q=q.getElementById(m[1]))?q.id===m[1]||typeof q.getAttributeNode!=="undefined"&&
q.getAttributeNode("id").nodeValue===m[1]?[q]:w:[]};n.filter.ID=function(m,q){var p=typeof m.getAttributeNode!=="undefined"&&m.getAttributeNode("id");return m.nodeType===1&&p&&p.nodeValue===q}}l.removeChild(g);l=g=null})();(function(){var g=s.createElement("div");g.appendChild(s.createComment(""));if(g.getElementsByTagName("*").length>0)n.find.TAG=function(h,l){l=l.getElementsByTagName(h[1]);if(h[1]==="*"){h=[];for(var m=0;l[m];m++)l[m].nodeType===1&&h.push(l[m]);l=h}return l};g.innerHTML="<a href='#'></a>";
if(g.firstChild&&typeof g.firstChild.getAttribute!=="undefined"&&g.firstChild.getAttribute("href")!=="#")n.attrHandle.href=function(h){return h.getAttribute("href",2)};g=null})();s.querySelectorAll&&function(){var g=k,h=s.createElement("div");h.innerHTML="<p class='TEST'></p>";if(!(h.querySelectorAll&&h.querySelectorAll(".TEST").length===0)){k=function(m,q,p,v){q=q||s;if(!v&&q.nodeType===9&&!x(q))try{return z(q.querySelectorAll(m),p)}catch(t){}return g(m,q,p,v)};for(var l in g)k[l]=g[l];h=null}}();
(function(){var g=s.createElement("div");g.innerHTML="<div class='test e'></div><div class='test'></div>";if(!(!g.getElementsByClassName||g.getElementsByClassName("e").length===0)){g.lastChild.className="e";if(g.getElementsByClassName("e").length!==1){n.order.splice(1,0,"CLASS");n.find.CLASS=function(h,l,m){if(typeof l.getElementsByClassName!=="undefined"&&!m)return l.getElementsByClassName(h[1])};g=null}}})();var E=s.compareDocumentPosition?function(g,h){return!!(g.compareDocumentPosition(h)&16)}:
function(g,h){return g!==h&&(g.contains?g.contains(h):true)},x=function(g){return(g=(g?g.ownerDocument||g:0).documentElement)?g.nodeName!=="HTML":false},ga=function(g,h){var l=[],m="",q;for(h=h.nodeType?[h]:h;q=n.match.PSEUDO.exec(g);){m+=q[0];g=g.replace(n.match.PSEUDO,"")}g=n.relative[g]?g+"*":g;q=0;for(var p=h.length;q<p;q++)k(g,h[q],l);return k.filter(m,l)};c.find=k;c.expr=k.selectors;c.expr[":"]=c.expr.filters;c.unique=k.uniqueSort;c.text=a;c.isXMLDoc=x;c.contains=E})();var eb=/Until$/,fb=/^(?:parents|prevUntil|prevAll)/,
gb=/,/;R=Array.prototype.slice;var Ia=function(a,b,d){if(c.isFunction(b))return c.grep(a,function(e,j){return!!b.call(e,j,e)===d});else if(b.nodeType)return c.grep(a,function(e){return e===b===d});else if(typeof b==="string"){var f=c.grep(a,function(e){return e.nodeType===1});if(Ua.test(b))return c.filter(b,f,!d);else b=c.filter(b,f)}return c.grep(a,function(e){return c.inArray(e,b)>=0===d})};c.fn.extend({find:function(a){for(var b=this.pushStack("","find",a),d=0,f=0,e=this.length;f<e;f++){d=b.length;
c.find(a,this[f],b);if(f>0)for(var j=d;j<b.length;j++)for(var i=0;i<d;i++)if(b[i]===b[j]){b.splice(j--,1);break}}return b},has:function(a){var b=c(a);return this.filter(function(){for(var d=0,f=b.length;d<f;d++)if(c.contains(this,b[d]))return true})},not:function(a){return this.pushStack(Ia(this,a,false),"not",a)},filter:function(a){return this.pushStack(Ia(this,a,true),"filter",a)},is:function(a){return!!a&&c.filter(a,this).length>0},closest:function(a,b){if(c.isArray(a)){var d=[],f=this[0],e,j=
{},i;if(f&&a.length){e=0;for(var o=a.length;e<o;e++){i=a[e];j[i]||(j[i]=c.expr.match.POS.test(i)?c(i,b||this.context):i)}for(;f&&f.ownerDocument&&f!==b;){for(i in j){e=j[i];if(e.jquery?e.index(f)>-1:c(f).is(e)){d.push({selector:i,elem:f});delete j[i]}}f=f.parentNode}}return d}var k=c.expr.match.POS.test(a)?c(a,b||this.context):null;return this.map(function(n,r){for(;r&&r.ownerDocument&&r!==b;){if(k?k.index(r)>-1:c(r).is(a))return r;r=r.parentNode}return null})},index:function(a){if(!a||typeof a===
"string")return c.inArray(this[0],a?c(a):this.parent().children());return c.inArray(a.jquery?a[0]:a,this)},add:function(a,b){a=typeof a==="string"?c(a,b||this.context):c.makeArray(a);b=c.merge(this.get(),a);return this.pushStack(qa(a[0])||qa(b[0])?b:c.unique(b))},andSelf:function(){return this.add(this.prevObject)}});c.each({parent:function(a){return(a=a.parentNode)&&a.nodeType!==11?a:null},parents:function(a){return c.dir(a,"parentNode")},parentsUntil:function(a,b,d){return c.dir(a,"parentNode",
d)},next:function(a){return c.nth(a,2,"nextSibling")},prev:function(a){return c.nth(a,2,"previousSibling")},nextAll:function(a){return c.dir(a,"nextSibling")},prevAll:function(a){return c.dir(a,"previousSibling")},nextUntil:function(a,b,d){return c.dir(a,"nextSibling",d)},prevUntil:function(a,b,d){return c.dir(a,"previousSibling",d)},siblings:function(a){return c.sibling(a.parentNode.firstChild,a)},children:function(a){return c.sibling(a.firstChild)},contents:function(a){return c.nodeName(a,"iframe")?
a.contentDocument||a.contentWindow.document:c.makeArray(a.childNodes)}},function(a,b){c.fn[a]=function(d,f){var e=c.map(this,b,d);eb.test(a)||(f=d);if(f&&typeof f==="string")e=c.filter(f,e);e=this.length>1?c.unique(e):e;if((this.length>1||gb.test(f))&&fb.test(a))e=e.reverse();return this.pushStack(e,a,R.call(arguments).join(","))}});c.extend({filter:function(a,b,d){if(d)a=":not("+a+")";return c.find.matches(a,b)},dir:function(a,b,d){var f=[];for(a=a[b];a&&a.nodeType!==9&&(d===w||a.nodeType!==1||!c(a).is(d));){a.nodeType===
1&&f.push(a);a=a[b]}return f},nth:function(a,b,d){b=b||1;for(var f=0;a;a=a[d])if(a.nodeType===1&&++f===b)break;return a},sibling:function(a,b){for(var d=[];a;a=a.nextSibling)a.nodeType===1&&a!==b&&d.push(a);return d}});var Ja=/ jQuery\d+="(?:\d+|null)"/g,V=/^\s+/,Ka=/(<([\w:]+)[^>]*?)\/>/g,hb=/^(?:area|br|col|embed|hr|img|input|link|meta|param)$/i,La=/<([\w:]+)/,ib=/<tbody/i,jb=/<|&#?\w+;/,ta=/<script|<object|<embed|<option|<style/i,ua=/checked\s*(?:[^=]|=\s*.checked.)/i,Ma=function(a,b,d){return hb.test(d)?
a:b+"></"+d+">"},F={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],area:[1,"<map>","</map>"],_default:[0,"",""]};F.optgroup=F.option;F.tbody=F.tfoot=F.colgroup=F.caption=F.thead;F.th=F.td;if(!c.support.htmlSerialize)F._default=[1,"div<div>","</div>"];c.fn.extend({text:function(a){if(c.isFunction(a))return this.each(function(b){var d=
c(this);d.text(a.call(this,b,d.text()))});if(typeof a!=="object"&&a!==w)return this.empty().append((this[0]&&this[0].ownerDocument||s).createTextNode(a));return c.text(this)},wrapAll:function(a){if(c.isFunction(a))return this.each(function(d){c(this).wrapAll(a.call(this,d))});if(this[0]){var b=c(a,this[0].ownerDocument).eq(0).clone(true);this[0].parentNode&&b.insertBefore(this[0]);b.map(function(){for(var d=this;d.firstChild&&d.firstChild.nodeType===1;)d=d.firstChild;return d}).append(this)}return this},
wrapInner:function(a){if(c.isFunction(a))return this.each(function(b){c(this).wrapInner(a.call(this,b))});return this.each(function(){var b=c(this),d=b.contents();d.length?d.wrapAll(a):b.append(a)})},wrap:function(a){return this.each(function(){c(this).wrapAll(a)})},unwrap:function(){return this.parent().each(function(){c.nodeName(this,"body")||c(this).replaceWith(this.childNodes)}).end()},append:function(){return this.domManip(arguments,true,function(a){this.nodeType===1&&this.appendChild(a)})},
prepend:function(){return this.domManip(arguments,true,function(a){this.nodeType===1&&this.insertBefore(a,this.firstChild)})},before:function(){if(this[0]&&this[0].parentNode)return this.domManip(arguments,false,function(b){this.parentNode.insertBefore(b,this)});else if(arguments.length){var a=c(arguments[0]);a.push.apply(a,this.toArray());return this.pushStack(a,"before",arguments)}},after:function(){if(this[0]&&this[0].parentNode)return this.domManip(arguments,false,function(b){this.parentNode.insertBefore(b,
this.nextSibling)});else if(arguments.length){var a=this.pushStack(this,"after",arguments);a.push.apply(a,c(arguments[0]).toArray());return a}},remove:function(a,b){for(var d=0,f;(f=this[d])!=null;d++)if(!a||c.filter(a,[f]).length){if(!b&&f.nodeType===1){c.cleanData(f.getElementsByTagName("*"));c.cleanData([f])}f.parentNode&&f.parentNode.removeChild(f)}return this},empty:function(){for(var a=0,b;(b=this[a])!=null;a++)for(b.nodeType===1&&c.cleanData(b.getElementsByTagName("*"));b.firstChild;)b.removeChild(b.firstChild);
return this},clone:function(a){var b=this.map(function(){if(!c.support.noCloneEvent&&!c.isXMLDoc(this)){var d=this.outerHTML,f=this.ownerDocument;if(!d){d=f.createElement("div");d.appendChild(this.cloneNode(true));d=d.innerHTML}return c.clean([d.replace(Ja,"").replace(/=([^="'>\s]+\/)>/g,'="$1">').replace(V,"")],f)[0]}else return this.cloneNode(true)});if(a===true){ra(this,b);ra(this.find("*"),b.find("*"))}return b},html:function(a){if(a===w)return this[0]&&this[0].nodeType===1?this[0].innerHTML.replace(Ja,
""):null;else if(typeof a==="string"&&!ta.test(a)&&(c.support.leadingWhitespace||!V.test(a))&&!F[(La.exec(a)||["",""])[1].toLowerCase()]){a=a.replace(Ka,Ma);try{for(var b=0,d=this.length;b<d;b++)if(this[b].nodeType===1){c.cleanData(this[b].getElementsByTagName("*"));this[b].innerHTML=a}}catch(f){this.empty().append(a)}}else c.isFunction(a)?this.each(function(e){var j=c(this),i=j.html();j.empty().append(function(){return a.call(this,e,i)})}):this.empty().append(a);return this},replaceWith:function(a){if(this[0]&&
this[0].parentNode){if(c.isFunction(a))return this.each(function(b){var d=c(this),f=d.html();d.replaceWith(a.call(this,b,f))});if(typeof a!=="string")a=c(a).detach();return this.each(function(){var b=this.nextSibling,d=this.parentNode;c(this).remove();b?c(b).before(a):c(d).append(a)})}else return this.pushStack(c(c.isFunction(a)?a():a),"replaceWith",a)},detach:function(a){return this.remove(a,true)},domManip:function(a,b,d){function f(u){return c.nodeName(u,"table")?u.getElementsByTagName("tbody")[0]||
u.appendChild(u.ownerDocument.createElement("tbody")):u}var e,j,i=a[0],o=[],k;if(!c.support.checkClone&&arguments.length===3&&typeof i==="string"&&ua.test(i))return this.each(function(){c(this).domManip(a,b,d,true)});if(c.isFunction(i))return this.each(function(u){var z=c(this);a[0]=i.call(this,u,b?z.html():w);z.domManip(a,b,d)});if(this[0]){e=i&&i.parentNode;e=c.support.parentNode&&e&&e.nodeType===11&&e.childNodes.length===this.length?{fragment:e}:sa(a,this,o);k=e.fragment;if(j=k.childNodes.length===
1?(k=k.firstChild):k.firstChild){b=b&&c.nodeName(j,"tr");for(var n=0,r=this.length;n<r;n++)d.call(b?f(this[n],j):this[n],n>0||e.cacheable||this.length>1?k.cloneNode(true):k)}o.length&&c.each(o,Qa)}return this}});c.fragments={};c.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){c.fn[a]=function(d){var f=[];d=c(d);var e=this.length===1&&this[0].parentNode;if(e&&e.nodeType===11&&e.childNodes.length===1&&d.length===1){d[b](this[0]);
return this}else{e=0;for(var j=d.length;e<j;e++){var i=(e>0?this.clone(true):this).get();c.fn[b].apply(c(d[e]),i);f=f.concat(i)}return this.pushStack(f,a,d.selector)}}});c.extend({clean:function(a,b,d,f){b=b||s;if(typeof b.createElement==="undefined")b=b.ownerDocument||b[0]&&b[0].ownerDocument||s;for(var e=[],j=0,i;(i=a[j])!=null;j++){if(typeof i==="number")i+="";if(i){if(typeof i==="string"&&!jb.test(i))i=b.createTextNode(i);else if(typeof i==="string"){i=i.replace(Ka,Ma);var o=(La.exec(i)||["",
""])[1].toLowerCase(),k=F[o]||F._default,n=k[0],r=b.createElement("div");for(r.innerHTML=k[1]+i+k[2];n--;)r=r.lastChild;if(!c.support.tbody){n=ib.test(i);o=o==="table"&&!n?r.firstChild&&r.firstChild.childNodes:k[1]==="<table>"&&!n?r.childNodes:[];for(k=o.length-1;k>=0;--k)c.nodeName(o[k],"tbody")&&!o[k].childNodes.length&&o[k].parentNode.removeChild(o[k])}!c.support.leadingWhitespace&&V.test(i)&&r.insertBefore(b.createTextNode(V.exec(i)[0]),r.firstChild);i=r.childNodes}if(i.nodeType)e.push(i);else e=
c.merge(e,i)}}if(d)for(j=0;e[j];j++)if(f&&c.nodeName(e[j],"script")&&(!e[j].type||e[j].type.toLowerCase()==="text/javascript"))f.push(e[j].parentNode?e[j].parentNode.removeChild(e[j]):e[j]);else{e[j].nodeType===1&&e.splice.apply(e,[j+1,0].concat(c.makeArray(e[j].getElementsByTagName("script"))));d.appendChild(e[j])}return e},cleanData:function(a){for(var b,d,f=c.cache,e=c.event.special,j=c.support.deleteExpando,i=0,o;(o=a[i])!=null;i++)if(d=o[c.expando]){b=f[d];if(b.events)for(var k in b.events)e[k]?
c.event.remove(o,k):Ca(o,k,b.handle);if(j)delete o[c.expando];else o.removeAttribute&&o.removeAttribute(c.expando);delete f[d]}}});var kb=/z-?index|font-?weight|opacity|zoom|line-?height/i,Na=/alpha\([^)]*\)/,Oa=/opacity=([^)]*)/,ha=/float/i,ia=/-([a-z])/ig,lb=/([A-Z])/g,mb=/^-?\d+(?:px)?$/i,nb=/^-?\d/,ob={position:"absolute",visibility:"hidden",display:"block"},pb=["Left","Right"],qb=["Top","Bottom"],rb=s.defaultView&&s.defaultView.getComputedStyle,Pa=c.support.cssFloat?"cssFloat":"styleFloat",ja=
function(a,b){return b.toUpperCase()};c.fn.css=function(a,b){return X(this,a,b,true,function(d,f,e){if(e===w)return c.curCSS(d,f);if(typeof e==="number"&&!kb.test(f))e+="px";c.style(d,f,e)})};c.extend({style:function(a,b,d){if(!a||a.nodeType===3||a.nodeType===8)return w;if((b==="width"||b==="height")&&parseFloat(d)<0)d=w;var f=a.style||a,e=d!==w;if(!c.support.opacity&&b==="opacity"){if(e){f.zoom=1;b=parseInt(d,10)+""==="NaN"?"":"alpha(opacity="+d*100+")";a=f.filter||c.curCSS(a,"filter")||"";f.filter=
Na.test(a)?a.replace(Na,b):b}return f.filter&&f.filter.indexOf("opacity=")>=0?parseFloat(Oa.exec(f.filter)[1])/100+"":""}if(ha.test(b))b=Pa;b=b.replace(ia,ja);if(e)f[b]=d;return f[b]},css:function(a,b,d,f){if(b==="width"||b==="height"){var e,j=b==="width"?pb:qb;function i(){e=b==="width"?a.offsetWidth:a.offsetHeight;f!=="border"&&c.each(j,function(){f||(e-=parseFloat(c.curCSS(a,"padding"+this,true))||0);if(f==="margin")e+=parseFloat(c.curCSS(a,"margin"+this,true))||0;else e-=parseFloat(c.curCSS(a,
"border"+this+"Width",true))||0})}a.offsetWidth!==0?i():c.swap(a,ob,i);return Math.max(0,Math.round(e))}return c.curCSS(a,b,d)},curCSS:function(a,b,d){var f,e=a.style;if(!c.support.opacity&&b==="opacity"&&a.currentStyle){f=Oa.test(a.currentStyle.filter||"")?parseFloat(RegExp.$1)/100+"":"";return f===""?"1":f}if(ha.test(b))b=Pa;if(!d&&e&&e[b])f=e[b];else if(rb){if(ha.test(b))b="float";b=b.replace(lb,"-$1").toLowerCase();e=a.ownerDocument.defaultView;if(!e)return null;if(a=e.getComputedStyle(a,null))f=
a.getPropertyValue(b);if(b==="opacity"&&f==="")f="1"}else if(a.currentStyle){d=b.replace(ia,ja);f=a.currentStyle[b]||a.currentStyle[d];if(!mb.test(f)&&nb.test(f)){b=e.left;var j=a.runtimeStyle.left;a.runtimeStyle.left=a.currentStyle.left;e.left=d==="fontSize"?"1em":f||0;f=e.pixelLeft+"px";e.left=b;a.runtimeStyle.left=j}}return f},swap:function(a,b,d){var f={};for(var e in b){f[e]=a.style[e];a.style[e]=b[e]}d.call(a);for(e in b)a.style[e]=f[e]}});if(c.expr&&c.expr.filters){c.expr.filters.hidden=function(a){var b=
a.offsetWidth,d=a.offsetHeight,f=a.nodeName.toLowerCase()==="tr";return b===0&&d===0&&!f?true:b>0&&d>0&&!f?false:c.curCSS(a,"display")==="none"};c.expr.filters.visible=function(a){return!c.expr.filters.hidden(a)}}var sb=J(),tb=/<script(.|\s)*?\/script>/gi,ub=/select|textarea/i,vb=/color|date|datetime|email|hidden|month|number|password|range|search|tel|text|time|url|week/i,N=/=\?(&|$)/,ka=/\?/,wb=/(\?|&)_=.*?(&|$)/,xb=/^(\w+:)?\/\/([^\/?#]+)/,yb=/%20/g,zb=c.fn.load;c.fn.extend({load:function(a,b,d){if(typeof a!==
"string")return zb.call(this,a);else if(!this.length)return this;var f=a.indexOf(" ");if(f>=0){var e=a.slice(f,a.length);a=a.slice(0,f)}f="GET";if(b)if(c.isFunction(b)){d=b;b=null}else if(typeof b==="object"){b=c.param(b,c.ajaxSettings.traditional);f="POST"}var j=this;c.ajax({url:a,type:f,dataType:"html",data:b,complete:function(i,o){if(o==="success"||o==="notmodified")j.html(e?c("<div />").append(i.responseText.replace(tb,"")).find(e):i.responseText);d&&j.each(d,[i.responseText,o,i])}});return this},
serialize:function(){return c.param(this.serializeArray())},serializeArray:function(){return this.map(function(){return this.elements?c.makeArray(this.elements):this}).filter(function(){return this.name&&!this.disabled&&(this.checked||ub.test(this.nodeName)||vb.test(this.type))}).map(function(a,b){a=c(this).val();return a==null?null:c.isArray(a)?c.map(a,function(d){return{name:b.name,value:d}}):{name:b.name,value:a}}).get()}});c.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "),
function(a,b){c.fn[b]=function(d){return this.bind(b,d)}});c.extend({get:function(a,b,d,f){if(c.isFunction(b)){f=f||d;d=b;b=null}return c.ajax({type:"GET",url:a,data:b,success:d,dataType:f})},getScript:function(a,b){return c.get(a,null,b,"script")},getJSON:function(a,b,d){return c.get(a,b,d,"json")},post:function(a,b,d,f){if(c.isFunction(b)){f=f||d;d=b;b={}}return c.ajax({type:"POST",url:a,data:b,success:d,dataType:f})},ajaxSetup:function(a){c.extend(c.ajaxSettings,a)},ajaxSettings:{url:location.href,
global:true,type:"GET",contentType:"application/x-www-form-urlencoded",processData:true,async:true,xhr:A.XMLHttpRequest&&(A.location.protocol!=="file:"||!A.ActiveXObject)?function(){return new A.XMLHttpRequest}:function(){try{return new A.ActiveXObject("Microsoft.XMLHTTP")}catch(a){}},accepts:{xml:"application/xml, text/xml",html:"text/html",script:"text/javascript, application/javascript",json:"application/json, text/javascript",text:"text/plain",_default:"*/*"}},lastModified:{},etag:{},ajax:function(a){function b(){e.success&&
e.success.call(k,o,i,x);e.global&&f("ajaxSuccess",[x,e])}function d(){e.complete&&e.complete.call(k,x,i);e.global&&f("ajaxComplete",[x,e]);e.global&&!--c.active&&c.event.trigger("ajaxStop")}function f(q,p){(e.context?c(e.context):c.event).trigger(q,p)}var e=c.extend(true,{},c.ajaxSettings,a),j,i,o,k=a&&a.context||e,n=e.type.toUpperCase();if(e.data&&e.processData&&typeof e.data!=="string")e.data=c.param(e.data,e.traditional);if(e.dataType==="jsonp"){if(n==="GET")N.test(e.url)||(e.url+=(ka.test(e.url)?
"&":"?")+(e.jsonp||"callback")+"=?");else if(!e.data||!N.test(e.data))e.data=(e.data?e.data+"&":"")+(e.jsonp||"callback")+"=?";e.dataType="json"}if(e.dataType==="json"&&(e.data&&N.test(e.data)||N.test(e.url))){j=e.jsonpCallback||"jsonp"+sb++;if(e.data)e.data=(e.data+"").replace(N,"="+j+"$1");e.url=e.url.replace(N,"="+j+"$1");e.dataType="script";A[j]=A[j]||function(q){o=q;b();d();A[j]=w;try{delete A[j]}catch(p){}z&&z.removeChild(C)}}if(e.dataType==="script"&&e.cache===null)e.cache=false;if(e.cache===
false&&n==="GET"){var r=J(),u=e.url.replace(wb,"$1_="+r+"$2");e.url=u+(u===e.url?(ka.test(e.url)?"&":"?")+"_="+r:"")}if(e.data&&n==="GET")e.url+=(ka.test(e.url)?"&":"?")+e.data;e.global&&!c.active++&&c.event.trigger("ajaxStart");r=(r=xb.exec(e.url))&&(r[1]&&r[1]!==location.protocol||r[2]!==location.host);if(e.dataType==="script"&&n==="GET"&&r){var z=s.getElementsByTagName("head")[0]||s.documentElement,C=s.createElement("script");C.src=e.url;if(e.scriptCharset)C.charset=e.scriptCharset;if(!j){var B=
false;C.onload=C.onreadystatechange=function(){if(!B&&(!this.readyState||this.readyState==="loaded"||this.readyState==="complete")){B=true;b();d();C.onload=C.onreadystatechange=null;z&&C.parentNode&&z.removeChild(C)}}}z.insertBefore(C,z.firstChild);return w}var E=false,x=e.xhr();if(x){e.username?x.open(n,e.url,e.async,e.username,e.password):x.open(n,e.url,e.async);try{if(e.data||a&&a.contentType)x.setRequestHeader("Content-Type",e.contentType);if(e.ifModified){c.lastModified[e.url]&&x.setRequestHeader("If-Modified-Since",
c.lastModified[e.url]);c.etag[e.url]&&x.setRequestHeader("If-None-Match",c.etag[e.url])}r||x.setRequestHeader("X-Requested-With","XMLHttpRequest");x.setRequestHeader("Accept",e.dataType&&e.accepts[e.dataType]?e.accepts[e.dataType]+", */*":e.accepts._default)}catch(ga){}if(e.beforeSend&&e.beforeSend.call(k,x,e)===false){e.global&&!--c.active&&c.event.trigger("ajaxStop");x.abort();return false}e.global&&f("ajaxSend",[x,e]);var g=x.onreadystatechange=function(q){if(!x||x.readyState===0||q==="abort"){E||
d();E=true;if(x)x.onreadystatechange=c.noop}else if(!E&&x&&(x.readyState===4||q==="timeout")){E=true;x.onreadystatechange=c.noop;i=q==="timeout"?"timeout":!c.httpSuccess(x)?"error":e.ifModified&&c.httpNotModified(x,e.url)?"notmodified":"success";var p;if(i==="success")try{o=c.httpData(x,e.dataType,e)}catch(v){i="parsererror";p=v}if(i==="success"||i==="notmodified")j||b();else c.handleError(e,x,i,p);d();q==="timeout"&&x.abort();if(e.async)x=null}};try{var h=x.abort;x.abort=function(){x&&h.call(x);
g("abort")}}catch(l){}e.async&&e.timeout>0&&setTimeout(function(){x&&!E&&g("timeout")},e.timeout);try{x.send(n==="POST"||n==="PUT"||n==="DELETE"?e.data:null)}catch(m){c.handleError(e,x,null,m);d()}e.async||g();return x}},handleError:function(a,b,d,f){if(a.error)a.error.call(a.context||a,b,d,f);if(a.global)(a.context?c(a.context):c.event).trigger("ajaxError",[b,a,f])},active:0,httpSuccess:function(a){try{return!a.status&&location.protocol==="file:"||a.status>=200&&a.status<300||a.status===304||a.status===
1223||a.status===0}catch(b){}return false},httpNotModified:function(a,b){var d=a.getResponseHeader("Last-Modified"),f=a.getResponseHeader("Etag");if(d)c.lastModified[b]=d;if(f)c.etag[b]=f;return a.status===304||a.status===0},httpData:function(a,b,d){var f=a.getResponseHeader("content-type")||"",e=b==="xml"||!b&&f.indexOf("xml")>=0;a=e?a.responseXML:a.responseText;e&&a.documentElement.nodeName==="parsererror"&&c.error("parsererror");if(d&&d.dataFilter)a=d.dataFilter(a,b);if(typeof a==="string")if(b===
"json"||!b&&f.indexOf("json")>=0)a=c.parseJSON(a);else if(b==="script"||!b&&f.indexOf("javascript")>=0)c.globalEval(a);return a},param:function(a,b){function d(i,o){if(c.isArray(o))c.each(o,function(k,n){b||/\[\]$/.test(i)?f(i,n):d(i+"["+(typeof n==="object"||c.isArray(n)?k:"")+"]",n)});else!b&&o!=null&&typeof o==="object"?c.each(o,function(k,n){d(i+"["+k+"]",n)}):f(i,o)}function f(i,o){o=c.isFunction(o)?o():o;e[e.length]=encodeURIComponent(i)+"="+encodeURIComponent(o)}var e=[];if(b===w)b=c.ajaxSettings.traditional;
if(c.isArray(a)||a.jquery)c.each(a,function(){f(this.name,this.value)});else for(var j in a)d(j,a[j]);return e.join("&").replace(yb,"+")}});var la={},Ab=/toggle|show|hide/,Bb=/^([+-]=)?([\d+-.]+)(.*)$/,W,va=[["height","marginTop","marginBottom","paddingTop","paddingBottom"],["width","marginLeft","marginRight","paddingLeft","paddingRight"],["opacity"]];c.fn.extend({show:function(a,b){if(a||a===0)return this.animate(K("show",3),a,b);else{a=0;for(b=this.length;a<b;a++){var d=c.data(this[a],"olddisplay");
this[a].style.display=d||"";if(c.css(this[a],"display")==="none"){d=this[a].nodeName;var f;if(la[d])f=la[d];else{var e=c("<"+d+" />").appendTo("body");f=e.css("display");if(f==="none")f="block";e.remove();la[d]=f}c.data(this[a],"olddisplay",f)}}a=0;for(b=this.length;a<b;a++)this[a].style.display=c.data(this[a],"olddisplay")||"";return this}},hide:function(a,b){if(a||a===0)return this.animate(K("hide",3),a,b);else{a=0;for(b=this.length;a<b;a++){var d=c.data(this[a],"olddisplay");!d&&d!=="none"&&c.data(this[a],
"olddisplay",c.css(this[a],"display"))}a=0;for(b=this.length;a<b;a++)this[a].style.display="none";return this}},_toggle:c.fn.toggle,toggle:function(a,b){var d=typeof a==="boolean";if(c.isFunction(a)&&c.isFunction(b))this._toggle.apply(this,arguments);else a==null||d?this.each(function(){var f=d?a:c(this).is(":hidden");c(this)[f?"show":"hide"]()}):this.animate(K("toggle",3),a,b);return this},fadeTo:function(a,b,d){return this.filter(":hidden").css("opacity",0).show().end().animate({opacity:b},a,d)},
animate:function(a,b,d,f){var e=c.speed(b,d,f);if(c.isEmptyObject(a))return this.each(e.complete);return this[e.queue===false?"each":"queue"](function(){var j=c.extend({},e),i,o=this.nodeType===1&&c(this).is(":hidden"),k=this;for(i in a){var n=i.replace(ia,ja);if(i!==n){a[n]=a[i];delete a[i];i=n}if(a[i]==="hide"&&o||a[i]==="show"&&!o)return j.complete.call(this);if((i==="height"||i==="width")&&this.style){j.display=c.css(this,"display");j.overflow=this.style.overflow}if(c.isArray(a[i])){(j.specialEasing=
j.specialEasing||{})[i]=a[i][1];a[i]=a[i][0]}}if(j.overflow!=null)this.style.overflow="hidden";j.curAnim=c.extend({},a);c.each(a,function(r,u){var z=new c.fx(k,j,r);if(Ab.test(u))z[u==="toggle"?o?"show":"hide":u](a);else{var C=Bb.exec(u),B=z.cur(true)||0;if(C){u=parseFloat(C[2]);var E=C[3]||"px";if(E!=="px"){k.style[r]=(u||1)+E;B=(u||1)/z.cur(true)*B;k.style[r]=B+E}if(C[1])u=(C[1]==="-="?-1:1)*u+B;z.custom(B,u,E)}else z.custom(B,u,"")}});return true})},stop:function(a,b){var d=c.timers;a&&this.queue([]);
this.each(function(){for(var f=d.length-1;f>=0;f--)if(d[f].elem===this){b&&d[f](true);d.splice(f,1)}});b||this.dequeue();return this}});c.each({slideDown:K("show",1),slideUp:K("hide",1),slideToggle:K("toggle",1),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"}},function(a,b){c.fn[a]=function(d,f){return this.animate(b,d,f)}});c.extend({speed:function(a,b,d){var f=a&&typeof a==="object"?a:{complete:d||!d&&b||c.isFunction(a)&&a,duration:a,easing:d&&b||b&&!c.isFunction(b)&&b};f.duration=c.fx.off?0:typeof f.duration===
"number"?f.duration:c.fx.speeds[f.duration]||c.fx.speeds._default;f.old=f.complete;f.complete=function(){f.queue!==false&&c(this).dequeue();c.isFunction(f.old)&&f.old.call(this)};return f},easing:{linear:function(a,b,d,f){return d+f*a},swing:function(a,b,d,f){return(-Math.cos(a*Math.PI)/2+0.5)*f+d}},timers:[],fx:function(a,b,d){this.options=b;this.elem=a;this.prop=d;if(!b.orig)b.orig={}}});c.fx.prototype={update:function(){this.options.step&&this.options.step.call(this.elem,this.now,this);(c.fx.step[this.prop]||
c.fx.step._default)(this);if((this.prop==="height"||this.prop==="width")&&this.elem.style)this.elem.style.display="block"},cur:function(a){if(this.elem[this.prop]!=null&&(!this.elem.style||this.elem.style[this.prop]==null))return this.elem[this.prop];return(a=parseFloat(c.css(this.elem,this.prop,a)))&&a>-10000?a:parseFloat(c.curCSS(this.elem,this.prop))||0},custom:function(a,b,d){function f(j){return e.step(j)}this.startTime=J();this.start=a;this.end=b;this.unit=d||this.unit||"px";this.now=this.start;
this.pos=this.state=0;var e=this;f.elem=this.elem;if(f()&&c.timers.push(f)&&!W)W=setInterval(c.fx.tick,13)},show:function(){this.options.orig[this.prop]=c.style(this.elem,this.prop);this.options.show=true;this.custom(this.prop==="width"||this.prop==="height"?1:0,this.cur());c(this.elem).show()},hide:function(){this.options.orig[this.prop]=c.style(this.elem,this.prop);this.options.hide=true;this.custom(this.cur(),0)},step:function(a){var b=J(),d=true;if(a||b>=this.options.duration+this.startTime){this.now=
this.end;this.pos=this.state=1;this.update();this.options.curAnim[this.prop]=true;for(var f in this.options.curAnim)if(this.options.curAnim[f]!==true)d=false;if(d){if(this.options.display!=null){this.elem.style.overflow=this.options.overflow;a=c.data(this.elem,"olddisplay");this.elem.style.display=a?a:this.options.display;if(c.css(this.elem,"display")==="none")this.elem.style.display="block"}this.options.hide&&c(this.elem).hide();if(this.options.hide||this.options.show)for(var e in this.options.curAnim)c.style(this.elem,
e,this.options.orig[e]);this.options.complete.call(this.elem)}return false}else{e=b-this.startTime;this.state=e/this.options.duration;a=this.options.easing||(c.easing.swing?"swing":"linear");this.pos=c.easing[this.options.specialEasing&&this.options.specialEasing[this.prop]||a](this.state,e,0,1,this.options.duration);this.now=this.start+(this.end-this.start)*this.pos;this.update()}return true}};c.extend(c.fx,{tick:function(){for(var a=c.timers,b=0;b<a.length;b++)a[b]()||a.splice(b--,1);a.length||
c.fx.stop()},stop:function(){clearInterval(W);W=null},speeds:{slow:600,fast:200,_default:400},step:{opacity:function(a){c.style(a.elem,"opacity",a.now)},_default:function(a){if(a.elem.style&&a.elem.style[a.prop]!=null)a.elem.style[a.prop]=(a.prop==="width"||a.prop==="height"?Math.max(0,a.now):a.now)+a.unit;else a.elem[a.prop]=a.now}}});if(c.expr&&c.expr.filters)c.expr.filters.animated=function(a){return c.grep(c.timers,function(b){return a===b.elem}).length};c.fn.offset="getBoundingClientRect"in s.documentElement?
function(a){var b=this[0];if(a)return this.each(function(e){c.offset.setOffset(this,a,e)});if(!b||!b.ownerDocument)return null;if(b===b.ownerDocument.body)return c.offset.bodyOffset(b);var d=b.getBoundingClientRect(),f=b.ownerDocument;b=f.body;f=f.documentElement;return{top:d.top+(self.pageYOffset||c.support.boxModel&&f.scrollTop||b.scrollTop)-(f.clientTop||b.clientTop||0),left:d.left+(self.pageXOffset||c.support.boxModel&&f.scrollLeft||b.scrollLeft)-(f.clientLeft||b.clientLeft||0)}}:function(a){var b=
this[0];if(a)return this.each(function(r){c.offset.setOffset(this,a,r)});if(!b||!b.ownerDocument)return null;if(b===b.ownerDocument.body)return c.offset.bodyOffset(b);c.offset.initialize();var d=b.offsetParent,f=b,e=b.ownerDocument,j,i=e.documentElement,o=e.body;f=(e=e.defaultView)?e.getComputedStyle(b,null):b.currentStyle;for(var k=b.offsetTop,n=b.offsetLeft;(b=b.parentNode)&&b!==o&&b!==i;){if(c.offset.supportsFixedPosition&&f.position==="fixed")break;j=e?e.getComputedStyle(b,null):b.currentStyle;
k-=b.scrollTop;n-=b.scrollLeft;if(b===d){k+=b.offsetTop;n+=b.offsetLeft;if(c.offset.doesNotAddBorder&&!(c.offset.doesAddBorderForTableAndCells&&/^t(able|d|h)$/i.test(b.nodeName))){k+=parseFloat(j.borderTopWidth)||0;n+=parseFloat(j.borderLeftWidth)||0}f=d;d=b.offsetParent}if(c.offset.subtractsBorderForOverflowNotVisible&&j.overflow!=="visible"){k+=parseFloat(j.borderTopWidth)||0;n+=parseFloat(j.borderLeftWidth)||0}f=j}if(f.position==="relative"||f.position==="static"){k+=o.offsetTop;n+=o.offsetLeft}if(c.offset.supportsFixedPosition&&
f.position==="fixed"){k+=Math.max(i.scrollTop,o.scrollTop);n+=Math.max(i.scrollLeft,o.scrollLeft)}return{top:k,left:n}};c.offset={initialize:function(){var a=s.body,b=s.createElement("div"),d,f,e,j=parseFloat(c.curCSS(a,"marginTop",true))||0;c.extend(b.style,{position:"absolute",top:0,left:0,margin:0,border:0,width:"1px",height:"1px",visibility:"hidden"});b.innerHTML="<div style='position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;'><div></div></div><table style='position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;' cellpadding='0' cellspacing='0'><tr><td></td></tr></table>";
a.insertBefore(b,a.firstChild);d=b.firstChild;f=d.firstChild;e=d.nextSibling.firstChild.firstChild;this.doesNotAddBorder=f.offsetTop!==5;this.doesAddBorderForTableAndCells=e.offsetTop===5;f.style.position="fixed";f.style.top="20px";this.supportsFixedPosition=f.offsetTop===20||f.offsetTop===15;f.style.position=f.style.top="";d.style.overflow="hidden";d.style.position="relative";this.subtractsBorderForOverflowNotVisible=f.offsetTop===-5;this.doesNotIncludeMarginInBodyOffset=a.offsetTop!==j;a.removeChild(b);
c.offset.initialize=c.noop},bodyOffset:function(a){var b=a.offsetTop,d=a.offsetLeft;c.offset.initialize();if(c.offset.doesNotIncludeMarginInBodyOffset){b+=parseFloat(c.curCSS(a,"marginTop",true))||0;d+=parseFloat(c.curCSS(a,"marginLeft",true))||0}return{top:b,left:d}},setOffset:function(a,b,d){if(/static/.test(c.curCSS(a,"position")))a.style.position="relative";var f=c(a),e=f.offset(),j=parseInt(c.curCSS(a,"top",true),10)||0,i=parseInt(c.curCSS(a,"left",true),10)||0;if(c.isFunction(b))b=b.call(a,
d,e);d={top:b.top-e.top+j,left:b.left-e.left+i};"using"in b?b.using.call(a,d):f.css(d)}};c.fn.extend({position:function(){if(!this[0])return null;var a=this[0],b=this.offsetParent(),d=this.offset(),f=/^body|html$/i.test(b[0].nodeName)?{top:0,left:0}:b.offset();d.top-=parseFloat(c.curCSS(a,"marginTop",true))||0;d.left-=parseFloat(c.curCSS(a,"marginLeft",true))||0;f.top+=parseFloat(c.curCSS(b[0],"borderTopWidth",true))||0;f.left+=parseFloat(c.curCSS(b[0],"borderLeftWidth",true))||0;return{top:d.top-
f.top,left:d.left-f.left}},offsetParent:function(){return this.map(function(){for(var a=this.offsetParent||s.body;a&&!/^body|html$/i.test(a.nodeName)&&c.css(a,"position")==="static";)a=a.offsetParent;return a})}});c.each(["Left","Top"],function(a,b){var d="scroll"+b;c.fn[d]=function(f){var e=this[0],j;if(!e)return null;if(f!==w)return this.each(function(){if(j=wa(this))j.scrollTo(!a?f:c(j).scrollLeft(),a?f:c(j).scrollTop());else this[d]=f});else return(j=wa(e))?"pageXOffset"in j?j[a?"pageYOffset":
"pageXOffset"]:c.support.boxModel&&j.document.documentElement[d]||j.document.body[d]:e[d]}});c.each(["Height","Width"],function(a,b){var d=b.toLowerCase();c.fn["inner"+b]=function(){return this[0]?c.css(this[0],d,false,"padding"):null};c.fn["outer"+b]=function(f){return this[0]?c.css(this[0],d,false,f?"margin":"border"):null};c.fn[d]=function(f){var e=this[0];if(!e)return f==null?null:this;if(c.isFunction(f))return this.each(function(j){var i=c(this);i[d](f.call(this,j,i[d]()))});return"scrollTo"in
e&&e.document?e.document.compatMode==="CSS1Compat"&&e.document.documentElement["client"+b]||e.document.body["client"+b]:e.nodeType===9?Math.max(e.documentElement["client"+b],e.body["scroll"+b],e.documentElement["scroll"+b],e.body["offset"+b],e.documentElement["offset"+b]):f===w?c.css(e,d):this.css(d,typeof f==="string"?f:f+"px")}});A.jQuery=A.$=c})(window);


var XHR = function(strURL, fncCallback, fncError, oHTTP) {
	//var oHTTP = null;
	//if (window.XMLHttpRequest) {
	//	oHTTP = new XMLHttpRequest();
	//} else if (window.ActiveXObject) {
	//	oHTTP = new ActiveXObject("Microsoft.XMLHTTP");
	//}
	if (oHTTP) {
		if (fncCallback) {
			/*if(typeof(oHTTP.onload)!="undefined")
				oHTTP.onload = function() {
					fncCallback(this);
					oHTTP = null;
				};
			else {*/
				oHTTP.onreadystatechange = function() {
					if (oHTTP.readyState == 4) {
						if (oHTTP.status == 200){
						    fncCallback(this);
						    oHTTP = null;
					    }
					    else{
					    	fncError();
					        oHTTP = null;
					    }
					}
				};
			//}
		}
		/*if(fncError){
			if(typeof(oHTTP.onerror)!="undefined"){
				oHTTP.onerror = function(){
					fncError();
					oHTTP = null;
				};
			}
		}*/
		oHTTP.open("GET", strURL, true);
		oHTTP.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
		oHTTP.send(null);
	} 
	/*else {
		if (fncError) fncError();
	}*/
};


//jquery.gamequery.js

(function($) {
			
    $.extend({ gameQuery: {
        /**
         * This is the Animation Object
         */
        Animation: function (options) {
            // private default values
            var defaults = {
                imageURL:		"",
                numberOfFrame:	1,
                delta:			0,
                rate: 			30,
                type:			0,
                distance:		0,
                offsetx:        0,
                offsety:        0
            };
            
            // options extends defaults
            options = $.extend(defaults, options);

            //"public" attributes:
            this.imageURL		= options.imageURL;		// The url of the image to be used as an animation or sprite 
            this.numberOfFrame	= options.numberOfFrame;// The number of frame to be displayed when playing the animation
            this.delta			= options.delta;		// The the distance in pixels between two frame
            this.rate			= options.rate;			// The rate at which the frame must be played in miliseconds
            this.type			= options.type;			// The type of the animation.This is bitwise OR of the properties.
            this.distance		= options.distance;		// The the distance in pixels between two animation
            this.offsetx        = options.offsetx;      // The x coordinate where the first sprite begin
            this.offsety        = options.offsety;      // The y coordinate where the first sprite begin
            
            var imgDownload = new Image();
            imgDownload.src = this.imageURL;
            this.image = imgDownload;
            
            //Whenever a new animation is created we add it to the ResourceManager animation list
            $.gameQuery.resourceManager.addAnimation(this);
            
            return true;
        },
        
        // "constants" for the different type of an animation
        ANIMATION_VERTICAL:   1,  // genertated by a verical offset of the background
        ANIMATION_HORIZONTAL: 2,  // genertated by a horizontal offset of the background
        ANIMATION_ONCE:       4,  // played only once (else looping indefinitly)
        ANIMATION_CALLBACK:   8,  // A callack is exectued at the end of a cycle 
        ANIMATION_MULTI:      16, // The image file contains many animations
        
        // "constants" for the different type of geometry for a sprite
        GEOMETRY_RECTANGLE:   1,
        GEOMETRY_DISC:        2, 
        
        // basic values
		refreshRate: 		  30,
        
        /**
         * An object to manages the resources loading
         **/
		resourceManager: {
            animations: [],    // List of animation / images used in the game
            sounds:     [],    // List of sounds used in the game
            callbacks:  [],    // List of the functions called at each refresh
            running:    false, // State of the game,
            
            /**
             * This function the covers things to load befor to start the game.
             **/
            preload: function() {
                //Start loading the images
                for (var i = this.animations.length-1 ; i >= 0; i --){
                	this.animations[i].domO = this.animations[i].image;
                	this.animations[i].domO.src = this.animations[i].image.src;
                    //this.animations[i].domO = new Image();
                    //this.animations[i].domO.src = this.animations[i].imageURL;
                }
                
                //Start loading the sounds
                for (var i = this.sounds.length-1 ; i >= 0; i --){
                    this.sounds[i].load();
                }
                 
                $.gameQuery.resourceManager.waitForResources();
            },
            
            /**
             * This function the waits for all the resources called for in preload() to finish loading.
             **/
            waitForResources: function() {
                var loadbarEnabled = ($.gameQuery.loadbar != undefined);
                if(loadbarEnabled){
                    $($.gameQuery.loadbar.id).width(0); 
                    var loadBarIncremant = $.gameQuery.loadbar.width / (this.animations.length + this.sounds.length);
                }
                //check the images
                var imageCount = 0; 
                for(var i=0; i < this.animations.length; i++){
                    if(this.animations[i].domO.complete){
                        imageCount++;
                    }
                }
                //check the sounds 
                var soundCount = 0; 
                for(var i=0; i < this.sounds.length; i++){
                    var temp = this.sounds[i].ready();
                    if(temp){
                        soundCount++;
                    }
                }
                //update the loading bar
                if(loadbarEnabled){
                    $("#"+$.gameQuery.loadbar.id).width((imageCount+soundCount)*loadBarIncremant); 
                    if($.gameQuery.loadbar.callback){
                        $.gameQuery.loadbar.callback((imageCount+soundCount)/(this.animations.length + this.sounds.length)*100);
                    }
                }
                if($.gameQuery.resourceManager.loadCallback){
                    var percent = (imageCount+soundCount)/(this.animations.length + this.sounds.length)*100;
                    $.gameQuery.resourceManager.loadCallback(percent); 
                }
                if(imageCount + soundCount < (this.animations.length + this.sounds.length)){
                    imgWait=setTimeout(function () {
                        $.gameQuery.resourceManager.waitForResources();
                    }, 100);
                } else {
                    // all the resources are loaded!
                    // We can associate the animation's images to their coresponding sprites
                    $.gameQuery.sceengraph.children().each(function(){
                        // recursive call on the children:
                        $(this).children().each(arguments.callee);
                        // add the image as a background
                        if(this.gameQuery && this.gameQuery.animation){
                            $(this).css("background-image", "url("+this.gameQuery.animation.imageURL+")");
                            // we set the correct kind of repeat
                            if(this.gameQuery.animation.type & $.gameQuery.ANIMATION_VERTICAL) {
                                $(this).css("background-repeat", "repeat-x");
                            } else if(this.gameQuery.animation.type & $.gameQuery.ANIMATION_HORIZONTAL) {
                                $(this).css("background-repeat", "repeat-y");
                            } else {
                                $(this).css("background-repeat", "no-repeat");
                            }
                        }
                    });
                    
                    // And launch the refresh loop
                    $.gameQuery.resourceManager.running = true;
                    setInterval(function () {
                        $.gameQuery.resourceManager.refresh();
                    },($.gameQuery.refreshRate));
                    if($.gameQuery.startCallback){
                        $.gameQuery.startCallback();
                    }
                    //make the sceengraph visible
                    $.gameQuery.sceengraph.css("visibility","visible");
                }
            },
            
            /**
             * This function refresh a unique sprite here 'this' represent a dom object
             **/
            refreshSprite: function() {
                //Call this function on all the children:
                // is 'this' a sprite ? 
                if(this.gameQuery != undefined){
                    var gameQuery = this.gameQuery;
                    // does 'this' has an animation ?
                    if(gameQuery.animation){
                        //Do we have anything to do?
                        if(gameQuery.idleCounter == gameQuery.animation.rate-1){
                            // does 'this' loops?
                            if(gameQuery.animation.type & $.gameQuery.ANIMATION_ONCE){
                                if(gameQuery.currentFrame < gameQuery.animation.numberOfFrame-2){
                                    gameQuery.currentFrame++;
                                } else if(gameQuery.currentFrame == gameQuery.animation.numberOfFrame-2) {
                                    gameQuery.currentFrame++;
                                    // does 'this' has a callback ?
                                    if(gameQuery.animation.type & $.gameQuery.ANIMATION_CALLBACK){
                                        if($.isFunction(gameQuery.callback)){
                                            gameQuery.callback(this);
                                        }
                                    }
                                }
                            } else {
                                gameQuery.currentFrame = (gameQuery.currentFrame+1)%gameQuery.animation.numberOfFrame;
                                if(gameQuery.currentFrame == 0){
                                    // does 'this' has a callback ?
                                    if(gameQuery.animation.type & $.gameQuery.ANIMATION_CALLBACK){
                                        if($.isFunction(gameQuery.callback)){
                                            gameQuery.callback(this);
                                        }
                                    }
                                }
                            }
                            // update the background:
                            if(gameQuery.animation.type & $.gameQuery.ANIMATION_VERTICAL){
                                if(gameQuery.multi){
                                    $(this).css("background-position",""+(-gameQuery.animation.offsetx-gameQuery.multi)+"px "+(-gameQuery.animation.offsety-gameQuery.animation.delta*gameQuery.currentFrame)+"px");
                                } else {
                                    $(this).css("background-position",""+(-gameQuery.animation.offsetx)+"px "+(-gameQuery.animation.offsety-gameQuery.animation.delta*gameQuery.currentFrame)+"px");
                                }
                            } else if(gameQuery.animation.type & $.gameQuery.ANIMATION_HORIZONTAL) {
                                if(gameQuery.multi){
                                    $(this).css("background-position",""+(-gameQuery.animation.offsetx-gameQuery.animation.delta*gameQuery.currentFrame)+"px "+(-gameQuery.animation.offsety-gameQuery.multi)+"px");
                                } else {
                                    $(this).css("background-position",""+(-gameQuery.animation.offsetx-gameQuery.animation.delta*gameQuery.currentFrame)+"px "+(-gameQuery.animation.offsety)+"px");
                                }
                            }
                        }
                        gameQuery.idleCounter = (gameQuery.idleCounter+1)%gameQuery.animation.rate;
                    }
                }
                return true;
            },
            
            /**
             * This function refresh a unique tile-map here 'this' represent a dom object
             **/
            refreshTilemap: function() {
                //Call this function on all the children:
                // is 'this' a sprite ? 
                if(this.gameQuery != undefined){
                    var gameQuery = this.gameQuery;
                    if($.isArray(gameQuery.frameTracker)){
                        for(var i=0; i<gameQuery.frameTracker.length; i++){
                            //Do we have anything to do?
                            if(gameQuery.idleCounter[i] == gameQuery.animations[i].rate-1){
                                // does 'this' loops?
                                if(gameQuery.animations[i].type & $.gameQuery.ANIMATION_ONCE){
                                    if(gameQuery.frameTracker[i] < gameQuery.animations[i].numberOfFrame-1){
                                        gameQuery.frameTracker[i]++;
                                    }
                                } else {
                                    gameQuery.frameTracker[i] = (gameQuery.frameTracker[i]+1)%gameQuery.animations[i].numberOfFrame;
                                }
                            }
                            gameQuery.idleCounter[i] = (gameQuery.idleCounter[i]+1)%gameQuery.animations[i].rate;
                        }
                    } else {
                        //Do we have anything to do?
                        if(gameQuery.idleCounter == gameQuery.animations.rate-1){
                            // does 'this' loops?
                            if(gameQuery.animations.type & $.gameQuery.ANIMATION_ONCE){
                                if(gameQuery.frameTracker < gameQuery.animations.numberOfFrame-1){
                                    gameQuery.frameTracker++;
                                }
                            } else {
                                gameQuery.frameTracker = (gameQuery.frameTracker+1)%gameQuery.animations.numberOfFrame;
                            }
                        }
                        gameQuery.idleCounter = (gameQuery.idleCounter+1)%gameQuery.animations.rate;
                    }


                    // update the background of all active tiles:
                    $(this).find(".active").each(function(){
                        if($.isArray(gameQuery.frameTracker)){
                            var animationNumber = this.gameQuery.animationNumber
                            if(gameQuery.animations[animationNumber].type & $.gameQuery.ANIMATION_VERTICAL){
                                $(this).css("background-position",""+(-gameQuery.animations[animationNumber].offsetx)+"px "+(-gameQuery.animations[animationNumber].offsety-gameQuery.animations[animationNumber].delta*gameQuery.frameTracker[animationNumber])+"px");
                            } else if(gameQuery.animations[animationNumber].type & $.gameQuery.ANIMATION_HORIZONTAL) {
                                $(this).css("background-position",""+(-gameQuery.animations[animationNumber].offsetx-gameQuery.animations[animationNumber].delta*gameQuery.frameTracker[animationNumber])+"px "+(-gameQuery.animations[animationNumber].offsety)+"px");
                            }
                        } else {
                            if(gameQuery.animations.type & $.gameQuery.ANIMATION_VERTICAL){
                                $(this).css("background-position",""+(-gameQuery.animations.offsetx-this.gameQuery.multi)+"px "+(-gameQuery.animations.offsety-gameQuery.animations.delta*gameQuery.frameTracker)+"px");
                            } else if(gameQuery.animations.type & $.gameQuery.ANIMATION_HORIZONTAL) {
                                $(this).css("background-position",""+(-gameQuery.animations.offsetx-gameQuery.animations.delta*gameQuery.frameTracker)+"px "+(-gameQuery.animations.offsety-this.gameQuery.multi)+"px");
                            }
                        }
                    });
                }
                return true;
            },
            
            /**
             * This function is called periodically to refresh the state of the game.
             **/
            refresh: function() {
                $.gameQuery.playground.find(".sprite").each(this.refreshSprite);
                $.gameQuery.playground.find(".tileSet").each(this.refreshTilemap);
                var deadCallback= new Array();
                for (var i = this.callbacks.length-1; i >= 0; i--){
                    if(this.callbacks[i].idleCounter == this.callbacks[i].rate-1){
                        var returnedValue = this.callbacks[i].fn();
                        if(typeof returnedValue == 'boolean'){
                            // if we have a boolean: 'true' means 'no more execution', 'false' means 'execute once more'
                            if(returnedValue){
                                deadCallback.push(i);
                            }
                        } else if(typeof returnedValue == 'number') {
                            // if we have a number it re-defines the time to the nex call
                            this.callbacks[i].rate = Math.round(returnedValue/$.gameQuery.refreshRate);
                            this.callbacks[i].idleCounter = 0;
                        }
                    }
                    this.callbacks[i].idleCounter = (this.callbacks[i].idleCounter+1)%this.callbacks[i].rate;
                }
                for(var i = deadCallback.length-1; i >= 0; i--){
                    this.callbacks.splice(deadCallback[i],1);
                }
            },
            
            addAnimation: function(animation) {
                if($.inArray(animation,this.animations)<0){
                    //normalize the animationRate:
                    animation.rate = Math.round(animation.rate/$.gameQuery.refreshRate);
                    if(animation.rate==0){
                        animation.rate = 1;
                    }
                    this.animations.push(animation);
                }
            },
            
            addSound: function(sound){
                if($.inArray(sound,this.sounds)<0){
                    this.sounds.push(sound);
                }
            },

            
            registerCallback: function(fn, rate){
                rate  = Math.round(rate/$.gameQuery.refreshRate);
                if(rate==0){
                    rate = 1;
                }
                this.callbacks.push({fn: fn, rate: rate, idleCounter: 0});
            }
        },
        
        // This is a single place to update the underlying data of sprites/groups/tiles
        update: function(descriptor, transformation) {
            // Did we really recieve a descriptor or a jQuery object instead?
            if(!$.isPlainObject(descriptor)){
                // Then we must get real descriptor
                if(descriptor.length > 0){
                    var gameQuery = descriptor[0].gameQuery;
                } else {
                    var gameQuery = descriptor.gameQuery;
                }
            } else {
                var gameQuery = descriptor;
            }
            // If we couldn't find one we return
            if(!gameQuery) return;
            if(gameQuery.tileSet === true){
                //then we have a tilemap!
                descriptor = $(descriptor);
                // find the tilemap offset relatif to the playground:
                var playgroundOffset = $.gameQuery.playground.offset();
                var tileSetOffset = descriptor.offset();
                tileSetOffset = {top: tileSetOffset.top - playgroundOffset.top, left: tileSetOffset.left - playgroundOffset.left};
                // test what kind of transformation we have and react accordingly:
                // Update the descriptor
                for(property in transformation){
                    switch(property){
                        case "left":
                            //Do we need to activate/desactive the first/last column
                            var left = parseFloat(transformation.left);
                            //Get the tileSet offset (relatif to the playground)
                            var playgroundOffset = $.gameQuery.playground.offset();
                            var tileSetOffset = descriptor.parent().offset();
                            tileSetOffset = {top: tileSetOffset.top - playgroundOffset.top, left: tileSetOffset.left + left - playgroundOffset.left};
                            
                            //actvates the visible tiles
                            var firstColumn = Math.max(Math.min(Math.floor(-tileSetOffset.left/gameQuery.width), gameQuery.sizex),0);
                            var lastColumn = Math.max(Math.min(Math.ceil(($.gameQuery.playground[0].width-tileSetOffset.left)/gameQuery.width), gameQuery.sizex),0);
                            
                            for(var i = gameQuery.firstRow; i < gameQuery.lastRow; i++){
                                // if old first col < new first col
                                // deactivate the newly invisible tiles
                                for(var j = gameQuery.firstColumn; j < firstColumn ; j++) {
                                    $("#tile_"+descriptor.attr("id")+"_"+i+"_"+j).removeClass("active");
                                }
                                //and activate the newly visible tiles
                                for(var j = gameQuery.lastColumn; j < lastColumn ; j++) {
                                    $("#tile_"+descriptor.attr("id")+"_"+i+"_"+j).addClass("active");
                                }
                                
                                // if old first col > new first col
                                // deactivate the newly invisible tiles
                                for(var j = lastColumn; j < gameQuery.lastColumn ; j++) {
                                    $("#tile_"+descriptor.attr("id")+"_"+i+"_"+j).removeClass("active");
                                }
                                //activate the newly visible tiles
                                for(var j = firstColumn; j < gameQuery.firstColumn ; j++) {
                                    $("#tile_"+descriptor.attr("id")+"_"+i+"_"+j).addClass("active");
                                }
                            }
                            
                            gameQuery.firstColumn = firstColumn;
                            gameQuery.lastColumn = lastColumn;
                            break;
                        case "top":
                            //Do we need to activate/desactive the first/last row
                            var top = parseFloat(transformation.top);
                            //Get the tileSet offset (relatif to the playground)
                            var playgroundOffset = $.gameQuery.playground.offset();
                            var tileSetOffset = descriptor.parent().offset();
                            tileSetOffset = {top: tileSetOffset.top + top - playgroundOffset.top, left: tileSetOffset.left - playgroundOffset.left};
                            
                            //actvates the visible tiles
                            var firstRow = Math.max(Math.min(Math.floor(-tileSetOffset.top/gameQuery.height), gameQuery.sizey), 0);
                            var lastRow = Math.max(Math.min(Math.ceil(($.gameQuery.playground[0].height-tileSetOffset.top)/gameQuery.height), gameQuery.sizey), 0);
                            
                            
                            for(var j = gameQuery.firstColumn; j < gameQuery.lastColumn ; j++) {
                                 // if old first row < new first row
                                // deactivate the newly invisible tiles
                                for(var i = gameQuery.firstRow; i < firstRow; i++){
                                    $("#tile_"+descriptor.attr("id")+"_"+i+"_"+j).removeClass("active");
                                }
                                //and activate the newly visible tiles
                                for(var i = gameQuery.lastRow; i < lastRow; i++){
                                    $("#tile_"+descriptor.attr("id")+"_"+i+"_"+j).addClass("active");
                                }
                                
                                // if old first row < new first row
                                // deactivate the newly invisible tiles
                                for(var i = lastRow; i < gameQuery.lastRow; i++){
                                    $("#tile_"+descriptor.attr("id")+"_"+i+"_"+j).removeClass("active");
                                }
                                //and activate the newly visible tiles
                                for(var i = firstRow; i < gameQuery.firstRow; i++){
                                    $("#tile_"+descriptor.attr("id")+"_"+i+"_"+j).addClass("active");
                                }
                            }
                            
                            gameQuery.firstRow = firstRow;
                            gameQuery.lastRow = lastRow;
                            
                            break;
                        case "angle": //(in degree)
                            //TODO
                            break;
                        case "factor":
                            //TODO
                            break;
                    }
                }
                
            } else {
                var refreshBoundingCircle = $.gameQuery.playground && !$.gameQuery.playground.disableCollision;
                
                // Update the descriptor
                for(property in transformation){
                    switch(property){
                        case "left":
                            gameQuery.posx = parseFloat(transformation.left);
                            if(refreshBoundingCircle){
                                gameQuery.boundingCircle.x = gameQuery.posx+gameQuery.width/2;
                            }
                            break;
                        case "top":
                            gameQuery.posy = parseFloat(transformation.top);
                            if(refreshBoundingCircle){
                                gameQuery.boundingCircle.y = gameQuery.posy+gameQuery.height/2;
                            }
                            break;
                        case "width":
                            gameQuery.width = parseFloat(transformation.width);
                            break;
                        case "height":
                            gameQuery.height = parseFloat(transformation.height);
                            break;
                        case "angle": //(in degree)
                            gameQuery.angle = parseFloat(transformation.angle);
                            break;
                        case "factor":
                            gameQuery.factor = parseFloat(transformation.factor);
                            if(refreshBoundingCircle){
                                gameQuery.boundingCircle.radius = gameQuery.factor*gameQuery.boundingCircle.originalRadius;
                            }
                            break;
                    }
                }
            }
        },
        
        // This is a utility function that returns the radius for a geometry
        proj: function (elem, angle) {
            switch (elem.geometry){
                case $.gameQuery.GEOMETRY_RECTANGLE :
                    var b = angle*Math.PI*2/360;
                    var Rx = Math.abs(Math.cos(b)*elem.width/2*elem.factor)+Math.abs(Math.sin(b)*elem.height/2*elem.factor);
                    var Ry = Math.abs(Math.cos(b)*elem.height/2*elem.factor)+Math.abs(Math.sin(b)*elem.width/2*elem.factor);
                    
                    return {x: Rx, y: Ry};
            }
        },
        
        // This is a utility function for collision of two object 
        collide: function(elem1, offset1, elem2, offset2) {
            // test real collision (only for two rectangle...)
            if((elem1.geometry == $.gameQuery.GEOMETRY_RECTANGLE && elem2.geometry == $.gameQuery.GEOMETRY_RECTANGLE)){
                
                var dx = offset2.x + elem2.boundingCircle.x - elem1.boundingCircle.x - offset1.x;
                var dy = offset2.y + elem2.boundingCircle.y - elem1.boundingCircle.y - offset1.y;
                var a  = Math.atan(dy/dx);

                var Dx = Math.abs(Math.cos(a-elem1.angle*Math.PI*2/360)/Math.cos(a)*dx);
                var Dy = Math.abs(Math.sin(a-elem1.angle*Math.PI*2/360)/Math.sin(a)*dy);
                
                var R = $.gameQuery.proj(elem2, elem2.angle-elem1.angle);
                
                if((elem1.width/2*elem1.factor+R.x <= Dx) || (elem1.height/2*elem1.factor+R.y <= Dy)) {
                    return false;
                } else {                  
                    var Dx = Math.abs(Math.cos(a-elem2.angle*Math.PI*2/360)/Math.cos(a)*-dx);
                    var Dy = Math.abs(Math.sin(a-elem2.angle*Math.PI*2/360)/Math.sin(a)*-dy);
                    
                    var R = $.gameQuery.proj(elem1, elem1.angle-elem2.angle);
                    
                    if((elem2.width/2*elem2.factor+R.x <= Dx) || (elem2.height/2*elem2.factor+R.y <= Dy)) {
                        return false;
                    } else {  
                        return true;
                    }
                }
            } else {
                return false;
            }
        }
    // This function mute (or unmute) all the sounds. 
    }, muteSound: function(muted){
        for (var i = $.gameQuery.resourceManager.sounds-1 ; i >= 0; i --) {
            $.gameQuery.resourceManager.sounds[i].muted(muted);
        }
    }, playground: function() { 
        return $.gameQuery.playground
    // This function define a callback that will be called upon during the 
    // loading of the game's resources. The function will recieve as unique
    // parameter a number representing the progess percentage.
    }, loadCallback: function(callback){
        $.gameQuery.resourceManager.loadCallback = callback;
    }});
    
    $.fn.extend({	
        /**
         * Define the div to use for the display the game and initailize it.
         * This could be called on any node it doesn't matter.
         * The returned node is the playground node.
         * This IS a desrtuctive call
         **/
        playground: function(options) {
            if(this.length == 1){
                if(this[0] == document){ // Old usage check
                    throw "Old playground usage, use $.playground() to retreive the playground and $('mydiv').playground(options) to set the div!";
                }
                options = $.extend({
                    height:		320,
                    width:		480,
                    refreshRate: 30,
                    position:	"absolute",
                    keyTracker:	false,
                    disableCollision: false
                }, options);
                //We save the playground node and set some variable for this node:
                $.gameQuery.playground = this;
                $.gameQuery.refreshRate = options.refreshRate;
                $.gameQuery.playground[0].height = options.height;
                $.gameQuery.playground[0].width = options.width;

                // We initialize the apearance of the div
                $.gameQuery.playground.css({
                        position: options.position,
                        display:  "block",
                        overflow: "hidden",
                        //height:   options.height+"px",
                        //width:    options.width+"px"
                        width:    "100%",
                        height:   "100%"
                    })
                    .append("<div id='sceengraph' style='visibility: hidden'/>");
                    
                $.gameQuery.sceengraph = $("#sceengraph");
                
                //Add the keyTracker to the gameQuery object:
                $.gameQuery.keyTracker = {};
                // we only enable the real tracking if the users wants it
                if(options.keyTracker){
                    $(document).keydown(function(event){
                        $.gameQuery.keyTracker[event.keyCode] = true;
                        if(event.keyCode==37){
                        	//alert("left");
                        }
                        else if(event.keyCode==38){
                        	//alert("up");
                        }
                        else if(event.keyCode==39){
                        	//alert("right");
                        }
                        else if(event.keyCode==40){
                        	//alert("down");
                        }
                    });
                    $(document).keyup(function(event){
                        $.gameQuery.keyTracker[event.keyCode] = false;
                    });
                }
            }
            return this;
        },
        
        /**
        * Starts the game. The resources from the resource manager are preloaded if necesary 
        * Works only for the playgroung node.
        * This is a non-desrtuctive call
        **/
        startGame: function(callback) {
            //if the element is the playground we start the game:
            $.gameQuery.startCallback = callback;
            $.gameQuery.resourceManager.preload();
            return this;
        },
        
        addBinaryUpload: function(input, options){
        	options = $.extend({
        		x:           0,
        		y:           0,
                width:		 0,
                height:		 0,
                preemptiveLocalReferenceId:      -1
            }, options);
            
            

            var newElement = "<input type='file' id='"+input+"' name='binary_"+options.preemptiveLocalReferenceId+"' style='position:absolute;pointer-events:visible;top:"+options.y+"px; left:"+options.x+"px; font-size: 185px; width:"+options.width+"px; height:"+options.height+"px;'>";
            
            
            this.append(newElement);
        },
        
        addInputText: function(input, options){
        	options = $.extend({
        		x:           0,
        		y:           0,
                width:		 0,
                height:		 0,
                preemptiveLocalReferenceId:      -1,
                hideText:    false
            }, options);
            
            var typeString = "text";
            
            if(options.hideText){
            	typeString = "password";
            }

            var newElement = "<input type='"+typeString+"' id='"+input+"' name='input_"+options.preemptiveLocalReferenceId+"' style='position:absolute;pointer-events:visible;top:"+options.y+"px; left:"+options.x+"px; font-size:100px; width:"+options.width+"px; height:"+options.height+"px;'>";
            
            
            this.append(newElement);
            
        },
        
        addTextArea: function(input, options){
        	options = $.extend({
        		x:           0,
        		y:           0,
                width:		 0,
                height:		 0,
                preemptiveLocalReferenceId:      -1
            }, options);

            var newElement = "<textarea id='"+input+"' name='textarea_"+options.preemptiveLocalReferenceId+"' style='position:absolute;pointer-events:visible;overflow:hidden;top:"+options.y+"px; left:"+options.x+"px; font-size:100px; width:"+options.width+"px; height:"+options.height+"px;'>";
            
            
            this.append(newElement);
            
        },
        
        /**
        * Add a group to the sceen graph
        * works only on the sceengraph root or on another group
        * This IS a desrtuctive call and should be terminated with end() to go back one level up in the chaining
        **/
        addGroup: function(group, options) {
            options = $.extend({
                width:		32,
                height:		32,
                preemptiveLocalReferenceId:      -1,
                overflow: 	"visible",
                geometry:   $.gameQuery.GEOMETRY_RECTANGLE,
                angle:          0,
                factor:         1
            }, options);
            
            var hideOrVisible = options.preemptiveLocalReferenceId==-1?"hidden;":"visible;"
            
            
            if(this.css("visibility")=="inherit" || this.css("visibility")=="hidden"){
            	hideOrVisible = "inherit";
            }
            
            //take out left and top
            var newGroupElement = "<div id='"+group+"' name='group_"+options.preemptiveLocalReferenceId+"' class='group' style='pointer-events: none; position: absolute; display: block; visibility: "+hideOrVisible+"; clip: rect(0px "+options.width+"px "+options.height+"px 0px); overflow: "+options.overflow+"; height: "+options.height+"px; width: "+options.width+"px;' />";
            
            //default enable clipping
            //var newGroupElement = "<div id='"+group+"' name='group_"+options.preemptiveLocalReferenceId+"' class='group' style='position: absolute; display: block; visibility: "+hideOrVisible+"; clip: rect(0px "+options.width+"px "+options.height+"px 0px); overflow: "+options.overflow+"; top: "+options.posy+"px; left: "+options.posx+"px; height: "+options.height+"px; width: "+options.width+"px;' />";
            
            //var newGroupElement = null;
            //if(group=='group_0'){
            	/*if(options.clipping){
            	    var newGroupElement = "<div id='"+group+"' name='group_"+options.preemptiveLocalReferenceId+"' class='group' style='position: absolute; display: block; visibility: "+hideOrVisible+"; clip: rect(0px "+options.width+"px "+options.height+"px 0px); overflow: "+options.overflow+"; top: "+options.posy+"px; left: "+options.posx+"px; height: "+options.height+"px; width: "+options.width+"px;' />";
        	    }
        	    else{
        	    	var newGroupElement = "<div id='"+group+"' name='group_"+options.preemptiveLocalReferenceId+"' class='group' style='position: absolute; display: block; visibility: "+hideOrVisible+"; overflow: "+options.overflow+"; top: "+options.posy+"px; left: "+options.posx+"px; height: "+options.height+"px; width: "+options.width+"px;' />";
        	    }*/
            //}
            //else{
            	//var newGroupElement = "<div id='"+group+"' name='group_"+options.preemptiveLocalReferenceId+"' class='group' style='position: absolute; display: block; visibility: "+hideOrVisible+"; overflow: "+options.overflow+"; top: "+options.posy+"px; left: "+options.posx+"px; height: "+options.height+"px; width: "+options.width+"px;' />";
            	
            //}
            
            
            if(this == $.gameQuery.playground){
                $.gameQuery.sceengraph.append(newGroupElement);
            } else if ((this == $.gameQuery.sceengraph)||(this.hasClass("group"))||(this.hasClass("sprite"))){
                this.append(newGroupElement);
            }
            
            
            //if(this == $.gameQuery.playground){
            //    $.gameQuery.sceengraph.append(newGroupElement);
            //} else if ((this == $.gameQuery.sceengraph)||(this.hasClass("group"))||(this.hasClass("sprite"))){
            //    this.append(newGroupElement);
            //}
            var newGroup = $("#"+group);
            newGroup[0].gameQuery = options;
            newGroup[0].gameQuery.boundingCircle = {x: options.posx + options.width/2,
                                                    y: options.posy + options.height/2,
                                                    originalRadius: Math.sqrt(Math.pow(options.width,2) + Math.pow(options.height,2))/2};
            newGroup[0].gameQuery.boundingCircle.radius = newGroup[0].gameQuery.boundingCircle.originalRadius;
            newGroup[0].gameQuery.group = true;
            return this.pushStack(newGroup);
        },
        
        addCanvas: function(group, options) {
            options = $.extend({
                width:		32,
                height:		32,
                preemptiveLocalReferenceId:      -1,
                posx:		0,
                posy:		0,
                overflow: 	"visible",
                geometry:   $.gameQuery.GEOMETRY_RECTANGLE,
                angle:          0,
                factor:         1
            }, options);
            
            var hideOrVisible = options.preemptiveLocalReferenceId==-1?"hidden;":"visible;"
            
            var newGroupElement = "<canvas id='"+group+"' name='group_"+options.preemptiveLocalReferenceId+"' width='"+options.width+"' height='"+options.height+"' class='group' style='position: absolute; display: block; visibility: "+hideOrVisible+" overflow: "+options.overflow+"; top: "+options.posy+"px; left: "+options.posx+"px;'></canvas>";
            if(this == $.gameQuery.playground){
                $.gameQuery.sceengraph.append(newGroupElement);
            } else if ((this == $.gameQuery.sceengraph)||(this.hasClass("group"))||(this.hasClass("sprite"))){
                this.append(newGroupElement);
            }
            var newGroup = $("#"+group);
            newGroup[0].gameQuery = options;
            newGroup[0].gameQuery.boundingCircle = {x: options.posx + options.width/2,
                                                    y: options.posy + options.height/2,
                                                    originalRadius: Math.sqrt(Math.pow(options.width,2) + Math.pow(options.height,2))/2};
            newGroup[0].gameQuery.boundingCircle.radius = newGroup[0].gameQuery.boundingCircle.originalRadius;
            newGroup[0].gameQuery.group = true;
            return this.pushStack(newGroup);
        },
        
        triggerClick: function(anevent){
        	this.trigger(anevent);
        },
        
        pause: function(){
        	var currentEle = this;
        	if(currentEle.data("video")!=null){
        		currentEle.data("video").pause();
        	}
        },
        
        cancelDownload: function(){
        	var currentEle = this;
        	currentEle.data("notPaused", false);
        	if(currentEle.data("conn")!=null){
        		currentEle.data("conn").abort();
        		currentEle.data("conn", null);
        	}
        },
        
        play2: function(){
        	var currentEle = this;
        	
        	
        	try{
	            //$(this).find("video")[0].play();
	            
	            var myVideo = $(this).find("video")[0];
	            
	            /*if (myVideo.requestFullscreen) {
				      myVideo.requestFullscreen();
				} else if (myVideo.mozRequestFullScreen) {
				      myVideo.mozRequestFullScreen();
				} else if (myVideo.webkitRequestFullscreen) {
				      myVideo.webkitRequestFullscreen();
				}*/
				
				//$(myVideo).bind("touchstart", function(event){
				//	event.stopPropagation();
				//	event.preventDefault();
				//	return false;
				//});
				//$(myVideo).bind("mousedown", function(event){
				//	event.stopPropagation();
				//	event.preventDefault();
				//	return false;
				//});
				
	            
	            myVideo.addEventListener('canplay', function() {
				    //$(this).find("video")[0].play();
				    this.play();
				    $(currentEle).find("div[id='loading_video']").remove();
				});
				
				//myVideo.addEventListener('loadstart', function() {
				  //  $(currentEle).find("div[id='loading_video']").remove();
				//});
				
				myVideo.load();
				myVideo.play();
            }
            catch(err){};
        	
        },
        
        play: function(){  
        	var currentEle = this;
        	
        	
        	var JSVideo = (function() {
	
        	processVideos();

			function addEvent(oElement, strEvent, fncAction) {
				if (oElement.addEventListener) { 
					oElement.addEventListener(strEvent, fncAction, false); 
				} else if (oElement.attachEvent) { 
					oElement.attachEvent(strEvent, fncAction); 
				}
			}
			
			
			// run through the page and initiate videos
			function processVideos() {
				
				var aDiv = currentEle.find("div")[0];
				var oVP = new xxx(aDiv);
				oVP.onload = function(){
					oVP.play();
				}
				oVP.loadFile(aDiv.getAttribute("videosrc"));
				
				
				/*var aDiv = currentEle.find("div");
				//var aDiv = document.getElementsByTagName("div");
				for (var i=0;i<aDiv.length;i++) {
					if (aDiv[i].getAttribute("videosrc")) {
						//var oVP = new JSVideo(aDiv[i]);
						//if (aDiv[i].getAttribute("videoautoplay") == "true") {
						//	oVP.onload = function() {
						//		this.play();
						//	}
						//}
						
						var oVP = new xxx(aDiv[i]);
						oVP.onload = function(){
							oVP.play();
						}
						oVP.loadFile(aDiv[i].getAttribute("videosrc"));
					}
				}*/
			}
			//addEvent(window, "load", processVideos);
			
			function xxx(oContainer) {
			
				this.onload = null;
				this.onerror = null;
			
				var oVideoData;
			
				var oCanvas = oContainer.appendChild(document.createElement("div"));
				
				if(currentEle.data("playing")!=null){
					$(oCanvas).css("visibility", "hidden");
				}
				
				//oCanvas.style.backgroundColor = "black";
				//oCanvas.style.width = "320px";
				//oCanvas.style.height = "240px";
				oCanvas.style.width = "100%";
				oCanvas.style.height = "100%";
				oCanvas.style.position = "absolute";
			
				/*var oLogoImage = document.createElement("img");
				var oStyle = oLogoImage.style;
				oStyle.display = "none";
				oStyle.position = "absolute";
				oStyle.width = "320px";
				oStyle.height = "240px";
				oStyle.zIndex = 10;
				oStyle.padding = oStyle.borderWidth = 0;
				oCanvas.appendChild(oLogoImage);*/
			
				// we embed this awesome logo (and a few other images) using a base64 encoded data:uri
				var strLogo = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAHwAA/+4ADkFkb2JlAGTAAAAAAf/bAIQAEAsLCwwLEAwMEBcPDQ8XGhQQEBQaHhcXFxcXHh0XGhkZGhcdHSMkJiQjHS8vMjIvL0BAQEBAQEBAQEBAQEBAQAERDw8RExEVEhIVFBETERQZFBUVFBklGRkbGRklLyIdHR0dIi8qLSYmJi0qNDQvLzQ0QEA+QEBAQEBAQEBAQEBA/8AAEQgAeACgAwEiAAIRAQMRAf/EAJAAAQADAQEBAQAAAAAAAAAAAAAEBQYDAQIHAQEAAwEBAAAAAAAAAAAAAAAAAQIDBAUQAAICAQIEBAMGBgMBAAAAAAECAAMEEQUhMRIGQVEiE2FxFIGRwdEyQqGxUmJyI/DhFTMRAAIBAwMCBAUFAQAAAAAAAAABAhESAyExBEFRYeEyE3GB0SJSocFCYjMU/9oADAMBAAIRAxEAPwD8/iIgCIiAIiIAiJ6FY8hrAPInVcW9uSGdBt+Uf2GTR9irlFbtEaJKO3ZQ/YZybFvXmhij7BTi9mjlE9KkcxpPJBYREQBERAEREAREQBET0AsdBzgHkk4u35OSwWtCdfhLTadia7S6/wBNfh5n5TSVV0Y1WiAV1qOJ/MzWGJvV6I48/MUHbD7pFJh9r8myW6f7RxMtqdn26hdSgOnMseH4Svz+46qdUxh1N/WfwEocnd8zIOrufvl3LHDRK5mSx8nNrKVi7GxbK2rH4E1gjwAB/Ccn7h25ORJ+wfnMO1tjc2JnzqZV530SRouBD+UpS+Ztz3Nt/LpJ+ek9Xedku4PWOP8AaJh57qR4yPel2RP/AA4+jkvmbJ9q2bcDrjWhGP7T+GukrM7tXJoUvV60Hiv5SkrybqzqrGWuF3Jm45ALll/pPERdCXqVPFD2s+P/ADnevxl9Sqtx7ajo66TlNiubtG8L0ZCii88rBy1+Mpty2DJxn1QddbcUdeKkfCVlj6x+5GmPkJu3Ivbl4/sU8Tu2FkrzQzkyMvBhpKUZupJ7NM+YiIJEREAS+2LahafqLx/rXkPM+Uq9uxWyclKwOZAm1rrSqtaqxoqDQfnNsMLnV7I4+ZncI2R9Uj7LIilmIVFHHyAEy+8bxZkOaqiVqHISw7gzTXWMZDoTxf7eQmcWstxk5ZutqM+JgSXuT1b2OJ1J1MaGShQY9gzGh23oi6GNDJIoM+hjmKC9ETpMdJkz6czz6cxQXoiaGOkyX7Bj6c+UUF6IyM6HVTpNDsncL0MtGQeuo8ND4fKUxoM+DWVOolotxdUUyQhljbJH6Wi4uRWHVUdGHA6Ayt3Lt3DyqmNSiu3mNORlL25vTUWjHuOtbcOPgfObIEEajkZ1RcckdUePljl4+TSTpuvE/LMvHbGuapxoVOhnGXHc3T/6VvT58fnKecclRtHt4pOUIyfVCIiQXND2zSNXuI4qOB+J4TQLxIHnKnt4AYLHxLD+Rlsh0YH4zsxKkEePypVzS8NDJ7tabc6wnzMndu4m35WdXi5yWML2VKzWwXpYnm2oOsgbohrzrAf6jLXtzL2XDtTLzzf9TRZ11JUFKMAOGvVoddZzPdnow9EabURJv2Cn/wBLMxqcmnGpxnCqcmzpLa68tFOvLjPG7WzxmPh61m1avfTRtVsr5a1nTjJ2J3Jt/uZ91qWY+RlWe5TfUqWOij9nr00+cZfc9D71h7ljizox61qtD9PWw1fr5HQ6hvvk6lXb36/oVVex3tRjZDvVVVluyVGxun9H6mbhwUec739uXUUJki+i/GawVNbS5ZUY/wBWoWTre4tts3fFv+nY7biJ0JQwXqBOur9PUV11Pn4TpuHceBk4D4fVfeTetgaxUQe2NNUHQ3CNSKKj1+B2zu29swB7dmJl3VBdWza2UgHz6AOAHxlRR21k3Y9WRZbRjLf/APBb7OhrP8RoZbYnce17f1WY12ZcvSRXh3FfbU/5angPhIlm77PuGLipuIvquw09vSgKVsQch6iOk8I1JdH9PMh19tZb4iZvXSlL9ehewKdayVI0PP8ATw0kizt6zIfAoxqUptyccW9RtZxZ6eosdVHRr5DWccvdsW7Z8PArVxZjPYzFgOkh2Zl0OuvI+Uutn3bFzd12tK+pDi4ppsL6AFlTT06E8I8yFRtLvb5lJZ2zkJXVcbqHoe0U2PXYCKmPg500H8Zx7o7fo2jIK4962VHpAraxWvUleol0VV0HlLHL3XAqwW23bltutvyBba1gA4qeCIEJ8QJ03TFwe4MsZ6tfiK9a2XtYilEqQ+0WXRtT6hx+AJ8hIZeKXTfQw3Ua3DDmJtNo3qptsd7m9VCjn4g8pB3LY9rq2Rr8b3GyafZsyb7WChBdT7q1rXr+4so/6BmUFrqCoJAPMRCbi6ojNx45YpS6Op23DIORlPYeOpJkaDxiUbqbxVEkugiIgk1PbjhsR18QQfs4iW0zPbuWKsn2nOi2ek/bNMRodDOvC6w+B5HLg45W/wAtSm7gwy2mUg114P8AMSiWwrNsyq6lHHUjcCDM/uOw2KxsxvWnPQcx85TLjdbkb8bkRtUJulNmVovnv1Hxkeym6o6OpE59RmNWdlieqJn1Hxnv1EhjrKlgCVBAJ04Anl/KGLqxVgVZToQeBBHgYuHtomfUTvg125+ZViUkB7W0DMdFUcyzHyAGs609uZV+Vj41dyD3sarKttcFUqW/ToViOrX9Q/4DJlPbeGdltynzAmeK1yU1IWkY7WNTxJGpLdJPD4DTUxcPbRYWU9t7fVRXXXZvWTku1Sslhor616B/r6RxGr6DnynS/bKkpp2jHcO+XnWFLv1MKsY21OSF4+kcfv8Ashblv9e2YeBi7XiJRb9Kli5TMbLK/eBZ+gEaK51Pq58eGkoU3vOTAO3qy+11F1sKg2p1aF1Sz9ShiBrIqTYuxsLrNo2xsDcUQY60e5UyAizINrqPbZ1JHqRG628j6ZE3zf8A6L6MYVdZF2Ggeq5BaopY9dCsG4Fxp1k+ZmNaxmYsxLMx1JPEknxM8Lk8zr4RUlROl+RbfY9trl7LWLuxPFmPEk/fOMaxILCIiAIiIB91WGtww5ibDbM5czHHH/ag0YeY85jJIw8y3FtFlZ00l8c7X4GHIwLLH+y2NvPQZAwd2xstQGIrt8QeR/KT+InXGSaqjyZwlB0kqHPIxaMlStyg/wB3Ij7ZjrMUPnri1MB7lgrVm4AdR6QTNtwIKnkwIP2ygze3LHdrKXDAnlyP3GZZoN0ojq4eaMW1OVF0qXtteLj4NmxbaFNaZuDjNeNC117M1lrOw4cOgcPDT7uV2Bj5Obj5l/Q+JYL82v3+mqi7JybHtWrr6QWVUQFuJ5acNdJW7XTu+20X49VKWJf6laxdTVYFZBZWdR6ulzK7M2vKx8VfqLdK69faqZ9dOrieheIGswsl2O9ZsbdFJNsttx7h26jNvysULn1bjRXVbQeuhakqRECf6+k8WU6qOHTw8ZnsvdtwzKzTfcxoLdYpHCtTqSAqjko1Og8JDPOJU0GpPOIiAIiIAiIgCIiAIiIAiIgH0rsh1U6GWOLvuZQAvV1KPA8R/GVkSU2tmVlCMlSSTNHV3MmmllQJ8xqJ0Pc1A5V/eZmIl/dn3MXxMP4l9kdzXMCKlCfL8zKjIzLshizsST5zhEq5ye7NIYccPTFIRESpoIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgH//Z";
			
				//oLogoImage.src = strLogo;
				//oLogoImage.style.display = "block";
			
				var fScale = 1;
			
				var bFileLoaded = false;
				var bPlaying = false;
				var bPaused = false;
			
				var iLastTime = -1;
				var iFrameCtr = -1;
				var bFrameSkip = true;
			
				var oLoading = document.createElement("div");
				oLoading.style.width = "100%";
				oLoading.style.height = "100%";
				oLoading.style.position = "absolute";
				oLoading.style.bottom = "0px";
				oLoading.style.paddingTop = "10px";
				oLoading.style.color = "black";
				oLoading.style.textAlign = "center";
				oLoading.style.display = "none";
				oLoading.style.fontFamily = "verdana";
				//oLoading.style.fontSize = "12px";
				oLoading.style.fontSize = "3em";
				oLoading.style.zIndex = 50;
				oLoading.innerHTML = "<table width='100%' height='100%'><tr height='100%'><td vertical-align='middle'>DOWNLOADING VIDEO...</td></tr></table>"
				oCanvas.appendChild(oLoading); 
				
				$(oCanvas).data("oLoading", oLoading);
			
				// create controls
				/*var oControls = document.createElement("div");
				oControls.style.width = "100%";
				oControls.style.height = "30px";
				oControls.style.position = "absolute";
				oControls.style.background = "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA9JREFUeNpiYGBgaAUIMAAAigCGNg2QYwAAAABJRU5ErkJggg==)";
				oControls.style.bottom = "0px";
				oControls.style.textAlign = "center";
				oControls.style.paddingTop = "10px";
				oControls.style.zIndex = 100;
				oControls.style.display = "none";
				oCanvas.appendChild(oControls);
			
				// show/hide controls on mouseover/out
				addEvent(oCanvas, "mouseover", function() {
					if (bFileLoaded) oControls.style.display = "block";
				});
				addEvent(oCanvas, "mouseout", function(e) {
					oControls.style.display = "none";
				});
			
				// create buttons
				var oBtnStop = oControls.appendChild(document.createElement("img"));
				oBtnStop.style.padding = oBtnStop.style.borderWidth = 0;
				oBtnStop.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAUCAYAAACaq43EAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAS9JREFUeNrslk1qwzAQhf0jYtcQg8+QK2RRss1ZcqqepdvSRa/QMwTS4MSNrekInsyQSI4jLbpoHzwsGzOfnpA9Sokokdp9fqV8ydg5W7EXYmyep4lbppBm9+yB/S3G5jm9rJbjy8pRIAXIAJ/YFbvEvYW7ZKEGeGa37BPuCb4LVoCaKTbsGhMoMCmXTLIOwAN7LyY0XL/sAmdIZ0DN69v7RxKg7eZ5DeAFHq4hvqUukTRUNWrkrn2ReRIrkTpU1dS+8G0UCy8iwMXUZvSB5ScVqnzq88uSX9I/+O+Cyfere0Bjc3gEbH/4XQS4Qw09Fyy7TBsBbkVr1HPAhGU6o8uE6oAag2u5lSexTbtHlwltiza1ngMmLM9JFDxGHAT6uYlJHF00eukx8uhzA/4RYAA+32qUpPWCZAAAAABJRU5ErkJggg==";
			
				var oBtnPlay = document.createElement("img");
				oBtnPlay.style.padding = oBtnPlay.style.borderWidth = 0;
				oBtnPlay.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAUCAYAAACaq43EAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAllJREFUeNq8lk1v00AQhrv22mkSWpo2/UipEmhKEVLECaQKceulv4ELp575BT31yAVxQxRx4cYf4MKBCwoUKaJQgYBUAgTlQ22axkkTf6wZS6/REryJgwojPXI2XvudmfXOLPN9f0i21WqD0UUjdIITBq4MUwThEC4Q4PcXRdid4siv3zziPoOoSSSJFJGQ5gZiHaINbDgSOhDLVMIcooGLGeIExDWINAkLNCUH3LjRRwlriDaINPPoSfm5fNPgfLdwanaDKNOwRtSJQ6IFB5w46dd6pHqYGO2+6bhu7v2Hj2vPtl6uWa1Wif6aI2aIccxP4bvQFe9XCmvIRBh1pDWs5tLm1qubb6o7V2mYJ2aJSeIkkcbzSnHewyGOdVWaECL1+dv3a3sHB1cWCvm709lsBc/sI3MtTPW7065Khbyl+lq7Yxe331XXK9uvr3dsO4+0p6UPksVJ9V8Z1QN9v15fKVde3Kb0r3Sl+98Jh+Z6XpbSv95vjY9dODBd13elNfX/i3A6mXx66UJpFdXNUVUzflyCjDEnNzV5/3xx/gENv6KiBdXMi4paJezDUy+OqGkYX87Nn7k1NTEebKcf2E6hsBhEWEjNoKeNjY48Li0ubiRM4xMN91BCLal8xo44FLWlAvDnx8HY0Vxu5t7Z04WHUpRhze70axhckWYPHh9GPZQwzR1ayxsTmbG3aBQ1RHnUta7+IN1JSNHWli8vXUTxH8a+9CAQCDVA2JncuH1ZFbGLlw9ByELHYRjb0kGgLZ1IYvXifqm2pWOOJdVcIR173EGilO2nAAMAzaLgW9GvhTsAAAAASUVORK5CYII=";
			
				var oBtnPause = document.createElement("img");
				oBtnPause.style.padding = oBtnPause.style.borderWidth = 0;
				oBtnPause.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAUCAYAAACaq43EAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAWlJREFUeNrsll9OAjEQxlta5Y+I4QxegQfllbN4Ks/iq/rgFTyDERDYpdsyTb4lE5i2a2JiYpzky7LMdH6dZtuODiEobg/vK02PHsmQLOkS7/H/GOxJezwVfBdCTE1ypAbv4fH2+six6tw0oBE4JI0YvE24A1wBOhBiNqQtfgeoCLaAxilOAbeoICZcIqECcCLEfMDvUbUqgXtIFhNNn55f304DFvO7eySPNqKYFyFmBuAearpUbLB8EyXbDSaosCqSxbFrSHet2LKqJRuzCsaJmPbbsGySWTCH9xP+PpKqQowIzYH5lkqNM4Uchm0xsbJfsX/w3wenvsiQOupgjvlcIuZ4OXwH7JGwSvgrdlbnYhy7xYrgFlqz8/jU1syX2usbdjX6LuCAZdrhFpLsk4F9ImaJHI203FpoBAyOu6sfuBZXpC8se1NqBAIGb9lHUmoE6kwj4KSKc0tdszYn1/pUUK71OQMfBBgAX9GM3df/0c4AAAAASUVORK5CYII=";
			
				oControls.appendChild(oBtnPlay);
				oControls.appendChild(oBtnPause);
			
				
			
				// and link them to video control functions
				addEvent(oBtnStop, "click", function() { me.stop(); });
				addEvent(oBtnPlay, "click", function() { me.play(); });
				addEvent(oBtnPause, "click", function() { me.pause(); });
				
				*/
				
				
				var me = this;
				currentEle.data("video", me);
			
				var aFrameImages = [];
			
				// load a video file from URL.
				this.loadFile = function(strURL) {
					if(currentEle.data("playing")==null){
					    oLoading.style.display = "block";
				    }
					var me = this;
					
           			/*var fncError = function() {
						if(typeof me.onerror == "function"){
							me.onerror("Error loading video file");
						}
					}*/
					
					var fncError = function(){
						new makeRequest2();
					}
					
					var fncLoad = function(oHTTP) {
						try {
							/*if(!oHTTP.getResponseHeader("Content-Length")){
								new makeRequest();
								return;
							}*/
							
							var responseText = oHTTP.responseText;
														
							if(responseText){
								oVideoData = eval("(" + responseText + ")");
							}
							else{
								if(oHTTP.getResponseHeader("Content-Length")){
									if(oHTTP.getResponseHeader("Content-Length")=="0"){
										//alert("last chunck received");
										currentEle.data("lastChunkReceived", true);
										
										
										return;
									}
								}
								else{
									new makeRequest2();
									//alert("no content length");
									return;
								}
							}
							
							
							
							aFrameImages = [];
							oLoading.style.display = "none";
							//take off white background color
							//currentEle.style.backgroundColor = "transparent";
							if(currentEle.data("playing")==null){
							    currentEle.css("background-color", "");
							    currentEle.data("playing", "yes");
						    }
					    	
						    
							bFileLoaded = true;
							
						} catch(e) {
							//alert("error");
							new makeRequest2();
							return;
							//if(typeof me.onerror == "function")
							//	me.onerror("Error parsing video data");
						}
						if (typeof me.onload == "function"){
							
							if(oVideoData){	
								//alert("oVideoData is OK!!");
								setTimeout(function(){
									if(currentEle.data("nextChunk")==null){
										currentEle.data("nextChunk", "yes");
										
										if(currentEle.data("oCanvas")!=null){
										    currentEle.data("oCanvas").remove();
									    }
								    	currentEle.data("oCanvas", $(oCanvas));
										
									    setTimeout(function(){
									    	//download next chunk!!
									    	var aDiv2 = currentEle.find("div")[0];
									    	var source = aDiv2.getAttribute("videosrc");
									    	var indexPart = source.indexOf("?part");
									    	var nextPart = parseInt(source.substring(indexPart+6))+1;
									    	var newSource = source.substring(0, indexPart+6)+nextPart;
									    	aDiv2.setAttribute("videosrc", newSource);
									    	currentEle.play();
									    }, 0);
									    
									    
								    	
								    	setTimeout(function(){
											$(oCanvas).css("visibility", "visible");
											me.onload();
										}, 0);
							    	}
							    	else{
							    		setTimeout(arguments.callee, 200);
							    	}
								}, 0);
							}
							else{
								new makeRequest2();
							    return;
							}
							//else{
							//	currentEle.data("lastChunkReceived", true);
							//}
							//else{
							//	setTimeout(function(){
							//		if(currentEle.data("nextChunk")==null){
							//			currentEle.remove();
							//		}
							//		else{
							//			setTimeout(arguments.callee, 200);
							//		}
							//	});
							//}
					    }
					}
					
					function makeRequest2(){
						var oHTTP = null;
						if (window.XMLHttpRequest) {
							oHTTP = new XMLHttpRequest();
						} else if (window.ActiveXObject) {
							oHTTP = new ActiveXObject("Microsoft.XMLHTTP");
						}
						if(currentEle.data("notPaused")){
							currentEle.data("conn", oHTTP);
						    XHR(strURL, fncLoad, fncError, oHTTP);
						}
					}
					makeRequest2();
				}
			
				// scale the video with the factor fScale.
				this.setScale = function(fScale) {
					fScale = parseFloat(fScale);
					this._updateCanvas();
				}
			
				// start playback
				this.play = function() {
					if (bPlaying) {
						if (bPaused) bPaused = false;
						return;
					}
					//oLogoImage.style.display = "none";
					this._updateCanvas();
					bPlaying = true;
					this._nextFrame();
				}
			
				// update the videocanvas when scaling/dimensions have changed
				this._updateCanvas = function() {
					//oCanvas.style.width = Math.round(oVideoData.width*fScale)+"px";
					//oCanvas.style.height = Math.round(oVideoData.height*fScale)+"px";
					
					oCanvas.style.width = "100%"
					oCanvas.style.height = "100%"
				}
			
				var aFrameImages;
				var oLastImage = null;
			
				function renderImage(oImage) {
					//oImage.style.width = "320px";
					//oImage.style.height = "240px";
					
					oImage.style.width = "100%";
					oImage.style.height = "100%";
					
					if (oLastImage) {
						oCanvas.replaceChild(oImage, oLastImage);
					} else {
						oCanvas.appendChild(oImage);
					}
					oLastImage = oImage;
				}
			
				// render next frame
				this._nextFrame = function() {
					if (!bPlaying) return;
			
					var iFrameRate = Math.round(1000 / oVideoData.rate);
					var iNow = new Date().getTime();
					var iLag = 0;
					var me = this;
					if (!bPaused&&currentEle.data("notPaused")) {
						iFrameCtr++;
						if (bFrameSkip && iLastTime > -1) {
							var iDeltaTime = iNow - iLastTime;
							iLag = iDeltaTime - iFrameRate;
							while (iLag > iFrameRate) {
								iFrameCtr++;
								iLag -= iFrameRate;
							}
						}
						if (iLag < 0) iLag = 0;
						//if (iFrameCtr >= oVideoData.frames) iFrameCtr = 0;
			
						if (!aFrameImages[iFrameCtr]) {
							var oImage = document.createElement("img");
							var oStyle = oImage.style;
							oStyle.display = "block";
							oStyle.position = "absolute";
							oStyle.padding = 0;
							oStyle.borderWidth = 0;
							oImage.onload = function() {
								renderImage(this);
							}
							aFrameImages[iFrameCtr] = oImage;
							if(oVideoData.data.video[iFrameCtr]){
								//alert(currentEle.data("notPaused"));
							    oImage.src = oVideoData.data.video[iFrameCtr];
						    }
						    else{
						    	oLoading.innerHTML = "<table width='100%' height='100%'><tr height='100%'><td vertical-align='middle'>Loading more...</td></tr></table>"
								oLoading.style.display = "block";
								oLoading.style.color = "white";
								    	
						    	currentEle.data("nextChunk", null);
						    	if(currentEle.data("lastChunkReceived")!=null){
						    		currentEle.data("oCanvas").data("oLoading").innerHTML = "<table width='100%' height='100%'><tr height='100%'><td vertical-align='middle'>Video finished</td></tr></table>"
									currentEle.data("oCanvas").data("oLoading").style.display = "block";
							        currentEle.data("oCanvas").data("oLoading").style.color = "white";
						    		currentEle.data("finished", true);
						    	}						    	
						    	if(iFrameCtr<(oVideoData.rate*10)){
						    		currentEle.data("finished", true);
						    	}
						    	aFrameImages = null;
						    	oVideoData = null;
						    }
						} 
						else {
							renderImage(aFrameImages[iFrameCtr]);
						}
						iLastTime = iNow;
						if(oVideoData!=null && currentEle.data("notPaused")){
							setTimeout(
								function() {
									if(oVideoData.data.video[iFrameCtr]){
									    me._nextFrame();
								    }
								    else{
								    	oVideoData = null;
								    	aFrameImages = null;
								    	currentEle.data("nextChunk", null);
								    	if(currentEle.data("lastChunkReceived")!=null){
								    		currentEle.data("oCanvas").data("oLoading").innerHTML = "<table width='100%' height='100%'><tr height='100%'><td vertical-align='middle'>Video finished...</td></tr></table>"
											currentEle.data("oCanvas").data("oLoading").style.display = "block";
									        currentEle.data("oCanvas").data("oLoading").style.color = "white";
								    		currentEle.data("finished", true);
								    	}
								    }
								}, iFrameRate - iLag
							);
						}
						else{
							aFrameImages = null;
							oVideoData = null;
						}
					}
					/*iLastTime = iNow;
					setTimeout(
						function() {
							if(oVideoData.data.video[iFrameCtr]){
							    me._nextFrame();
						    }
						    else{
						    	oVideoData = null;
						    	currentEle.data("nextChunk", null);
						    }
						}, iFrameRate - iLag
					);*/
				}
			
				// stop playback and rewind
				this.stop = function() {
					bPaused = false;
					bPlaying = false;
					iFrameCtr = -1;
					iLastTime = -1;
					//oLogoImage.src = strLogo;
					//oLogoImage.style.display = "block";
					//oLogoImage.zIndex = 20;
				}
			
				// temporarily pause playback, resume by calling pause() or play() again
				this.pause = function() {
					bPaused = !bPaused;
				}
			};
			
			
			
			})();
        },
        
        addVideo2: function(video, options) {
            options = $.extend({
                width:			32,
                height:			32,
                preemptiveLocalReferenceId:      0,
                mediaType:      1,
                videoUrl:       "",
                posx:			0,
                posy:			0,
            }, options);
            
            
            
            //var newVideoElem = "<div id='"+video+"' name='video_"+options.preemptiveLocalReferenceId+"' class='video' style='pointer-events: visible; position: absolute; display: block; overflow: hidden; height: "+options.height+"px; width: "+options.width+"px; left: "+options.posx+"px; top: "+options.posy+"px; background-position: "+((options.animation)? -options.animation.offsetx : 0)+"px "+((options.animation)? -options.animation.offsety : 0)+"px;'><video width='"+options.width+"' height='"+options.height+"' controls><source src='"+options.videoUrl+"'><table width='"+options.width+"' height='"+options.height+"'><tr><td width='"+options.width+"' height='"+options.height+"'><p style='color:white;text-align:center'>Loading video, please wait...</p></td></tr></table></div>";
            
            var newVideoElem = "<div id='"+video+"' name='video_"+options.preemptiveLocalReferenceId+"' class='video' style='pointer-events: visible; position: absolute; display: block; overflow: hidden; height: "+options.height+"px; width: "+options.width+"px; left: "+options.posx+"px; top: "+options.posy+"px; background-position: "+((options.animation)? -options.animation.offsetx : 0)+"px "+((options.animation)? -options.animation.offsety : 0)+"px;'><video width='"+options.width+"' height='"+options.height+"' controls><source src='"+options.videoUrl+"'></video><div id='loading_video' style='position:absolute; top:0px; left:0px; height: "+options.height+"px; width: "+options.width+"px;'><table width='100%' height='100%'><tr height='100%'><td valign='middle'><p style='margin-top:0px;margin-bottom:0px;color:red;font-size:80px;text-align:center'>Loading Video, please wait...</p></td></tr></table></div></div>";
            
            this.append(newVideoElem);
            
            return this;
        },
        
        /**
        * This is added by Victor
        * Add a video to the current node.
        * Works only on the playground, the sceengraph root or a sceengraph group
        * This is a non-desrtuctive call
        **/
        addVideo: function(video, options) {
            options = $.extend({
                width:			32,
                height:			32,
                preemptiveLocalReferenceId:      0,
                mediaType:      1,
                videoName:      "",
                videoUrl:       "",
                posx:			0,
                posy:			0,
                idleCounter:	0,
                currentFrame:	0,
                geometry:       $.gameQuery.GEOMETRY_RECTANGLE,
                angle:          0,
                factor:         1
            }, options);
            
            this.css("background-color", "black");
            
            if(options.mediaType>=4 && options.mediaType<=10){
            	var newVideoElem = "<div id='"+video+"' name='video_"+options.preemptiveLocalReferenceId+"' class='video' style='pointer-events: visible; position: absolute; display: block; overflow: hidden; height: "+options.height+"px; width: "+options.width+"px; left: "+options.posx+"px; top: "+options.posy+"px; background-position: "+((options.animation)? -options.animation.offsetx : 0)+"px "+((options.animation)? -options.animation.offsety : 0)+"px;'><div videosrc='"+options.videoUrl+"' videoautoplay='false' style='width:100%;height:100%'></div></div>";
            	
            }
            
            //if(options.mediaType>=4 && options.mediaType<=10){
            //	var newVideoElem = "<div id='"+video+"' name='video_"+options.preemptiveLocalReferenceId+"' class='video' style='position: absolute; display: block; overflow: hidden; height: "+options.height+"px; width: "+options.width+"px; left: "+options.posx+"px; top: "+options.posy+"px; background-position: "+((options.animation)? -options.animation.offsetx : 0)+"px "+((options.animation)? -options.animation.offsety : 0)+"px;'><video width='100%' height='100%'><source src='"+options.videoUrl+"'></video></div>";
            	
            //}
            
            /*if(options.mediaType==4){
                var newVideoElem = "<div id='"+video+"' name='video_"+preemptiveLocalReferenceId+"' class='video' style='position: absolute; display: block; overflow: hidden; height: "+options.height+"px; width: "+options.width+"px; left: "+options.posx+"px; top: "+options.posy+"px; background-position: "+((options.animation)? -options.animation.offsetx : 0)+"px "+((options.animation)? -options.animation.offsety : 0)+"px;'><object id="+video+" classid=clsid:D27CDB6E-AE6D-11cf-96B8-444553540000 name=player width=100% height=100%><param name=movie value=player.swf><param name=allowfullscreen value=true><param name=allowscriptaccess value=always><param name=bgcolor value=#000000><param name=scale value=exactfit><param name=wmode value=window><param name=flashvars value=file=/addVideo?sessionId="+sessionId+"?videoName="+options.videoName+"&controlbar=none&stretching=exactfit&autostart=true&repeat=single><embed type=application/x-shockwave-flash id=video_player2 name=video_player2 src=player.swf width=100% height=100% allowscriptaccess=always allowfullscreen=true bgcolor=#000000 wmode=window scale=exactfit flashvars=file=/addVideo?sessionId="+sessionId+"?videoName="+options.videoName+"&controlbar=none&stretching=exactfit&autostart=true&repeat=single></object></div>";
            }
            else if(options.mediaType==5){
            	var newVideoElem = "<div id='"+video+"' name='video_"+preemptiveLocalReferenceId+"' class='video' style='position: absolute; display: block; overflow: hidden; height: "+options.height+"px; width: "+options.width+"px; left: "+options.posx+"px; top: "+options.posy+"px; background-position: "+((options.animation)? -options.animation.offsetx : 0)+"px "+((options.animation)? -options.animation.offsety : 0)+"px;'><object CLASSID=clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B width=100% height=100% CODEBASE=http://www.apple.com/qtactivex/qtplugin.cab id=video_"+preemptiveLocalReferenceId+"><param name=scale value=aspect><param name=src value=sample.mov><param name=qtsrc value=/addVideo?sessionId="+sessionId+"?videoName="+options.videoName+"><param name=autoplay value=true><param name=loop value=true><param name=controller value=false><embed src=sample.mov qtsrc=/addVideo?sessionId="+sessionId+"?videoName="+options.videoName+" width=100% height=100% enablejavascript=true cache=true scale=aspect autoplay=true loop=true controller=false pluginspage=http://www.apple.com/quicktime/></embed></object></div>";
            }*/
            
            if(options.mediaType>=4 && options.mediaType<=10){
	            if(this == $.gameQuery.playground){
	                $.gameQuery.sceengraph.append(newVideoElem);
	            } else {
	                this.append(newVideoElem);
	            }
	            
	            var videoDOMObject = $("#"+video)[0];
	            if(videoDOMObject != undefined){
	                videoDOMObject.gameQuery = options;
	                //Compute bounding Cirlce:
	                videoDOMObject.gameQuery.boundingCircle = {x: options.posx + options.width/2,
	                                                            y: options.posy + options.height/2,
	                                                            originalRadius: Math.sqrt(Math.pow(options.width,2) + Math.pow(options.height,2))/2};
	                videoDOMObject.gameQuery.boundingCircle.radius = videoDOMObject.gameQuery.boundingCircle.originalRadius;
	            }
            }
            return this;
        },
        
        /**
        * Add a sprite to the current node.
        * Works only on the playground, the sceengraph root or a sceengraph group
        * This is a non-desrtuctive call
        **/
        addSprite: function(sprite, options) {
            options = $.extend({
                width:			32,
                height:			32,
                preemptiveLocalReferenceId:      0,
                idleCounter:	0,
                currentFrame:	0,
                geometry:       $.gameQuery.GEOMETRY_RECTANGLE,
                angle:          0,
                factor:         1
            }, options);
            
            //var newSpriteElem = "<div id='"+sprite+"' name='image_"+options.preemptiveLocalReferenceId+"' class='sprite' style='position: absolute; display: block; overflow: hidden; height: "+options.height+"px; width: "+options.width+"px; left: "+options.posx+"px; top: "+options.posy+"px; background-position: "+((options.animation)? -options.animation.offsetx : 0)+"px "+((options.animation)? -options.animation.offsety : 0)+"px;' />";
            
            //var newSpriteElem = "<div id='"+sprite+"' name='image_"+options.preemptiveLocalReferenceId+"' class='sprite' style='background: url("+options.animation.imageURL+"); position: absolute; display: block; visibility: inherit; overflow: hidden; height: "+options.height+"px; width: "+options.width+"px; left: "+options.posx+"px; top: "+options.posy+"px; background-position: "+((options.animation)? -options.animation.offsetx : 0)+"px "+((options.animation)? -options.animation.offsety : 0)+"px;' />";
            
            //current best one
            //var newSpriteElem = "<div id='"+sprite+"' name='image_"+options.preemptiveLocalReferenceId+"' class='sprite' style='pointer-events: visible; background: url("+options.animation.imageURL+"); position: absolute; display: block; visibility: inherit; overflow: hidden; height: "+options.height+"px; width: "+options.width+"px; background-position: "+((options.animation)? -options.animation.offsetx : 0)+"px "+((options.animation)? -options.animation.offsety : 0)+"px;' />";
            var newSpriteElem = "<div id='"+sprite+"' name='image_"+options.preemptiveLocalReferenceId+"' class='sprite' style='pointer-events: visible; position: absolute; display: block; visibility: inherit; overflow: hidden; height: "+options.height+"px; width: "+options.width+"px;' />";
            
            //var newSpriteElem = "<div id='"+sprite+"' name='image_"+options.preemptiveLocalReferenceId+"' class='sprite' style='background: url("+options.animation.imageURL+"); position: absolute; display: block; visibility: inherit; overflow: hidden; height: "+options.height+"px; width: "+options.width+"px;' />";
            
            
            if(this == $.gameQuery.playground){
                $.gameQuery.sceengraph.append(newSpriteElem);
            } else {
                this.append(newSpriteElem);
            }
            
            //if the game has already started we want to add the animation's image as a background now:
            if(options.animation){
                if($.gameQuery.resourceManager.running){
                    //$("#"+sprite).css("background-image", "url("+options.animation.imageURL+")");
                }
                if(options.animation.type & $.gameQuery.ANIMATION_VERTICAL) {
                    $("#"+sprite).css("background-repeat", "repeat-x");
                } else if(options.animation.type & $.gameQuery.ANIMATION_HORIZONTAL) {
                    $("#"+sprite).css("background-repeat", "repeat-y");
                } else {
                    $("#"+sprite).css("background-repeat", "no-repeat");
                }
            }
            
            
            var spriteDOMObject = $("#"+sprite)[0];
            if(spriteDOMObject != undefined){
                spriteDOMObject.gameQuery = options;
                //Compute bounding Cirlce:
                spriteDOMObject.gameQuery.boundingCircle = {x: options.posx + options.width/2,
                                                            y: options.posy + options.height/2,
                                                            originalRadius: Math.sqrt(Math.pow(options.width,2) + Math.pow(options.height,2))/2};
                spriteDOMObject.gameQuery.boundingCircle.radius = spriteDOMObject.gameQuery.boundingCircle.originalRadius;
            }
            return this;
        },
        
        /**
        * Remove the sprite  on which it is called. This is here for backward compatibility  but it doesn't
        * do anything more than simply calling .remove()
        * This is a non-desrtuctive call.
        **/
        removeSprite: function() {
            this.remove();
            return this;
        },
        
        /**
        * Add a Tile Map to the selected element. 
        * This is a non-destructive call.
        **/
        addTilemap: function(name, tileDescription, animationList, options){
        	options = $.extend({
                width:			32,
                height:			32,
                sizex:          32,
                sizey:          32,
                posx:			0,
                posy:			0
            }, options);
            
            //var newSpriteElem = "<div id='"+sprite+"' style='position: absolute; display: block; overflow: hidden; height: "+options.height+"px; width: "+options.width+"px; left: "+options.posx+"px; top: "+options.posy+"px; background-position: 0px 0px;' />";
            
            var tileSet = $("<div class='tileSet' style='position: absolute; display: block; overflow: hidden;' />");
            tileSet.css({top: options.posy, left: options.posx, height: options.height*options.sizey, width: options.width*options.sizex}).attr("id",name);
            if(this == $.gameQuery.playground){
                $.gameQuery.sceengraph.append(tileSet);
            } else {
                this.append(tileSet);
            }
            
            if($.isArray(animationList)){
                var frameTracker = [];
                var idleCounter = [];
                for(var i=0; i<animationList.length; i++){
                    frameTracker[i] = 0;
                    idleCounter[i] = 0;
                }
                tileSet[0].gameQuery = options
                tileSet[0].gameQuery.frameTracker = frameTracker; 
                tileSet[0].gameQuery.animations = animationList; 
                tileSet[0].gameQuery.idleCounter =  idleCounter; 
                tileSet[0].gameQuery.tileSet = true;
            } else {
                tileSet[0].gameQuery = options
                tileSet[0].gameQuery.frameTracker = 0; 
                tileSet[0].gameQuery.animations = animationList; 
                tileSet[0].gameQuery.idleCounter =  0; 
                tileSet[0].gameQuery.tileSet = true;
            }
            
            if(typeof tileDescription == "function"){
				for(var i=0; i<options.sizey; i++){
            		for(var j=0; j<options.sizex; j++){
                        if(tileDescription(i,j) != 0){
                            if($.isArray(animationList)){
                                // for many simple animation:
                                tileSet.addSprite("tile_"+name+"_"+i+"_"+j, 
                                                      {width: options.width, 
                                                       height: options.height, 
                                                       posx: j*options.width, 
                                                       posy: i*options.height, 
                                                       animation: animationList[tileDescription(i,j)-1]});
                                var newTile = $("#tile_"+name+"_"+i+"_"+j);
                                newTile.removeClass("sprite");
                                newTile.addClass("tileType_"+(tileDescription(i,j)-1));
                                newTile[0].gameQuery.animationNumber = tileDescription(i,j)-1;
                            } else {
                                // for multi-animation:
                                tileSet.addSprite("tile_"+name+"_"+i+"_"+j, 
                                                      {width: options.width, 
                                                       height: options.height, 
                                                       posx: j*options.width, 
                                                       posy: i*options.height, 
                                                       animation: animationList});
                                var newTile = $("#tile_"+name+"_"+i+"_"+j);
                                newTile.setAnimation(tileDescription(i,j)-1);
                                newTile.removeClass("sprite");
                                newTile.addClass("tileType_"+(tileDescription(i,j)-1));
                            }
                        }
            		}	
            	}
            } else if(typeof tileDescription == "object") {
            	for(var i=0; i<tileDescription.length; i++){
            		for(var j=0; j<tileDescription[0].length; j++){
                        if(tileDescription[i][j] != 0){
                            if($.isArray(animationList)){
                                // for many simple animation:
                                tileSet.addSprite("tile_"+name+"_"+i+"_"+j, 
                                                      {width: options.width, 
                                                       height: options.height, 
                                                       posx: j*options.width, 
                                                       posy: i*options.height, 
                                                       animation: animationList[tileDescription[i][j]-1]});
                                var newTile = $("#tile_"+name+"_"+i+"_"+j);
                                newTile.removeClass("sprite");
                                newTile.addClass("tileType_"+(tileDescription[i][j]-1));
                                newTile[0].gameQuery.animationNumber = tileDescription[i][j]-1;
                            } else {
                                // for multi-animation:
                                tileSet.addSprite("tile_"+name+"_"+i+"_"+j, 
                                                      {width: options.width, 
                                                       height: options.height, 
                                                       posx: j*options.width, 
                                                       posy: i*options.height, 
                                                       animation: animationList});
                                var newTile = $("#tile_"+name+"_"+i+"_"+j);
                                newTile.setAnimation(tileDescription[i][j]-1);
                                newTile.removeClass("active");
                                newTile.addClass("tileType_"+(tileDescription[i][j]-1));
                            }
                        }
            		}	
            	}
            }
            //Get the tileSet offset (relatif to the playground)
            var playgroundOffset = $.gameQuery.playground.offset();
            var tileSetOffset = tileSet.offset();
            tileSetOffset = {top: tileSetOffset.top - playgroundOffset.top, left: tileSetOffset.left - playgroundOffset.left};
            
            //actvates the visible tiles
            var firstRow = Math.max(Math.min(Math.floor(-tileSetOffset.top/options.height), options.sizey), 0);
            var lastRow = Math.max(Math.min(Math.ceil(($.gameQuery.playground[0].height-tileSetOffset.top)/options.height), options.sizey), 0);
            var firstColumn = Math.max(Math.min(Math.floor(-tileSetOffset.left/options.width), options.sizex), 0);
            var lastColumn = Math.max(Math.min(Math.ceil(($.gameQuery.playground[0].width-tileSetOffset.left)/options.width), options.sizex), 0);
            
            tileSet[0].gameQuery.firstRow = firstRow;
            tileSet[0].gameQuery.lastRow = lastRow;
            tileSet[0].gameQuery.firstColumn = firstColumn;
            tileSet[0].gameQuery.lastColumn = lastColumn;
            
            for(var i = firstRow; i < lastRow; i++){
                for(var j = firstColumn; j < lastColumn ; j++) {
                    $("#tile_"+name+"_"+i+"_"+j).toggleClass("active");
                }
            }
            return this.pushStack(tileSet);
        },
        
        /**
        * Changes the animation associated with a sprite.
        * WARNING: no check are made to ensure that the object is really a sprite
        * This is a non-desrtuctive call
        **/
        setAnimation: function(animation, callback) {
            var gameQuery = this[0].gameQuery;
            if(typeof animation == "number"){
                if(gameQuery.animation.type & $.gameQuery.ANIMATION_MULTI){
                    var distance = gameQuery.animation.distance * animation;
                    gameQuery.multi = distance;
                    if(gameQuery.animation.type & $.gameQuery.ANIMATION_VERTICAL) {
                       gameQuery.currentFrame = 0;
                        this.css("background-position",""+(-distance-gameQuery.animation.offsetx)+"px "+(-gameQuery.animation.offsety)+"px");
                    } else if(gameQuery.animation.type & $.gameQuery.ANIMATION_HORIZONTAL) {
                        gameQuery.currentFrame = 0;
                        this.css("background-position",""+(-gameQuery.animation.offsetx)+"px "+(-distance-gameQuery.animation.offsety)+"px");
                    }
                }
            } else {
                if(animation){
                    gameQuery.animation = animation;
                    gameQuery.currentFrame = 0;
                    this.css({"background-image": "url("+animation.imageURL+")", "background-position": ""+(-animation.offsetx)+"px "+(-animation.offsety)+"px"});
                    
                    if(gameQuery.animation.type & $.gameQuery.ANIMATION_VERTICAL) {
                        this.css("background-repeat", "repeat-x");
                    } else if(gameQuery.animation.type & $.gameQuery.ANIMATION_HORIZONTAL) {
                        this.css("background-repeat", "repeat-y");
                    } else {
                        this.css("background-repeat", "no-repeat");
                    }
                } else {
                    this.css("background-image", "");
                }
            }
            
            if(callback != undefined){
                this[0].gameQuery.callback = callback;	
            }
            
            return this;
        },
        
        /**
        * This function add the sound to the resourceManger for later use and associate it to the selected dom element(s).
        * This is a non-desrtuctive call
        **/
        addSound: function(sound, add) {
            // Does a SoundWrapper exists
            if($.gameQuery.SoundWrapper) {
                var gameQuery = this[0].gameQuery;
                // should we add to existing sounds ?
                if(add) {
                    // we do, have we some sound associated with 'this'?
                    var sounds = gameQuery.sounds;
                    if(sounds) {
                        // yes, we add it
                        sounds.push(sound);
                    } else {
                        // no, we create a new sound array
                        gameQuery.sounds = [sound];
                    }
                } else {
                    // no, we replace all sounds with this one
                    gameQuery.sounds = [sound];
                }
            }
            return this;
        },
        
        /**
        * This function plays the sound(s) associated with the selected dom element(s)
        * This is a non-desrtuctive call
        **/
        playSound: function() {
            $(this).each(function(){
                var gameQuery = this.gameQuery;
                if(gameQuery.sounds) {
                    for(var i = gameQuery.sounds.length-1 ; i >= 0; i --) {
                        gameQuery.sounds[i].play();
                    }
                }
            });
            
            return this;
        },
        
        /**
        * This function stops the sound(s) associated with the selected dom element(s) and rewind them
        * This is a non-desrtuctive call
        **/
        stopSound: function() {
            $(this).each(function(){
                var gameQuery = this.gameQuery;
                if(gameQuery.sounds) {
                    for(var i = gameQuery.sounds.length-1 ; i >= 0; i --) {
                        gameQuery.sounds[i].stop();
                    }
                }
            });
            return this;
        },
        
        /**
        * This function pauses the sound(s) associated with the selected dom element(s)
        * This is a non-desrtuctive call
        **/
        pauseSound: function() {
            $(this).each(function(){
                var gameQuery = this.gameQuery;
                if(gameQuery.sounds) {
                    for(var i = gameQuery.sounds.length-1 ; i >= 0; i --) {
                        gameQuery.sounds[i].pause();
                    }
                }
            });
            return this;
        },
        
        /**
        * this function mute or unmute the selected sound or all the sounds if none is specified
        **/
        muteSound: function(muted) {
            $(this).each(function(){
                var gameQuery = this.gameQuery;
                if(gameQuery.sounds) {
                    for(var i = gameQuery.sounds.length-1 ; i >= 0; i --) {
                        gameQuery.sounds[i].muted(muted);
                    }
                }
            });
        },
        
        /**
        * Register a callback to be trigered every "rate"
        * This is a non-desrtuctive call
        **/
        registerCallback: function(fn, rate) {
            $.gameQuery.resourceManager.registerCallback(fn, rate);
            return this;
        },
        
        /**
        * @DEPRECATED: use loadCallback() instead 
        * Set the id of the div to use as a loading bar while the games media are loaded during the preload.
        * If a callback function is given it will be called each time the loading progression changes with 
        * the precentage passed as unique argument.
        * This is a non-desrtuctive call
        **/
        setLoadBar: function(elementId, finalwidth, callback) {
            $.gameQuery.loadbar = {id: elementId, width: finalwidth, callback: callback};
            return this;
        },
        
        /**
         * This function retreive a list of object in collision with the subject:
         * - if 'this' is a sprite or a group, the function will retrieve the list of sprites (not groups) that touch it
         * - if 'this' is the playground, the function will return a list of all pair of collisioning elements. They are represented 
         *    by a jQuery object containing a series of paire. Each paire represents two object colliding.(not yet implemented)
         * For now all abject are considered to be boxes.
         * This IS a desrtuctive call and should be terminated with end() to go back one level up in the chaining
         **/
        collision: function(filter){
            var resultList = [];
            
            //retrieve 'this' offset by looking at the parents
            var itsParent = this[0].parentNode, offsetX = 0, offsetY = 0;
            while (itsParent != $.gameQuery.playground[0]){
                    if(itsParent.gameQuery){
                    offsetX += itsParent.gameQuery.posx;
                    offsetY += itsParent.gameQuery.posy;
                }
                itsParent = itsParent.parentNode;
            }
            
            // retrieve the gameQuery object
            var gameQuery = this[0].gameQuery;
            
            
            // retrieve the playground's absolute position and size information
            var pgdGeom = {top: 0, left: 0, bottom: $.playground().height(), right: $.playground().width()};
            
            // Does 'this' is inside the playground ?
            if( (gameQuery.boundingCircle.y + gameQuery.boundingCircle.radius + offsetY < pgdGeom.top)    || 
                (gameQuery.boundingCircle.x + gameQuery.boundingCircle.radius + offsetX < pgdGeom.left)   || 
                (gameQuery.boundingCircle.y - gameQuery.boundingCircle.radius + offsetY > pgdGeom.bottom) || 
                (gameQuery.boundingCircle.x - gameQuery.boundingCircle.radius + offsetX > pgdGeom.right)){
                return this.pushStack(new $([]));
            }
            
            if(this == $.gameQuery.playground){ 
                //TODO Code the "all against all" collision detection and find a nice way to return a list of pairs of elements
            } else {
                // we must find all the element that touches 'this'
                var elementsToCheck = new Array();
                elementsToCheck.push($.gameQuery.sceengraph.children(filter).get());
                elementsToCheck[0].offsetX = 0;
                elementsToCheck[0].offsetY = 0;
                
                for(var i = 0, len = elementsToCheck.length; i < len; i++) {
                    var subLen = elementsToCheck[i].length;
                    while(subLen--){
                        var elementToCheck = elementsToCheck[i][subLen];
                        // is it a gameQuery generated element?
                        if(elementToCheck.gameQuery){
                            // we don't want to check groups
                            if(!elementToCheck.gameQuery.group && !elementToCheck.gameQuery.tileSet){
                                // does it touches the selection?
                                if(this[0]!=elementToCheck){
                                    // check bounding circle collision
                                    // 1) distance between center:
                                    var distance = Math.sqrt(Math.pow(offsetY + gameQuery.boundingCircle.y - elementsToCheck[i].offsetY - elementToCheck.gameQuery.boundingCircle.y, 2) + Math.pow(offsetX + gameQuery.boundingCircle.x - elementsToCheck[i].offsetX - elementToCheck.gameQuery.boundingCircle.x, 2));
                                    if(distance - gameQuery.boundingCircle.radius - elementToCheck.gameQuery.boundingCircle.radius <= 0){
                                        // check real collision
                                        if($.gameQuery.collide(gameQuery, {x: offsetX, y: offsetY}, elementToCheck.gameQuery, {x: elementsToCheck[i].offsetX, y: elementsToCheck[i].offsetY})) {
                                            // add to the result list if collision detected
                                            resultList.push(elementsToCheck[i][subLen]);
                                        }
                                    }
                                }
                            }
                            // Add the children nodes to the list
                            var eleChildren = $(elementToCheck).children(filter);
                            if(eleChildren.length){
                                elementsToCheck.push(eleChildren.get());
                                elementsToCheck[len].offsetX = elementToCheck.gameQuery.posx + elementsToCheck[i].offsetX;
                                elementsToCheck[len].offsetY = elementToCheck.gameQuery.posy + elementsToCheck[i].offsetY;
                                len++;
                            }
                        }
                    }
                }
                return this.pushStack($(resultList));
            }
        },
        
        takeOffTransform: function(){
        	if(this.css("MozTransform")) {
                this.css("MozTransform", "");
            } 
            else if(this.css("-o-transform")){
                this.css("-o-transform", "");
            }
            else if(this.css("WebkitTransform")!==null && this.css("WebkitTransform")!==undefined) {
                this.css("WebkitTransform", "");
                
            } else if(this.css("filter")!==undefined){
                this.css("filter", "");
            }
            return this;
        },
        
        backToNormalTransform: function(factor, x, y){
        	if(this.css("MozTransform")) {
                var transform = "scale("+factor+")";
                
                this.css({
				    left: x,
				    top: y,
				    MozTransform: transform
				});
                
                
            } 
            else if(this.css("-o-transform")){
                var transform = "scale("+factor+")";
                this.css({
				    left: x,
				    top: y,
				    "-o-transform": transform
				});
            }
            else if(this.css("WebkitTransform")!==null && this.css("WebkitTransform")!==undefined) {
                //var transform = "scale("+factor+") translate("+x+"px, "+y+"px)";
                var transform = "scale("+factor+")";
                
                this.css({
                	WebkitTransform: transform,
				    left: x,
				    top: y
				});
				
                
                
                //this.css("WebkitTransform",transform);
                
            } else if(this.css("filter")!==undefined){
                var angle_rad = Math.PI * 2 / 360 * angle;
                // For ie from 5.5
                var cos = Math.cos(angle_rad) * factor;
                var sin = Math.sin(angle_rad) * factor;
                var previousWidth = this.width();
                var previousHeight = this.height();
                
                //this.css("filter","progid:DXImageTransform.Microsoft.Matrix(M11="+cos+",M12="+(-sin)+",M21="+sin+",M22="+cos+",SizingMethod='auto expand',FilterType='nearest neighbor')");
                
                
                
                
                var newWidth = this.width();
                var newHeight = this.height();
                
                
                //this.css("left", ""+(gameQuery.posx-(newWidth-previousWidth)/2)+"px");
                //this.css("top", ""+(gameQuery.posy-(newHeight-previousHeight)/2)+"px");
                
                
                this.css({
				    left: ""+(gameQuery.posx-(newWidth-previousWidth)/2)+"px",
				    top: ""+(gameQuery.posy-(newHeight-previousHeight)/2)+"px",
				    filter: "progid:DXImageTransform.Microsoft.Matrix(M11="+cos+",M12="+(-sin)+",M21="+sin+",M22="+cos+",SizingMethod='auto expand',FilterType='nearest neighbor')"
				});
            }
            return this;
        },
        
        transform2: function(factor, x, y) {
            
            
            if(this.css("MozTransform")) {
                // For firefox from 3.5
                //alert('firefox transform');
                var transform = "scale("+factor+")";
                this.css("MozTransform",transform);
            } 
            else if(this.css("-o-transform")){
            	//var center_corner_x=(parseInt(this.css("left"))+this.width())/2; //modifed by Victor
                //var center_corner_y=(parseInt(this.css("top"))+this.height())/2;  //modifed by Victor
                //alert('opera transform');
                var transform = "scale("+factor+")";
                //this.css("-o-transform-origin", "480 320"); //modifed by Victor
                this.css("-o-transform",transform);
            }
            else if(this.css("WebkitTransform")!==null && this.css("WebkitTransform")!==undefined) {
            	//alert('webkit transform');
                // For safari from 3.1 (and chrome)
                //var center_corner_x=(parseInt(this.css("left"))+this.width())/2; //modifed by Victor
                //var center_corner_y=(parseInt(this.css("top"))+this.height())/2;  //modifed by Victor
                var transform = "scale("+factor+") translate("+x+"px, "+y+"px)";
                //this.css("-webkit-transform-origin",center_corner_x+" "+center_corner_y); //modifed by Victor
                //var center_corner_x=parseFloat(this.offset().left)+(this.data("width")/2); //modifed by Victor
                //var center_corner_y=parseFloat(this.offset().top)+(this.data("height")/2); //modifed by Victor
                //this.css("-webkit-transform-origin",center_corner_x+" "+center_corner_y);
                this.css("WebkitTransform",transform);
                
            } else if(this.css("filter")!==undefined){
                var angle_rad = Math.PI * 2 / 360 * angle;
                // For ie from 5.5
                var cos = Math.cos(angle_rad) * factor;
                var sin = Math.sin(angle_rad) * factor;
                var previousWidth = this.width();
                var previousHeight = this.height();
                
                //var previousWidth = this.data("original_width");
                //var previousHeight = this.data("original_height");
                
                
                //this.css("-ms-filter","progid:DXImageTransform.Microsoft.Matrix(M11="+cos+",M12="+(-sin)+",M21="+sin+",M22="+cos+",SizingMethod='auto expand',FilterType='nearest neighbor')");
                this.css("filter","progid:DXImageTransform.Microsoft.Matrix(M11="+cos+",M12="+(-sin)+",M21="+sin+",M22="+cos+",SizingMethod='auto expand',FilterType='nearest neighbor')");
                //this.css("filter","alpha(opacity=50)");
                
                
                
                
                
                var newWidth = this.width();
                var newHeight = this.height();
                //this.css("width", this.data("original_width")*factor);
                //this.css("height", this.data("original_height")*factor);
                
                //this.css("left", ""+(gameQuery.posx-(newWidth-previousWidth)/2)+"px");
                //this.css("top", ""+(gameQuery.posy-(newHeight-previousHeight)/2)+"px");
                
                this.css("left", ""+(gameQuery.posx-(newWidth-previousWidth)/2)+"px");
                this.css("top", ""+(gameQuery.posy-(newHeight-previousHeight)/2)+"px");
            }
            return this;
        },
        
        
        /**
         * This is an internal function doing the combine action of rotate and scale
         * Both argument are mandatory. To get the values back use .rotate() or 
         * .scale()
         **/
        transform: function(angle, factor) {
            //var gameQuery = this[0].gameQuery;
            // Mark transformed and compute bounding box
            //$.gameQuery.update(gameQuery,{angle: angle, factor: factor});
            
            if(this.css("MozTransform")) {
                // For firefox from 3.5
                //alert('firefox transform');
                var transform = "scale("+factor+")";
                this.css("MozTransform",transform);
            } 
            else if(this.css("-o-transform")){
            	//var center_corner_x=(parseInt(this.css("left"))+this.width())/2; //modifed by Victor
                //var center_corner_y=(parseInt(this.css("top"))+this.height())/2;  //modifed by Victor
                //alert('opera transform');
                var transform = "scale("+factor+")";
                //this.css("-o-transform-origin", "480 320"); //modifed by Victor
                this.css("-o-transform",transform);
            }
            else if(this.css("WebkitTransform")!==null && this.css("WebkitTransform")!==undefined) {
            	//alert('webkit transform');
                // For safari from 3.1 (and chrome)
                //var center_corner_x=(parseInt(this.css("left"))+this.width())/2; //modifed by Victor
                //var center_corner_y=(parseInt(this.css("top"))+this.height())/2;  //modifed by Victor
                var transform = "scale("+factor+")";
                //this.css("-webkit-transform-origin",center_corner_x+" "+center_corner_y); //modifed by Victor
                //var center_corner_x=parseFloat(this.offset().left)+(this.data("width")/2); //modifed by Victor
                //var center_corner_y=parseFloat(this.offset().top)+(this.data("height")/2); //modifed by Victor
                //this.css("-webkit-transform-origin",center_corner_x+" "+center_corner_y);
                this.css("WebkitTransform",transform);
                
            } else if(this.css("filter")!==undefined){
                var angle_rad = Math.PI * 2 / 360 * angle;
                // For ie from 5.5
                var cos = Math.cos(angle_rad) * factor;
                var sin = Math.sin(angle_rad) * factor;
                var previousWidth = this.width();
                var previousHeight = this.height();
                
                //var previousWidth = this.data("original_width");
                //var previousHeight = this.data("original_height");
                
                
                //this.css("-ms-filter","progid:DXImageTransform.Microsoft.Matrix(M11="+cos+",M12="+(-sin)+",M21="+sin+",M22="+cos+",SizingMethod='auto expand',FilterType='nearest neighbor')");
                this.css("filter","progid:DXImageTransform.Microsoft.Matrix(M11="+cos+",M12="+(-sin)+",M21="+sin+",M22="+cos+",SizingMethod='auto expand',FilterType='nearest neighbor')");
                //this.css("filter","alpha(opacity=50)");
                
                
                
                
                
                var newWidth = this.width();
                var newHeight = this.height();
                //this.css("width", this.data("original_width")*factor);
                //this.css("height", this.data("original_height")*factor);
                
                //this.css("left", ""+(gameQuery.posx-(newWidth-previousWidth)/2)+"px");
                //this.css("top", ""+(gameQuery.posy-(newHeight-previousHeight)/2)+"px");
                
                this.css("left", ""+(gameQuery.posx-(newWidth-previousWidth)/2)+"px");
                this.css("top", ""+(gameQuery.posy-(newHeight-previousHeight)/2)+"px");
            }
            return this;
        },
        
        /**
         * This function rotates the selected element(s) clock-wise. The argument is a degree.
         **/
        rotate: function(angle){
            var gameQuery = this[0].gameQuery;
            
            if(angle !== undefined) {
                return this.transform(angle % 360, this.scale());
            } else {
                var ang = gameQuery.angle;
                return ang ? ang : 0;
            }
        },
        
        /**
         * This function change the scale of the selected element(s). The passed argument is a ratio: 
         * 1.0 = original size
         * 0.5 = half the original size
         * 2.0 = twice the original size
         **/
        scale: function(factor){
            //var gameQuery = this[0].gameQuery;
            return this.transform(0, factor);
            //if(factor !== undefined) {
            //    return this.transform(this.rotate(), factor);
            //} else {
            //    var fac = gameQuery.factor;
            //    return fac ? fac : 1;
            //}
        }
	});
    
	// This is an hijack to keep track of the change in the sprites, and group positions and size
	var oldCssFunction = $.fn.css;
	$.fn.css = function(key, value) {
        // This is the list of parameters that we watch for
        var sensibleKey = {left: true, top: true, height: true, width: true};
        
        // This is the function that takes care of warning $.gameQuery.update when a watched parameter
        // changes. If a function was passed instead of a fix value we call it first to compute the value.
        var testfunction = function (index, oldValue){
            var obj = {};
            var result;
            
            if(typeof value == "function"){
                result = value.apply(this,[index, oldValue]);
            } else {
                result = value;
            }
            
            obj[key]= result;
            $.gameQuery.update(this, obj);
            
            return result;
        };
        
        // This check for the kind of css() call
        if(typeof key == "object") {
            // Have we an object literal? Then we deserialize the call to the watched parameters
            for(sensible in sensibleKey){
                if(key[sensible]){
                    arguments.callee.apply(this,[sensible, key[sensible]]);
                    delete key[sensible];
                }
            }
            return oldCssFunction.apply(this,[key, value]);
        } else {
            // Otherwise we just interscept the call to watched parameters
            if(value && sensibleKey[key]){
                return oldCssFunction.apply(this,[key, testfunction]);
            }
            // or just call the standard css() function
            return oldCssFunction.apply(this,[key, value]);
        }
	};
	
})(jQuery);



//scrolling_images.js

// Global constants:
//var PLAYGROUND_WIDTH    = 960;
//var PLAYGROUND_HEIGHT   = 640;
//var PLAYGROUND_WIDTH    = 320;
//var PLAYGROUND_HEIGHT   = 480;
var PLAYGROUND_WIDTH    = 0;
var PLAYGROUND_HEIGHT   = 0;
//lines 1067 + 1069 had a *ratio added

var supported = false;

var images_base64 = new Array();
var timespots = new Array();
var relatedStoppables = new Array();
var globals = new Array();
var floats = new Array();
var event_element = new Array();
var event_type = new Array();
var event_type_function = new Array();
var running_timespots = new Array();
//var scanned_timespots = new Array();
//var nonexisting_bands = new Array();
var preloaded_timespots = new Array();
var preloading_timespots = new Array();
var pointerEvents = new Array();
var centreCamera = new Array();
var canvas = new Array();
var waitingTimespots = new Array();
var applications = new Array();
var applications_child_event = new Array();
var event_pro = new Array();
var applications_child_escape = new Array();
var restrains = new Array();
var restraint_groups = new Array();
var application_timespots = new Array();
var appAllBandsPreloadedTracer = new Array();
var appBandCheckedTracer = new Array();
var allAllBandTimespots = new Array();
var timespotImgsLoaded = new Array();
//var awaiting_bands = new Array();

var refresh_rate = 50;
var conn_err_wait = 2000;
var wait_time_for_ins_not_yet_arrived = 1000;  //in milliseconds
var debug_mode = false;
var checkItemLock = false;
var checkItemLock2 = false;
var lastBackPinch = false;
var preloadingItem = null;
var preloadingNotStarted = true;
var notFinishLoaded = true;
var coverLoaded = true;
var bringtofront_index = 10000000;
var full_screen_scale = 1000;
var canvas_size = 5000;
var cpuSpeed = 0;
var bandwidthSpeed = 0;
var bandwidthUsed = 0;

//var checkRenderCounter = 0;

//instruction codes
var ADD_MEDIA = 2;
var MOVE_LOCAL_REFERENCE = 3;
var COMPILE_ENTITY = 4;
var ADD_TO_ENTITY = 5;
var REMOVE_FROM_ENTITY = 6;
var CREATE_TIMESPOT = 8;
var END_TIMESPOT = 9;
var EXECUTE_TIMESPOT = 10;
var SLEEP = 11;
var SET_TOUCH_CONDITION = 12;
var REMOVE_CONDITION = 13;
var STOP_TIMESPOT = 14;
var SET_GLOBAL = 15;
var SET_GET_GLOBAL = 16;
var SET_GLOBAL_NULL = 17;
var EXECUTE_GLOBAL = 18;
var STOP_GLOBAL = 19;
var REMOVE_FROM_AND_ADD_TO_ENTITY = 20;
var BRING_TO_FRONT = 21;
var COMPILE_AND_ADD_ENTITY = 22;
var ADD_POLYGON = 23;
var CENTRE_CAMERA =24;
var IF_GREATER_THAN = 25;
var IF_LESS_THAN = 26;
var IF_EQUAL_TO = 27;
var FLOAT_PLUS = 28;
var FLOAT_MINUS = 29;
var SET_FLOAT = 30;
var SET_GET_FLOAT = 31;
var SET_KEY_PRESS = 34;
var SET_DRAG_CONDITION = 35;
var MEDIA_SET_CACHEABLE = 36;
var MEDIA_LOAD_PERCENTAGE = 37;
var MEDIA_LOAD_BYTES = 38;
var MEDIA_PLAY_SECONDS = 39;
var MEDIA_PLAY_BYTES = 40;
var MEDIA_PAUSE = 41;
var MEDIA_CANCEL_DOWNLOAD = 42;
var MEDIA_SET_PROGRESS_EVENT_PERCENTAGE = 43;
var MEDIA_SET_PROGRESS_EVENT_BYTES = 44;
var MEDIA_BUFFER_CRASH_AND_CONTINUE = 45;
var CLIPPING = 46;
var ADD_FORM = 47;
var ADD_FORM_COMPONENT = 48;
var GET_FORM_VALUES = 49;
var CENTER_SCREEN = 50;
var XY_CENTER = 51;
var SET_ESCAPE_CONDITION = 52;
var SET_RENDER_SIZE_CHANGE_EVENT_ON_BROWSER = 53;
var SET_RENDER_DEFINITION_BAND = 54;
var SET_RECEIVE_EVENT_FROM_CHILD_ON_BROWSER_EVENT = 55;
var SET_TOP_PRIORITY = 56;
var ADD_ACTIVE_DEEPREACH = 57;
var XY_CLICK_CENTER = 58;
var SET_ESCAPE_EVENT_ON_BROWSER_EVENT = 59;
var SET_EVENT_PROPAGATION = 60;
var GET_BINARY_FILE = 61;
var LOCAL_REFERNCE_ZOOM = 62;
var EXECUTE_FULLY = 1;
var EXECUTE_FREE = 2;

//media types
var media_jpg = 1;
var media_png = 2;
var media_gif = 3;
var media_flv = 4;
var media_mov = 5;
var media_mpg = 6;
var media_mp4 = 7;
var media_3gp = 8;
var media_avi = 9;
var media_wmv = 10;
var media_htmlcode = 12;

//priority types
var TOP_PRIORITY_CONTEXT = 1;
var TOP_PRIORITY_SERVER = 2;

//form component types
var form_input_text = 1;
var form_input_password = 2;
var form_textarea = 3;
var form_binary = 4;

//event types
var EVENT_TYPE_CLICK = 1;
var EVENT_TYPE_DRAG = 2;
var EVENT_TYPE_FORM_SUBMIT = 5;
var EVENT_TYPE_SWIPE_LEFT = 6;
var EVENT_TYPE_SWIPE_RIGHT = 7;
var EVENT_TYPE_SWIPE_UP = 8;
var EVENT_TYPE_SWIPE_DOWN = 9;
var EVENT_TYPE_BINARY_SUBMIT = 10;

//session info
var old_id = "";
var new_id = "";

//temporary fix for multiple clicks
var canClick = new Array();

var speedUp = true;

var allRequests = new Array();

var tempAccScales = new Array();
var bandCheckQueue = new Array();
var bandCheckQueue2 = new Array();
var preloadBandQueue = new Array();
var backStack = new Array();

var clickedChild_xy = new Array();

var bandChecking = false;
var camera = null;
var escapeItem = null;

//var allTimespotsString = "";


// Function to restart the game:
function restartgame(){
    window.location.reload();
};


function windowSizeChange2(){
	var the_playground = document.getElementById("playground");
	var p_width = the_playground.offsetWidth;
	var p_height = the_playground.offsetHeight;
	PLAYGROUND_WIDTH = p_width;
	PLAYGROUND_HEIGHT = p_height;
	//var ratio = (Math.min(p_width,p_height)/full_screen_scale);
	
	var backHeight = 0.0615*p_height>66?0.0615*p_height:66;
	var backWidth = (backHeight/33)*150;
	
	var ratio = (Math.min(p_width,p_height-backHeight)/full_screen_scale);
	
	
	
	
	$("div[name='group_0']").data("scale", ratio);
    $("div[name='group_0']").data("width",full_screen_scale*ratio);
    $("div[name='group_0']").data("height",full_screen_scale*ratio);
    $("div[name='group_0']").data("original_width",full_screen_scale);
    $("div[name='group_0']").data("original_height",full_screen_scale);
	
    
    
    
	var new_top = ((p_height-$("div[name='group_0']").data("height"))/2)+(backHeight/2);
    var new_left = (p_width-$("div[name='group_0']").data("width"))/2;
    
	$("div[name='group_0']").scale(ratio);
    $("div[name='group_0']").offset({top: new_top , left: new_left});
    
    $("div[id='back']").offset({top: new_top-backHeight , left: new_left });
    $("div[id='back']").scale(backHeight/33);
}
window.onresize = windowSizeChange2;

function windowSizeChange(){
    setPlaygroundSize(false);
};
function setPlaygroundSize(firsttime)
{
//    alert("window size change check = "+ firsttime);
    oldwidth = PLAYGROUND_WIDTH;
    oldHeight = PLAYGROUND_HEIGHT;
    if( typeof( window.innerWidth ) == 'number' )
    {
//        Non-IE
        PLAYGROUND_WIDTH = window.innerWidth;
        PLAYGROUND_HEIGHT = window.innerHeight;
    }
    else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) )
    {
//         IE 6+ in 'standards compliant mode'
        PLAYGROUND_WIDTH = document.documentElement.clientWidth;
        PLAYGROUND_HEIGHT = document.documentElement.clientHeight;
    }
    else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) )
    {
//         IE 4 compatible
        PLAYGROUND_WIDTH = document.body.clientWidth;
        PLAYGROUND_HEIGHT = document.body.clientHeight;
    }
    if(!(oldwidth == PLAYGROUND_WIDTH && oldHeight==PLAYGROUND_HEIGHT))
    {
        var ratio = (Math.min(PLAYGROUND_WIDTH,PLAYGROUND_HEIGHT)/full_screen_scale);
        //if(PLAYGROUND_HEIGHT -150 < PLAYGROUND_WIDTH)
        //    ratio = ratio*0.75;
        if(firsttime)
        {
            $("#playground").playground({height: PLAYGROUND_HEIGHT, width: PLAYGROUND_WIDTH});
            $.playground().addGroup("group_0", {width: full_screen_scale, height: full_screen_scale, preemptiveLocalReferenceId: 0});
            
            $("div[name='group_0']").css("left", (PLAYGROUND_WIDTH-full_screen_scale)/2);
            $("div[name='group_0']").css("top", (PLAYGROUND_HEIGHT-full_screen_scale)/2);
            
            
            
            $("div[name='group_0']").data("awaiting_imgs", 0);
            $("div[name='group_0']").scale(ratio);
            
            
            $("div[name='group_0']").css("visibility", "visible");
            //$("div[name='group_0']").css("clip", "rect(0px "+full_screen_scale+"px "+full_screen_scale+"px 0px)");
            $("div[name='group_0']").data("scale", ratio);
            $("div[name='group_0']").data("original_width",full_screen_scale);
		    $("div[name='group_0']").data("original_height",full_screen_scale);
            $("div[name='group_0']").data("width",full_screen_scale*ratio);
            $("div[name='group_0']").data("height",full_screen_scale*ratio);
        }
        else
        {
            $.playground().css("width", PLAYGROUND_WIDTH);
            $.playground().css("height", PLAYGROUND_HEIGHT);
            if($("div[name='group_0']"))
            {
            	$("div[name='group_0']").scale(ratio);
                
                var new_width = ratio*full_screen_scale;
                var new_x = (PLAYGROUND_WIDTH-new_width)/2;
                var new_y = (PLAYGROUND_HEIGHT-new_width)/2;
                $("div[name='group_0']").offset({top: new_y , left: new_x});
            }
        }
    }
};
// --------------------------------------------------------------------------------------------------------------------
// --                                      the main declaration:                                                     --
// --------------------------------------------------------------------------------------------------------------------
$(function()
{
//    Initialize the application:
    //setPlaygroundSize(true);
    // this sets the id of the loading bar:
    //$().setLoadBar("loadingBar", 400);
    //initialize the start button
    //$("#startbutton").click(function()
    //{
    	$("#playground").playground();
    	
    	//These lines for adultspace only
    	/*$("#playground").css("height", "90%");
    	$("#playground").css("bottom", "0px");
    	$('body').append('<div id="main_site" style="font-size:2em;background-color:#ffffff;position:absolute;top:0px;width:100%;height:10%;text-align:center"><a href="http://mobile2.adultspace.co.nz">Click here to go back to adultspace.co.nz</a></div>');
    	setTimeout(function(){
    		$("#main_site").fadeTo(1000,0,function(){
	    		$(this).remove();
			});
			$("#playground").css("height", "100%");
    	    $("#playground").css("bottom", "");
    	    $(window).trigger('onresize');
    	}, 10000); */
    	//end for adultspace
    	
    	
    	
        $.playground().startGame(function()
        {
        	var notSupportedMessage = "Sorry! The demo currently does not support your browser, please try Apple / Samsung mobile based browsers";
        	
        	var supports = (function() {  
				var div = document.createElement('div');
				//var vendors = 'Khtml Ms O Moz Webkit'.split(' ');
				//var vendors = 'O Moz Webkit'.split(' ');
				//var vendors = 'Moz Webkit'.split(' ');
				var vendors = 'Webkit'.split(' ');
				var len = vendors.length;
			    return function(prop) {  
			    	//check if its mobile browser
	        		var appVersion = navigator.appVersion.toLowerCase();
	        		
	        		//dont load app if its not webkit based browsers
	        		if(appVersion.indexOf("applewebkit")==-1){
	        			return false;
	        		}
	        		var appVersionSplit = appVersion.split(" ");
	        		for(var i=0; i<appVersionSplit.length; i++){
	        			if(appVersionSplit[i].indexOf("applewebkit")!=-1){
	        				var webkitVersion = appVersionSplit[i].substring(12, appVersionSplit[i].length-1);
	        				var firstDotIndex = webkitVersion.indexOf(".");
	        				if(firstDotIndex!=-1){
	        					webkitVersion = webkitVersion.substring(0, firstDotIndex);
	        				}
	        				if(!isNaN(webkitVersion)){
	        					webkitVersion = parseFloat(webkitVersion);
	        					if(webkitVersion>=534 && webkitVersion<536){
	        					//if(webkitVersion<536){
	        						//notSupportedMessage = "Your browser is out of date, this demo is currently not supported.  Please try to download the latest version of Google Chrome, press ok and you will be redirected";
	        						notSupportedMessage = "Your browser is out of date, this demo is currently not supported.  Please try to download the latest version of Google Chrome";
	        						speedUp = false;
	        						//return true;
	        						/*if(appVersion.indexOf("android")!=-1){
	        							alert(notSupportedMessage);
	        						    window.location = "https://play.google.com/store/apps/details?id=com.android.chrome&hl=en";
        						    }
	        						return false;*/
	        					}
	        				}
	        				break;
	        			}
	        		}
	        		
					if(!(appVersion.indexOf("mobile")>-1 || appVersion.indexOf("tablet")>-1)){
					    //return false;
					}
					
					
			        if (prop in div.style) return true;  
			        prop = prop.replace(/^[a-z]/, function(val) {  
			           return val.toUpperCase();  
			        });  
			        while(len--) {  
			           if ( vendors[len] + prop in div.style ) {  
			              // browser supports box-shadow. Do what you need.  
			              // Or use a bang (!) to test if the browser doesn't.  
			              $(div).remove();
			              return true;  
			           }
			        }
			        $(div).remove();
			        return false;  
			     };  
			})();
			
			supported = supports('Transform');
			
			
			
			new_id = Math.floor(Math.random()*9223372036854775807);
        	
			
			//get info
			//setTimeout(function(){new getInstruction("getInfo");}, refresh_rate);
			
        	
        	
			if (!supported) {
				//setTimeout(function(){new getInstruction("getInfo2");}, 0);
				getInstruction("getInfo2");
			    alert(notSupportedMessage);
			    var the_playground = document.getElementById("playground");
			    $(the_playground).remove();
			    return;
			}
			else{
				//get info
			    setTimeout(function(){new getInstruction("getInfo");}, 0);
			}
        	
        	
        	//var the_playground = document.getElementById("playground");
        	//var p_width = the_playground.offsetWidth;
        	//var p_height = the_playground.offsetHeight;
        	//PLAYGROUND_WIDTH = p_width;
        	//PLAYGROUND_HEIGHT = p_height;
        	//var ratio = (Math.min(p_width,p_height)/full_screen_scale);
        	
        	//var backHeight = 0.0615*p_height;
        	//var backWidth = (backHeight/33)*150;
        	
        	//var ratio = (Math.min(p_width,p_height-backHeight)/full_screen_scale);
        	
        	//var screenDifference = Math.abs(p_width-p_height);
        	//if(screenDifference<300){
        	//	ratio = (Math.min(p_width+screenDifference-300,p_height+screenDifference-300)/full_screen_scale);
        	//}
        	
        	$.playground().addGroup("group_0", {width: full_screen_scale, height: full_screen_scale, preemptiveLocalReferenceId: 0});
        	
        	
        	
        	
        	
        	
        	//$("div[name='group_1']").css("clip", "");
        	
        	
        	//$("div[name='group_0']").css("left", (p_width-full_screen_scale)/2);
        	//$("div[name='group_0']").css("top", (p_height-full_screen_scale)/2);
        	
        	
        	$("div[name='group_0']").data("awaiting_imgs", 0);
        	$("div[name='group_0']").css("pointer-events", "visible");
        	
        	windowSizeChange2();
        	windowSizeChange2();
        	
        	$("div[id='back']").bind("click", function(e){
            	e.preventDefault();
            	e.stopPropagation();
                if(backStack.length>0){
                	//(backStack.pop())();
                	backStack.pop().execBack();
                }
            });
        	
        	
        	//$("div[name='group_0']").data("scale", ratio);
            //$("div[name='group_0']").data("width",full_screen_scale*ratio);
            //$("div[name='group_0']").data("height",full_screen_scale*ratio);
            //$("div[name='group_0']").data("original_width",full_screen_scale);
		    //$("div[name='group_0']").data("original_height",full_screen_scale);
        	
		    
		    
		    
        	//var new_top = ((p_height-$("div[name='group_0']").data("height"))/2)+(backHeight/2);
            //var new_left = (p_width-$("div[name='group_0']").data("width"))/2;
		    
        	//$("div[name='group_0']").scale(ratio);
            //$("div[name='group_0']").offset({top: new_top , left: new_left});
            
            //$("div[id='back']").offset({top: new_top-backHeight+((backHeight-33)/2) , left: new_left+((backWidth-150)/2) });
            //$("div[id='back']").scale(backHeight/33);
            
            
            
            
		    var timeAllowed = 1000;
		    var start = new Date().getTime();
		    var counter = 0;
		    var testTimer = setInterval(function(){
		    	 var elapsed = new Date().getTime()-start;
				 if(elapsed<timeAllowed){
				 	counter++;
				 }
				 else{
				 	clearInterval(testTimer);
				 	cpuSpeed = counter;
				 }
		    }, 0);
            
            setTimeout(function(){
            	 if(document.title.indexOf("Pelican")!=-1){
            	     document.getElementById('group_0').innerHTML = "<div id='cover' style='position:absolute;bottom:0px;right:0px;width:40%;height:40%'><img src='data:image/jpeg;base64,/9j/4QthRXhpZgAATU0AKgAAAAgADAEAAAMAAAABBkAAAAEBAAMAAAABBLAAAAECAAMAAAADAAAAngEGAAMAAAABAAIAAAESAAMAAAABAAEAAAEVAAMAAAABAAMAAAEaAAUAAAABAAAApAEbAAUAAAABAAAArAEoAAMAAAABAAIAAAExAAIAAAAcAAAAtAEyAAIAAAAUAAAA0IdpAAQAAAABAAAA5AAAARwACAAIAAgACvyAAAAnEAAK/IAAACcQQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzADIwMTI6MDg6MTQgMTA6NDg6MjEAAASQAAAHAAAABDAyMjGgAQADAAAAAf//AACgAgAEAAAAAQAAAkigAwAEAAAAAQAAAmoAAAAAAAAABgEDAAMAAAABAAYAAAEaAAUAAAABAAABagEbAAUAAAABAAABcgEoAAMAAAABAAIAAAIBAAQAAAABAAABegICAAQAAAABAAAJ3wAAAAAAAABIAAAAAQAAAEgAAAAB/9j/7QAMQWRvYmVfQ00AAv/uAA5BZG9iZQBkgAAAAAH/2wCEAAwICAgJCAwJCQwRCwoLERUPDAwPFRgTExUTExgRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBDQsLDQ4NEA4OEBQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAKAAlwMBIgACEQEDEQH/3QAEAAr/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/APKkkkklKSSSSUpJJJJSkkkklKSSSSUlxsa3Js9OsDQFznOMNa0fSfY8/RYtLG6P0q4Ma/qgZa4wYoeWAkhv8641u2/9aVRlrq8P0GaesfUt8wz+bZ/V3e5TcAxlbW6kglx8T3SU3uufU3q/RqPtj/TzMCQ05mK7exriYDLmubXfQ7/jatiwl6R9WerEYrG3RZRZFOVU8bmva79E4PY76W5q4j6xdNZ0vrWXg1maqnzUf+DeBbT/AOBvakpzkkkklKSSSSUpJJJJT//Q8qSSSSUpJJJJSkkkklKSSSSUpJJJJSVzo08GgfhKMLA41idWN4+Kr3fT8iB+QKxQG3ZR9MQ3nXwASU9J0Kzbj2MPba4feFj/AFttNv1gynHt6bP8yuuv/vq1ums9MtPZ8NP+duauf6zb63VMm2Z32Og+QO1v/UpKaSSSSSlJJJJKUkkkkp//0fKkkkklKSSSSUpJJJJSkkkklKTgSD4pkklNunBy8moW1V7q2+wuJa2SPzWby3e7+oreLXXQ5tYHvfq9x5gcMUcy0+jiUN0rqoY4AfvPHqvd/nOQrL3G5pnUNEnzJSU77XBrWR+bY0fgFgdRwrGB2WLBYx1rmWAaOY6S5rXN/de36D2rYLwaZHIs1+Oiyc2xzWZbT/hbwB/Z3O/8ikpzkkkklKSSSSUpJJJJT//S8qSSSSUpJJJJSkkkklKSSSSUpP2ShMdUlN/KB31t7iqof9BqFWBZkAHidfgEXJI+0vPZmn+aNiDjkNM93H8AElOux5+zF3i8n8Vk9Qs3PZ/KBf8A5x/8i1qt237MMNn3EiB8SqGX/O/ABv3AJKQpJQkQkpSSSSSlJJJJKf/T8qSSSSUpJOGOPAS2EcpKWSTwEklKSThpdwPmnljOPc7x7BJS2xxEnQeJRcKoW5TG/mt97z/JYN7v+pQS5zjJMlW8b9Fi3Wfn3H0m/wBUQ+3/ANFtSUwscS1zjzY6f4qPER2SsPuj93RRcfaPNJSa/RlQ8Wh0eRJj/qUPJ/nD8f4KWQCPQd2fU2P7JLP++qOR/On4/wAElIY4Cdw007J/zkw4PmkpikkkkpSSSSSn/9TypJPt8wn2eYSUyNocBunTwTSzwS9L+UPvS9F/aD8CkpUsUmNDjDS0HzQ3Me3kQmSUlvpvrj1BoeCOEJXsS031uxLNXEH0ifEfmphjhldZaNznsDjpPJhJTUra4uECT2Hmrl0MIrBltIj4u/OP+epnZR7iZt4Hkqj7N35SkpiTrqmJ1CSikpu0tOThvqALraDvpA1JDiG2M/78iDB9TFtybQ6o4+1rw8FsucY2N3fnqPTqbntvdUJLKnOcZiAPztyBkZOZktb9pvsuDPoCx7nR/V3kpKQPY5joPxHwTdlbwqX5T2451Bktnkf1SgX0PpudU/6TTCSmLBIPnooubtMIg0TEB2p5SUjhJTDUklP/1fKkkkoKSlJJJJKZNte3g6eB1RAaLNH/AKN3iOEFJJSd1FtUWNMgate1bPTMj1HV3MDfWY6dp4JP0mH+RasSjIfSdNWn6TTwVcqH/ajE1A+nV3CSnQycfoDLC6Mmq0kk4jgNoJ/M9f6fprLvxmOtcansbOoY47Y/qrXOXRmY7RkM3uaIbYNHj+Q/95Zd9TA87Dvb2J0P9pJSJvTc9/0aiR+9I2/9uTsUmYWO136xl1sA5FYNjv8AojZ/00M1j5eCW0hJTcuz6GYjsHp9bqqbSDkXWQbLNurGe321Utd7tioHiApEKVVTnu2tEuKSm1054x3m88MHKqX3G+w2u5KfKuYGjHqO5rTL3DufL+Sq+oGqSlyU26FElIFJTPeUlGSkkp//1vKoPgknD3Dgqbb3DR7WvH8ofxSUwD3Dv96mH1u0ez5t/uRWjCt0JNDvvamswrGDcCHMPDhwkpgaJbvqO9veOR8QhKe2xh3CQR3HKnvrt0t9r+1g/wC/hJSIcKdVtlLxZWYcE1lVlRkjQ8OGoPzUQQeUlOvRk4+Tq2Krj9Jn5rk1tMmR9yySCDI7IzMy1ohx3BJTa9Jvcx8U4xgeHhVjmE+Kb7Wf3UlNo4rW6l0+TQhW+qWljAKqz9LxP9ZyCcu3sAEJ9tj/AKRlJS5DGcHcfwUSZTJJKUkBOiQEqUQIHPcpKW04H3pJapJKf//X8qSSSSUpEpyLaj7HQDyOQfi1DSSU3BZjXD3TQ/8Aeb7mH4t+kxRdgZBG6sC5v71Z3fkVUEjhEZZBng/vDQ/gkpIw20ywgweWOGn3FRLa3ahu34FHZm5MQLnEeDwH/wDVIgyMk/m1P+NYH/UpKaZY6IHCj6T/AAWiLbj9LFrd8AQpC4j/ALSAfMpKcwU2eCm3Eud+bAWh9peOKA1QL8u3QMPySU0n45rHud8kIjwV1+ORrfY1nlMn/NCTPszT+jYbD+87QfckpqV0W2fQaT5qwMENE2vA8gjG90Q2IHyaFWtvE6e93iePkkpk5uMwaAnzKGbax9FqEXFxkmSmSUzNuv0QkoJJKf/Q8qSSSSUpJJJJSkkkklLtdtPirVOe2vmkO+JKqJJKdRvWw0aYrP8AOKd3XXEaY1YPmXFZSSSm6/q2U76Oyv8AqtH/AH7cgvycm36driPMwEBJJSZra26vdPkE7shoENE/HQICSSmT7Xv+kdPDsopJJKUkkkkpSSSSSn//2f/tEjhQaG90b3Nob3AgMy4wADhCSU0EBAAAAAAADxwBWgADGyVHHAIAAAIAAAA4QklNBCUAAAAAABDNz/p9qMe+CQVwdq6vBcNOOEJJTQQ6AAAAAACTAAAAEAAAAAEAAAAAAAtwcmludE91dHB1dAAAAAUAAAAAQ2xyU2VudW0AAAAAQ2xyUwAAAABSR0JDAAAAAEludGVlbnVtAAAAAEludGUAAAAAQ2xybQAAAABNcEJsYm9vbAEAAAAPcHJpbnRTaXh0ZWVuQml0Ym9vbAAAAAALcHJpbnRlck5hbWVURVhUAAAAAQAAADhCSU0EOwAAAAABsgAAABAAAAABAAAAAAAScHJpbnRPdXRwdXRPcHRpb25zAAAAEgAAAABDcHRuYm9vbAAAAAAAQ2xicmJvb2wAAAAAAFJnc01ib29sAAAAAABDcm5DYm9vbAAAAAAAQ250Q2Jvb2wAAAAAAExibHNib29sAAAAAABOZ3R2Ym9vbAAAAAAARW1sRGJvb2wAAAAAAEludHJib29sAAAAAABCY2tnT2JqYwAAAAEAAAAAAABSR0JDAAAAAwAAAABSZCAgZG91YkBv4AAAAAAAAAAAAEdybiBkb3ViQG/gAAAAAAAAAAAAQmwgIGRvdWJAb+AAAAAAAAAAAABCcmRUVW50RiNSbHQAAAAAAAAAAAAAAABCbGQgVW50RiNSbHQAAAAAAAAAAAAAAABSc2x0VW50RiNQeGxAUgAAAAAAAAAAAAp2ZWN0b3JEYXRhYm9vbAEAAAAAUGdQc2VudW0AAAAAUGdQcwAAAABQZ1BDAAAAAExlZnRVbnRGI1JsdAAAAAAAAAAAAAAAAFRvcCBVbnRGI1JsdAAAAAAAAAAAAAAAAFNjbCBVbnRGI1ByY0BZAAAAAAAAOEJJTQPtAAAAAAAQAEgAAAABAAIASAAAAAEAAjhCSU0EJgAAAAAADgAAAAAAAAAAAAA/gAAAOEJJTQQNAAAAAAAEAAAAHjhCSU0EGQAAAAAABAAAAB44QklNA/MAAAAAAAkAAAAAAAAAAAEAOEJJTScQAAAAAAAKAAEAAAAAAAAAAjhCSU0D9QAAAAAASAAvZmYAAQBsZmYABgAAAAAAAQAvZmYAAQChmZoABgAAAAAAAQAyAAAAAQBaAAAABgAAAAAAAQA1AAAAAQAtAAAABgAAAAAAAThCSU0D+AAAAAAAcAAA/////////////////////////////wPoAAAAAP////////////////////////////8D6AAAAAD/////////////////////////////A+gAAAAA/////////////////////////////wPoAAA4QklNBAgAAAAAABAAAAABAAACQAAAAkAAAAAAOEJJTQQeAAAAAAAEAAAAADhCSU0EGgAAAAADUwAAAAYAAAAAAAAAAAAAAmoAAAJIAAAADwBnAGkAcgBsAF8AbwBuAF8AZABhAHIAawBfAG0AcQAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAACSAAAAmoAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAQAAAAAAAG51bGwAAAACAAAABmJvdW5kc09iamMAAAABAAAAAAAAUmN0MQAAAAQAAAAAVG9wIGxvbmcAAAAAAAAAAExlZnRsb25nAAAAAAAAAABCdG9tbG9uZwAAAmoAAAAAUmdodGxvbmcAAAJIAAAABnNsaWNlc1ZsTHMAAAABT2JqYwAAAAEAAAAAAAVzbGljZQAAABIAAAAHc2xpY2VJRGxvbmcAAAAAAAAAB2dyb3VwSURsb25nAAAAAAAAAAZvcmlnaW5lbnVtAAAADEVTbGljZU9yaWdpbgAAAA1hdXRvR2VuZXJhdGVkAAAAAFR5cGVlbnVtAAAACkVTbGljZVR5cGUAAAAASW1nIAAAAAZib3VuZHNPYmpjAAAAAQAAAAAAAFJjdDEAAAAEAAAAAFRvcCBsb25nAAAAAAAAAABMZWZ0bG9uZwAAAAAAAAAAQnRvbWxvbmcAAAJqAAAAAFJnaHRsb25nAAACSAAAAAN1cmxURVhUAAAAAQAAAAAAAG51bGxURVhUAAAAAQAAAAAAAE1zZ2VURVhUAAAAAQAAAAAABmFsdFRhZ1RFWFQAAAABAAAAAAAOY2VsbFRleHRJc0hUTUxib29sAQAAAAhjZWxsVGV4dFRFWFQAAAABAAAAAAAJaG9yekFsaWduZW51bQAAAA9FU2xpY2VIb3J6QWxpZ24AAAAHZGVmYXVsdAAAAAl2ZXJ0QWxpZ25lbnVtAAAAD0VTbGljZVZlcnRBbGlnbgAAAAdkZWZhdWx0AAAAC2JnQ29sb3JUeXBlZW51bQAAABFFU2xpY2VCR0NvbG9yVHlwZQAAAABOb25lAAAACXRvcE91dHNldGxvbmcAAAAAAAAACmxlZnRPdXRzZXRsb25nAAAAAAAAAAxib3R0b21PdXRzZXRsb25nAAAAAAAAAAtyaWdodE91dHNldGxvbmcAAAAAADhCSU0EKAAAAAAADAAAAAI/8AAAAAAAADhCSU0EEQAAAAAAAQEAOEJJTQQUAAAAAAAEAAAAAThCSU0EDAAAAAAJ+wAAAAEAAACXAAAAoAAAAcgAAR0AAAAJ3wAYAAH/2P/tAAxBZG9iZV9DTQAC/+4ADkFkb2JlAGSAAAAAAf/bAIQADAgICAkIDAkJDBELCgsRFQ8MDA8VGBMTFRMTGBEMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAENCwsNDg0QDg4QFA4ODhQUDg4ODhQRDAwMDAwREQwMDAwMDBEMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgAoACXAwEiAAIRAQMRAf/dAAQACv/EAT8AAAEFAQEBAQEBAAAAAAAAAAMAAQIEBQYHCAkKCwEAAQUBAQEBAQEAAAAAAAAAAQACAwQFBgcICQoLEAABBAEDAgQCBQcGCAUDDDMBAAIRAwQhEjEFQVFhEyJxgTIGFJGhsUIjJBVSwWIzNHKC0UMHJZJT8OHxY3M1FqKygyZEk1RkRcKjdDYX0lXiZfKzhMPTdePzRieUpIW0lcTU5PSltcXV5fVWZnaGlqa2xtbm9jdHV2d3h5ent8fX5/cRAAICAQIEBAMEBQYHBwYFNQEAAhEDITESBEFRYXEiEwUygZEUobFCI8FS0fAzJGLhcoKSQ1MVY3M08SUGFqKygwcmNcLSRJNUoxdkRVU2dGXi8rOEw9N14/NGlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vYnN0dXZ3eHl6e3x//aAAwDAQACEQMRAD8A8qSSSSUpJJJJSkkkklKSSSSUpJJJJSXGxrcmz06wNAXOc4w1rR9J9jz9Fi0sbo/Srgxr+qBlrjBih5YCSG/zrjW7b/1pVGWurw/QZp6x9S3zDP5tn9Xd7lNwDGVtbqSCXHxPdJTe659Ter9Go+2P9PMwJDTmYrt7GuJgMua5td9Dv+Nq2LCXpH1Z6sRisbdFlFkU5VTxua9rv0Tg9jvpbmriPrF01nS+tZeDWZqqfNR/4N4FtP8A4G9qSnOSSSSUpJJJJSkkkklP/9DypJJJJSkkkklKSSSSUpJJJJSkkkklJXOjTwaB+EowsDjWJ1Y3j4qvd9PyIH5ArFAbdlH0xDedfABJT0nQrNuPYw9trh94WP8AW202/WDKce3ps/zK66/++rW6az0y09nw0/525q5/rNvrdUybZnfY6D5A7W/9SkppJJJJKUkkkkpSSSSSn//R8qSSSSUpJJJJSkkkklKSSSSUpOBIPimSSU26cHLyahbVXurb7C4lrZI/NZvLd7v6it4tddDm1ge9+r3HmBwxRzLT6OJQ3SuqhjgB+88eq93+c5CsvcbmmdQ0SfMlJTvtcGtZH5tjR+AWB1HCsYHZYsFjHWuZYBo5jpLmtc3917foPatgvBpkcizX46LJzbHNZltP+FvAH9nc7/yKSnOSSSSUpJJJJSkkkklP/9LypJJJJSkkkklKSSSSUpJJJJSk/ZKEx1SU38oHfW3uKqh/0GoVYFmQAeJ1+ARckj7S89maf5o2IOOQ0z3cfwASU67Hn7MXeLyfxWT1Czc9n8oF/wDnH/yLWq3bfsww2fcSIHxKoZf878AG/cAkpCklCRCSlJJJJKUkkkkp/9PypJJJJSkk4Y48BLYRykpZJPASSUpJOGl3A+aeWM49zvHsElLbHESdB4lFwqhblMb+a33vP8lg3u/6lBLnOMkyVbxv0WLdZ+fcfSb/AFRD7f8A0W1JTCxxLXOPNjp/io8RHZKw+6P3dFFx9o80lJr9GVDxaHR5EmP+pQ8n+cPx/gpZAI9B3Z9TY/sks/76o5H86fj/AASUhjgJ3DTTsn/OTDg+aSmKSSSSlJJJJKf/1PKkk+3zCfZ5hJTI2hwG6dPBNLPBL0v5Q+9L0X9oPwKSlSxSY0OMNLQfNDcx7eRCZJSW+m+uPUGh4I4QlexLTfW7Es1cQfSJ8R+amGOGV1lo3OewOOk8mElNStri4QJPYeauXQwisGW0iPi784/56mdlHuJm3geSqPs3flKSmJOuqYnUJKKSm7S05OG+oAutoO+kDUkOIbYz/vyIMH1MW3JtDqjj7WvDwWy5xjY3d+eo9Opue291Qksqc5xmIA/O3IGRk5mS1v2m+y4M+gLHudH9XeSkpA9jmOg/EfBN2VvCpflPbjnUGS2eR/VKBfQ+m51T/pNMJKYsEg+eii5u0wiDRMQHanlJSOElMNSSU//V8qSSSgpKUkkkkpk217eDp4HVEBos0f8Ao3eI4QUklJ3UW1RY0yBq17Vs9MyPUdXcwN9Zjp2ngk/SYf5FqxKMh9J01afpNPBVyof9qMTUD6dXcJKdDJx+gMsLoyarSSTiOA2gn8z1/p+msu/GY61xqexs6hjjtj+qtc5dGZjtGQze5ohtg0eP5D/3ll31MDzsO9vYnQ/2klIm9Nz3/RqJH70jb/25OxSZhY7XfrGXWwDkVg2O/wCiNn/TQzWPl4JbSElNy7PoZiOwen1uqptIORdZBss26sZ7fbVS13u2KgeICkQpVVOe7a0S4pKbXTnjHebzwwcqpfcb7Da7kp8q5gaMeo7mtMvcO58v5Kr6gapKXJTboUSUgUlM95SUZKSSn//W8qg+CScPcOCptvcNHta8fyh/FJTAPcO/3qYfW7R7Pm3+5FaMK3Qk0O+9qazCsYNwIcw8OHCSmBolu+o72945HxCEp7bGHcJBHccqe+u3S32v7WD/AL+ElIhwp1W2UvFlZhwTWVWVGSNDw4ag/NRBB5SU69GTj5OrYquP0mfmuTW0yZH3LJIIMjsjMzLWiHHcElNr0m9zHxTjGB4eFWOYT4pvtZ/dSU2jitbqXT5NCFb6paWMAqrP0vE/1nIJy7ewAQn22P8ApGUlLkMZwdx/BRJlMkkpSQE6JASpRAgc9ykpbTgfeklqkkp//9fypJJJJSkSnItqPsdAPI5B+LUNJJTcFmNcPdND/wB5vuYfi36TFF2BkEbqwLm/vVnd+RVQSOERlkGeD+8ND+CSkjDbTLCDB5Y4afcVEtrdqG7fgUdmbkxAucR4PAf/ANUiDIyT+bU/41gf9SkppljogcKPpP8ABaItuP0sWt3wBCkLiP8AtIB8ykpzBTZ4KbcS535sBaH2l44oDVAvy7dAw/JJTSfjmse53yQiPBXX45Gt9jWeUyf80JM+zNP6NhsP7ztB9ySmpXRbZ9BpPmrAwQ0Ta8DyCMb3RDYgfJoVa28Tp73eJ4+SSmTm4zBoCfMoZtrH0WoRcXGSZKZJTM26/RCSgkkp/9DypJJJJSkkkklKSSSSUu120+KtU57a+aQ74kqokkp1G9bDRpis/wA4p3ddcRpjVg+ZcVlJJKbr+rZTvo7K/wCq0f8AftyC/Jybfp2uI8zAQEklJmtrbq90+QTuyGgQ0T8dAgJJKZPte/6R08OyikkkpSSSSSlJJJJKf//ZADhCSU0EIQAAAAAAVQAAAAEBAAAADwBBAGQAbwBiAGUAIABQAGgAbwB0AG8AcwBoAG8AcAAAABMAQQBkAG8AYgBlACAAUABoAG8AdABvAHMAaABvAHAAIABDAFMANQAAAAEAOEJJTQQGAAAAAAAH//8AAAABAQD/4REfaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOmNycz0iaHR0cDovL25zLmFkb2JlLmNvbS9jYW1lcmEtcmF3LXNldHRpbmdzLzEuMC8iIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiBjcnM6QWxyZWFkeUFwcGxpZWQ9IlRydWUiIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHhtcDpDcmVhdGVEYXRlPSIyMDEyLTA4LTE0VDA5OjA4KzEyOjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAxMi0wOC0xNFQxMDo0ODoyMSsxMjowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxMi0wOC0xNFQxMDo0ODoyMSsxMjowMCIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1IFdpbmRvd3MiIGRjOmZvcm1hdD0iaW1hZ2UvanBlZyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1OEQ0NjNFMTk4RTVFMTExQkE5REZDOThGMUZEODg2NiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1M0Q0NjNFMTk4RTVFMTExQkE5REZDOThGMUZEODg2NiIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjUzRDQ2M0UxOThFNUUxMTFCQTlERkM5OEYxRkQ4ODY2Ij4gPHhtcE1NOkhpc3Rvcnk+IDxyZGY6U2VxPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6NTNENDYzRTE5OEU1RTExMUJBOURGQzk4RjFGRDg4NjYiIHN0RXZ0OndoZW49IjIwMTItMDgtMTRUMTA6NDc6MzcrMTI6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6NTRENDYzRTE5OEU1RTExMUJBOURGQzk4RjFGRDg4NjYiIHN0RXZ0OndoZW49IjIwMTItMDgtMTRUMTA6NDc6MzcrMTI6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6NTVENDYzRTE5OEU1RTExMUJBOURGQzk4RjFGRDg4NjYiIHN0RXZ0OndoZW49IjIwMTItMDgtMTRUMTA6NDc6NTArMTI6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6NTZENDYzRTE5OEU1RTExMUJBOURGQzk4RjFGRDg4NjYiIHN0RXZ0OndoZW49IjIwMTItMDgtMTRUMTA6NDc6NTArMTI6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6NTdENDYzRTE5OEU1RTExMUJBOURGQzk4RjFGRDg4NjYiIHN0RXZ0OndoZW49IjIwMTItMDgtMTRUMTA6NDg6MjErMTI6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6NThENDYzRTE5OEU1RTExMUJBOURGQzk4RjFGRDg4NjYiIHN0RXZ0OndoZW49IjIwMTItMDgtMTRUMTA6NDg6MjErMTI6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD94cGFja2V0IGVuZD0idyI/Pv/uAA5BZG9iZQBkgAAAAAH/2wCEABIODg4QDhUQEBUeExETHiMaFRUaIyIXFxcXFyIRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBFBMTFhkWGxcXGxQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAmoCSAMBIgACEQEDEQH/3QAEACX/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/AOGSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJPCSlkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSU//Q4ZJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKTta5xDWiSeAE7Wlzg1oknha9VTMSrdE2nUk9klIaemtaA/JdtH7oWhju6TUYNLXR3dqsq7Jfa7U6IMlJT0ruo4O4CrHrAHfa1S+2Yb3gPoYWnn2hc6CQitsKSnqh0To+WNahWTwWHZ/1HsWdnfU25gL8K31R+4/R3/bjUXp+YTUNdWrYxuoOLgCdElPnuRjZGNYa76zW8dnD/AKlBXqGXjYufSa8hgcDwe4P8ly4Pq3R7cCwlsvoJ0d3H/GJKctJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSU//R4ZJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkSpm52vCSm9gVNYPVeNe3kFHLyS920cd1J1mxhHkqUyfMpKXmApMBJUDzCtVtAakpiVEO1UrDohBJToYdxY8eC2KrNdODqFz1TocCtmiyWtd4JKd3HywG+7torFtdWRUWvAcxwggrHa7XyKvYdpPsPZJTxvV+mnAydo1qfqw/+i1nLueuYf2nFdA97fc34hcMkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKf//S4ZJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkfH5JQEenhJTK53b5lQZ4pnmXlONAkpTBLiTwFZnRVgYVhvA+9JSO3gKLeE9yZnCSkjVpYb9IWY1XcYw4JKdmsyzzCLRaRYFWpPITgxYElO26LGfFcB1Gn0M26vsHSPg79J/wB+XcUu0hcn9YAB1EnxaD/1SSnJSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJT//0+GSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpHZoxARpitJSMcyp9lAKU6JKUPpQrDTr8lWb9IlHadQkpjcdUmcJOG5xSaICSmY5VqkwQVVHKsVmISU7FJ1HmpO+mELHM7UV30vgUlN6h0QuY6+6eouH7rWj/v3/fl0VbvaPKFy/V3buo3HzA+5rElNFJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP/9ThkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKRX/QCGEWz6ISUjCXZMEjwkpSKDAlCaJKm/wSUvvkyVOQRIQUYfRCSmY5R28SgtHfxVho9qSm/iukN8lYsPu+ap4nIVqx3u+GqSm00xUHeJB/Bcrnu3Zlx/lFdQdK2t8I/IuUyzOVaf5bvypKQpJJJKUkkkkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKUkkkkp//9XhkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklLjlFs+iEJvKNZ9EJKQJ0ySSklQl0eKlc3a6EscDdJ7KV0HUJKRAIo1PkhBHqbKSkoGiOwe1DA0R2DRJSTF0erZ9zwPH+9VW+3UeCOw+2T4QPmXNSU2Xvhjiex/g1crdra93iSfxXSZbiKPkudeNCUlIUkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSU/wD/1uGSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUuOUZ2rEAI7dWpKQJKUJoSUoEjhKSkkkpk1XKW+2VWqYXFXogAJKXAR2D2koLQrDR7QPFJTIj2BSBjYPmfkndo0IbXHU/JJTLqNkVgeKxLOFrdR1DVkWkFJSJJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP/9fhkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJS4TFOOU7gkpijVnsgolfKSmLtCkNU7+UwMJKVCmysuKkyD2ViuNNISUzrrbW2VIGSSh2v1a0dzqps4SUmaOEbhCZyiOPCSkjuJQQeyM76AQAfcUlMOovMD4LIJlbOYwWQDoD4Koem2OZuqcHfyToUlNBJO5paS1wgjkFMkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKUkkkkpSSSSSn//Q4ZJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpTOoUE4KSlQpN0USVd6dji67c76LNfmkpJj9OdYN9h2g9u6O/p+OBo6Sj5Vm0QDCzTa4HlJTJ+N6ZkahSgcjwT13bhBSc2OElNZx/SfBHrOiA7kojHe0nzSU3K+VIn3BQqOgKcavJSU2HfRHwVcclHd9EearzqkpnkcNPkoY1+0wVK8/o2nyVBr4ckpudToa+v7Qz6Q+l5hY622n1KHNPcFYiSlJJJJKUkkkkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKf/0eGSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpbXTwK8Td3cSf++rFW1UYw2f1UlNXItLnHVVyVKwy4qCSmTDBVjd7VWHKLMNSUhLjuU5itCJ1UifZ/r4uSU6FJ9jSiVjv4qvUf0TfgjtMEBJSa0+wKu46o1v0WoDuPgkplcf0Xw0WcD7loXfzUeIn7lnDlJTfqdtqe7waT+CyVoWO24zvF2iz0lKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSU//0uGSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkk6Slkk6SSllrsP6nX/VWQtWYxa/6oSU0ncpk7uVFJS45UnmBCi3lKw6pKYKR+gAopz2SU3KT+jaitP6RBq0YPgiM+n80lNqz80IDvyozjqPJBdykpezWrXt+Qqg1vujzV6x0M3Hg8/BVmibOO6SmGWYaxnzKqI2S/dafAaBBSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJT/9PhkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpOEycJKUkkkkpZartMav+qPyLKWrbpQweDR+RJTSPKinKZJTJqg/lTaoO5SUsnAkqKNUJKSmwNGgKVXMqLipVJKbJPCA88+aMe3y/Iq79TCSl7XQxo8UIGJPgPyJ7HTp4KNntoJ7xCSmkTJJ8UySSSlJJJJKUkkkkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKf/9ThkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSTpKWThMnCSlFJIpklKWtk6NA8BCymiXAeJhaeWdUlNIpk5SHKSl+AhlTeUNJSgrFY1CCwSUdv0klMydUWpAR6uElJj+b/r2QHHUorjoP9eyrPMJKYTL480sp3sA8Skz+cJQ8ky4DwCSmukkkkpSSSSSlJJJJKUknTJKUkkkkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKUkkkkp/9XhkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklLpJJJKVCSSSSlJkkklJKBN9Y/lD8qu5LpcVVxBOQ3yk/gjXGXFJSEp2jVRU28JKYWcoam4yVEcpKSsEBSYdVAmG/FSrSUzlFZoz4oAOqOzRoHiUlJnHT4FU7HfgrJcNfkVRtdLoSUlr1IQ7tXlErQnauKSkRTKTuVFJSkkk6SlkkkklLpk6SSlkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP/9bhkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSTpklKSSTpKUkkkkpSSSZJS6ZJJJSkkkklNrCHvc7wb+VKw6qeKIpe7xIH3ITzqkpipnQKLeUnlJSMpxymUmpKXcU9ZUU44SUzDijNdr8Aq/LviUVp5+cpKU53uPnIVY/SPxRbDyfNDOpnxSUmr4QzyUVnCEeSkpG5RUnKKSlJ0kklKKZOUklLwmUkySmKScpklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP/9fhkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJOkpSSSSSlJJJJKUkkkkpSSSSSlkk4CkGpKYwlCkYCiSkpSQTIuPX6lrW+J1+CSm3t9PHY3uRJ+arHlW8o6wOAqaSmTVBx1U+AhnlJSymOFEJ0lKS7JJFJS7fHwRmD2uKDPgrQG3HJ8UlNZ/Ch4KTvoqHgkpOzhCPJRWcIR5SUxcoKblFJSkkkklKKQSKcJKXSSKSSmJTKRUUlKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP//Q4ZJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJKE8JKWTpJJKUkkkkpSSSSSlJJJJKUkkpBspKYwphikABymc/wSUvoFEuUCZTJKXlMnTJKUtDp9cB9p7aBZ61w30cZrO8a/EpKal7pcUIcp3mSk1JSnHRCUnFRSUun7Jk/ZJSkxTqJSUyHKtXnbit83fwVVvKsZX9Hr+JSU1pkJh2TBOOySk7OEI8orOEI8pKYuUVJyYJKUmUimSUsU4TKQSUsUgkUwSUyKgpqJSUskkkkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKf//R4ZJJJJSkkkklKSSSSUpJJJJSkkk8JKWST7U+1JTFPClCZJS0JQnSSUtCdJJJSkySSSlJJJJKUkkkkpSSSUJKUkApBqlo1JSwanLgOFEvJUElMi4lRSSSUpJJJJSkkk6Sk2LXvuE8DUq5lWToh4jdlReeXcfAIVjpKSmHJTnQJgk4pKYFJMkkpSl2UVLskpRUU5UUlMm8qzkj9WYfB38FWHKuPbvw3eLYKSmgFIKKkElJ28IR5RWcITuUlMSmCcpgkpdMnTJKWUhwop+ySlkkkklMlEp0xSUskkkkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKf/S4ZJJJJSkkkklKSSSSUpOAknBSUyDE8AKQdooFJS8hNITQmhJS8pkoShJSkkoShJSkkoShJSySeEoSUskpbU8BJTCE8KUhNuSUoNUtAoFyaUlMi/wUSUySSlJJJ0lLJJJJKUkkkkpm0Jbdzg0cnRONAjYzJcXnhvHxSUneQxgYOwhVSi2ukoSSlxwoOKkTAQ0lKSSSSUpSUUklKJTJk6SlwtDGILC08EQVnK1jvgpKa1rDXYWHt+RMFdy6t7RY3kc/BUQUlNhhUHjVJhU3tnVJSEplItKbaUlLSkmKSSlJ0ySSlJJJJKXTFOmSUskknSUuAmKkOFEpKWSSSSUpJJJJSkkkklKSSSSU//T4ZJJJJSkkkklKSSSSUpJJJJTIOUtyGkkpJKUqEpSkpnKUqEpSkpnKUqEpSkpnKUqEp9ySmUppKaUtySl9UoKcOUw4JKRbSmgq0IKl6YKSmmmVh9MIJaQkpikkkkpSdMkkpSSdMkpSccpQpNSUvzorjR6dQHcoFLJcCUa53ZJSBxkpgkkdAkpi4qKcpklKSTJ0lKSSTJKWKScpklLqbHQVBOElOnju3DaeCs7Ir9O0t7dlZx3wQpZzJLXDukppsKtNIc2FUAIOqMxwSU2W45cJQXM2kgrWw2zQXHgLNyXD1DCSmm9uqhCORuEhCISUwTpQmSUukkkkpSSSSSllIJk4SUyTEJ04SUwIUURygUlLJJJJKUkknSUsknSSU//1OGSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpPKZJJSRryFYrsVNSa6ElOiAHBBsqTVWqyQC1JTmubCgrNrUENkpKYwnDSiiuFIABJSLYU4YiSm1SUwLU7G7j5KW0lGrZA/KkplWIE/chWOkoj3wICATKSlkzinKgSkpZMnTJKUkkkkpSSSZJS6ZIJFJS6dRCkkpPUdQrry3a1z+ByqNfKsXH9EAkp0Gs6de3U7Sl+z8Bvu9TTwWDJbwU/qv8Skp2MnOraz0qdGhZFlhcZUCSUySkjHwdUZzA5shVUVlhakpgWkKKsy16E+shJSNJJMkpdJJJJSlIJlJJSk6ZOkpYqJUkxSUwTpQnSUskkkkpSSdJJT//1eGSTp0lMUoUk8hJTGEoUpCeQkphCUFTkJ5CSkcFKCiSEpCSkcFKCi6JaJKRQlCLASgJKRQmRtqbYkpEkiFiiWpKYpJ4TJKUkkkkpSSSSSmbXQVdoskQVQRK3kFJTayGaSgUtlyuaWV+ar1DbZCSmdjQAgqzeNAgQkpgptakGmUWIgfekpZre5Sc+NAk9wA0QHOSUu4yoyokqMpKZEqKUpklKSSSSUpOkkkpZMnTJKUkkkkpQUgmCk0JKTVjVGu+iB4Jsdm5wV6/FDa9ztCUlOKeUoU3th0JklMYUSplRKSllIKIUwkpkwGVaDJbqg0j3BabKwQkpyba9pQVqZFWhWa4QUlMU4TKQCSlJ0kySlwkkkUlLKQEqClKSlEBRTpJKWTwknSUqEkkklP/1uGSSSSUpJJJJSkkkklLylKZJJS8pSmSSUvKUpkklLyU+4qKSSme8pxYhpJKTB4UtCq6kHFJSUsUCxIWIgcCkpAQmVktBQzWkpEkpFpTQkpZOEkklNvHtjQotjIduHCotMFX6nh7YPKSmTxvYCFBjdQCis0MJ3N7hJSZuKI3DwlZr3O3FbmHY1wDXdtFXzunlrt9erSkpxyXFRglWHUvHLSmbTY4wGpKQ7VFzSOVp14ZaNz1VyGidElNRJOQmSUpJMkkpeUpTJJKUkkkkpSSSkBKSlgEVrVKupzjoFpYvTy47naBJSTpmMXHceAo9UvBftB0CtZGVXjVenXysG611jiSkpG4yU6inSUoqJUlEpKWCIFEBTCSk1A9y1a+FmUDVX2ugJKYXkahZdw9yuXWe5U7DJSUjTpJJKUmTlMkpSRSTJKUkkkkpdJNKeUlLpJpSlJTJJMkkp//1+GSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKTyUySSkjXwiteCqycEhJTaLQVB1ag2whGbYDykpAWEKMK2WhyG6pJSBFqftKgWwmCSnSaQ4SOUVuoVGm2NFcae44SUy9zDuar+PnNI22fiqzWh4QLaXNMhJTsmzGcOAgWWUN4AWK59g7oZtf3KSm9dduMBV3AEaoYemL0lMHV+CE6tw7I/rx2UhkDuElNIghMtCaH8iE32VjvolJTQSV8dPce6I3p4H0nBJTnBpKI3HsdwFo7cWkeJQnZrR9BqSkLMKw9laqwANXGFXOdYeNEN2Vae6SnVDsWgeJQb+pkjazQLLL3HkpklMrLXPMkqCeEklKSSSSUsmTpwElLgKQCZTaElJ6hCKXwhN0CDZZykpi98vKgSozJTpKWSSTJKUmSSSUpJMkkpSSSSSlJJJJKUlKSSSlSklCSSn//Q4ZJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKTgkJkklJmWkKw14cqKm15CSm26sEKu5pCPXYDypuYHBJTUBhWqbexQHMIUQSCkp1K3lpkcK80stbBWRVbOhVllhYZHCSkl+L3CoPrIK12XBw1Q7aWO4SU5EEJHUK1ZjkKs5pCSkRCaFNMkphqptscOCmhNCSkoybB3TG+w90KEklLlxPJTQkkkpUJJJJKUkkkkpSSSSSlJk6SSlQnCQTpKXCLW1QaJRwNrZSUxsdtCpudJRLrJKAkpcFSlQUgkpdIpJklKTJJklLpkkklKSSTpKUlCdMkpSdMmSUySTSkkp//9HhkkkklKSSSSUpJJOkpZJPCUJKWSTwlCSlkkoSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJTJriFaqt7FU04JCSnRLQ4Ku+shKq7xVobXhJTSBLSrdVkiCh2VeCGJaUlN4OLT5IgtMKsx4Igp9QkpsesO6DaGPGnKg4yhlxCSkbmkKCKXTyoEBJTFMpJklLJJ0klMUoUkklMUk6SSlkk6SSlkydJJSydJPCSlAKQEpASjsYGiSkpTGQJKFfb2CV1/ZqqkkpKUTKZJOElKTpwEklLJkkklKTJJJKUkkkkpSSSSSl0ySSSlJJJJKUknSSU//0uGSSSSUpJJJJSkkkklLylKZJJTKU8hQSSUlgFLYhypB5SUuWKJapiwKYLSkpBCZWCwFQLElIklItTQkpZJJJJSkkkklKSSSSUpOkkkpSSSSSlAwrFV0Kukkp1GOa8Jn0zwqVVpaVertDgkpBBaUVrpEFFcwFBLCElKIhQIRAUzm+CSkBCiikKMJKYJKUJQkpimUtqW1JTFJShNCSlkylCUJKYpKUJQkpjCUKW0qQYSkphCm1hKmKwNSmfaG6NSUz9lYk8qvbeXaDhDe9zioJKUTKZPCfakpYBSATwkSkpZMkSmSUpMkkkpSSSSSlJJJJKUkkkkpSSSSSlKSQCRSUpJMkkp//9Phkk+1PtSUxSUtpS2lJTFJS2lNBSUskkkkpSSSSSlJJJJKUnkpkklMxYQiteDyq6dJTZLAeENzFFthCMHtdykprlqaFYdX4IRbCSkaSchMkpSSSSSlwnhRUgUlKSSSSUsknTJKUiMsLShpklOjVeDyjyCFkhxCs13kcpKbRaopCwFPKSmJAKgWoiZJSJJTICjCSlJ4CjCUlJTLaE+0KElPuSUz2BL0wo70+9JS/phLYEtyW5JSoaExcBwkokJKYOcShESjloTQElINifYimFAuASUttCYwExeoEykpcuUZSTJKXTJJJKUkkkkpSSSSSlJJJJKUkkkkpScBIBS4SUsUySSSlkkkklP/1OHlPuUUklM9ycPCGkkpOHNKltaVXlOHEJKTGoFRNRTNtIRW2g8pKQFhCjBV0bHJGkHhJTRhJWXUIZrISUiSUi0hNCSlkk8JQkpZPMJkklJG2uCKHtdyqycGElJ3V9whFsKbLSOUSGvGiSmtCZFdWQhkJKWSSSSUuCnUU8pKXTJwkkpZJOkkpZKUkySkjbCEdtsqonBSU3d6fcqgeph6Sk+5Lchb0tySkkppUNyW5JTOU0qG5LcElJJSlD3hNvCSkspSheomNiSk8ptyB6hUd5SU2C9QNiDuKaUlMy9RJJUUklLpkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSk4EpAKXCSlcKKSSSlJJJJKUkmSSU//V4ZJJJJSkkkklKSSSSUpPKZJJTMPIRWXkcqunSU3m3NdyplrTws8EhEZc4JKbDqkF1aMy9ruVPaHcJKaZYU0Ky5hCGQkpDCaESE0JKRwmRCFEhJTFTa8hRTJKbTXtcIKZ9XcKuCQjMtjQpKRuaQoK5DXBCfV4JKQJKRaQopKXTyopJKZJJk4SUsknhKElMUk8JklKTymSSUylPuUEklM9xTbiopJKZbimlMkkpeUpTJJKUkkkkpSSeEySlJJJJKUkkkkpSSSSSlJJJJKUkkkkpSSSSSlJJJ0lKShJOkpdMkkkpZJPCUJKWTKUJQkpikpQkkp//9bhkkkklKSSSSUpJJJJSkkkklKSSSSUpOmSSUuCittc1BTykpusva7QqZY13CoBEZa5qSkr6nBCMhWWXtdo5SdU1wkJKacpkZ9DgglpCSmJTQn1SSUskknSUya8hGbbPKrqQSU2C1rkF1RCQcpb0lIS0poRyQUxDUlIE4RCwKO1JSgUiU0JoSUumSSSUskkkkpSSSSSlJJJJKUkkkkpSSdKElLJ04aU+0pKYpKW0ptqSmKSlCZJSySdMkpSSSSSlJJJJKUknAUwwlJSNPBRhUpioJKa+0p9hVnYEtoSU19hT7EfRRkJKR7EtqnuCiXJKW2pbUi5NuSUvtTQm3JtySl4CSjKSSn/1+GSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUunBUUklM1Ntr290KU8pKbrMkHRyJtrfws9SDnN4KSmy7G8EJ1DgpMynjlGblMP0gkppmsjsm2rQBpemOOw8FJTnwlCuOxj2UDju8ElNeEkY0u8FH03JKYJaqewpbSkphqlBU9pS2lJTCE0Im0pQUlIoKW0okJ4SUh2lLaUaCn2lJSDYU+wo20pbCkpDsKWxH2FOKykpBsT+mrIqUhWkpqitSFasbAEtAkpCGJ9qmSoFySmJAUCnLlAlJSxKinShJTFJShPtSUwTwiNrJRm0pKa4YVNtRKshjQpaBJSJtQUw0BOSolySmWgUS5DL1EklJTMvUS9RUSQElLlxTFygXJpSUyLk25RSSUvKUpkklKSSSSUpJJJJT//0OGSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklLypAqCdJTPQpbVGU4KSl9RwpNtsb3TAp9ElJW5dg5Uxm+IVeAm2JKbgy6zyFIX0lUNqW0pKdDfQU/6E91nQUpckp0ttR7penWVnbnKQseO6Sm/6LVH0AqgveO6kMh6Smz6Cb0UEZLlIZJSUl9EJekEP7Sl9oSUl9MJbAhfaE3rpKTbQloED1k3qpKTkhRLkE2FR3lJSYuUC5CLimkpKZlygSm1SgpKWTKYaVIVkpKRQpBhKO2lT2takpA2olFFQHKc2NHCgbCUlJIaExchySnSUy3JFygXQoFxKSmZcoEkpASn0CSloTEgJnWeCEXEpKZOeoymSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP/0eGSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpPKZJJTKU8qKSSmcp5UJTgpKSSnlQCkElLp4CcKYCSke0JbEYNUw1JTW9NL01bDE/phJTT9MpvTKvemEvSCSmjsKWwq96QTekElNLYUthVz0wm9MJKam0pbSrXp+Sf0vJJTU2lPsVr0Sn9Id0lNTYnDFa21jkqJtqakpCKipilM7JA4QnZDikpsbWN5TG1g4VQ2OKaSkpO689kMvJUAFMNSUoSVIBOBCYlJTLhRLk2pTgJKWglSgBMXBqC6wlJSV1gHCC55KikkpSSSSSlJJJJKUkkkkpSSSSSlJJJJKUkkkkpSSSSSn/0uGSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklLpwop5SUkCI0IAcpCwhJTaa1FawKkLnBOMhwSU6LawpisLNGU9P9ssSU6gYE+wLK+2WJfbLfFJTrbAlsCyftlvil9st8UlOrsCWxqyvtlnionJsPdJTqn0xyVA2Ujusk2vPdRLnHukp1HZVQQXZo7BUEklNp2Y48Ibsh57oSZJTMvceSoyU0pJKXSSAUw1JTEBSDVIBSSUsAnSlMXAJKXShQNgUC8lJSUvAQ3WEqEpklLkkpkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJT/9PhkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpOmSSUukmSSUukmSSUvKUpkklKTpkklLgKQCjKUlJSQQpSgyUpKSku4KJeoSmSUyLimkpkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJT/AP/U4ZJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJT/AP/V4ZJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJT/AP/Z' style='width:100%'></div>";
        	    }
            	
        	    if(document.title.indexOf("Pelican")!=-1){
	            	document.getElementById('loading_gif').innerHTML = "<div id='text' style='font-family:\"Courier New\";position:absolute;top:50%;left:50%;margin-left:-500px;margin-top:-500px;width:1000px;height:1000px;font-weight:bold;font-size:60px;color:#00ff00'><table><tr height='1000px'><td width='1000px' valign='top' align='left'>>Loading Anti-Track Layer...</td></tr></table></div>";
	            	
	            	$("div[name='group_0']").append($("div[id='loading_gif']"));
	            	
	            	//$("div[id='loading_gif']").scale($("div[name='group_0']").data("scale"));
	            	$("div[id='loading_gif']").scale(1);
	            	
	            	
	            	setTimeout(function(){
	            		setTimeout(function(){
	            			document.getElementById('loading_gif').innerHTML = "<div id='text' style='font-family:\"Courier New\";position:absolute;top:50%;left:50%;margin-left:-500px;margin-top:-500px;width:1000px;height:1000px;font-weight:bold;font-size:60px;color:#00ff00'><table><tr height='1000px'><td width='1000px' valign='top' align='left'>>Loading Anti-Track Layer...<br/>>Loaded...</td></tr></table></div>";
	            			setTimeout(function(){
	            			    document.getElementById('loading_gif').innerHTML = "<div id='text' style='font-family:\"Courier New\";position:absolute;top:50%;left:50%;margin-left:-500px;margin-top:-500px;width:1000px;height:1000px;font-weight:bold;font-size:60px;color:#00ff00'><table><tr height='1000px'><td width='1000px' valign='top' align='left'>>Loading Anti-Track Layer...<br/>>Loaded...<br/>>History = OFF<br/>>Cookies = OFF<br/>>Track = OFF</td></tr></table></div>";
	            			    setTimeout(function(){
	            			    	document.getElementById('loading_gif').innerHTML = "<div id='text' style='font-family:\"Courier New\";position:absolute;top:50%;left:50%;margin-left:-500px;margin-top:-500px;width:1000px;height:1000px;font-weight:bold;font-size:60px;color:#00ff00'><table><tr height='1000px'><td width='1000px' valign='top' align='left'>>Loading Anti-Track Layer...<br/>>Loaded...<br/>>History = OFF<br/>>Cookies = OFF<br/>>Track = OFF<br/>>Disable network monitoring...<br/>>Using encrypted transmission on client...</td></tr></table></div>";
	            			    	setTimeout(function(){
	            			    		document.getElementById('loading_gif').innerHTML = "<div id='text' style='font-family:\"Courier New\";position:absolute;top:50%;left:50%;margin-left:-500px;margin-top:-500px;width:1000px;height:1000px;font-weight:bold;font-size:60px;color:#00ff00'><table><tr height='1000px'><td width='1000px' valign='top' align='left'>>Loading Anti-Track Layer...<br/>>Loaded...<br/>>History = OFF<br/>>Cookies = OFF<br/>>Track = OFF<br/>>Disable network monitoring...<br/>>Using encrypted transmission on client...<br/>>Loading secure session now...</td></tr></table></div>";
	            			    		setTimeout(function(){
	            			    			notFinishLoaded = false;
	            			    		}, 1000);
	            			    	}, 1000);
	            			    }, 1000);
	            			}, 1000);
	            		}, 1000);
	            	}, 1000);
            	}
            	else{
            		notFinishLoaded = false;
            	}
	        }, 0);
            
            
            //var loadingImg = "<img src=\"data:image/gif;base64,R0lGODlhZABkAPQAAAAAAMkQ+VgHbXYJk54MxJkMvoAKnrIO3boO56YNzYYKpowLrmUIfV4HdawN1ckQ+WsIhZMLtsEP7wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH+GkNyZWF0ZWQgd2l0aCBhamF4bG9hZC5pbmZvACH5BAAHAAAAIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAZABkAAAF/yAgjmRpnmiqrmzrvnAsz3Rt33iu73zfMgoDw0csAgSEh/JBEBifucRymYBaaYzpdHjtuhba5cJLXoHDj3HZBykkIpDWAP0YrHsDiV5faB3CB3c8EHuFdisNDlMHTi4NEI2CJwWFewQuAwtBMAIKQZGSJAmVelVGEAaeXKEkEaQSpkUNngYNrCWEpIdGj6C3IpSFfb+CAwkOCbvEy8zNzs/Q0dLT1NUrAgOf1kUMBwjfB8rbOQLe3+C24wxCNwPn7wrjEAv0qzMK7+eX2wb0mzXu8iGIty1TPRvlBKazJgBVnBsN8okbRy6VgoUUM2rcyLGjx48gQ4ocSbKkyZMoJf8JMFCAwAJfKU0gOUDzgAOYHiE8XDGAJoKaalAoObHERFESU0oMFbF06YikKQQsiKCJBYGaNR2ocPr0AQCuQ8F6Fdt1rNeuLSBQjRDB3qSfPm1uPYvUbN2jTO2izQs171e6J9SuxXjCAFaaQYkC9ku2MWCnYR2rkDqV4IoEWG/O5fp3ceS7nuk2Db0YBQS3UVm6xBmztevXsGPLnk27tu3buHOvQU3bgIPflscJ4C3D92/gFNUWgHPj2G+bmhkWWL78xvPjDog/azCdOmsXzrF/dyYgAvUI7Y7bDF5N+QLCM4whM7BxvO77+PPr38+//w4GbhSw0xMQDKCdJAwkcIx2ggMSsQABENLHzALILDhMERAQ0BKE8IUSwYILPjEAhCQ2yMoCClaYmA8NQLhhh5I0oOCCB5rAQI0mGEDiRLfMQhWOI3CXgIYwotBAA/aN09KQCVw4m4wEMElAkTEhIWUCSaL0IJPsySZVlC/5J+aYZJZppgghAAAh+QQABwABACwAAAAAZABkAAAF/yAgjmRpnmiqrmzrvnAsz3Rt33iu73zfMhAIw0csAgQDhESCGAiM0NzgsawOolgaQ1ldIobZsAvS7ULE6BW5vDynfUiFsyVgL58rwQLxOCzeKwwHCIQHYCsLbH95Dg+OjgeAKAKDhIUNLA2JVQt4KhGPoYuSJEmWlgYuSBCYLRKhjwikJQqnlgpFsKGzJAa2hLhEuo6yvCKUv549BcOjxgOVhFdFdbAOysYNCgQK2HDMVAXexuTl5ufo6err7O3kAgKs4+48AhEH+ATz9Dj2+P8EWvET0YDBPlX/Eh7i18CAgm42ICT8l2ogAAYPFSyU0WAiPjcDtSkwIHCGAAITE/+UpCeg4EqTKPGptEikpQEGL2nq3Mmzp8+fQIMKHUq0qNGjSJO6E8DA4RyleQw4mOqgk1F4LRo4OEDVwTQUjk48MjGWxC6zD0aEBbBWbdlJBhYsAJlC6lSuDiKoaOuWbdq+fMMG/us37eCsCuRaVWG3q94UfEUIJlz48GHJsND6VaFJ8UEAWrdS/SqWMubNgClP1nz67ebIJQTEnduicdWDZ92aXq17N+G1kV2nwEqnqYGnUJMrX868ufPn0KNLn069Or+N0hksSFCArkWmORgkcJCgvHeWCiIYOB9jAfnx3D+fE5A+woKKNSLAh4+dXYMI9gEonwoKlPeeON8ZAOCgfTc0UB5/OiERwQA5xaCJff3xM6B1HHbo4YcghigiNXFBhEVLGc5yEgEJEKBPFBBEUEAE7M0yAIs44leTjDNGUKEkBrQopDM+NFDAjEf+CMiNQhJAWpE8zqjkG/8JGcGGIjCQIgoMyOhjOkwNMMCWJTTkInJZNYAlPQYU4KKT0xnpopsFTKmUPW8ScOV0N7oJ53TxJAbBmiMWauihiIIYAgAh+QQABwACACwAAAAAZABkAAAF/yAgjmRpnmiqrmzrvnAsz3Rt33iu73zv/8AZo4BAFBjBpI5xKBYPSKWURnA6CdNszGrVeltc5zcoYDReiXDCBSkQCpDxShA52AuCFoQribMKEoGBA3IpdQh2B1h6TQgOfisDgpOQhSMNiYkIZy4CnC0Ek4IFliVMmnYGQAmigWull5mJUT6srRGwJESZrz+SrZWwAgSJDp8/gJOkuaYKwUADCQ4JhMzW19jZ2tvc3d7f4NoCCwgPCAs4AwQODqrhIgIOD/PzBzYDDgfsDgrvAAX0AqKjIW0fuzzhJASk56CGwXwOaH1bGLBGQX0H31Gch6CGgYf93gGkOJCGgYIh3/8JUBjQHg6J/gSMlBABob+bOHPq3Mmzp8+fQIMKHUq0qNEUAiBAOHZ0RYN10p41PZGg6jQHNk/M07q1BD2vX0l0BdB1rIiKKhgoMMD0BANpVqmpMHv2AVm7I7aa1Yu3bl6+YvuuUEDYXdq40qqhoHu38d+wfvf2pRjYcYq1a0FNg5vVBGPAfy03lhwa8mjBJxqs7Yzi6WapgemaPh0b9diythnjSAqB9dTfwIMLH068uPHjyJMrX84cnIABCwz4Hj4uAYEEeHIOMAAbhjrr1lO+g65gQXcX0a5fL/nOwIL3imlAUG/d8DsI7xfAlEFH/SKcEAywHw3b9dbcgQgmqOByggw26KAIDAxwnnAGEGAhe0AIoEAE0mXzlBsWTojDhhFwmE0bFroR3w8RLNAiLtg8ZaGFbfVgwIv2WaOOGzn+IIABCqx4TRk1pkXYgMQNUUAERyhnwJIFFNAjcTdGaWJydCxZ03INBFjkg2CGKeaYCYYAACH5BAAHAAMALAAAAABkAGQAAAX/ICCOZGmeaKqubOu+cCzPdG3feK7vfO//wBnDUCAMBMGkTkA4OA8EpHJKMzyfBqo2VkBcEYWtuNW8HsJjoIDReC2e3kPEJRgojulVPeFIGKQrEGYOgCoMBwiJBwx5KQMOkJBZLQILkAuFKQ2IiYqZjQANfA4HkAltdKgtBp2tA6AlDJGzjD8KrZ0KsCSipJCltT63uAiTuyIGsw66asQHn6ACCpEKqj8DrQevxyVr0D4NCgTV3OXm5+jp6uvs7e7v6gIQEQkFEDgNCxELwfACBRICBtxGQ1QCPgn6uRsgsOE9GgoQ8inwLV2ChgLRzKCHsI9Cdg4wBkxQw9LBPhTh/wG4KHIODQYnDz6Ex1DkTCEL6t189w+jRhsf/Q04WACPyqNIkypdyrSp06dQo0qdSrWqVUcL+NER0MAa1AYOHoh9kKCiiEoE6nl1emDsWAIrcqYlkDKF2BNjTeQl4bbEXRF//47oe8KABLdjg4qAOTcBAcWAH+iVLBjA3cqXJQ/WbDkzX84oFCAey+wEg8Zp136e3Pnz3sitN28mDLsyiQWjxRo7EaFxXRS2W2OmDNqz7NrDY5swkPsB5FC91a6gHRm08OKvYWu3nd1EW8Rw9XA1q1TAd7Flr76wo1W9+/fw48ufT7++/fv48+s/wXUABPLwCWAAAQRiolQD/+FDIKRdBOz0TjgKkGNDAwsSSJBKEESowHOUEFjEY0lJEyGAegyw4G5HNcAAiS0g2ACL+8Uo44w01mjjjTi+wMCKMs5TQAQO+iCPAQme00AEP/4IIw0DZLVAkLA0kGQBBajGQ5MLKIDiMUcmGYGVO0CQZXvnCIAkkFOsYQCH0XQVAwP+sRlgVvssadU8+6Cp3zz66JmfNBFE8EeMKrqZ46GIJqrooi6EAAAh+QQABwAEACwAAAAAZABkAAAF/yAgjmRpnmiqrmzrvnAsz3Rt33iu73zv/0Baw2BoBI88g2N5MCCfNgZz6WBArzEl1dHEeluGw9Sh+JpTg+1y8GpABGdWQxFZWF0L7nLhEhAOgBFwcScNCYcOCXctAwsRbC5/gIGEJwuIh3xADJOdg5UjEQmJowlBYZ2AEKAkeZgFQZypB0asIgyYCatBCakEtiQMBQkFu0GGkwSfwGYQBovM0dLT1NXW19jZ2ts+AgYKA8s0As6Q3AADBwjrB9AzogkEytwN6uvs4jAQ8fxO2wr3ApqTMYAfgQSatBEIeK8MjQEHIzrUBpAhgoEyIkSct62BxQP5YAhoZCDktQEB2/+d66ZAQZGVMGPKnEmzps2bOHPq3Mmzp88v5Iz9ZLFAgtGLjCIU8IezqFGjDzCagCBPntQSDx6cyKoVa1avX0mEBRB2rAiuXU00eMoWwQoF8grIW2H2rFazX/HeTUs2Lde+YvmegMCWrVATC+RWpSsYsN6/I/LyHYtWL+ATAwo/PVyCatWrgU1IDm3Zst2+k/eiEKBZgtsVA5SGY1wXcmTVt2v77aq7cSvNoIeOcOo6uPARAhhwPs68ufPn0KNLn069uvXrfQpklSAoRwOT1lhXdgC+BQSlEZZb0175QcJ3Sgt039Y+6+sZDQrI119LW/26MUQQ33zaSFDfATY0kFh2euewV9l748AkwAGVITidAAA9gACE2HXo4YcghijiiN0YEIEC5e3QAAP9RWOiIxMd0xKK0zhSRwRPMNCSAepVYoCNTMnoUopxNDLbEysSuVIDLVLXyALGMSfAAgsosICSP01J5ZXWQUBlj89hSeKYZJZpJoghAAAh+QQABwAFACwAAAAAZABkAAAF/yAgjmRpnmiqrmzrvnAsz3Rt33iu73zv/0Bag8FoBI+8RmKZMCKfNQbTkSAIoNgYZElNOBjZcGtLLUPE6JSg601cXQ3IO60SQAzyF9l7bgkMbQNzdCUCC1UJEWAuAgOCLwYOkpIDhCdbBIiVQFIOB5IHVpYlBpmmC0EMk6t9oyIDplUGqZ+ek06uAAwEpqJBCqsOs7kjDAYLCoM/DQa1ycSEEBCL0NXW19jZ2tvc3d7fPwJDAsoz4hC44AIFB+0R5TGwvAbw2Q0E7fnvNQIEBbwEqHVj0A5BvgPpYtzj9W+TNwUHDR4QqBAgr1bdIBzMlzCGgX8EFtTD1sBTPgQFRv/6YTAgDzgAJfP5eslDAAMFDTrS3Mmzp8+fQIMKHUq0qNGjSJMisYNR6YotCBAE9GPAgE6fEKJqnbiiQYQCYCmaePDgBNmyJc6mVUuC7Ai3AOC+ZWuipAStUQusGFDgawQFK+TOjYtWhFvBhwsTnlsWseITDfDibVoCAtivgFUINtxY8VnHiwdz/ty2MwoBkrVSJtEAbNjAjxeDnu25cOLaoU2sSa236wCrKglvpss5t/DHcuEO31z57laxTisniErganQSNldf3869u/fv4MOLH0++vHk/A5YQeISjQfBr6yTIl5/Sxp2/76sNmM9fuwsDESyAHzgJ8DdfbzN4JWCkBBFYd40DBsqXgA0DMIhMfsQUGGEENjRQIR4v7Rehfy9gWE18/DkEnh0RJELieTDGKOOMNAa1DlkS1Bceap894ICJUNjhCJAyFNAjWahAA8ECTKrow5FkIVDNMcgMAwSUzFnCAJMLvHiDBFBKWQ1LLgERAZRJBpVTiQ70eMBQDSigAHSnLYCAj2kCJYCcBjwz3h98EnkUM1adJ2iNiCaq6KKLhgAAIfkEAAcABgAsAAAAAGQAZAAABf8gII5kaZ5oqq5s675wLM90bd94ru987//AoHAYEywShIWAyKwtCMjEokmFCaJQwrLKVTWy0UZ3jCqAC+SfoCF+NQrIQrvFWEQU87RpQOgbYg0MMAwJDoUEeXoiX2Z9iT0LhgmTU4okEH0EZgNCk4WFEZYkX5kEEEJwhoaVoiIGmklDEJOSgq0jDAOnRBBwBba3wcLDxMXGx8jJysvMzUJbzgAGn7s2DQsFEdXLCg4HDt6cNhHZ2dDJAuDqhtbkBe+Pxgze4N8ON+Tu58jp6+A3DPJtU9aNnoM/OBrs4wYuAcJoPYBBnEixosWLGDNq3Mixo8ePIEOKxGHEjIGFKBj/DLyY7oDLA1pYKIgQQcmKBw9O4MxZYmdPnyRwjhAKgOhQoCcWvDyA4IC4FAHtaLvJM2hOo0WvVs3K9ehRrVZZeFsKc0UDmnZW/jQhFOtOt2C9ingLt+uJsU1dolmhwI5NFVjnxhVsl2tdwkgNby0RgSyCpyogqGWbOOvitlvfriVc2LKKli9jjkRhRNPJ0ahTq17NurXr17Bjy55NG0UDBQpOvx6AoHdTiTQgGICsrIFv3wdQvoCwoC9xZAqO+34Ow0DfBQ+VEZDeW4GNOgsWTC4WnTv1QQaAJ2vA9Hhy1wPaN42XWoD1Acpr69/Pv79/ZgN8ch5qBUhgoIF7BSMAfAT07TDAgRCON8ZtuDWYQwIQHpigKAzgpoCEOGCYoQQJKGidARaaYB12LhAwogShKMhAiqMc8JYDNELwIojJ2EjXAS0UCOGAywxA105EjgBBBAlMZdECR+LESmpQRjklagxE+YB6oyVwZImtCUDAW6K51mF6/6Wp5po2hAAAIfkEAAcABwAsAAAAAGQAZAAABf8gII5kaZ5oqq5s675wLM90bd94ru987//AoHAYE0AWC4iAyKwNCFDCoEmFCSJRQmRZ7aoaBWi40PCaUc/o9OwTNMqvhiE84LYYg4GSnWpEChEQMQ0MVlgJWnZ8I36AgHBAT4iIa4uMjo9CC5MECZWWAI2Oij4GnaefoEcFBYVCAlCIBK6gIwwNpEACCgsGubXAwcLDxMXGx8jJysvMZ7/KDAsRC5A1DQO9z8YMCQ4J39UzBhHTCtrDAgXf3gkKNg3S0hHhx9zs3hE3BvLmzOnd6xbcYDCuXzMI677RenfOGAR1CxY26yFxosWLGDNq3Mixo8ePIEOKHEmyZDEBAwz/GGDQcISAlhMFLHBwwIEDXyyOZFvx4MGJnj5LABU6lETPEUcBJEVa9MQAm1Ad0CshE4mCqUaDZlWqlatXpl9FLB26NGyKCFBr3lyxCwk1nl3F+iwLlO7crmPr4r17NqpNAzkXKMCpoqxcs0ftItaaWLFhEk9p2jyAlSrMukTjNs5qOO9hzipkRiVsMgXKwSxLq17NurXr17Bjy55Nu7ZtIoRWwizZIMGB3wR2f4FQuVjv38gLCD8hR8HVg78RIEdQnAUD5woqHjMgPfpv7S92Oa8ujAHy8+TZ3prYgED331tkp0Mef7YbJctv69/Pv7//HOlI0JNyQ+xCwHPACOCAmV4S5AfDAAhEKF0qfCyg14BANCChhAc4CAQCFz6mgwIbSggYKCGKmAOJJSLgDiggXiiBC9cQ5wJ3LVJ4hoUX5rMCPBIEKcFbPx5QYofAHKAXkissIKSQArGgIYfgsaGAki62JMCTT8J0Wh0cQcClkIK8JuaYEpTpGgMIjIlAlSYNMKaOq6HUpgQIgDkbAxBAAOd/gAYqKA0hAAAh+QQABwAIACwAAAAAZABkAAAF/yAgjmRpnmiqrmzrvnAsz3Rt33iu73zv/8CgcChrQAYNotImiBQKi+RyCjM4nwOqtmV4Og3bcIpRuDLEaBNDoTjDGg1BWmVQGORDA2GfnZusCxFgQg17BAUEUn4jEYGNQwOHhhCLJFYREQpDEIZ7ipUCVgqfQAt7BYOVYkduqq6vsLGys7S1tre4ubq7UwIDBn04DAOUuwJ7CQQReDUMC8/FuXrJydE0Bs92uwvUBAnBNM7P4LcK3ufkMxDAvMfnBbw9oQsDzPH3+Pn6+/z9/v8AAwocSLCgwYO9IECwh9AEBAcJHCRq0aAOqRMPHmDMaCKjRhIeP47gKIIkyZEeU/8IgMiSABc2mlacRAlgJkebGnGizCmyZk8UAxIIHdoqRR02LGaW5AkyZFOfT5c6pamURFCWES+aCGWgKIqqN3uGfapzqU+xTFEIiChUYo+pO0uM3fnzpMm6VUs8jDixoVoIDBj6HUy4sOHDiBMrXsy4sWMSTSRkLCD4ltcZK0M+QFB5lgIHEFPNWKB5cq7PDg6AFh0DQem8sVaCBn0gQY3XsGExSD0bdI0DryXgks0bYg3SpeHhQj07HQzgIR10lmWAr/MYC1wjWDD9sffv4MOLR3j1m5J1l/0UkMCevXIgDRIcQHCAQHctENrrv55D/oH/B7ynnn7t2fYDAwD+R59zVmEkQCB7BvqgQIIAphdGBA9K4JILcbzQAID0/cfgFvk9aE0KDyFA34kp+AdgBK4MQKCAKEqg4o0sniBAAQBS9goEESQQQY4nJHDjjRGy0EBg/Rx55GFO3ngYAVFuWBiCRx4w4kENFKBiAVuOJ+aYZIoZAgAh+QQABwAJACwAAAAAZABkAAAF/yAgjmRpnmiqrmzrvnAsz3Rt33iu73zv/8CgcChrMBoNotImUCwiiuRyCoNErhEIdduCPJ9arhgleEYWgrHaxIBAGDFkep1iGBhzobUQkdJLDAtOYUENEXx8fn8iBguOBkMNiImLJF6CA0MCBYh9lSMCEAYQikAMnBFwn2MCRquvsLGys7S1tre4ubq7vDqtpL5HvAIGBMYDeTTECgrJtwwEBcYEzjIMzKO7A9PGpUUGzN61EMbSBOIxoei0ZdOQvTuhAw3V8Pb3+Pn6+/z9/v8AAwocSBCQo0wFUwhI8KDhgwPrerUSUK8EAYcOD/CTRCABGhUMMGJ8d6JhSZMlHP+mVEkCJQCULkVgVFggQUcCC1QoEOlQQYqYMh+8FDrCZEyjRIMWRdoyaZ2bNhOoOmGAZ8OcKIAO3bqUpdKjSXk25XqiQdSb60JaJWlCK9OlZLeChetVrtMSm85iTXFRpMafdYfefRsUqEuYg7WWkGTTk4qFGB1EHEavIpuDCTNr3sy5s+fPoEOLHk063YCaCZD1mlpjk4TXrwtYjgWh5gLWMiDA3o3wFoQECRwExw2jwG7YCXDlFS58r4wEx187wMUgOHDgEWpEiC4h+a281h34pKE7em9b1YUDn7xiwHHZugKdYc/CSoIss0vr38+/v//RTRAQhRIC4AHLAAcgoCCkAuf50IACDkTYzCcCJLiggvTRAKEDB0TIFh0GXLjgeD4wwGGEESaQIREKiKggiT2YiOKJxI0xgIsIfKgCPS+YFWGHwq2oiYULHpCfCFZE+FELBszoQIN0NEDkATWaIACHB2TpwJEAEGOdaqsIMIACYLKwQJZoHuDcCkZweUsBaCKQJQGfEZBmlgV8ZkCCceqYWXVpUgOamNEYIOR/iCaq6KIAhAAAIfkEAAcACgAsAAAAAGQAZAAABf8gII5kaZ5oqq5s675wLM90bd94ru987//AoHBIExCPOMhiAUE6ZYLl0vissqJSqnWLGiwUA64Y1WiMfwKGmSgwgM+otsKwFhoWkYgBbmIo/gxEeXgLfCUNfwp1QQp4eoaHakdRelqQl5iZmpucnZ6foKGioz8LCA8IC5akOAcPr68Oq6CzMguwuAWjEBEFC4syDriwEqICvcg2w7iiDQXPBRHAMKfLD8bR0RE2t8u6ogzPEU01AsK4ErWdAtMzxxKvBeqs9PX29/j5+vv8/f7/AAMKNAEBwryBJAYgkMCwEMIUAxhKlOBQn4AB0cKsWDiRYTsRr07AMjGSBDOT10D/pgyJkmUXAjAJkEMBoaPEmSRTogTgkue1niGB6hwptAXMAgR8qahpU4JGkTpHBI06bGdRlSdV+lQRE6aCjU3n9dRatCzVoT/NqjCAFCbOExE7VoQ6tqTUtC2jbtW6967eE2wjPFWhUOLchzQNIl7MuLHjx5AjS55MubJlGQ3cKDj4kMEBBKARDKZ1ZwDnFQI+hwb9UZMAAglgb6uhcDXor6EUwN49GoYC26AJiFoQu3jvF7Vt4wZloDjstzBS2z7QWtPuBKpseA594LinAQYU37g45/Tl8+jTq19fmUF4yq8PfE5QPQeEAgkKBLpUQL7/BEJAkMCADiSwHx8NyIeAfH8IHOgDfgUm4MBhY0Dg34V7ACEhgQnMxocACyoon4M9EBfhhJdEcOEBwrkwQAQLeHcCAwNKSEB9VRzjHwHmAbCAA0Ci6AIDeCjiGgQ4jjBAkAcAKSNCCgQZ5HKOGQBkk0Bm+BgDUjZJYmMGYOmAlpFlRgd7aKap5poyhAAAIfkEAAcACwAsAAAAAGQAZAAABf8gII5kaZ5oqq5s675wLM90bd94ru987//AoHBIExCPOIHB0EA6ZUqFwmB8WlkCqbR69S0cD8SCy2JMGd3f4cFmO8irRjPdW7TvEaEAYkDTTwh3bRJCEAoLC35/JIJ3QgaICwaLJYGND0IDkRCUJHaNBXoDAxBwlGt3EqadRwIFEmwFq6y0tba3uLm6u7y9viYQEQkFpb8/AxLJybLGI7MwEMrSA81KEQNzNK/SyQnGWQsREZM1CdzJDsYN4RHh2TIR5xLev1nt4zbR59TqCuOcNVxxY1btXcABBBIkGPCsmcOHECNKnEixosWLGDNq3MjxCIRiHV0wIIAAQQKAIVX/MDhQsqQElBUFNFCAjUWBli0dGGSEyUQbn2xKOOI5IigAo0V/pmBQIEIBgigg4MS5MynQoz1FBEWKtatVrVuzel2h4GlTflGntnzGFexYrErdckXaiGjbEv6aEltxc+qbFHfD2hUr+GvXuIfFmmD6NEJVEg1Y4oQJtC3ixDwtZzWqWfGJBksajmhA0iTllCk+ikbNurXr17Bjy55Nu7bt20HkKGCwOiWDBAeC63S4B1vvFAIIBF+e4DEuAQsISCdHI/Ly5ad1QZBeQLrzMssRLFdgDKF0AgUUybB+/YB6XiO7Sz9+QkAE8cEREPh+y8B5hjbYtxxU6kDQAH3I7XEgnG4MNujggxBGCAVvt2XhwIUK8JfEIX3YYsCFB2CoRwEJJEQAgkM0ANyFLL7HgwElxphdGhCwCKIDLu4QXYwEUEeJAAnc6EACOeowAI8n1TKAjQ74uIIAo9Bnn4kRoDgElEEmQIULNWY54wkMjAKSLQq+IMCQQwZp5UVdZpnkbBC4OeSXqCXnJpG1qahQc7c1wAADGkoo6KCEFrpCCAA7AAAAAAAAAAAA\" style=\"position:absolute;left:300px;top:300px;width:400px;height:400px\">"

			//$("div[name='group_0']").append(loadingImg);
        	
        	
            //setTimeout(function(){new getInstruction();}, refresh_rate);
            //$("#welcomeScreen").fadeTo(1000,0,function(){$(this).remove();});
        });
        
        
        
        /*$.playground().bind('click',function(e)
        {
        	var xx = e.pageX || e.screenX;
	        var yy = e.pageY || e.screenY;
	        var aneventt = new jQuery.Event('mousedown');
	        aneventt.pageX = xx;
	        aneventt.pageY = yy;
	        
	        $('*').trigger(aneventt);
        });*/
        
                
        function addPinchUndo(cameraX, cameraY, cameraZoom){
        	/*var hisLength = camera.getHistory().length;
        	
        	var createHistory = false;
        	if(hisLength==0){
        		createHistory = true;
        	}
        	else{
	        	try{
	        		if(typeof(camera.getHistory()[hisLength-1].length)=="undefined" || typeof(camera.getHistory()[hisLength-1])=="function"){
	        			createHistory = true;
	        		}
	        	}
	        	catch(err){
	        		createHistory = true;
	        	}
        	}
        	
        	createHistory = true;*/
        	
        	//if(!lastBackPinch){
        		if(backStack.length==0 || (backStack.length>0 && !backStack[backStack.length-1].isPinch())){
	        		//lastBackPinch = true;
		        	var backFunc = new function(){
		        		this.isSwipe = function(){
				    		return false;
				    	}
				    	this.isPinch = function(){
				    		return true;
				    	}
						this.execBack = function(){
							backFunction(camera.getCamera(), cameraX, cameraY, cameraZoom);
						}
					};
					backStack.push(backFunc);
				}
			//}
        	
        	
        	/*if(createHistory){
	        	var newHistory = new Array();
	    		//newHistory["x"] = -(camera.getX()-cameraX);
	    		//newHistory["y"] = -(camera.getY()-cameraY);
	    		
	    		newHistory["x"] = -cameraX;
	    		newHistory["y"] = -cameraY;
	    		newHistory["s"] = cameraZoom;
	    		
	    		
	            camera.getHistory().push(newHistory);
	            
	            
	            var backFunction = function(){
	            	camera.undoCamera();
	            };
	            backStack.push(backFunction);
	            
	            //$("div[id='back']").unbind("click");
	        
	            //$("div[id='back']").bind("click", function(e){
	            //	e.preventDefault();
	            //	e.stopPropagation();
	                            
	            //    $("div[id='back']").unbind("click");
	            	
	            //	camera.undoCamera();
	            //});
            }*/
        }
        
    	
    	var timer500 = null;
    	
    	var pinchEvent = "";
    	var endPinchEvent = "";
        var moveEvent = "";
        var pinchNotStop = true;
        var notStopTimer = true;
        
        if(checkIfMobile()){
        	pinchEvent = 'touchstart';
        	endPinchEvent = 'touchend';
        	moveEvent = "touchmove";
        }
        else{
            //event_type[preemptiveLocalReferenceId]='click';
            pinchEvent = 'mousedown';
            endPinchEvent = 'mouseup';
            moveEvent = "mousemove";
        }
        
        
    	
    	$(window).bind(pinchEvent, function(e){
            function pinchEndEvent(moved, cameraX, cameraY, cameraZoom){
		    	$(window).unbind("touchcancel");
		    	$(window).unbind(endPinchEvent);
		    	$(window).unbind(moveEvent);
		    	
		    	pinchNotStop = false;
		    	
		    	if(!checkIfNull(timer500)){
		    	    clearTimeout(timer500);
	    	    }
	    	    setTimeout(function(){
	    	    	$(window).data("pinching", null);
	    	    }, 1000);
		    	
		    	notStopTimer = false;
		    	
	    		
            	if(moved){
                	notStopTimer = false;
                	
                	setTimeout(function(){
	                	addPinchUndo(cameraX, cameraY, cameraZoom);
			            
			            resumePreloadBands();
		            
		            }, 100);
            	}
		    }
		    
		    if(e.target==$("div[id='playground']")[0]){
		        return;
	        }

            pinchNotStop = true;
    		
    		var originalEvent = e.originalEvent;

    		
    		if(checkIfNull(originalEvent.touches)|| originalEvent.touches.length==1){
    			if(!checkIfNull($(window).data("pinching"))){
    				$(window).trigger(endPinchEvent);
    				$(window).unbind(endPinchEvent);
    				return;
    			}
    			
    			if(!checkIfNull($(document).data("draggingElement"))){
					return;
				}
    			
    			
    			notStopTimer = true;
    			
    			var x1;
    			var y1;
    			
    			if(checkIfNull(originalEvent.touches)){
    				x1 = e.pageX || e.screenX;
	                y1 = e.pageY || e.screenY;
    			}
    			else{
    				x1 = originalEvent.touches[0].pageX || originalEvent.touches[0].screenX;
    				y1 = originalEvent.touches[0].pageY || originalEvent.touches[0].screenY;
    			}
                
                var meX = x1;
                var meY = y1;
                    
                    
	            var cameraZoom = camera.getScale();
                    
                var currentZoom = camera.getScale();
                
                var moved = false;
                var cameraX = camera.getX();
                var cameraY = camera.getY();

				
				var x_dist = 0;
	            var y_dist = 0;
	            
	            
				
				var xTransform = (x1-$("div[name='group_00']").offset().left)/$("div[name='group_0']").data("scale")/$("div[name='group_00']").data("scale");
				var yTransform = (y1-$("div[name='group_00']").offset().top)/$("div[name='group_0']").data("scale")/$("div[name='group_00']").data("scale");
				
				
				
				
				
				timer500 = setTimeout(function(){
			    	if(!checkIfNull($(document).data("draggingElement"))){
			    		clearTimeout(timer500);
    					return;
    				}
    				$(window).data("pinching", "1");
    				
    				e.preventDefault();
	    			e.stopPropagation();
	    			
	    			
                    
	    			$(window).bind(moveEvent, function(me){
	    				me.preventDefault();
	    			    me.stopPropagation();
	    			    if(checkIfNull(me.originalEvent.touches)){
	    			    	meX = me.pageX || me.screenX;
                            meY = me.pageY || me.screenY;
    			    	}
    			    	else{
			    	        meX = me.originalEvent.touches[0].pageX || me.originalEvent.touches[0].screenX;
                            meY = me.originalEvent.touches[0].pageY || me.originalEvent.touches[0].screenY;
                        }
		    	    });
	    			
	                                    
                    if(notStopTimer){
                    	moved = true;
                    	pausePreloadBands();
                    	
                    	
                    	setTimeout(function pinchExpand(){
	                    	if(notStopTimer){
	                    		if(!checkIfNull(meX)){
	                    			
	                    			//xTransform = (meX-$("div[name='group_00']").offset().left)/$("div[name='group_0']").data("scale")/$("div[name='group_00']").data("scale");
				                    //yTransform = (meY-$("div[name='group_00']").offset().top)/$("div[name='group_0']").data("scale")/$("div[name='group_00']").data("scale");
	                    			
	                    			
	                    			
	                    			
	                    			//var increment = 0.05*$("div[name='group_00']").data("scale");
	                    			var increment = 0.03*$("div[name='group_00']").data("scale");
	                    			
	                    			currentZoom += increment;
	                    			
	                    			var smoothFactor = 10;
		                            //x_dist = (meX-x1)/$("div[name='group_0']").data("scale")/smoothFactor;
		                            //y_dist = (meY-y1)/$("div[name='group_0']").data("scale")/smoothFactor;
		                            x_dist = (meX-x1)/$("div[name='group_0']").data("scale");
		                            y_dist = (meY-y1)/$("div[name='group_0']").data("scale");
		                            
		                            
		                            setTimeout(function(){
			                            $("div[name='group_00']").scale(currentZoom);
			                            $("div[name='group_00']").data("scale", currentZoom);
			                            $("div[name='group_00']").data("width", currentZoom*$("div[name='group_00']").data("original_width"));
			                            $("div[name='group_00']").data("height", currentZoom*$("div[name='group_00']").data("original_height"));
			                            
				                        $("div[name='group_00']").css({
										    left: cameraX-((xTransform-500)*(camera.getScale()-cameraZoom))+x_dist,
										    top: cameraY-((yTransform-500)*(camera.getScale()-cameraZoom))+y_dist
										});
				                    	new pinchExpand();
		                            }, 0);
		                            
		                            
		                            /*setTimeout(function(){
		                            	if(speedUp){
		                            		//$("div[name='group_00']").backToNormalTransform(currentZoom, camera.getX()-((xTransform-500)*increment)+x_dist, camera.getY()-((yTransform-500)*increment)+y_dist);
		                            		$("div[name='group_00']").backToNormalTransform(currentZoom, cameraX-((xTransform-500)*(camera.getScale()-cameraZoom))+x_dist, cameraY-((yTransform-500)*(camera.getScale()-cameraZoom))+y_dist);
	                            		}
	                            		else{
				                          $("div[name='group_00']").css({
										      left: camera.getX()-((xTransform-500)*increment)+x_dist,
										      top: camera.getY()-((yTransform-500)*increment)+y_dist
										  });
				                        }
	                            	}, 0);
	                            	
	                            	setTimeout(function(){
	                            		if(!speedUp){
	                            			$("div[name='group_00']").scale(currentZoom);
                            			}
                            			$("div[name='group_00']").data("scale", currentZoom);
			                            $("div[name='group_00']").data("width", currentZoom*$("div[name='group_00']").data("original_width"));
			                            $("div[name='group_00']").data("height", currentZoom*$("div[name='group_00']").data("original_height"));
                            			
                            			setTimeout(function(){
                            				preXTransform = xTransform;
                            				preYTransform = yTransform;
			                    			new pinchExpand();
			                    		}, 0);
                            		}, 0);*/
		                            
		                            //$("div[name='group_00']").scale(currentZoom);
						            //$("div[name='group_00']").data("scale", currentZoom);
		                            //$("div[name='group_00']").data("width", currentZoom*$("div[name='group_00']").data("original_width"));
		                            //$("div[name='group_00']").data("height", currentZoom*$("div[name='group_00']").data("original_height"));
		                            
								    
								    
								    //$("div[name='group_00']").css({
								    //    left: camera.getX()-((xTransform-500)*increment)+x_dist,
								    //    top: camera.getY()-((yTransform-500)*increment)+y_dist
								    //});
								    								    
							    }
	                            
	                    		//setTimeout(function(){
	                    		//	new pinchExpand();
	                    		//}, 100);
	                    		
	                    	}
	                    }, 0);
                	}
			    }, 1000);
			    
			    
			    $(window).bind("touchcancel", function(tc){
			    	pinchEndEvent(moved, cameraX, cameraY, cameraZoom);
                });
			    
			    $(window).bind(endPinchEvent, function(te){
			    	pinchEndEvent(moved, cameraX, cameraY, cameraZoom);
                });
    		}
    		else{
    			if(!checkIfNull(originalEvent.touches)){
		    		if(originalEvent.touches.length==2){
		    			//$(window).trigger(endPinchEvent);
		    			//$(window).unbind(endPinchEvent);
		    			if(!checkIfNull($(window).data("pinching"))){
		    				$(window).trigger(endPinchEvent);
		    				return;
		    			}
		    			if(!checkIfNull($(document).data("draggingElement"))){
							return;
						}
		    			
		    			notStopTimer = false;

			    	    $(window).unbind(endPinchEvent);
		    			
			    	    setTimeout(function(){
			    	    	$(window).data("pinching", "1");
			    	    }, 100);
		    			
		    			if(!checkIfNull(timer500)){
		    			    clearTimeout(timer500);
	    			    }
	    			    
	    			    
		    			
		    			e.preventDefault();
		    			e.stopPropagation();
		    			
		    			var x1;
		                var y1;
		                var x2;
		                var y2;
		                
                    	x1 = e.originalEvent.touches[0].pageX || e.originalEvent.touches[0].screenX;
                        y1 = e.originalEvent.touches[0].pageY || e.originalEvent.touches[0].screenY;
		                x2 = e.originalEvent.touches[1].pageX || e.originalEvent.touches[1].screenX;
                        y2 = e.originalEvent.touches[1].pageY || e.originalEvent.touches[1].screenY;  		                
                        
                        var mX1 = (x1+x2)/2;
	                    var mY1 = (y1+y2)/2;
	                    
	                    var firstMX = mX1;
	                    var firstMY = mY1;
	                    var lastMX = mX1;
	                    var lastMY = mY1;
	                    
	                    var cameraX = camera.getX();
	                    var cameraY = camera.getY();
	                    var fingerZoom = Math.sqrt(Math.pow(x1-x2, 2)+Math.pow(y1-y2, 2));
	                    
	                    var cameraZoom = camera.getScale();
	                    
	                    var currentZoom = camera.getScale();
	                    
	                    
	                    var x_dist = 0;
	                    var y_dist = 0;
	                    
	                    var moved = false;
	                     
	                    
	                    var xTransform = (mX1-$("div[name='group_00']").offset().left)/$("div[name='group_0']").data("scale")/$("div[name='group_00']").data("scale");
				        var yTransform = (mY1-$("div[name='group_00']").offset().top)/$("div[name='group_0']").data("scale")/$("div[name='group_00']").data("scale");
				        var moveNotLock = true;
				        
				        var tmx1 = x1;
		                var tmy1 = y1;
		                var tmx2 = x2;
		                var tmy2 = y2;
                        
                        $(window).bind(moveEvent, function(tm){
                        	tm.preventDefault();
                        	tm.stopPropagation();
                        	
                        	if(!moveNotLock){
                        		//return;
                        	}
                        	moveNotLock = false;
                        	
                        	
                        	moved = true;
                        	pausePreloadBands();
                        	
                        	
			                tmx1 = tm.originalEvent.touches[0].pageX || tm.originalEvent.touches[0].screenX;
	                        tmy1 = tm.originalEvent.touches[0].pageY || tm.originalEvent.touches[0].screenY;
			                tmx2 = tm.originalEvent.touches[1].pageX || tm.originalEvent.touches[1].screenX;
	                        tmy2 = tm.originalEvent.touches[1].pageY || tm.originalEvent.touches[1].screenY;  	
	                        
                        });
	                      
                        timer500 = setTimeout(function pinchExpand2(){  
	                        var mX2 = (tmx1+tmx2)/2;
	                        var mY2 = (tmy1+tmy2)/2;
	                        
	                        //var ratio = calAccScale($("div[name='group_00']").parent());
	                        var smoothFactor = 50;
	                        //x_dist = (mX2-firstMX)/$("div[name='group_0']").data("scale")/smoothFactor;
	                        //y_dist = (mY2-firstMY)/$("div[name='group_0']").data("scale")/smoothFactor;
	                        x_dist = (mX2-firstMX)/$("div[name='group_0']").data("scale");
	                        y_dist = (mY2-firstMY)/$("div[name='group_0']").data("scale");
	                        
	                        //x_dist = 0;
	                        //y_dist = 0;
	                        
	                        //var increment = 0.05*$("div[name='group_00']").data("scale");
	                        //currentZoom += increment;
	                        
	                        //currentZoom = (Math.sqrt(Math.pow(tmx1-tmx2, 2)+Math.pow(tmy1-tmy2, 2)))/fingerZoom*cameraZoom;
	                        
	                        
	                        //var increment = 0.01*(Math.sqrt(Math.pow(tmx1-tmx2, 2)+Math.pow(tmy1-tmy2, 2))-fingerZoom)/fingerZoom*cameraZoom;
	                        //var increment = (Math.sqrt(Math.pow(tmx1-tmx2, 2)+Math.pow(tmy1-tmy2, 2))-fingerZoom)/$("div[name='group_00']").data("scale")/$("div[name='group_0']").data("scale")/1000*cameraZoom*camera.getScale()/20;
	                        var increment = (Math.sqrt(Math.pow(tmx1-tmx2, 2)+Math.pow(tmy1-tmy2, 2))-fingerZoom)/fingerZoom;
	                        //currentZoom = increment+camera.getScale();
	                        
	                        currentZoom = ((Math.sqrt(Math.pow(tmx1-tmx2, 2)+Math.pow(tmy1-tmy2, 2))-fingerZoom)/fingerZoom*cameraZoom)+cameraZoom;
	                        
	                        if(currentZoom<1){
	                        	currentZoom = 1;
	                        }
	                        
	                        
	                        setTimeout(function(){
	                            $("div[name='group_00']").scale(currentZoom);
	                            $("div[name='group_00']").data("scale", currentZoom);
	                            $("div[name='group_00']").data("width", currentZoom*$("div[name='group_00']").data("original_width"));
	                            $("div[name='group_00']").data("height", currentZoom*$("div[name='group_00']").data("original_height"));
	                            
		                        $("div[name='group_00']").css({
								    left: cameraX-((xTransform-500)*(camera.getScale()-cameraZoom))+x_dist,
								    top: cameraY-((yTransform-500)*(camera.getScale()-cameraZoom))+y_dist
								});
								if(pinchNotStop){
								    new pinchExpand2();
							    }
                            }, 0);
	                        
	                        
	                        /*setTimeout(function(){
                            	if(speedUp){
                            		$("div[name='group_00']").backToNormalTransform(currentZoom, camera.getX()-((xTransform-500)*increment)+x_dist, camera.getY()-((yTransform-500)*increment)+y_dist);
                            		moveNotLock = true;
                        		}
                        		else{
		                          $("div[name='group_00']").css({
								      left: camera.getX()-((xTransform-500)*increment)+x_dist,
								      top: camera.getY()-((yTransform-500)*increment)+y_dist
								  });
		                        }
		                        
		                        
                        	}, 0);
                        	
                        	setTimeout(function(){
	                        		if(!speedUp){
	                        			$("div[name='group_00']").scale(currentZoom);
	                        			moveNotLock = true;
	                    			}
	                    			$("div[name='group_00']").data("scale", currentZoom);
		                            $("div[name='group_00']").data("width", currentZoom*$("div[name='group_00']").data("original_width"));
		                            $("div[name='group_00']").data("height", currentZoom*$("div[name='group_00']").data("original_height"));
	                    		}, 0);*/
	                        
	                        
                    		
                    		
	                        /*$("div[name='group_00']").scale(currentZoom);
	                        $("div[name='group_00']").data("scale", currentZoom);
                            $("div[name='group_00']").data("width", currentZoom*$("div[name='group_00']").data("original_width"));
                            $("div[name='group_00']").data("height", currentZoom*$("div[name='group_00']").data("original_height"));
	                        
	                        
	                        $("div[name='group_00']").css({
						        left: camera.getX()-((xTransform-500)*increment)+x_dist,
						        top: camera.getY()-((yTransform-500)*increment)+y_dist
						    });*/

                        }, 0);
                        
                        $(window).bind("touchcancel", function(te){
                        	pinchEndEvent(moved, cameraX, cameraY, cameraZoom);
                    	});
                        
                        $(window).bind(endPinchEvent, function(te){
                        	pinchEndEvent(moved, cameraX, cameraY, cameraZoom);
                    	});
		    		}
	    		}
    		}
    	});
        
        
        /*$.playground().bind(eventTypeClick,function(e)
        {
            var x = e.pageX;
            var y = e.pageY;
            
            var element = document.elementFromPoint(x, y);
            //alert(element.getAttribute("id"));
            if(element!=null && typeof element !="undefined")
            {
            	if($(element).data("hasClick")!=null){
            		var anevent = new jQuery.Event(eventTypeClick);
                    anevent.pageX = x;
                    anevent.pageY = y;
                    $(element).data("visibility", $(element).css("visibility"));
                    $(element).css("visibility", "hidden");
                    pointerEvents.push(element);
                    $(element).trigger(anevent);
                    while(pointerEvents.length>0){
                        var popElement = pointerEvents.pop();
                        $(popElement).css("visibility", $(popElement).data("visibility"));
                    }
            	}
            	else{
            		if(element.getAttribute("id")=='playground' || element.getAttribute("id")=='startbutton' || element.getAttribute("id")=='sceengraph'){
            			while(pointerEvents.length>0){
	                        var popElement = pointerEvents.pop();
	                        $(popElement).css("visibility", $(popElement).data("visibility"));
	                    }
            		}
            		else{
            			if($(element).data("image")==null){
	            			var anevent = new jQuery.Event(eventTypeClick);
		                    anevent.pageX = x;
		                    anevent.pageY = y;
		                    $(element).data("visibility", $(element).css("visibility"));
		                    $(element).css("visibility", "hidden");
		                    pointerEvents.push(element);
		                    $.playground().trigger(anevent);
	                    }
	                    else{
	                    	while(pointerEvents.length>0){
		                        var popElement = pointerEvents.pop();
		                        $(popElement).css("visibility", $(popElement).data("visibility"));
		                    }
	                    }
            		}
            	}
            }
        });*/
        
        
        /*$.playground().bind('click',function(e)
        {
            var x = e.pageX;
            var y = e.pageY;
            var element = document.elementFromPoint(x, y);
            //alert(element.getAttribute("id"));
            if(element!=null && typeof element !="undefined")
            {
                if(element.getAttribute("id")!='playground' && element.getAttribute("id")!='startbutton' && element.getAttribute("id")!='sceengraph')
                {
                    var anevent = new jQuery.Event('click');
                    anevent.pageX = x;
                    anevent.pageY = y;
                    $(element).data("visibility", $(element).css("visibility"));
                    $(element).css("visibility", "hidden");
                    pointerEvents.push(element);
                    $.playground().trigger(anevent);
                }
                if(element.getAttribute("id")=='playground' || element.getAttribute("id")=='startbutton' || element.getAttribute("id")=='sceengraph')
                {
                    var anevent = new jQuery.Event('click');
                    anevent.pageX = x;
                    anevent.pageY = y;
                    while(pointerEvents.length>0)
                    {
                        var popElement = pointerEvents.pop();
                        $(popElement).css("visibility", $(popElement).data("visibility"));
                        
                        if(pointerEvents.length!=1){
                            var popElement = pointerEvents.pop();
                            //anevent.stopPropagation();
                            //$(popElement).triggerClick(anevent);
                            $(popElement).css("visibility", $(popElement).data("visibility"));
                        }
                        else{
                            var popElement = pointerEvents.pop();
                            $(popElement).css("visibility", $(popElement).data("visibility"));
                        }
                    }
                }
            }
        });*/
    //})
    
    function backFunction(entity, x, y, finalScale){
    	pausePreloadBands();
    	//alert(entity.attr("name").substring(6, entity.attr("name").length));
    	lastBackPinch = false;
    	
    	var time_allowed = 700;
        
        var base_ratio = entity.data("scale");
        var ratio = finalScale;
        var start = new Date().getTime();
        
        
		var x_orig = parseFloat(entity.css("left"));
		var y_orig = parseFloat(entity.css("top"));
		
		var x_dist = x-x_orig;
		var y_dist = y-y_orig;
		
      
        setTimeout(function pinchItem(){
        	
            var elapsed = new Date().getTime()-start;
            if(elapsed<time_allowed)
            {
          	    setTimeout(function(){
          	  	  if(speedUp){
      	    		  entity.backToNormalTransform(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), x_orig+((elapsed/time_allowed)*x_dist), y_orig+((elapsed/time_allowed)*y_dist));
                
                  }
                  else{
                      entity.css({
					      left: x_orig+((elapsed/time_allowed)*x_dist),
					      top: y_orig+((elapsed/time_allowed)*y_dist)
					  });
                  }
                }, 0);
              
              
              
                setTimeout(function(){
                    if(ratio!=base_ratio){
                  	    if(!speedUp){
                            entity.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
                        }
                  	
                      
                      
                        entity.data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
                        entity.data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*entity.data("original_width"));
                        entity.data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*entity.data("original_height"));
                    }
                    
                    new pinchItem();
                    
                }, 0);
            }
            else
            {
          	    setTimeout(function(){
          	  	    if(speedUp){
  	        	        entity.backToNormalTransform(ratio, x_orig+x_dist, y_orig+y_dist);
        	        }
        	        else{
        	      	    entity.css({
					        left: x_orig+x_dist,
					        top: y_orig+y_dist
				    	});
                	}
                }, 0);  
              
              
                setTimeout(function(){
              	    if(ratio!=base_ratio){
              	  	    if(!speedUp){
              	            entity.scale(ratio);
          	            }
                  	  
                      
                        entity.data("scale", ratio);
                        entity.data("width", ratio*entity.data("original_width"));
                        entity.data("height", ratio*entity.data("original_height"));
                    }
                }, 0);
                resumePreloadBands();
            }
        }, 0);
    }
    
    /*function findParentAndZoom(entity, x, y, finalZoom){
    	var currentEntity = entity;
    	if(checkIfNull(currentEntity)){
    		return;
    	}
    	
    	currentEntity = currentEntity.parent();
    	
    	var entityToZoom = null;
    	
    	while(true){
    		var currentEntityString = currentEntity.attr("id");
    		if(currentEntityString=="group_0"){
    			return;
    		}
    		else{
    		    var currentEntityLFId = currentEntity.attr("name").substring(6, currentEntity.attr("name").length);
    			
    			if(!checkIfNull(restraint_groups[currentEntityLFId])){
    				entityToZoom = currentEntity;
    				break;
    			}
    		}
    		currentEntity = currentEntity.parent();
		}
		
		entityToZoom = $(entityToZoom.find("div")[0]);
		
		//zoom here
		var time_allowed = 700;
		
             
        var base_ratio = entityToZoom.data("scale");
        //var ratio = zoom/origZoom/base_ratio*base_ratio;
        var ratio = finalZoom;
        //alert(base_ratio+" "+ratio+" "+zoom+" "+entity.data("scale"));
        var start = new Date().getTime();
        
        
		var x_orig = parseFloat(entityToZoom.css("left"));
		var y_orig = parseFloat(entityToZoom.css("top"));
		
		//alert(x_orig);
		
		//var x_dist = (-(x*base_ratio)-x_orig)*ratio;
		//var y_dist = (-(y*base_ratio)-y_orig)*ratio;
		
		//var x_dist = (-(x)-x_orig)-(x_orig*(ratio-base_ratio));
		//var y_dist = (-(y)-y_orig)-(y_orig*(ratio-base_ratio));
		
		
		
		var x_dist = (-(x*ratio)-(x_orig));
		var y_dist = (-(y*ratio)-(y_orig));
		
		
		
		
		//x_dist = x_dist+(x_dist*(ratio-base_ratio));
		//y_dist = y_dist+(y_dist*(base_ratio-base_ratio));
				
		
		lastBackPinch = false;
		var backFunc = new function(){
			backFunction(entityToZoom, x_orig, y_orig, base_ratio);
		};
		backStack.push(backFunc);
		
      
        setTimeout(function pinchItem(){
        	
            var elapsed = new Date().getTime()-start;
            if(elapsed<time_allowed)
            {
          	    setTimeout(function(){
          	  	  if(speedUp){
      	    		  entityToZoom.backToNormalTransform(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), x_orig+((elapsed/time_allowed)*x_dist), y_orig+((elapsed/time_allowed)*y_dist));
                
                  }
                  else{
                      entityToZoom.css({
					      left: x_orig+((elapsed/time_allowed)*x_dist),
					      top: y_orig+((elapsed/time_allowed)*y_dist)
					  });
                  }
                }, 0);
              
              
              
                setTimeout(function(){
                    if(ratio!=base_ratio){
                  	    if(!speedUp){
                            entityToZoom.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
                        }
                  	
                      
                      
                        entityToZoom.data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
                        entityToZoom.data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*entityToZoom.data("original_width"));
                        entityToZoom.data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*entityToZoom.data("original_height"));
                    }
                    
                    new pinchItem();
                    
                }, 0);
            }
            else
            {
          	    setTimeout(function(){
          	  	    if(speedUp){
	        	        entityToZoom.backToNormalTransform(ratio, x_orig+x_dist, y_orig+y_dist);
        	        }
        	        else{
        	      	    entityToZoom.css({
				            left: x_orig+x_dist,
				            top: y_orig+y_dist
				        });
                    }
                }, 0);  
              
              
              
                setTimeout(function(){
              	    if(ratio!=base_ratio){
              	  	    if(!speedUp){
              	            entityToZoom.scale(ratio);
          	            }
                  	  
                      
                        entityToZoom.data("scale", ratio);
                        entityToZoom.data("width", ratio*entityToZoom.data("original_width"));
                        entityToZoom.data("height", ratio*entityToZoom.data("original_height"));
                    }
                }, 0);
            }
        }, 0);
    }*/
    
    function globalCamera(){
    	//var history = new Array();
    	
    	//var previousDistX = null;
    	//var previousDistY = null;
    	//var previousScale = null;
    	
    	var cam = $("div[name='group_00']");
    	
    	this.getCamera = function(){
    		return cam;
    	}
    	
    	//this.removeLastHistory = function(){
    		
    		/*if(typeof(history[history.length-1])=="function"){
    			history.pop();
    			history.pop();
			}
			else{
				history.pop();
			}*/
			//if(checkIfNull(history[history.length-1])){
			//	history.pop();
			//	history.pop();
			//}
			//else{
			    //history.pop();
		    //}
			
			//history.pop();
    		
			/*if(!checkIfNull(history[history.length-1])){
    			if(typeof(history[history.length-1])=="function"){
    				history[history.length-1]();
    			}
    			else{
	    			$("div[id='back']").unbind("click");
	                        
		            $("div[id='back']").bind("click", function(e){
		            	
		            	e.stopPropagation();
		                            
		                $("div[id='back']").unbind("click");
		                                
		                
		                //setTimeout(function(){new executeTimespot(timespotId, parentTimespot);}, 0);
		                
		                
		                //setTimeout(function(){
						//	new getInstruction("/sendEscapeSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?r="+Math.floor(Math.random()*9223372036854775807));
						//}, 0);
		            	
		            	
		            	camera.undoCamera();
		            });
	            }
    		}*/
    	//}
    	
    	//this.getLastHistory = function(){
    	//	return history[history.length-1];
    	//}
    	
    	//this.getHistory = function(){
    	//	return history;
    	//}
    	
    	//this.hasUndo = function(){
    		//return !checkIfNull(previousDistX);
    		
    		//if(checkIfNull(history[history.length-1])){
    		//	return false;
    		//}
    		//return true;
    	//}
    	
    	this.getScale = function(){
    		return cam.data("scale");
    	}
    	
    	this.getX = function(){
    		return parseFloat(cam.css("left"));
    	}
    	
    	this.getY = function(){
    		return parseFloat(cam.css("top"));
    	}
    	
    	/*this.undoCamera = function(){
    		var time_allowed = 700;
    		//if(!this.hasUndo()){
    		//	return;
    		//}
    		
    		var cuurentHistory;
    		
    		//if(checkIfNull(history[history.length-1])){
    		//	cuurentHistory = history[history.length-2];
    		//}
    		//else{
    			cuurentHistory = history[history.length-1];
    		//}
    		
    		
    		//if(!checkIfNull(history[history.length-1])){
    			pausePreloadBands();
    			
	    		var start = new Date().getTime();
										                          
	            var base_ratio = this.getScale();
	            
	            //var cuurentHistory = history[history.length-1];
	            var ratio = cuurentHistory["s"];
	            
	    		var x_orig = parseFloat(cam.css("left"));
				var y_orig = parseFloat(cam.css("top"));
				
				
				var x_dist = cuurentHistory["x"]-x_orig;
				var y_dist = cuurentHistory["y"]-y_orig;
				
				
				
				this.removeLastHistory();
				
				
				//setTimeout(function(){
				//	new getInstruction("/sendEscapeSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?r="+Math.floor(Math.random()*9223372036854775807));
				//}, 0);
				
	          
				//alert(camera.data("scale")+" "+ratio+" "+x_dist+" "+y_dist);
	          
	            setTimeout(function pinchItem(){
	                var elapsed = new Date().getTime()-start;
	                if(elapsed<time_allowed)
	                {
	              	    setTimeout(function(){
	              	  	  if(speedUp){
	          	    		  cam.backToNormalTransform(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), x_orig+((elapsed/time_allowed)*x_dist), y_orig+((elapsed/time_allowed)*y_dist));
	                    
	                      }
	                      else{
	                          cam.css({
							      left: x_orig+((elapsed/time_allowed)*x_dist),
							      top: y_orig+((elapsed/time_allowed)*y_dist)
							  });
	                      }
	                    }, 0);
	                  
	                  
	                  
	                    setTimeout(function(){
	                        if(ratio!=base_ratio){
	                      	    if(!speedUp){
	                                cam.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
	                            }
	                      	
	                          
	                          
	                            cam.data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
	                            cam.data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*cam.data("original_width"));
	                            cam.data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*cam.data("original_height"));
	                        }
	                        
	                        new pinchItem();
	                        
	                    }, 0);
	                }
	                else
	                {
	              	    setTimeout(function(){
	              	  	    if(speedUp){
	      	        	        cam.backToNormalTransform(ratio, x_orig+x_dist, y_orig+y_dist);
	  	        	        }
	  	        	        else{
	  	        	      	    cam.css({
							        left: x_orig+x_dist,
							        top: y_orig+y_dist
							    });
	                        }
	                    }, 0);  
	                  
	                  
	                  
	                    setTimeout(function(){
	                  	    if(ratio!=base_ratio){
	                  	  	    if(!speedUp){
	                  	            cam.scale(ratio);
	              	            }
	                      	  
	                          
	                            cam.data("scale", ratio);
	                            cam.data("width", ratio*cam.data("original_width"));
	                            cam.data("height", ratio*cam.data("original_height"));
	                            
	                        }
	                    }, 0);
	                    
	                    resumePreloadBands();
	                }
	            }, 0);
	            
            //}
    	}*/
    	
    	/*this.centerIntoLF = function(entityLocalReferenceId){
    		
	            if(globals[entityLocalReferenceId]!=null)
	            {
	                entityLocalReferenceId = globals[entityLocalReferenceId];
	            }
	            var occupyPercentage = 1;
	            	
	            
	            var time_allowed = 700;
	            
	    		
	            
	        	if($("div[name='group_"+entityLocalReferenceId+"']").length>0)
	              {
	              	 
              	
                  var currentEntity = $("div[name='group_"+entityLocalReferenceId+"']");
                  
                  var accumulatedScale = calAccScale(currentEntity);
                  var accumulatedScale2 = accumulatedScale*$("div[name='group_0']").data("scale");
                  
                  
                  var entityToZoom = cam;
                  
                  var tempElementX = parseFloat(currentEntity.offset().left);
                  var tempElementY = parseFloat(currentEntity.offset().top);
                  
                  var width = currentEntity.data("original_width")*accumulatedScale2;
                  var height = currentEntity.data("original_height")*accumulatedScale2;
                  
                  
                  
                  
                  var factor = ($("div[name='group_0']").data("scale")*1000*occupyPercentage)/(1000*accumulatedScale2);
                  
                  
                  //var ratio = 1/accumulatedScale;
                  var base_ratio = entityToZoom.data("scale");
                  var ratio = factor*base_ratio;
                   
                  //entityToZoom.scale(1/accumulatedScale);
                   
                  var final_ratio = ratio/base_ratio;
                 
                                                 
                  		                                  
                  var x_orig = parseFloat(entityToZoom.css("left"));
                  var y_orig = parseFloat(entityToZoom.css("top"));
                  
                  
                  
                  var x_dist = (($("div[name='group_0']").offset().left+(1000*$("div[name='group_0']").data("scale")/2))-(width/2)-tempElementX)/(accumulatedScale2/accumulatedScale);
                  x_dist = (x_dist)*(final_ratio)+(x_orig*(ratio-base_ratio)/base_ratio);
                  
                  var y_dist = (($("div[name='group_0']").offset().top+(1000*$("div[name='group_0']").data("scale")/2))-(height/2)-tempElementY)/(accumulatedScale2/accumulatedScale);
                  y_dist = (y_dist)*(final_ratio)+(y_orig*(ratio-base_ratio)/base_ratio);
                  
                  
                  
		            
		            lastBackPinch = false;
		            var backFunc = new function(){
		            	backFunction(cam, x_orig, y_orig, base_ratio);
		            };
		            backStack.push(backFunc);
                  
                  
                  
                  var start = new Date().getTime();
                  
                  var hasMoreTime = true;
                  
                  
                  setTimeout(function moveItem4(){
                      if(hasMoreTime){
                          var elapsed = new Date().getTime()-start;
                          if(elapsed<time_allowed)
                          {
                          	  
                              
                          	  setTimeout(function(){
	                          	  	if(speedUp){
	                      	    		entityToZoom.backToNormalTransform(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), x_orig+((elapsed/time_allowed)*x_dist), y_orig+((elapsed/time_allowed)*y_dist));
	                                
	                                }
	                                else{
	                                    entityToZoom.css({
										    left: x_orig+((elapsed/time_allowed)*x_dist),
										    top: y_orig+((elapsed/time_allowed)*y_dist)
										});
	                                }
                          	  	
                          	  	
                                  //entityToZoom.css("left", x_orig+((elapsed/time_allowed)*x_dist));
                                  //entityToZoom.css("top", y_orig+((elapsed/time_allowed)*y_dist));
                              }, 0);
                              
                              
                              
                              setTimeout(function(){
                                  if(ratio!=base_ratio){
	                                  	if(!speedUp){
	                                        entityToZoom.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
	                                    }
                                  	
                                      //entityToZoom.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
                                      
                                      entityToZoom.data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
                                      entityToZoom.data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*entityToZoom.data("original_width"));
                                      entityToZoom.data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*entityToZoom.data("original_height"));
                                  }
                                 
					              new moveItem4();
                                      
                                  
                              }, 0);
                          }
                          else
                          {
                          	  hasMoreTime = false;
                          	  setTimeout(function(){
	                          	  	if(speedUp){
	                  	        	    entityToZoom.backToNormalTransform(ratio, x_orig+x_dist, y_orig+y_dist);
	              	        	    }
	              	        	    else{
	                                    entityToZoom.css({
										    left: x_orig+x_dist,
										    top: y_orig+y_dist
										});
	              	        	    }
                          	  	
                          	  	
                                  //entityToZoom.css("left", x_orig+x_dist);
                                  //entityToZoom.css("top", y_orig+y_dist);
                              }, 0);  
                              
                              
                              
                              setTimeout(function(){
                              	  if(ratio!=base_ratio){
		                          	  	if(!speedUp){
		                          	        entityToZoom.scale(ratio);
		                      	        }
                              	  	
                                  	  //entityToZoom.scale(ratio);
                                      
                                      entityToZoom.data("scale", ratio);
                                      entityToZoom.data("width", ratio*entityToZoom.data("original_width"));
                                      entityToZoom.data("height", ratio*entityToZoom.data("original_height"));
                                  }
                                  
                                  
                              }, 0);
                              
                          }
                      }
                  }, 0);
            }       
            
    	}*/
    	
    	/*this.moveCamera = function(ratio, x_dist, y_dist, enableHistory, enableTimeLimit){
    		//previousDistX = -x_dist;
    		//previousDistY = -y_dist;
    		
    		pausePreloadBands();
    		
    		if(enableHistory){
	    		//var newHistory = new Array();
	    		//newHistory["x"] = -this.getX();
	    		//newHistory["y"] = -this.getY();
	    		//newHistory["s"] = this.getScale();
	            //history.push(newHistory);
	            
	            lastBackPinch = false;
	            var backFunc = new function(){
	            	backFunction(cam, this.getX(), this.getY(), this.getScale());
	            };
	            backStack.push(backFunc);
            }
            
            var time_allowed = 700;
            if(!enableTimeLimit){
            	time_allowed = 0;
            }
    		
                 
            var base_ratio = this.getScale();
            var start = new Date().getTime();
            
            
    		var x_orig = parseFloat(cam.css("left"));
			var y_orig = parseFloat(cam.css("top"));
			
			
			//$("div[id='back']").unbind("click");
                        
            //$("div[id='back']").bind("click", function(e){
            	
            	//e.stopPropagation();
                            
                //$("div[id='back']").unbind("click");
                                
                
                //setTimeout(function(){new executeTimespot(timespotId, parentTimespot);}, 0);
                
                
                //setTimeout(function(){
				//	new getInstruction("/sendEscapeSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?r="+Math.floor(Math.random()*9223372036854775807));
				//}, 0);
            	
            	
            	//camera.undoCamera();
            //});
			
			
			
          
			//alert(camera.data("scale")+" "+ratio+" "+x_dist+" "+y_dist);
          
            setTimeout(function pinchItem(){
            	
                var elapsed = new Date().getTime()-start;
                if(elapsed<time_allowed)
                {
              	    setTimeout(function(){
              	  	  if(speedUp){
          	    		  cam.backToNormalTransform(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), x_orig+((elapsed/time_allowed)*x_dist), y_orig+((elapsed/time_allowed)*y_dist));
                    
                      }
                      else{
                          cam.css({
						      left: x_orig+((elapsed/time_allowed)*x_dist),
						      top: y_orig+((elapsed/time_allowed)*y_dist)
						  });
                      }
                    }, 0);
                  
                  
                  
                    setTimeout(function(){
                        if(ratio!=base_ratio){
                      	    if(!speedUp){
                                cam.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
                            }
                      	
                          
                          
                            cam.data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
                            cam.data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*cam.data("original_width"));
                            cam.data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*cam.data("original_height"));
                        }
                        
                        new pinchItem();
                        
                    }, 0);
                }
                else
                {
              	    setTimeout(function(){
              	  	    if(speedUp){
      	        	        cam.backToNormalTransform(ratio, x_orig+x_dist, y_orig+y_dist);
  	        	        }
  	        	        else{
  	        	      	    cam.css({
						        left: x_orig+x_dist,
						        top: y_orig+y_dist
						    });
                        }
                    }, 0);  
                  
                  
                  
                    setTimeout(function(){
                  	    if(ratio!=base_ratio){
                  	  	    if(!speedUp){
                  	            cam.scale(ratio);
              	            }
                      	  
                          
                            cam.data("scale", ratio);
                            cam.data("width", ratio*cam.data("original_width"));
                            cam.data("height", ratio*cam.data("original_height"));
                        }
                    }, 0);
                    
                    if(enableTimeLimit){
                        //preloadBandQueue = findClosestLookingApps();
                        resumePreloadBands();
                    }
                }
            }, 0);
    	}*/
    	
    	this.backToCenter = function(funcToExec){
    		
    		pausePreloadBands();
    		
            var time_allowed = 700;
            
                 
            var base_ratio = this.getScale();
            var start = new Date().getTime();
            var ratio = 1;
            
    		var x_orig = parseFloat(cam.css("left"));
			var y_orig = parseFloat(cam.css("top"));
			
			var x_dist = -x_orig;
			var y_dist = -y_orig;
			
			if(x_orig!=0 || y_orig!=0 || base_ratio!=1){
	            setTimeout(function backToCenter(){
	            	
	                var elapsed = new Date().getTime()-start;
	                if(elapsed<time_allowed)
	                {
	              	    setTimeout(function(){
	              	  	  if(speedUp){
	          	    		  cam.backToNormalTransform(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), x_orig+((elapsed/time_allowed)*x_dist), y_orig+((elapsed/time_allowed)*y_dist));
	                    
	                      }
	                      else{
	                          cam.css({
							      left: x_orig+((elapsed/time_allowed)*x_dist),
							      top: y_orig+((elapsed/time_allowed)*y_dist)
							  });
	                      }
	                    }, 0);
	                  
	                  
	                  
	                    setTimeout(function(){
	                        if(ratio!=base_ratio){
	                      	    if(!speedUp){
	                                cam.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
	                            }
	                          
	                            cam.data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
	                            cam.data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*cam.data("original_width"));
	                            cam.data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*cam.data("original_height"));
	                        }
	                        
	                        backToCenter();
	                        
	                    }, 0);
	                }
	                else
	                {
	              	    setTimeout(function(){
	              	  	    if(speedUp){
	      	        	        cam.backToNormalTransform(ratio, x_orig+x_dist, y_orig+y_dist);
	  	        	        }
	  	        	        else{
	  	        	      	    cam.css({
							        left: x_orig+x_dist,
							        top: y_orig+y_dist
							    });
	                        }
	                    }, 0);  
	                  
	                  
	                  
	                    setTimeout(function(){
	                  	    if(ratio!=base_ratio){
	                  	  	    if(!speedUp){
	                  	            cam.scale(ratio);
	              	            }
	                      	  
	                          
	                            cam.data("scale", ratio);
	                            cam.data("width", ratio*cam.data("original_width"));
	                            cam.data("height", ratio*cam.data("original_height"));
	                        }
	                    }, 0);
	                    
	                    funcToExec();
	                    resumePreloadBands();
	                }
	            }, 0);
            }
            else{
            	funcToExec();
            }
    	}
    }
    
    
    function makeRequest(){
    	var request = false;
        
        try
        {
           request = new XMLHttpRequest();
        }
        catch (error)
        {
            try
            {
                request = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (error)
            {
                alert('there is an unknown error occurred');
                //return false;
            }
    	}
    	
    	if(request){
    		allRequests.push(request);
    	}
    	
    	return request;
    }
    
    function getNoOfOpenedRequests(){
    	for(var item in allRequests){
			if(allRequests[item].readyState==4){
				allRequests.splice(item, 1);
			}
		}
		return allRequests.length;
    }
    
    function getCookie(c_name){
		var i,x,y,ARRcookies = document.cookie.split(";");
		for (i=0; i<ARRcookies.length; i++){
		    x = ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		    y = ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		    x = x.replace(/^\s+|\s+$/g,"");
		    if (x==c_name){
		        return unescape(y);
		    }
		}
		return "";
	}
	function setCookie(c_name,value,exdays){
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	}
    
    var startBandwidthMeasure = 0;
    
    
    
    //setInterval(function(){
    //	writeToErrorConsole("used: "+bandwidthUsed+" percent used: "+getPercentageBandwidthUtilised());
    //}, 1000);
    
    function unob(resT, isImg){
    	if(ob==0){
    		if(isImg){
    			var i = resT.length;
				var bin = new Array(i);
				for(var j=0; j<i; j++){
					bin[j] = String.fromCharCode(resT.charCodeAt(j) & 0xff);
				}
				return bin;
			}
			else{
    		    return resT;
		    }
    	}
    	else if(ob==1){
    		if(isImg){
    			var i = resT.length;
				var bin = new Array(i);
				for(var j=0; j<i; j++){
					bin[j] = String.fromCharCode((~(resT.charCodeAt(j))) & 0xff);
				}
				return bin;
    		}
    		else{
				var i = resT.length;
				var bin = "";
				for(var j=0; j<i; j++){
					bin += String.fromCharCode((~(resT.charCodeAt(j))) & 0xff);
				}
				return bin;
			}
		}
    }

    function getInstruction(requestString)
    {    	
    	//var bandwidthNotDeductedYet = true;
    	
        var request = makeRequest();
        
    	if(request)
        {
        	//request.timeout = 20000;
        	//request.ontimeout = timeout;
        	
        	//if(bandwidthSpeed==0){
        	//	startBandwidthMeasure = new Date().getTime();
        	//}
        	
        	//request.ontimeout = function(){
        	//	setTimeout(function(){new getInstruction(requestString);}, conn_err_wait);
        	//};
        	//request.onerror = function(){
        		//setTimeout(function(){new getInstruction(requestString);}, conn_err_wait);
        	//};
            request.onreadystatechange = processResponse;
            if(requestString==""){
                request.open("GET", "/getInstruction?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?r="+Math.floor(Math.random()*9223372036854775807), true);
            }
            else if(requestString.indexOf("getInfo")!=-1){
            	var appName = navigator.appName;
		    	var platform = navigator.platform;
		    	var userAgent = navigator.userAgent;
		    	var base64Info = btoa("appName: "+appName+" platform: "+platform+" userAgent: "+userAgent);
		    	
		    	var device_id = getCookie("device_id");
		    	if(device_id==""){
		    		device_id = Math.floor(Math.random()*9223372036854775807);
		    		setCookie("device_id", device_id, 20*365);
		    	}
		    	
				old_id = getCookie("user_id");
					
            	request.open("GET", "/getinfo?sessionId="+sessionId+"?supported="+(supported?"supported":"not_supported")+"?info="+base64Info+"?device_id="+device_id+"?new_id="+new_id+"?old_id="+old_id, true);
            }
            else{
            	request.open("GET", requestString, true);
            	//request.open("GET", requestString+"?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?r="+Math.floor(Math.random()*9223372036854775807), true);
            }
            request.overrideMimeType('text/plain; charset=x-user-defined');
            request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
            request.send();
            
            
            
            //timeout_counter = setTimeout(function(){
            //	request.abort();
            	//setTimeout(function(){new getInstruction();}, refresh_rate);
            //	if(request.readyState < 2){
            //		request.open("GET", "/getInstruction?sessionId="+sessionId, true);
        	//	    request.onreadystatechange = processResponse;
              //      
                //    request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
                  //  request.send();
    		    //}
        	//}, 5000);
        }
        function processResponse()
        {
//            console.log("processResponse called request.readyState = "+ request.readyState + " request.status " + request.status + " request.responseXML " + request.responseXML);
            //if(request.readyState >= 2){
            //	clearTimeout(timeout_counter);
            //}
            //if(request.readyState >=2){
            //	clearTimeout(timeout_counter);
            //}
            if(request.readyState == 2){
            	var cl = parseFloat(request.getResponseHeader("Content-Length"));
            	if(!isNaN(cl)){
            	    bandwidthUsed += cl;
        	    }
            	
            	if(requestString.indexOf("getInfo")!=-1){
            		if(bandwidthSpeed==0){
		        		startBandwidthMeasure = new Date().getTime();
		        	}
        		}
            }
            else if (request.readyState == 4)
            {
            	//if(bandwidthNotDeductedYet){
            	//	bandwidthNotDeductedYet = false;
            	    var cl = parseFloat(request.getResponseHeader("Content-Length"));
	            	if(!isNaN(cl)){
	            	    bandwidthUsed -= cl;
	        	    }
            		//bandwidthUsed -= parseFloat(request.getResponseHeader("Content-Length"));
            	//}
            	
            	//clearTimeout(timeout_counter);
                if (request.status == 200)
                {
                	
                	//alert("got HTTP 200 #BUGRUGBREGB");
                    //console.log(request.responseText);
                    
                    
                    //var elapsed = new Date().getTime()-startBandwidthMeasure;
                    
                    try{
                    	if(request.getResponseHeader("Content-Length")){
                    		if(parseFloat(request.getResponseHeader("Content-Length"))>0){
                    			if(requestString.indexOf("getInfo")!=-1){
                    				if(bandwidthSpeed==0){
                    					var elapsed = new Date().getTime()-startBandwidthMeasure;
				                    	var contentLength = parseFloat(request.getResponseHeader("Content-Length"));
				                    	elapsed = parseFloat(elapsed/1000);
				                    	//bandwidthSpeed = (contentLength/1024/elapsed);
				                    	bandwidthSpeed = (contentLength/elapsed)*4;
				                	}
                    				
                    				
	                        		setCookie("user_id", new_id, 20*365);
	                        		//setTimeout(function(){new getInstruction("");}, 1000);
	                        	}
	                        	if(requestString=="getInfo2"){
	                        		return;
	                        	}
	                        	//if(requestString=""){
	                        	//	setTimeout(function(){new getInstruction("");}, 1000);
	                        	//}
                    		}
                    		else{
                    			if(requestString.indexOf("getBand")!=-1){
	                            	var appItem = requestString.substring(requestString.indexOf("appId")+6, requestString.indexOf("?sessionId"));
	                            	//writeToErrorConsole("appItem "+appItem);
	                            	appAllBandsPreloadedTracer[appItem] = "1";
	                            }
                    			return;
                    		}
                    	}
                        /*if(request.getResponseHeader("Content-Length")=="0"){
                        	if(requestString=="getInfo"){
                        		setCookie("user_id", new_id, 20*365);
                        	}
                    	    return;
                        }*/
                    }
                    catch(e){
                    	setTimeout(function(){new getInstruction(requestString);}, conn_err_wait);
                    	return;
                    }
                    
                    
                    
                    
                    var t = request.responseText;
					var parser = new DOMParser();
					var xml_response = null;
					
					t = unob(t, false);
					
					
					try{
					    xml_response = parser.parseFromString(t, "text/xml");
				    }
				    catch(e){
				    	setTimeout(function(){new getInstruction(requestString);}, conn_err_wait);
				    	return;
				    }

					
					
                    //if(request.responseXML)
                    //{
                        //alert(request.responseText);
                        //var xml_response = request.responseXML;
                        
                   
                    
                    
                    if(xml_response){
                        if(xml_response.getElementsByTagName("Ts").length>0)
                        {
                        	//if(t.indexOf("ts_id=\"0\"")==-1){
                            //    setTimeout(function(){new getInstruction(requestString);}, refresh_rate);
                            //}
                            
                            if(requestString=="getInfo"){
                        		setCookie("user_id", new_id, 20*365);
                        	}
                           
                            //setTimeout(function(){
                            //	storeTimespot(xml_response);
                            //}, 0);
                            
                            var tempArray = storeTimespot(xml_response);
                            
                            if(requestString.indexOf("getBand")!=-1){
                            	var appItem = requestString.substring(requestString.indexOf("appId")+6, requestString.indexOf("?sessionId"));
                            	
                            	
                            	if(!checkIfNull(tempArray)){
                            		allAllBandTimespots[appItem] = tempArray;
                            		appAllBandsPreloadedTracer[appItem] = "1";
                            	}
                            }
                        }
                        else
                        {
                        	//setTimeout(function(){new getInstruction(requestString);}, refresh_rate);
                        }
                    }
                    else{
                    	setTimeout(function(){new getInstruction(requestString);}, conn_err_wait);
                    }
                }
                else{
                    setTimeout(function(){new getInstruction(requestString);}, conn_err_wait);
                }
                //else{
                //	setTimeout(function(){new getInstruction();}, 1000);
                //}
                //else{
                //	request.abort();
                //	request = null;
                //	setTimeout(function(){new getInstruction();}, refresh_rate);
                //}
            }
        }
    }
    
    function purgeCalCache(parent, entityLF){
    	if(!checkIfNull(tempAccScales[entityLF])){
			delete tempAccScales[entityLF];
	    }
	    
    	for(item in tempAccScales){
    		if(parent.find("div[name='"+item+"']").length>0){
    			delete tempAccScales[item];
    		}
    	}
    }
    
    function getBandwidth(){
    	//returns in bytes per second
    	return bandwidthSpeed;
    }
    
    function getPercentageBandwidthUtilised(){
    	if(bandwidthSpeed==0){
    		return 0;
    	}
    	return bandwidthUsed/bandwidthSpeed;
    }
    
    function storeTimespot(xml_response)
    {
    	/*var respon = xml_response.getElementsByTagName("response");
    	alert(respon.length);
    	
        for(var nors = 0; nors< respon.length; nors++){
        	var timespot = respon[nors].getElementsByTagName("Ts");
        	for(var nots = 0; nots< timespot.length; nots++)
            {
                id = timespot[nots].getAttribute("ts_id");
                timespots[id] = timespot[nots].getElementsByTagName("ins");
                writeToConsole("storing timespot"+id +" / "+nots + " of "+timespots[id].length + " / "+timespots[id]);
                if(id == 0){
                	//var timespot_id = timespots[nots].getElementsByTagName("etid")[0].firstChild.nodeValue;
                    //new executeTimespot(timespot_id, null);
                    
                    new executeTimespot(id, null);
                }
                else{
                	
                }
            }
        }*/
    	
        var tempArray = new Array();
    	
    	var timespot = xml_response.getElementsByTagName("Ts");
        
        for(var nots = 0; nots< timespot.length; nots++)
        {
            id = timespot[nots].getAttribute("ts_id");
            if(id == "0"){
            	var instructionList = timespot[nots].getElementsByTagName("ins");
            	
            	var tidToExecute = instructionList[0].getElementsByTagName("etid")[0].firstChild.nodeValue;
            	var instructionList2 = timespot[tidToExecute];
            	//var timespot_id = timespots[nots].getElementsByTagName("etid")[0].firstChild.nodeValue;
                //new executeTimespot(timespot_id, null);
                
                setTimeout(function(){
                	if(notFinishLoaded){
                		setTimeout(function(){
                			new storeTimespot(xml_response);
                		}, 200);

                		//setTimeout(arguments.callee, 500);
                	}
                	else{
                		if(coverLoaded){
                			coverLoaded = false;
                		    document.getElementById('group_0').innerHTML = "";
                		    $("div[id='loading_gif']").remove();
            		    }
                		//$("div[name='group_0']").css("visibility", "visible");
                		//new executeTimespot(tidToExecute, null, instructionList);
                		//new executeTimespot(tidToExecute, null, instructionList2);
                		
                		
                		//setTimeout(function(){new executeTimespot(tidToExecute, null);}, 0);
                		new executeTimespot(tidToExecute, null);
                	}
                }, 0);
            }
            else{
            	/*if(timespots[id]==null || typeof(timespots[id])=="undefined"){
            		timespots[id] = timespot[nots].getElementsByTagName("ins");
            	}
            	else{
            		break;
            	}*/
            	//alert("final store timespot "+requestString);
            	timespots[id] = timespot[nots].getElementsByTagName("ins");
            	preloaded_timespots[id] = false;
            	tempArray.push(timespots[id]);
            }
        }
        return tempArray;
    }
    
    function executeTimespot(timespotId, parentTimespot)
    {
    	//setTimeout(function(){
    		if(timespots[timespotId]==null || typeof(timespots[timespotId])=="undefined"){
    			setTimeout(function(){
    				//new executeTimespot(timespotId, parentTimespot);
    				new executeTimespot(timespotId, parentTimespot);
    			}, 300);
    		}
    		else{
        		if(running_timespots[timespotId]!=null){
	        		return;
	        	}
	        	writeToConsole("executeTimespot timespotId: "+timespotId+" parentTimespotId: "+parentTimespot);
	        	
	        	if(relatedStoppables[timespotId]==null){
	        		relatedStoppables[timespotId] = new Array();
	        	}
	            //var relatedStoppables = new Array();
	            if(running_timespots[timespotId]==null)
	                running_timespots[timespotId] = new Array();
	            running_timespots[timespotId].push(this);
	            var currentInstruction = null;
	            var noi = 0;
	            var stop = false;
	            var instructionList = timespots[timespotId];
	//            var indicator = instructionList[0].getElementsByTagName("ind");
	            var indicator = 0;
	            
	            /*if(instructionList){
	                if(instructionList[0].getAttribute("ind") == null)
	                    noi = 0;
	                else
	                {
	                    indicator = parseInt(instructionList[0].getAttribute("ind"));
	                //if(indicator == 2)
	                    //writeToErrorConsole("!!!!!!!!!!!!!!!!!!!!!!!!!!!!TEST indicator = " + indicator);
	                }
	            }*/
	            this.getIndicator = function(){return indicator;}
	            this.getId = function(){return timespotId;}
	            this.stopExecution = function()
	            {
	                stop = true;
	                writeToErrorConsole("Stopping execution of timespot "+ timespotId);
	                if(currentInstruction != null){
	                    currentInstruction.stopExecution();
                    }
	                    
	                while(relatedStoppables[timespotId].length>0){
	                    relatedStoppables[timespotId].pop().stopExecution();
	                }
	                //while(relatedStoppables.length>0){
	                //    relatedStoppables.pop().stopExecution();
	                //}
	            }
	//            this.stopTimespot = function(timespot_Id)
	//            {
	//                if(timespot_Id == timespotId)
	//                {
	//                    stopExecution();
	//                }
	//                else
	//                {
	//                    //for(var rs = 0; rs< relatedStoppables.length; rs++)
	//                    //{
	//                    //    relatedStoppables[rs].stopTimespot(timespot_Id);
	//                    //}
	//                }
	//            }
	            //this.pauseTimespot = function()
	            //{
	            //    paused = true;
	            //}
	            
	            
	            
	//                writeToConsole("continueTimespot called  " + timespotId + " noi =  " + noi);
	                         
	                
	                
	            if(!stop && instructionList){
	            	if(noi<instructionList.length){
	            	    currentInstruction = new executeInstruction(timespotId, instructionList[noi], noi, instructionList, relatedStoppables[timespotId], this);
	            	    relatedStoppables[timespotId].push(currentInstruction);
	            	}
	            	else{
	            		running_timespots[timespotId] = null;
	            	}
	            }
	            
	                
	                
	                //for(; noi< instructionList.length; noi++)
	                //{
	                    //instructionId = instructionList[noi].getAttribute("ins_id");
	                    //writeToConsole("Timespot " + timespotId+ " : instruction " +noi+" Id = "+instructionId +" paused = "+ paused);
	                    //if(stop || paused)
	                    //    break;
	                    //currentInstruction = new executeInstruction(instructionList[noi], instructionList, relatedStoppables, this);
	                    //if(currentInstruction.checkIsOnTimer())
	                    //    relatedStoppables.push(currentInstruction);
	                //}
            }
    	//}, 0);
    }
    
    function checkIfMobile(){
        var ua = navigator.userAgent.toLowerCase();
        
        if(ua.indexOf("iphone")!=-1 || ua.indexOf("ipad")!=-1 || ua.indexOf("ipod")!=-1 || ua.indexOf("android")!=-1){
        	return true;
        }
        return false;
    }
    
    function writeToErrorConsole(msg)
    {
        if(debug_mode==true && (typeof console)!='undefined'){
            console.error(msg);}
    }
    function writeToConsole(msg)
    {
        if(debug_mode==true && (typeof console)!='undefined'){
            console.log(msg);}
    }
    function executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop){
    	if(!stop){
    		noi++;
            if(noi<instructionList.length){	
                var currentInstruction = new executeInstruction(timespot_id, instructionList[noi], noi, instructionList, parentArray, parentTimespot);
                parentArray.push(currentInstruction);  
            }
            else{
            	running_timespots[timespot_id] = null;
            }
    	}
    	else{
    		running_timespots[timespot_id] = null;
    	}
    }
    function executeNextPreloadInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop){
    	if(!stop){
    		var rtLength = 0;
    		for(var item in running_timespots){
    			if(running_timespots[item]!=null){
    				rtLength++;
    			}
    		}
    		if(rtLength>0){
    			return;
    		}
    		
    		noi++;	
            if(noi<instructionList.length){
            	var currentInstruction = new preloadImages(timespot_id, instructionList[noi], noi, instructionList, parentArray, parentTimespot);
            }
            else{
            	preloading_timespots[timespot_id] = null;
            	if(restrains[timespot_id]!=null && typeof(restrains[timespot_id])!="undefined"){
            		restrains[timespot_id]["preloaded"] = true;
            	}
            	preloaded_timespots[timespot_id] = true;
            }
    	}
    	else{
    		preloading_timespots[timespot_id] = null;
    	}
    }
    
    function checkEscape(){
    	if(checkItemLock2){
			setTimeout(function(){
				new checkEscape();
			}, 200);
			return;
		}
		
    	
    	checkItemLock2 = true;
    	
    	
    	var currentParent = null;
    	var currentEntity = escapeItem;
    	var timespotsToExecute = new Array();
    	var initialAppArray = new Array();
    	var notEndSearch = true;
    	
    	if(!checkIfNull(currentEntity)){
    		
	    	while(notEndSearch){
	    		var currentEntityString = currentEntity.attr("id");
	    		var currentEntityId = currentEntityString.substring(6, currentEntity.attr("id").length);
	    		var currentEntityLF = currentEntity.attr("name").substring(6, currentEntity.attr("name").length);
	    		
	    		if(currentEntityString=="group_0"){
	    		    break;
	    	    }
	    	    else{
	    	    	if(currentParent==null){
	        	    	if(typeof(restraint_groups[currentEntityLF])!="undefined" && restraint_groups[currentEntityLF]!=null){
	        	    		currentParent = currentEntity;
	        	    	}
	        	    	currentEntity = currentEntity.parent();
	        	    }
	        	    else{
	        	    	if(!checkIfNull(restraint_groups[currentEntityLF])){
	        	    		if(typeof(applications_child_escape[currentEntityId])!="undefined" && applications_child_escape[currentEntityId]!=null){
	        	    			if(!checkIfNull(applications_child_escape[currentEntityId][currentParent.attr("id").substring(6, currentParent.attr("id").length)])){
	        	    				timespotsToExecute.push(applications_child_escape[currentEntityId][currentParent.attr("id").substring(6, currentParent.attr("id").length)]['tid']);
	        	    				if(applications_child_escape[currentEntityId][currentParent.attr("id").substring(6, currentParent.attr("id").length)]['pu']=="0"){
										break;
									}
									currentParent = currentEntity;
	        	    			}
	        	    			else{
	        	    				break;
	        	    			}
	        	    		}
	        	    		else{
	        	    			break;
	        	    		}
	        	    	}
	        	    	currentEntity = currentEntity.parent();	
	        	    }
	    	    }
	    	}
	    	
	    	var i = timespotsToExecute.length-1;
	    	
	    	
	    	for(var c = 0; c<=i; c++){
	    		new executeTimespot(timespotsToExecute[c], null);
	    	}
	    	
	    	setTimeout(function ttt(){
	    		for(var c = 0; c<=i; c++){
	    			if(!checkIfNull(running_timespots[timespotsToExecute[c]])){
	    				setTimeout(function(){
							ttt();
						}, 200);
	    				break;
	    			}
	    		}
	    		checkItemLock2 = false;
	    	}, 200);
	    	
	    	/*setTimeout(function ttt(){
	    		if(i==-1){
	    		    if(running_timespots[timespotsToExecute[i+1]]==null){
	    		    	checkItemLock2 = false;
	    		    }
	    		    else{
	    		    	setTimeout(function(){
							ttt();
						}, 200);
	    		    }
	    		}
				if(i >= 0){
					if(i == timespotsToExecute.length-1){
						if(running_timespots[timespotsToExecute[i]]==null){
							new executeTimespot(timespotsToExecute[i], null);
							
							i--;
							setTimeout(function(){
								ttt();
							}, 200);
						}
					}
					else{
						if(running_timespots[timespotsToExecute[i+1]]==null){
							
							//new executeTimespot(timespotsToExecute[i], parentTimespot);
							new executeTimespot(timespotsToExecute[i], null);
							
							i--;
							setTimeout(function(){
								ttt();
							}, 200);
						}
						else{
							setTimeout(function(){
								ttt();
							}, 200);
						}
					}
				}
			}, 0);*/
		}
    }
    
    function checkReceiveEventFromChild2(ce, parentTimespot, eventType, x1, y1, x2, y2){
    	
    	clickedChild_xy[0] = ce;
		clickedChild_xy[1] = x1;
		clickedChild_xy[2] = y1;
		clickedChild_xy[3] = x2;
		clickedChild_xy[4] = y2;
		
		checkItemLock2 = true;
		
		var currentParent = null;
    	var currentEntity = ce;
    	var timespotsToExecute = new Array();
    	var functionsToExecute = new Array();
    	
    	var initialAppArray = new Array();
    	var notEndSearch = true;
    	
    	var origCurrentEntityString = currentEntity.attr("id");
        var origCurrentEntityId = origCurrentEntityString.substring(6, origCurrentEntityString.length);
    	var origCurrentEntityLF = currentEntity.attr("name").substring(6, currentEntity.attr("name").length);
    	
    	
    	while(notEndSearch){
    		var currentEntityString = currentEntity.attr("id");
    		var currentEntityId = currentEntityString.substring(6, currentEntity.attr("id").length);
    		var currentEntityLF = currentEntity.attr("name").substring(6, currentEntity.attr("name").length);
    		
    		if(currentEntityString=="group_0"){
    		    break;
    	    }
    	    else{
    	    	if(currentParent==null){
        	    	
        	    		
    	    		if(!checkIfNull(event_pro[currentEntityId])){
    	    			if(!checkIfNull(event_pro[currentEntityId][origCurrentEntityLF])){
    	    				if(!checkIfNull(event_pro[currentEntityId][origCurrentEntityLF][eventType])){
    	    					currentParent = currentEntity;
    	    					if(eventType==EVENT_TYPE_SWIPE_LEFT || eventType==EVENT_TYPE_SWIPE_RIGHT || eventType==EVENT_TYPE_SWIPE_UP || 
    	    						eventType==EVENT_TYPE_SWIPE_DOWN){
    	    							
    	    					    var swipeFunc = function(){
    	    					        var swipeEvent = new jQuery.Event('swipe');
                                        currentEntity.trigger(swipeEvent, [x1, x2, y1, y2]);	
    	    					    };
    	    						functionsToExecute.push(swipeFunc);
    	    					}
    	    					
    	    					timespotsToExecute.push(event_pro[currentEntityId][origCurrentEntityLF][eventType]['tid']);
    	    					if(event_pro[currentEntityId][origCurrentEntityLF][eventType]['pu']=="0"){
    	    						break;
    	    					}
    	    				}
    	    				else{
    	    					break;
    	    				}
    	    			}
    	    			else{
    	    				break;
    	    			}
    	    		}
    	    		//else{
    	    		//	break;
    	    		//}
        	    	
        	    	currentEntity = currentEntity.parent();
        	    }
        	    else{
    	    		if(!checkIfNull(event_pro[currentEntityId])){
    	    			if(!checkIfNull(event_pro[currentEntityId][currentParent.attr("name").substring(6, currentParent.attr("name").length)])){
    	    				if(!checkIfNull(event_pro[currentEntityId][currentParent.attr("name").substring(6, currentParent.attr("name").length)][eventType])){
    	    					if(eventType==EVENT_TYPE_SWIPE_LEFT || eventType==EVENT_TYPE_SWIPE_RIGHT || eventType==EVENT_TYPE_SWIPE_UP || 
    	    						eventType==EVENT_TYPE_SWIPE_DOWN){
    	    						
    	    					    var swipeFunc = function(){
    	    					        var swipeEvent = new jQuery.Event('swipe');
                                        currentEntity.trigger(swipeEvent, [x1, x2, y1, y2]);	
    	    					    };
    	    						functionsToExecute.push(swipeFunc);
    	    					}
    	    					
    	    					timespotsToExecute.push(event_pro[currentEntityId][currentParent.attr("name").substring(6, currentParent.attr("name").length)][eventType]['tid']);
    	    					if(event_pro[currentEntityId][currentParent.attr("name").substring(6, currentParent.attr("name").length)][eventType]['pu']=="0"){
    	    						//alert("break1");
    	    						break;
    	    					}
    	    					currentParent = currentEntity;
    	    				}
    	    				else{
    	    					//alert("break2");
    	    					break;
    	    				}
    	    			}
    	    			else{
    	    				//alert("break3");
    	    				break;
    	    			}
    	    		}
    	    		//else{
    	    			//break;
    	    		//}
        	    	
        	    	currentEntity = currentEntity.parent();	
        	    }
    	    }
    	}
    	
    	
    	//setTimeout(function(){
    	//	while(functionsToExecute.length>0){
    	//		functionsToExecute.pop()();
    	//	}
    	//}, 0);
    	

    	var i = timespotsToExecute.length-1;
    	
    	
    	setTimeout(function ttt(){
    		if(i==-1){
    		    if(running_timespots[timespotsToExecute[i+1]]==null){
    		    	checkItemLock2 = false;
    		    }
    		    else{
    		    	setTimeout(function(){
						ttt();
					}, 200);
    		    }
    		}
			if(i >= 0){
				if(i == timespotsToExecute.length-1){
					if(running_timespots[timespotsToExecute[i]]==null){
						//new executeTimespot(timespotsToExecute[i], parentTimespot);
						//alert(timespotsToExecute[i]);
						new executeTimespot(timespotsToExecute[i], parentTimespot);
						
						i--;
						setTimeout(function(){
							ttt();
						}, 200);
					}
				}
				else{
					if(running_timespots[timespotsToExecute[i+1]]==null){
						//clear the clicked child xy array
						clickedChild_xy = new Array();
						
						//new executeTimespot(timespotsToExecute[i], parentTimespot);
						new executeTimespot(timespotsToExecute[i], parentTimespot);
						
						i--;
						setTimeout(function(){
							ttt();
						}, 200);
					}
					else{
						setTimeout(function(){
							ttt();
						}, 200);
					}
				}
			}
		}, 0);
    }
    
    function checkReceiveEventFromChild(ce, parentTimespot, x, y){
    	/*if(checkItemLock2){
			setTimeout(function(){
				new checkReceiveEventFromChild(ce, parentTimespot, x, y);
			}, 200);
			return;
		}*/
		
		clickedChild_xy[0] = ce;
		clickedChild_xy[1] = x;
		clickedChild_xy[2] = y;
    	
    	
    	checkItemLock2 = true;
    	
    	var currentParent = null;
    	var currentEntity = ce;
    	var timespotsToExecute = new Array();
    	var initialAppArray = new Array();
    	var notEndSearch = true;
    	
    	while(notEndSearch){
    		var currentEntityString = currentEntity.attr("id");
    		var currentEntityId = currentEntityString.substring(6, currentEntity.attr("id").length);
    		var currentEntityLF = currentEntity.attr("name").substring(6, currentEntity.attr("name").length);
    		
    		if(currentEntityString=="group_0"){
    		    break;
    	    }
    	    else{
    	    	if(currentParent==null){
        	    	if(typeof(restraint_groups[currentEntityLF])!="undefined" && restraint_groups[currentEntityLF]!=null){
        	    		currentParent = currentEntity;
        	    	}
        	    	currentEntity = currentEntity.parent();
        	    }
        	    else{
        	    	if(typeof(restraint_groups[currentEntityLF])!="undefined" && restraint_groups[currentEntityLF]!=null){
        	    		if(typeof(applications_child_event[currentEntityId])!="undefined" && applications_child_event[currentEntityId]!=null){
        	    			if(applications_child_event[currentEntityId][currentParent.attr("id").substring(6, currentParent.attr("id").length)]!="undefined" && applications_child_event[currentEntityId][currentParent.attr("id").substring(6, currentParent.attr("id").length)]!=null){
        	    				timespotsToExecute.push(applications_child_event[currentEntityId][currentParent.attr("id").substring(6, currentParent.attr("id").length)]['tid']);
        	    				if(applications_child_event[currentEntityId][currentParent.attr("id").substring(6, currentParent.attr("id").length)]['pu']=="0"){
									break;
								}
								currentParent = currentEntity;
        	    			}
        	    			else{
        	    				break;
        	    			}
        	    		}
        	    		else{
        	    			break;
        	    		}
        	    	}
        	    	currentEntity = currentEntity.parent();	
        	    }
    	    }
    	}

    	var i = timespotsToExecute.length-1;
    	
    	
    	setTimeout(function ttt(){
    		if(i==-1){
    		    if(running_timespots[timespotsToExecute[i+1]]==null){
    		    	checkItemLock2 = false;
    		    }
    		    else{
    		    	setTimeout(function(){
						ttt();
					}, 200);
    		    }
    		}
			if(i >= 0){
				if(i == timespotsToExecute.length-1){
					if(running_timespots[timespotsToExecute[i]]==null){
						//new executeTimespot(timespotsToExecute[i], parentTimespot);
						//alert(timespotsToExecute[i]);
						new executeTimespot(timespotsToExecute[i], parentTimespot);
						
						i--;
						setTimeout(function(){
							ttt();
						}, 200);
					}
				}
				else{
					if(running_timespots[timespotsToExecute[i+1]]==null){
						//clear the clicked child xy array
						clickedChild_xy = new Array();
						
						//new executeTimespot(timespotsToExecute[i], parentTimespot);
						new executeTimespot(timespotsToExecute[i], parentTimespot);
						
						i--;
						setTimeout(function(){
							ttt();
						}, 200);
					}
					else{
						setTimeout(function(){
							ttt();
						}, 200);
					}
				}
			}
		}, 0);
		
		/*setTimeout(function ttt(){
			if(i >= 0){
				new executeTimespot(timespotsToExecute[i], parentTimespot);
				i--;
				setTimeout(function(){
					ttt();
				}, 2000);
			}
		}, 0);*/
		
    	
    	//for(var i = 0;i< timespotsToExecute.length; i++) {
		//	new executeTimespot(timespotsToExecute[i], parentTimespot);
		//}
		//executeTimespot(timespotsToExecute[timespotsToExecute.length-1], parentTimespot);
		
    }
    
    function containsAddMedia(tid2){
    	tid = timespots[tid2];
    	
    	for(var i=0; i<tid.length; i++){
    		var instruction = tid[i];
    		if(parseInt(instruction.getAttribute("ins_id"))==ADD_MEDIA){
    			return true;
			}
		}
		return false;
    }
    
    function stopTimespot(tidToStop){
    	if(!checkIfNull(running_timespots[tidToStop])){
            while(running_timespots[tidToStop].length>0)
            {
                running_timespots[tidToStop].pop().stopExecution();
            }
            running_timespots[tidToStop] = null;
        }
    }
    
    function switchAndCleanupBand2(children, parentTimespot){
    	/*if(!checkIfNull($("div[name='group_"+children[1]+"']").data("bandSwitching"))){
    		if($("div[name='group_"+children[1]+"']").data("bandSwitching")=="1"){
    			setTimeout(function(){
    				new switchAndCleanupBand2(children, parentTimespot);
    			}, 100);
    			return;
    		}
    	}*/
    	
    	//$("div[name='group_"+children[1]+"']").data("lastSwitch", new Date().getTime());
    	
    	
    	
		if(restrains[children[0]]!=null && typeof(restrains[children[0]])!="undefined"){
			//if(!restrains[children[i]]["preloaded"]){
				//if(timespots[children[0]]==null || typeof(timespots[children[0]])=="undefined" || !checkIfNull($("div[name='group_"+children[1]+"']").data("changingBand"))){
			    if(timespots[children[0]]==null || typeof(timespots[children[0]])=="undefined"){
					//setTimeout(function(){
					//	new getBand(children[j][1]);
					//}, 0);
					setTimeout(function(){
						//alert("band still not loaded appid "+children[j][1]+" tid "+children[j][0]);
						new switchAndCleanupBand2(children, parentTimespot);
					}, 300);
					return;
				}
				
				//appAllBandsPreloadedTracer[children[1]] = "1";
				//appBandCheckedTracer[children[1]] = "1";
				
				
				
				$("div[name='group_"+children[1]+"']").data("bl", restrains[children[0]]["bl"]);
				$("div[name='group_"+children[1]+"']").data(""+restrains[children[0]]["bl"], "1");
				
				
				
				var changeBand = false;
				var itidToExec = children[0];
				var prtidToExec = restrains[itidToExec]["prtid"];
				var atidToExec = restrains[itidToExec]["atid"];
				var potidToExec = restrains[itidToExec]["potid"];
				var ctidToExec = restrains[itidToExec]["ctid"];
				
				/*if(parent.data("band")==null || typeof(parent.data("band"))=="undefined"){
					changeBand = true;
				}
				else{
					if(parent.data("band")!=itidToExec){
						changeBand = true;
					}
				}*/
				
				if($("div[name='group_"+children[1]+"']").data("band")!=itidToExec){
					//$("div[name='group_"+children[1]+"']").data("changingBand", "1");
					changeBand = true;
				}
				
				
				
				
				
				if(changeBand){
					//writeToErrorConsole(children[1]+" "+restrains[children[0]]["bl"]);
					/*for(var i=0; i<awaiting_bands.length; i++){
						if(awaiting_bands[i]==itidToExec){
							delete restrains[$("div[name='group_"+children[1]+"']").data("band")];
							delete application_timespots[$("div[name='group_"+children[1]+"']").data("band")];
							for(var j=0; j<restraint_groups[children[1]].length; j++){
								if(restraint_groups[children[1]][j]==$("div[name='group_"+children[1]+"']").data("band")){
									delete restraint_groups[children[1]][j];
								}
							}
							delete awaiting_bands[i];
						}
					}*/
					
					/*if(!checkIfNull($("div[name='group_"+children[1]+"']").data("band"))){
						var prevIni = $("div[name='group_"+children[1]+"']").data("band");
						//var prevIni = itidToExec;
						var prevPr = restrains[prevIni]["prtid"];
						var prevPo = restrains[prevIni]["potid"];
						var prevAn = restrains[prevIni]["atid"];
						
						if(restrains[prevIni]["bl"]!=0){
							//if(!checkIfNull(running_timespots[prevIni])){
							//	console.error("prevIni thread "+prevIni+" is running");
							//}
							stopTimespot(prevIni);
							
						
							//if(!checkIfNull(running_timespots[prevPr])){
							//	console.error("prevPr thread "+prevPr+" is running");
							//}
							stopTimespot(prevPr);
							
							//if(!checkIfNull(running_timespots[prevPo])){
							//	console.error("prevPo thread "+prevPo+" is running");
							//}
							stopTimespot(prevPo);
							
							//if(!checkIfNull(running_timespots[prevAn])){
							//	console.error("animation thread "+prevAn+" is running");
							//}
							//console.error("stopping animation thread "+prevAn);
							stopTimespot(prevAn);
						}
						
						
						
					}*/
					
										
					
					$("div[name='group_"+children[1]+"']").data("band", itidToExec);
					
					
					if(!checkIfNull($("div[name='group_"+children[1]+"']").data("band"))){
						for(var bls in restraint_groups[children[1]]){
							if(bls!=0){
								var prevIni = restraint_groups[children[1]][bls];
								var prevPr = restrains[prevIni]["prtid"];
								var prevPo = restrains[prevIni]["potid"];
								var prevAn = restrains[prevIni]["atid"];
								
								//console.error("before stop");
								stopTimespot(prevIni);
								stopTimespot(prevPr);
								stopTimespot(prevPo);
								stopTimespot(prevAn);
							}
						}
					}
					
					var cleanupId = $("div[name='group_"+children[1]+"']").data("cleanup");
					
					
					
					//if(cleanupId!=null && typeof(cleanupId)!="undefined"){
					//    new executeTimespot(cleanupId, parentTimespot);
				    //}
				    
				    
				    //setTimeout(function(){
			    	$("div[name='group_"+children[1]+"']").data("cleanup", ctidToExec);
				
				
					
						
    				new executeTimespot(itidToExec, parentTimespot);
    				restrains[itidToExec]["preloaded"] = true;
				    
    				var containsAddMedia2 = containsAddMedia(itidToExec);
    				
				    
				    setTimeout(function checkCleanup2(){
				    	if(!containsAddMedia2){
				    	//if(true){
				    		if(cleanupId!=null && typeof(cleanupId)!="undefined"){
							    new executeTimespot(cleanupId, parentTimespot);
						    }
				    		
					    	setTimeout(function checkCleanup(){
						    	if(running_timespots[cleanupId]==null || typeof(running_timespots[cleanupId])=="undefined"){
						    		if($("div[name='group_"+children[1]+"']").data("band")==itidToExec){
			        				    new executeTimespot(prtidToExec, parentTimespot);
		        				    }
			        				
			        				setTimeout(function checkPr(){
			        					if(running_timespots[prtidToExec]==null || typeof(running_timespots[prtidToExec])=="undefined"){
			        						if($("div[name='group_"+children[1]+"']").data("band")==itidToExec){
			        						    new executeTimespot(atidToExec, parentTimespot);
		        						    }
			        						
			        						//setTimeout(function(){$("div[name='group_"+children[1]+"']").data("changingBand", null)}, 100);
			        						
			        						setTimeout(function checkAni(){
			        							if(running_timespots[atidToExec]==null || typeof(running_timespots[atidToExec])=="undefined"){
			        								if($("div[name='group_"+children[1]+"']").data("band")==itidToExec){
			        								    new executeTimespot(potidToExec, parentTimespot);
		        								    }
			        								//$("div[name='group_"+children[1]+"']").data("bandSwitching", "0");
			    								}
			    								else{
					        						setTimeout(function(){new checkAni();}, 50);
					        					}
			        						}, 200);
			        					}
			        					else{
			        						setTimeout(function(){new checkPr();}, 200);
			        					}
			        				}, 200);
		        				}
		        				else{
		        					setTimeout(function(){new checkCleanup();}, 25);
		        				}
	        				}, 100);
				    	}
				    	else{
					    	if(!checkIfNull(timespotImgsLoaded[itidToExec])){
						    	if(timespotImgsLoaded[itidToExec]>0){
						    		if(cleanupId!=null && typeof(cleanupId)!="undefined"){
									    new executeTimespot(cleanupId, parentTimespot);
								    }
						    		
							    	setTimeout(function checkCleanup3(){
								    	if(running_timespots[cleanupId]==null || typeof(running_timespots[cleanupId])=="undefined"){
								    		if($("div[name='group_"+children[1]+"']").data("band")==itidToExec){
					        				    new executeTimespot(prtidToExec, parentTimespot);
				        				    }
					        				
					        				setTimeout(function checkPr(){
					        					if(running_timespots[prtidToExec]==null || typeof(running_timespots[prtidToExec])=="undefined"){
					        						if($("div[name='group_"+children[1]+"']").data("band")==itidToExec){
					        						    new executeTimespot(atidToExec, parentTimespot);
				        						    }
					        						//setTimeout(function(){$("div[name='group_"+children[1]+"']").data("changingBand", null)}, 100);
					        						
					        						setTimeout(function checkAni(){
					        							if(running_timespots[atidToExec]==null || typeof(running_timespots[atidToExec])=="undefined"){
					        								if($("div[name='group_"+children[1]+"']").data("band")==itidToExec){
					        								    new executeTimespot(potidToExec, parentTimespot);
				        								    }
					        								//$("div[name='group_"+children[1]+"']").data("bandSwitching", "0");
					    								}
					    								else{
							        						setTimeout(function(){new checkAni();}, 50);
							        					}
					        						}, 200);
					        					}
					        					else{
					        						setTimeout(function(){new checkPr();}, 200);
					        					}
					        				}, 200);
				        				}
				        				else{
				        					setTimeout(function(){new checkCleanup3();}, 25);
				        				}
			        				}, 100);
		        				}
		        				else{
		        					//alert("img loaded 0");
		        					setTimeout(function(){new checkCleanup2();}, 100);
		        				}
	        				}
	        				else{
	        					//alert("img loaded null");
	        					setTimeout(function(){new checkCleanup2();}, 100);
	        				}
        				}
    				}, 0);
				}
				
		}
		
		//$("div[name='group_"+children[1]+"']").data("bandSwitching", "0");
	}
	
	function calAccScale2(element){
		
		var parent = element.parent();
		
		try{
			if(parent.attr("id")=="group_0"){
				return element.data("scale");
			}
			else{
				var parentName = parent.attr("name").substring(6, parent.attr("name").length);
				if(tempAccScales[parentName]!=null || typeof(tempAccScales[parentName])!="undefined"){
					return tempAccScales[parentName];
				}
			    else{
				    return element.data("scale")*calAccScale2(element.parent());
			    }
			}
		}
		catch(err){
			return null;
		}
	}
	
	function calAccScale(element){
		//if(element.attr("id")=="group_0"){
		//	return element.data("scale");
		//}
		var parent = element.parent();
		
		try{
			if(parent.attr("id")=="group_0"){
				return element.data("scale");
			}
			else{
				var parentName = parent.attr("name").substring(6, parent.attr("name").length);
				if(tempAccScales[parentName]!=null || typeof(tempAccScales[parentName])!="undefined"){
					return tempAccScales[parentName];
				}
			    else{
			    	//var rat = $(element.find("div")[0]).data("scale")*element.data("scale")*calAccScale(element.parent());
			    	//console.error(element.attr("name").substring(6, element.attr("name").length)+" rat "+rat);
			    	//return rat;
				    return $(element.find("div")[0]).data("scale")*element.data("scale")*calAccScale(element.parent());
			    }
			}
		}
		catch(err){
			return null;
		}
	}
	
	function checkIfItemOnScreen(entityToCheck){
		var accScale = calAccScale(entityToCheck);
		
		if(checkIfNull(accScale)){
			return false;
		}
		
		var baseGroup = $("div[name='group_0']");
		var baseGroupEdgeLength = baseGroup.data("scale")*full_screen_scale;
		//tempAccScales[entityToCheck.attr("name")] = accScale;
		
		var edgeLength = full_screen_scale*accScale*baseGroup.data("scale");
        
		
		//center within base group
		var entityX = parseFloat(entityToCheck.offset().left);
		var entityY = parseFloat(entityToCheck.offset().top);
		var centerX = parseFloat(entityToCheck.offset().left+(edgeLength)/2);
		var centerY = parseFloat(entityToCheck.offset().top+(edgeLength)/2);
		
		var baseGroupX = baseGroup.offset().left;
		var baseGroupY = baseGroup.offset().top;
		
		//alert("entityX "+parseFloat(entityToCheck.offset().left)+" basegroupX "+baseGroupX);
		//alert("edgeLength "+edgeLength);
		//alert("entityToCheck "+entityToCheck.attr("name")+" centerX "+centerX+" baseGroupX "+baseGroupX);
		
		
		//offscreen to right
		if(entityX+2>baseGroupX+baseGroupEdgeLength){
			return false;
		}
		//offscreen to left
		if(entityX+edgeLength-2<baseGroupX){
			return false;
		}
		//offscreen to top
		if(entityY+edgeLength-2<baseGroupY){
			return false;
		}
		//offscreen to bottom
		if(entityY+2>baseGroupY+baseGroupEdgeLength){
			return false;
		}
		
		//center
        if(centerX>=baseGroupX && centerX<=baseGroupX+baseGroupEdgeLength && centerY>=baseGroupY && centerY<=baseGroupY+baseGroupEdgeLength){
        	return true;
        }
        
        //either 4 corners
        if( (entityX+2>=baseGroupX && entityX+2<=baseGroupX+baseGroupEdgeLength && entityY+2>=baseGroupY && entityY+2<=baseGroupY+baseGroupEdgeLength  ) || (  entityX+edgeLength-2>baseGroupX &&  entityX+edgeLength-2<baseGroupX+baseGroupEdgeLength && entityY+2>=baseGroupY && entityY+2<=baseGroupY+baseGroupEdgeLength  ) || (  entityX+2>=baseGroupX &&  entityX+2<=baseGroupX+baseGroupEdgeLength && entityY+edgeLength-2>baseGroupY && entityY+edgeLength-2<baseGroupY+baseGroupEdgeLength  ) || (  entityX+edgeLength-2>baseGroupX &&  entityX+edgeLength-2<baseGroupX+baseGroupEdgeLength && entityY+edgeLength-2>baseGroupY && entityY+edgeLength-2<baseGroupY+baseGroupEdgeLength   )      ){
        	return true;
        }
        
        //y top and bottom larger than basegroup y top and bottom, but either x is within basegroup x
        if( (entityY<baseGroupY && entityY+edgeLength>baseGroupY+baseGroupEdgeLength)&&(entityX+2>=baseGroupX || entityX+edgeLength-2<baseGroupX+baseGroupEdgeLength) ){
        	return true;
        }
        
        //x left and right larger than basegroup x left and right, but either y is within basegroup y
        if( (entityX<baseGroupX && entityX+edgeLength>baseGroupX+baseGroupEdgeLength)&&(entityY+2>=baseGroupY || entityY+edgeLength-2<baseGroupY+baseGroupEdgeLength) ){
        	return true;
        }
        
        //entity contain base group
        if(entityX<baseGroupX && entityX+edgeLength>baseGroupX+baseGroupEdgeLength && entityY<baseGroupY && entityY+edgeLength>baseGroupY+baseGroupEdgeLength){
        	return true;
        }
        
        
        //alert("currentEntity2 "+currentEntity2.data("scale"));
        
        //either 4 corners within base group
                   
        
        
        
        
        
        return false;
	}
	
	function checkWhichBandToChoose(item, occupyPercentage){
		var itids = restraint_groups[item];
		
		var currentMaxScore = -1;
		var maxScore = 0;
		var nextMaxScore = 0;
		var bandToChoose = -1;
		
		if(typeof(itids)!="undefined" && itids!=null){
			//if(checkIfNull($("div[name='group_"+item+"']").data("0"))){
			//	bandToChoose = itids[0];
			//}
			
			//if(false){
				
			//}
			//else{
	    		if(itids.length==1){
	    			bandToChoose = itids[0];
	    		}
	    		else if(checkIfNull($("div[name='group_"+item+"']").data("band"))){
	    			bandToChoose = itids[0];
	    		}
	    		else{
	    			//if(checkIfNull($("div[name='group_"+item+"']").data("band"))){
	    			//	bandToChoose = itids[0];
	    			//}
	    			//else{
	    				for (var i = 0; i < itids.length; i++) {
		    				if(i<itids.length-1){
		    					if(restrains[itids[i]]["p"]==restrains[itids[i+1]]["p"]){
		    						if(occupyPercentage>=(restrains[itids[i-1]]["p"]+restrains[itids[i]]["p"])/2){
		    							maxScore += 50;
		    							nextMaxScore += 40;
		    							
		    							
		    							
		    							if(!checkIfNull($("div[name='group_"+item+"']").data(""+restrains[itids[i]]["bl"]))){
		    								if(restrains[itids[i+1]]["b"]>restrains[itids[i]]["b"]){
		    									//alert("+20");
		    									nextMaxScore += 20;
		    								}
		    							}
		    						}
		    					}
		    					else{
								    if(occupyPercentage<(restrains[itids[i]]["p"]+restrains[itids[i+1]]["p"])/2){
							    		maxScore += 40;
							    		if(occupyPercentage>restrains[itids[i]]["p"] && occupyPercentage<restrains[itids[i+1]]["p"]){
				                			maxScore += 40;
				                		}
				                		/*if(i==itids.length-2){
				                			if(occupyPercentage>restrains[itids[i+1]]["p"]){
				                			    nextMaxScore += 40;
			                			    }
			                			}*/
							    	}
							    	else{
							    		nextMaxScore += 40;
							    		currentMaxScore = -1;
							    		if(occupyPercentage>restrains[itids[i]]["p"] && occupyPercentage<restrains[itids[i+1]]["p"]){
				                			nextMaxScore += 40;
				                		}
				                		/*if(i==itids.length-2){
				                			if(occupyPercentage>restrains[itids[i+1]]["p"]){
				                			    nextMaxScore += 40;
			                			    }
			                			}*/
			                		}
		                		}
		                		
		                		
		                		if(maxScore>nextMaxScore){
		                			if(maxScore>=currentMaxScore){
		                				currentMaxScore = maxScore;
		                				bandToChoose = itids[i];
		                			}
		                		}
		                		else if(nextMaxScore>maxScore){
		                			if(nextMaxScore>=currentMaxScore){
		                				currentMaxScore = nextMaxScore;
		                				bandToChoose = itids[i+1];
		                			}
		                		}
		                		maxScore = 0;
		                		nextMaxScore = 0;
		                		
		                		//if(occupyPercentage>restrains[itids[i]]["p"] && occupyPercentage<restrains[itids[i+1]]["p"]){
		                		//	break;
		                		//}
		                		//if(i==itids.length-2){
		                		//	bandToChoose = itids[i+1];
		                		//}
					    	}
						}
					//}
	    		}
    		//}
    		
    		//writeToErrorConsole("bl chosen "+restrains[bandToChoose]["bl"]);
    		
    		//if(checkIfNull($("div[name='group_"+item+"']").data("band"))){
			//	bandToChoose = itids[0];
			//}
			
			var bandToChooseArray = new Array();
			bandToChooseArray[0] = bandToChoose;
			bandToChooseArray[1] = item;
			//writeToErrorConsole("bl chosen "+restrains[bandToChoose]["bl"]+" item "+item);
			return bandToChooseArray;
		}
	}
	
	
	function checkItem(item, checkOneItem, entityLocalReferenceId, ratio, base_ratio, parentTimespot, nextInsFnc){
		
		setTimeout(function(){
			/*if(checkItemLock || checkItemLock2 || $(document).data("draggingElement")!=null){
		    //if(checkItemLock2){
		    	
				setTimeout(function(){
					new checkItem(item, checkOneItem, entityLocalReferenceId, ratio, base_ratio, parentTimespot);
				}, 20);
				return;
			}*/
			checkItemLock = true;
			
			
            var start = new Date().getTime();
            
			var parent = $("div[name='group_"+entityLocalReferenceId+"']");
			var foundChildren = null;
			
			if(!checkOneItem){
    		    foundChildren = parent.find("div[name='group_"+item+"']");
		    }
		    
    		if((foundChildren==null?false:(foundChildren.length>0)) || item==entityLocalReferenceId){
    			var currentEntity = $("div[name='group_"+item+"']");
    			if(currentEntity.data("scale")==0 && (currentEntity.data("band")!=null || typeof(currentEntity.data("band"))!="undefined")){
    				checkItemLock = false;
    				
    				delete bandCheckQueue[item];
		    
					checkBandQueue();
					if(nextInsFnc!=null){
					    nextInsFnc.exeNext();
				    }
					
    				return;
    			}
    			
                
                var baseGroup = $("div[name='group_0']");
                
              
                var withinDisplay = checkIfItemOnScreen(currentEntity);
                
                
                
                
                var accumulatedScale2 = calAccScale(currentEntity);
                
                var occupyPercentage = accumulatedScale2;
                
                var accumulatedScale3 = accumulatedScale2*baseGroup.data("scale");
                
                var width = full_screen_scale*accumulatedScale3;
                var height = full_screen_scale*accumulatedScale3;
                
                var baseGroupEdgeLength = baseGroup.data("scale")*full_screen_scale;
                currentEntity = $("div[name='group_"+item+"']");
                var currentEntity2 = currentEntity;
                
                
                
                if(width>baseGroupEdgeLength && withinDisplay){
                	withinDisplay = false;
                	currentEntity = currentEntity.parent();
	                
		        	while(true){
		        		var currentEntityString = currentEntity.attr("id");
		        		var currentEntityId = currentEntityString.substring(6, currentEntity.attr("id").length);

		        		
		        		if(currentEntityString=="group_0" || (typeof applications_child_event[currentEntityId] != "undefined" && applications_child_event[currentEntityId]!=null)){
		        			if(currentEntityString!="group_0"){
		        				currentEntity2 = $("div[id='group_"+currentEntityId+"']");
		        				
		        				withinDisplay = checkIfItemOnScreen(currentEntity2);
		        				
		        			}
		        			break;
		        		}
		        		currentEntity = currentEntity.parent();
		        	}
                }
                
                
                
                if(withinDisplay){
                	var bandToChooseArray = checkWhichBandToChoose(item, occupyPercentage);
                	if(!checkIfNull(bandToChooseArray)){
                		if(checkIfNull(timespots[bandToChooseArray[0]])){
							setTimeout(function(){
								new getInstruction("/getBand?appId="+item+"?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?r="+Math.floor(Math.random()*9223372036854775807));
							}, 0);
						}
						new switchAndCleanupBand2(bandToChooseArray, parentTimespot);
                	}
                }
			}
			
			
			checkItemLock = false;
			
			$("div[name='group_"+item+"']").data("bandChecked", "1");
		    
		    
		    
		    delete bandCheckQueue[item];
			
			checkBandQueue();
			if(nextInsFnc!=null){
			    nextInsFnc.exeNext();
		    }
			
		}, 0);
		
	}
	
			
	
	function checkBandQueue(){
		if(checkItemLock2){
			setTimeout(function(){
				new checkBandQueue();
			}, 100);
			return;
		}
		
		bandChecking = true;
		
		if(bandCheckQueue2.length>0){
			var tempArray = bandCheckQueue2.pop();
			//var tempArray = bandCheckQueue2[0];
			//bandCheckQueue2.splice(0, 1);
			new checkItem(tempArray[0], tempArray[1], tempArray[2], tempArray[3], tempArray[4], tempArray[5], tempArray[6]);
			return;
		}
        bandChecking = false;


		
		/*for(var item in bandCheckQueue){
		    //var tempArray = bandCheckQueue.pop();
		    if(bandCheckQueue[item]!=null && typeof(bandCheckQueue[item])!="undefined"){
			    var tempArray = bandCheckQueue[item];
			    
			    //alert("check "+item);

				new checkItem(tempArray[0], tempArray[1], tempArray[2], tempArray[3], tempArray[4], tempArray[5]);
			    //bandCheckQueue.splice(item, 1);
			    
			    //delete bandCheckQueue[item];
			    
			    //alert(bandCheckQueue[item][0]);
			    return;
		    }
	    }*/
		//bandChecking = false;
	}
	
	
	function addItemToBandCheckQueue(item, checkOneItem, entityLocalReferenceId, ratio, base_ratio, parentTimespot, nextInsFnc){
		//if(bandCheckQueue[item]==null || typeof(bandCheckQueue[item])=="undefined"){
			
			
		if(checkIfNull(bandCheckQueue[item])){	
		    var tempArray = new Array();
		    tempArray[0] = item;
		    tempArray[1] = checkOneItem;
		    tempArray[2] = entityLocalReferenceId;
		    tempArray[3] = ratio;
		    tempArray[4] = base_ratio;
		    tempArray[5] = parentTimespot;
		    tempArray[6] = nextInsFnc;
		    
		    bandCheckQueue2.push(tempArray);
		    
		    bandCheckQueue[item] = "1";
	    }
	}
    
    function checkRenderDefinitionBands(entityLocalReferenceId, checkOneItem, ratio, base_ratio, parentTimespot, nextInsFnc){
    	
    	if(checkOneItem){
    		if($("div[name='group_"+entityLocalReferenceId+"']").data("scale")>0){
    			addItemToBandCheckQueue(entityLocalReferenceId, checkOneItem, entityLocalReferenceId, ratio, base_ratio, parentTimespot, nextInsFnc);
    		}
    		else{
    			nextInsFnc.exeNext();
    		}
    	}
    	/*else{    
    		addItemToBandCheckQueue(entityLocalReferenceId, true, entityLocalReferenceId, ratio, base_ratio, parentTimespot, null);
		    
        	for(var item22 in restraint_groups){
        		if(item22!=entityLocalReferenceId){
        			if($("div[name='group_"+entityLocalReferenceId+"']").find("div[name='group_"+item22+"']").length>0){
        			    addItemToBandCheckQueue(item22, checkOneItem, entityLocalReferenceId, ratio, base_ratio, parentTimespot, null);
        			}
        		}
        	}
    	}*/
    	
    	if(!bandChecking){
    	    checkBandQueue();
	    }
    }
    
    function checkIfNull(objectToCheck){
    	if(typeof(objectToCheck)=="undefined" || objectToCheck==null){
    		return true;
    	}
    	return false;
    }
    
    function storeBandTs(bandTs){
    	if(checkIfNull(bandTs)){
    		return;
    	}
    	
    	for(var i=0; i<bandTs.length; i++){
    		var instruction = bandTs[i];
    		if(parseInt(instruction.getAttribute("ins_id"))==IF_GREATER_THAN || parseInt(instruction.getAttribute("ins_id"))==IF_LESS_THAN){
    			var timespotId_ToExecuteIfTrue = instruction.getElementsByTagName("t")[0].firstChild.nodeValue;
                var timespotId_ToExecuteIfFalse = instruction.getElementsByTagName("f")[0].firstChild.nodeValue;
                
                storeBandTs(timespots[timespotId_ToExecuteIfTrue]);
                storeBandTs(timespots[timespotId_ToExecuteIfFalse]);
    		}
    		else if(parseInt(instruction.getAttribute("ins_id"))==EXECUTE_TIMESPOT){
    			var tid = instruction.getElementsByTagName("etid")[0].firstChild.nodeValue;
    			storeBandTs(timespots[tid]);
    		}
    		else if(parseInt(instruction.getAttribute("ins_id"))==SET_RENDER_DEFINITION_BAND){
    			
    			var entityLocalReferenceId = instruction.getElementsByTagName("elr")[0].firstChild.nodeValue;
        		if(!checkIfNull(globals[entityLocalReferenceId]))
                {
                    entityLocalReferenceId = globals[entityLocalReferenceId];
                }
                var newBandLimit_KBpsBandwidth = parseFloat(instruction.getElementsByTagName("b")[0].firstChild.nodeValue);
                var newBandLimit_averageDeepreachInstructionsPerSecond = parseFloat(instruction.getElementsByTagName("ips")[0].firstChild.nodeValue);
        		var newBandLimit_kilobytesVolatileMemory = parseFloat(instruction.getElementsByTagName("m")[0].firstChild.nodeValue);
        		var newBand_percentageOfScreenAvailable = parseFloat(instruction.getElementsByTagName("p")[0].firstChild.nodeValue);
        		var itid = instruction.getElementsByTagName("itid")[0].firstChild.nodeValue;
        		var prtid = instruction.getElementsByTagName("prtid")[0].firstChild.nodeValue;
        		var atid = instruction.getElementsByTagName("atid")[0].firstChild.nodeValue;
        		var potid = instruction.getElementsByTagName("potid")[0].firstChild.nodeValue;
        		var ctid = instruction.getElementsByTagName("ctid")[0].firstChild.nodeValue;
        		
        		var bandLevel = parseInt(instruction.getElementsByTagName("bl")[0].firstChild.nodeValue);
        		
        		if(checkIfNull(restrains[itid])){
    				restrains[itid] = new Array();
    				restrains[itid]["elf"] = entityLocalReferenceId;
    				restrains[itid]["prtid"] = prtid;
    				restrains[itid]["atid"] = atid;
    				restrains[itid]["potid"] = potid;
    				restrains[itid]["ctid"] = ctid;
    				restrains[itid]["b"] = newBandLimit_KBpsBandwidth;
    				restrains[itid]["bl"] = bandLevel;
    				restrains[itid]["ips"] = newBandLimit_averageDeepreachInstructionsPerSecond;
    				restrains[itid]["m"] = newBandLimit_kilobytesVolatileMemory;
    				restrains[itid]["p"] = newBand_percentageOfScreenAvailable;
    				restrains[itid]["preloaded"] = false;
    				restrains[itid]["executed"] = false;
    				if(checkIfNull(restraint_groups[entityLocalReferenceId])){
    					restraint_groups[entityLocalReferenceId] = new Array();
    				}
    				
    				restraint_groups[entityLocalReferenceId][bandLevel] = itid;
    				
    				application_timespots[itid] = entityLocalReferenceId;
    				
    				
				}
    		}
    	}
    }
    
    function storeAllBands(allBandTs){
    	if(checkIfNull(allBandTs)){
    		return;
    	}
    	
    	for(var i=0; i<allBandTs.length; i++){
    		storeBandTs(allBandTs[i]);
    	}
    }
    
    function preloadImages2(tid2){
    	tid = timespots[tid2];
    	
    	if(checkIfNull(tid)){
    		return;
    	}
    	
    	for(var i=0; i<tid.length; i++){
    		var instruction = tid[i];
    		//if(parseInt(instruction.getAttribute("ins_id"))==SET_RENDER_DEFINITION_BAND){
    		//	storeBandTs(tid);
    		//}
    		if(parseInt(instruction.getAttribute("ins_id"))==IF_GREATER_THAN){
    			var timespotId_ToExecuteIfTrue = instruction.getElementsByTagName("t")[0].firstChild.nodeValue;
                var timespotId_ToExecuteIfFalse = instruction.getElementsByTagName("f")[0].firstChild.nodeValue;
                preloadImages2(timespotId_ToExecuteIfTrue);
                preloadImages2(timespotId_ToExecuteIfFalse);
    		}
    		if(parseInt(instruction.getAttribute("ins_id"))==EXECUTE_TIMESPOT){
    			var tidToExe = instruction.getElementsByTagName("etid")[0].firstChild.nodeValue;
    			preloadImages2(tidToExe);
    		}
    		else if(parseInt(instruction.getAttribute("ins_id"))==ADD_MEDIA){
    			var preemptiveEntityId = instruction.getElementsByTagName("peid")[0].firstChild.nodeValue;
                if(globals[preemptiveEntityId]!=null)
                    preemptiveEntityId = globals[preemptiveEntityId];
                var mediaType = parseInt(instruction.getElementsByTagName("mt")[0].firstChild.nodeValue);
                var entityName = instruction.getElementsByTagName("en")[0].firstChild.nodeValue;
                
                
                if(mediaType==media_jpg || mediaType==media_png || mediaType==media_gif){
                	if(checkIfNull(images_base64[preemptiveEntityId])){
                		function addImage(){
                			//writeToErrorConsole("preloading media !!!!!!!!!!!!!!!!!!!...");
				            var request2 = makeRequest();
				        	
				        	function processResponseTwo(){
				        		if(request2.readyState == 2){
				        			var cl = parseFloat(request2.getResponseHeader("Content-Length"));
					            	if(!isNaN(cl)){
					            	    bandwidthUsed += cl;
					        	    }
				        			//bandwidthUsed += parseFloat(request2.getResponseHeader("Content-Length"));
				        		}
					            else if (request2.readyState == 4){
					            	var cl = parseFloat(request2.getResponseHeader("Content-Length"));
					            	if(!isNaN(cl)){
					            	    bandwidthUsed -= cl;
					        	    }
					            	//bandwidthUsed -= parseFloat(request2.getResponseHeader("Content-Length"));
					                if (request2.status == 200){
					                	var responseString = request2.responseText;
					                	
					                	if(!responseString){
					                		setTimeout(function(){new addImage();}, conn_err_wait);
					                		//new addImage();
					                		return;
					                	}
					                	
					                	/*var i = responseString.length;
									    var binaryString = new Array(i);
									    var j = 0;
									    while (i--)
									    {
									        binaryString[j] = String.fromCharCode(responseString.charCodeAt(i) & 0xff);
									        j++;
									    }*/
									    
									    var binaryString = unob(responseString, true);
									    var data = binaryString.join('');
									    
									
									    var base64 = btoa(data);
									    
									    
									
									    var base64MediaHeader = "data:image/jpeg;base64,";
					                	if(mediaType==media_png){
					                		base64MediaHeader = "data:image/png;base64,";
					                	}
					                	else if(mediaType==media_gif){
					                		base64MediaHeader = "data:image/gif;base64,";
					                	}
					                	
					                	images_base64[preemptiveEntityId] = base64MediaHeader+base64;
						                timespotImgsLoaded[tid2] = 1;
					                }
					                else{
					                	setTimeout(function(){new addImage();}, conn_err_wait);
					                	//new addImage();
					                }
				                }
				                
					        }
					        
					    	if(request2)
					        {
					        	//request2.ontimeout = function(){
					        	//	setTimeout(function(){new addImage();}, conn_err_wait);
					        	//};
					        	//request2.onerror = function(){
					        	//	setTimeout(function(){new addImage();}, conn_err_wait);
					        	//};
					            request2.onreadystatechange = processResponseTwo;
					            
					            request2.open("GET", "/addImage?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?imageName="+entityName+"?mediaType="+mediaType, true);
					            request2.overrideMimeType('text/plain; charset=x-user-defined');
					            request2.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
					            request2.send();
					        }
				        }
				        addImage();
                	}
                }
			}
		}
    }
    
    function checkItem2(item, nextInsToExe){    	
    	var itemToCheck = $("div[name='group_"+item+"']");
		if(checkIfItemOnScreen(itemToCheck)){
			//writeToErrorConsole("item is on screen "+item);
			var occupyPercentage = calAccScale(itemToCheck);
			
			if(checkIfNull(occupyPercentage)){
				return;
			}
			
			var bandToChooseArray = checkWhichBandToChoose(item, occupyPercentage);
			if(!checkIfNull(bandToChooseArray)){
				//writeToErrorConsole("item band found");
				var exeBandArray = new Array();
				exeBandArray[0] = bandToChooseArray[0];
				exeBandArray[1] = item;
			    if(checkIfNull(timespots[bandToChooseArray[0]])){
					setTimeout(function(){
						new getInstruction("/getBand?appId="+item+"?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?r="+Math.floor(Math.random()*9223372036854775807));
					}, 0);
				}
				new switchAndCleanupBand2(exeBandArray, null);
				//nextInsToExe();
				setTimeout(function(){new nextInsToExe();}, 0);
			}
			else{
				//nextInsToExe();
				setTimeout(function(){new nextInsToExe();}, 0);
			}
		}
		else{
			nextInsToExe();
		}
	}
	
	function removeItemFromBandQueue(item){
		if(preloadBandQueue.length>0){
			if(preloadBandQueue[preloadBandQueue.length-1]==item){
				preloadBandQueue.splice(preloadBandQueue.length-1, 1);
			}
		}
	}
    
    function checkAndPreloadItem(item){
    	preloadingItem = item;
    	
    	var itemToCheck = $("div[name='group_"+item+"']");
    	var occupyPercentage = calAccScale(itemToCheck);
    	
    	if(checkIfNull(occupyPercentage)){
    		removeItemFromBandQueue(item);
    		setTimeout(function(){
			    new preloadBands();
			}, 100);
    		return;
    	}
    	
    	//var initialBandTid = restraint_groups[item][0];
    	//storeBandTs(timespots[initialBandTid]);
    	
    	//if(checkIfNull($("div[name='group_"+item+"']").data("band"))){
    	//	appBandCheckedTracer[item] = "1";
    	//	preloadingItem = null;
    	//	return;
    	//}
    	
    	
                
        var bandToChooseArray = checkWhichBandToChoose(item, occupyPercentage);
        if(!checkIfNull(bandToChooseArray)){
        	if(checkIfNull(appAllBandsPreloadedTracer[item])){
				setTimeout(function(){
					new getInstruction("/getBand?appId="+item+"?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?r="+Math.floor(Math.random()*9223372036854775807));
					
					setTimeout(function checkAppBandsLoaded(){
						if(checkIfNull(appAllBandsPreloadedTracer[item])){
							setTimeout(function(){
								new checkAppBandsLoaded();
							}, 100);
						}
						else{
							//Go through all the timespots and store the band definitions, then make a new call to this function to choose the right band
							if(!checkIfNull(allAllBandTimespots[item])){
							    storeAllBands(allAllBandTimespots[item]);
						    }
						    
						    if(itemToCheck.data("band")!=bandToChooseArray[0]){
					        	storeBandTs(timespots[bandToChooseArray[0]]);
								storeBandTs(timespots[restrains[bandToChooseArray[0]]["prtid"]]);
								storeBandTs(timespots[restrains[bandToChooseArray[0]]["potid"]]);
							}
						    
							new checkAndPreloadItem(item);
						}
					}, 100);
					
					//for now
					//appAllBandsPreloadedTracer[item] = "1";
					//appBandCheckedTracer[item] = "1";
				}, 0);
			}
			else{
				//this will preload the images for correct band
				
				if(checkIfNull(appBandCheckedTracer[item])){
					var itemIsOnScreen = checkIfItemOnScreen($("div[name='group_"+item+"']"));
					//put transparent layer for high band if app is not taking full size of screen
					/*if(!checkIfNull($("div[name='group_"+item+"']").data("band")) && itemIsOnScreen){
						if(restrains[bandToChooseArray[0]]["bl"]==restraint_groups[item].length-1 && restraint_groups[item].length>1){
							var occupyPercentage = calAccScale($("div[name='group_"+item+"']"));
							if(occupyPercentage<0.95){
								$("div[name='group_hidden_"+item+"']").scale(1);
							}
						}
						else{
							$("div[name='group_hidden_"+item+"']").scale(0);
						}
					}
					else{
						$("div[name='group_hidden_"+item+"']").scale(0);
					}*/
					
					if(itemToCheck.data("band")!=bandToChooseArray[0]){
						//if(restrains[bandToChooseArray[0]]["b"]>0 && getNoOfOpenedRequests()==0){
					    if(itemIsOnScreen){
					    //if(true){
					    	//if(restrains[bandToChooseArray[0]]["b"]>0){
								var exeBandArray = new Array();
								exeBandArray[0] = bandToChooseArray[0];
								exeBandArray[1] = item;
								new switchAndCleanupBand2(exeBandArray, null);
								removeItemFromBandQueue(item);
								//preloadBandQueue.splice(preloadBandQueue.length-1, 1);
								appBandCheckedTracer[item] = "1";
							//}
							//else{
								//preloadImages2(bandToChooseArray[0]);
								//preloadImages2(restrains[bandToChooseArray[0]]["prtid"]);
								//preloadImages2(restrains[bandToChooseArray[0]]["potid"]);
							//}
				    	}
				    	else{
						    //if(restrains[bandToChooseArray[0]]["b"]>0){
							//	var exeBandArray = new Array();
							//	exeBandArray[0] = bandToChooseArray[0];
							//	exeBandArray[1] = item;
							//	new switchAndCleanupBand2(exeBandArray, null);
							//}
							//else{
								if(getPercentageBandwidthUtilised()<0.5){
									new preloadImages2(bandToChooseArray[0]);
									new preloadImages2(restrains[bandToChooseArray[0]]["prtid"]);
									new preloadImages2(restrains[bandToChooseArray[0]]["potid"]);
									removeItemFromBandQueue(item);
									//preloadBandQueue.splice(preloadBandQueue.length-1, 1);
									appBandCheckedTracer[item] = "1";
								}
							//}
						}
						
						setTimeout(function(){
						    new preloadBands();
						}, 100);
					}
					else{
						removeItemFromBandQueue(item);
						//preloadBandQueue.splice(preloadBandQueue.length-1, 1);
						appBandCheckedTracer[item] = "1";
						setTimeout(function(){
						    new preloadBands();
						}, 50);
					}
					
					
					//appBandCheckedTracer[item] = "1";
				}
				else{
					removeItemFromBandQueue(item);
					//preloadBandQueue.splice(preloadBandQueue.length-1, 1);
					setTimeout(function(){
					    new preloadBands();
					}, 50);
				}
				
				preloadingItem = null;
				
			    
			}
        }
        else{
        	writeToErrorConsole("no band found to be chosen...");
        }
    }
    
    
    $(window).data("notStopPreloadBand", true);
    
    
    function pausePreloadBands(){
    	$(window).data("notStopPreloadBand", false);
    }
    function resumePreloadBands(){
    	if(!$(window).data("notStopPreloadBand")){
    	    //preloadBandQueue = findClosestLookingApps();
    	    getNewPreloadBandQueue();
	    }
    	$(window).data("notStopPreloadBand", true);
    }
    
    var newBandQueueNotLocked = true;
    function getNewPreloadBandQueue(){
    	if(!newBandQueueNotLocked){
    		return;
    	}
    	else{
    		newBandQueueNotLocked = false;
    		pausePreloadBands();
    		preloadBandQueue = findClosestLookingApps();
    		$(window).data("notStopPreloadBand", true);
    		newBandQueueNotLocked = true;
    	}
    }
    
    function startPreloadingBands(){
    	if(preloadingNotStarted){
    		preloadingNotStarted = false;
    		new preloadBands();
    	}
    }
    
    function preloadBandCondChecker(){
    	var preloadBandTimer = setInterval(function(){
    		writeToErrorConsole("checker "+getNoOfOpenedRequests()+" "+getPercentageBandwidthUtilised());
	    	//if(getNoOfOpenedRequests()==0 && getPercentageBandwidthUtilised()==0){
	    	if(getPercentageBandwidthUtilised()==0){
	    		clearInterval(preloadBandTimer);
	    		//new preloadBands(null);
	    		startPreloadingBands();
	    	}
    	}, 4000);
    }
    
    var prevItemInfoArray = null;
    
    function preloadBands(){
    	//console.error("start preloadBands...");
    	
    	if(!$(window).data("notStopPreloadBand")){
    		setTimeout(function(){
				new preloadBands();
			}, 100);
			return;
    	}
    	
    	if(!checkIfNull($(document).data("dragEndTime"))){
    	    if(new Date().getTime()-$(document).data("dragEndTime")>6000){
    	    	//writeToErrorConsole("start preloadBands1");
    	    	//preloadBandQueue = findClosestLookingApps();
    	    	getNewPreloadBandQueue();
    	    	//writeToErrorConsole("start preloadBands2");
    	    	$(document).data("dragEndTime", null);
    	    }
	    }
    	
    	
    	//else{
    	//if(getNoOfOpenedRequests()==0 && preloadingItem==null){
        //if(getPercentageBandwidthUtilised()<0.8 && preloadingItem==null){
        //if(getPercentageBandwidthUtilised()<0.7 && preloadingItem==null){
    	//if(preloadingItem==null){	
	    //if(true){		
	    	//var closestItemInfoArray = null;
	    	
	    	//if(checkIfNull(item)){
	    		//closestItemInfoArray = findClosestLookingApp("0");
	    	//}
			//else{
				//closestItemInfoArray = findClosestLookingApp(item);
			//}
			
			if(preloadBandQueue.length!=0){
				//writeToErrorConsole("start preloadBands3");
				var itemNextToCheck = preloadBandQueue[preloadBandQueue.length-1];
				//writeToErrorConsole("start preloadBands4");
				//preloadBandQueue.splice(preloadBandQueue.length-1, 1);
				//writeToErrorConsole("preloadBands band in queue... "+itemNextToCheck);
				new checkAndPreloadItem(itemNextToCheck);
				//writeToErrorConsole("start preloadBands5");
				//setTimeout(function(){
					//writeToErrorConsole("waiting 0");
					//new preloadBands();
				//}, 50);
			}
			else{
				//writeToErrorConsole("preloadBands no band in queue...");
				//appBandCheckedTracer = new Array();
				//writeToErrorConsole("start preloadBands6");
				//preloadBandQueue = findClosestLookingApps();
				getNewPreloadBandQueue();
				//writeToErrorConsole("start preloadBands7");
		    	setTimeout(function(){
					new preloadBands();
				}, 100);
			}
		//}
		//else{
    	//	setTimeout(function(){
    	//		writeToErrorConsole("waiting 1");
		//		new preloadBands();
		//	}, 100);
    	//}
    }
    
    new preloadBandCondChecker();
    
    //new preloadBands();
    
    var prevTopTen = new Array();
    
    function findClosestLookingApps(){
    	var distArray = new Array();
    	var itemArray = new Array();
    	
    	appBandCheckedTracer = new Array();
    	
    	var newTopTen = new Array();
    	
    	for(var item in applications){
    		if($("div[name='group_"+item+"']").length>0){
	    		
	    		
				var nextIndex = distArray.length;
				distArray[nextIndex] = findAppDistanceFromCenter(item);
				
				if(distArray[nextIndex]==0){
					newTopTen.push(item);
				}
				
				//var occupyPercentage = calAccScale($("div[name='group_"+item+"']"));
				//if(checkIfNull(occupyPercentage)){
				//	distArray[nextIndex] = 0;
				//}
				//else{
				//	distArray[nextIndex] = occupyPercentage;
				//}
				
				
				//appBandCheckedTracer[item] = "1";
				
				if(checkIfNull(itemArray[distArray[nextIndex]])){
					var anArray = new Array();
					anArray.push(item);
					itemArray[distArray[nextIndex]] = anArray;
				}
				else{
					var anArray = itemArray[distArray[nextIndex]];
				    anArray.push(item);
			    }
    		}
    		
    	}
    	//last one is shortest distance
    	
    	distArray.sort(function(a,b){return b - a});
    	//distArray.sort(function(a,b){return a - b});
    	
    	
    	var newItemArray = new Array();
    	var counter = 1;
    	var counter2 = 0;
    	var checked = new Array();
    	
    	for(var item in distArray){
    		if(checkIfNull(checked[distArray[item]])){
    			checked[distArray[item]] = "1";
	    		
	    		
	    		for(var i=0; i<itemArray[distArray[item]].length; i++){
	    			newItemArray.push(itemArray[distArray[item]][i]);
	    		}
    		}
    		
    	}
    	
    	
    	//for(var i=newItemArray.length-1; i>=0; i--){
    	//	newTopTen.push(newItemArray[i]);
    	//}
    	
    	//push prev top ten
    	for(var i=0; i<prevTopTen.length; i++){
    		newItemArray.push(prevTopTen[i]);
    	}
    	
    	prevTopTen = newTopTen;
    	
    	return newItemArray;
    }
    
    
    function findClosestLookingApp(entityLocalReferenceId){
    	if(checkIfNull($("div[name='group_"+entityLocalReferenceId+"']").data("children"))){
    		return null;
    	}
    	var childrenArray = $("div[name='group_"+entityLocalReferenceId+"']").data("children");
    	var distArray = new Array();
    	
    	var noItemRemaining = true;
    	
    	for(var item in childrenArray){
    		if(checkIfNull(appBandCheckedTracer[item])){
    			distArray[item] = findAppDistanceFromCenter(item);
    			noItemRemaining = false;
    		}
    	}
    	if(noItemRemaining){
    		return null;
    	}
    	
    	var closestDist = 999999999;
    	var closestItem = null;
    	
    	
    	for(var item in distArray){
    		if(distArray[item]<=closestDist){
    			closestItem = item;
    			closestDist = distArray[item];
    		}
    	}
    	
    	//delete childrenArray[closestItem];
    	//$("div[name='group_"+closestItem+"']").remove();
    	
    	var distInfoArray = new Array();
    	distInfoArray[0] = closestItem;
    	distInfoArray[1] = closestDist;
    	
    	return distInfoArray;
    }
    
    
    function findAppDistanceFromCenter(entityLocalReferenceId){
    	var entityToCheck = $("div[name='group_"+entityLocalReferenceId+"']");
    	
    	var accRatio = calAccScale(entityToCheck);
    	
    	if(checkIfNull(accRatio)){
    		return 0;
    	}
    	
    	var baseGroup = $("div[name='group_0']");
    	
    	//var edgeLength = full_screen_scale*accRatio*baseGroup.data("scale");
    	var halfEdgeLength = full_screen_scale*accRatio*baseGroup.data("scale")/2;
    	
    	var entityX = parseFloat(entityToCheck.offset().left);
		var entityY = parseFloat(entityToCheck.offset().top);
		var centerX = entityX+halfEdgeLength;
		var centerY = entityY+halfEdgeLength;
		
		var baseGroupHalfEdgeLength = full_screen_scale*baseGroup.data("scale")/2;
		var baseGroupX = baseGroup.offset().left;
		var baseGroupY = baseGroup.offset().top;
		
		//var baseGroupEdgeLength = full_screen_scale*baseGroup.data("scale");
		var baseGroupCenterX = baseGroupX+baseGroupHalfEdgeLength;
		var baseGroupCenterY = baseGroupY+baseGroupHalfEdgeLength;
    	
		//var dist = Math.abs(centerY-baseGroupCenterY);
		
		var dist = Math.sqrt(Math.pow(centerX-baseGroupCenterX, 2)+Math.pow(centerY-baseGroupCenterY, 2));
		//alert("centerX "+centerX+" baseGroupCenterX "+baseGroupCenterX+" centerY "+centerY+" baseGroupCenterY "+baseGroupCenterY+" dist "+dist);
		
    	return dist;
    }
    
    function removeFromParentApp(entityLocalReferenceId){
    	var currentEntity = $("div[name='group_"+entityLocalReferenceId+"']");
    	if(checkIfNull(currentEntity)){
    		return;
    	}
    	
    	currentEntity = currentEntity.parent();
    	
    	while(true){
    		var currentEntityString = currentEntity.attr("id");
    		if(currentEntityString=="sceengraph"){
    			return;
    		}
    		else{
    		    var currentEntityLFId = currentEntity.attr("name").substring(6, currentEntity.attr("name").length);
    			
    			if(!checkIfNull(restraint_groups[currentEntityLFId]) || currentEntityString=="group_0"){
    				if(!checkIfNull($("div[name='group_"+currentEntityLFId+"']").data("children"))){
    					var childrenArray = $("div[name='group_"+currentEntityLFId+"']").data("children");
    					delete childrenArray[entityLocalReferenceId];
    				}
    				return;
    			}
    		}
    		currentEntity = currentEntity.parent();
		}
    }
    
    function findParent(entityLocalReferenceId){
    	var currentEntity = $("div[name='group_"+entityLocalReferenceId+"']");
    	if(checkIfNull(currentEntity)){
    		return;
    	}
    	
    	currentEntity = currentEntity.parent();
    	
    	while(true){
    		var currentEntityString = currentEntity.attr("id");
    		if(currentEntityString=="sceengraph"){
    			return;
    		}
    		else{
    		    var currentEntityLFId = currentEntity.attr("name").substring(6, currentEntity.attr("name").length);
    			
    			if(!checkIfNull(restraint_groups[currentEntityLFId]) || currentEntityString=="group_0"){
    				if(checkIfNull($("div[name='group_"+currentEntityLFId+"']").data("children"))){
    					$("div[name='group_"+currentEntityLFId+"']").data("children", new Array());
    				}
    				var childrenArray = $("div[name='group_"+currentEntityLFId+"']").data("children");
    				
    				
    				
    				childrenArray[entityLocalReferenceId] = "1";
    				
    				//$("div[name='group_"+currentEntityLFId+"']").data("children", childrenArray);
    				return;
    			}
    		}
    		currentEntity = currentEntity.parent();
		}
    }
    
    function executePreloadTimespot(timespotId, parentTimespot)
    {
    	if(timespots[timespotId]==null || typeof(timespots[timespotId])=="undefined"){
			setTimeout(function(){
				new executePreloadTimespot(timespotId, parentTimespot);
			}, 300);
		}
		else{
        	if(preloading_timespots[timespotId]!=null || running_timespots[timespotId]!=null){
        		return;
        	}
        	
        	
        	if(relatedStoppables[timespotId]==null){
        		relatedStoppables[timespotId] = new Array();
        	}
            
            if(preloading_timespots[timespotId]==null)
                preloading_timespots[timespotId] = new Array();
            preloading_timespots[timespotId].push(this);
            var currentInstruction = null;
            var noi = 0;
            var stop = false;
            var instructionList = timespots[timespotId];

            var indicator = 0;
            
            this.getIndicator = function(){return indicator;}
            this.getId = function(){return timespotId;}
            this.stopExecution = function()
            {
                stop = true;
                //if(currentInstruction != null)
                    //currentInstruction.stopExecution();
                    
                //while(relatedStoppables[timespotId].length>0){
                //    relatedStoppables[timespotId].pop().stopExecution();
                //}
            }
                
            if(!stop && instructionList){
            	if(noi<instructionList.length){
            	    currentInstruction = new preloadImages(timespotId, instructionList[noi], noi, instructionList, relatedStoppables[timespotId], this);
            	    //relatedStoppables[timespotId].push(currentInstruction);
            	}
            	else{
            		preloading_timespots[timespotId] = null;
            		if(restrains[timespotId]!=null && typeof(restrains[timespotId])!="undefined"){
                		restrains[timespotId]["preloaded"] = true;
                	}
            	}
            }
            else{
            	if(restrains[timespotId]!=null && typeof(restrains[timespotId])!="undefined"){
            		restrains[timespotId]["preloaded"] = true;
            	}
            	preloading_timespots[timespotId] = null;
            }
        }
    }
    
    function checkIdle(){
    	//writeToErrorConsole("called checkIdle");
    	
    	var idleTimer = setInterval(function(){
    		/*var rtLength = 0;
    		var ptLength = 0;
    		
    		for(var item in running_timespots){
    			if(running_timespots[item]!=null){
    				rtLength++;
    			}
    		}
    		for(var item in preloading_timespots){
    			if(preloading_timespots[item]!=null){
    				ptLength++;
    			}
    		}
    		if(rtLength==0 && ptLength==0){
    			for(var item in restrains){
    				if(!restrains[item]["preloaded"]){
    					function exPT(item){
    						new executePreloadTimespot(item, null);
    						new executePreloadTimespot(restrains[item]["prtid"], null);
    						new executePreloadTimespot(restrains[item]["potid"], null);
    					}
    					exPT(item);
    				}
    			}
    		}*/
    		//writeToErrorConsole("opened requests "+getNoOfOpenedRequests());
    		
    		if(getNoOfOpenedRequests()==0){
    			//writeToErrorConsole("start preload bands");
    			preloadBands();
    			//setTimeout(function(){
        		//	new checkIdle();
        		//}, 3000);
    			//clearInterval(idleTimer);
    		}
    		        		
    		/*if(getNoOfOpenedRequests()==0 && awaiting_bands.length>0){
    			for(var i=0; i<awaiting_bands.length; i++){
    				if(typeof(awaiting_bands[i])!="undefined" && awaiting_bands[i]!=null){
    				    var item = application_timespots[awaiting_bands[i]];
    				    new checkItem(item, true, item, 0, 0, null, null);
				    }
    			}
    		}*/
    		
    		/*if(getNoOfOpenedRequests()==0){
    			for(var item in restrains){
    				if(!restrains[item]["preloaded"]){
    					if(getNoOfOpenedRequests()>0){
    						break;
    					}
    					function exPT(item){
    						new executePreloadTimespot(item, null);
    						new executePreloadTimespot(restrains[item]["prtid"], null);
    						new executePreloadTimespot(restrains[item]["potid"], null);
    					}
    					exPT(item);
    				}
    			}
    		}*/
    		else{
        		//setTimeout(function(){
        		//	new checkIdle();
        		//}, 3000);
    		}
    		
    	}, 3000);
    }
    //new checkIdle();
    //setTimeout(checkIdle(), 0);
    
    function preloadImages(timespot_id, instruction, noi, instructionList, parentArray, parentTimespot){
    	var stop = false;
    	if(stop){
    		
    	}
    	else{
    		switch(parseInt(instruction.getAttribute("ins_id"))){
    			case EXECUTE_GLOBAL:
                {
                    var localReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                    var globals_localReferenceId = globals[localReferenceId];
                    if(globals_localReferenceId!=null)
                    {
                        if(!stop){
                        	new executePreloadTimespot(globals_localReferenceId, parentTimespot);
                        }
                    }
                    
                    setTimeout(function(){new executeNextPreloadInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);}, 0);
                    
                    
                    break;
                }
                case EXECUTE_TIMESPOT:
                {
                    var tid = instruction.getElementsByTagName("etid")[0].firstChild.nodeValue;
                    if(!stop){
                    	new executePreloadTimespot(tid, parentTimespot);
                    }
                    
                    
                    setTimeout(function(){new executeNextPreloadInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);}, 0);
                    break;
                }
                case SET_RENDER_DEFINITION_BAND:{
                	var entityLocalReferenceId = instruction.getElementsByTagName("elr")[0].firstChild.nodeValue;
                
            		
                    var newBandLimit_KBpsBandwidth = parseFloat(instruction.getElementsByTagName("b")[0].firstChild.nodeValue);
                    var newBandLimit_averageDeepreachInstructionsPerSecond = parseFloat(instruction.getElementsByTagName("ips")[0].firstChild.nodeValue);
            		var newBandLimit_kilobytesVolatileMemory = parseFloat(instruction.getElementsByTagName("m")[0].firstChild.nodeValue);
            		var newBand_percentageOfScreenAvailable = parseFloat(instruction.getElementsByTagName("p")[0].firstChild.nodeValue);
            		var itid = instruction.getElementsByTagName("itid")[0].firstChild.nodeValue;
            		var prtid = instruction.getElementsByTagName("prtid")[0].firstChild.nodeValue;
            		var atid = instruction.getElementsByTagName("atid")[0].firstChild.nodeValue;
            		var potid = instruction.getElementsByTagName("potid")[0].firstChild.nodeValue;
            		var ctid = instruction.getElementsByTagName("ctid")[0].firstChild.nodeValue;
            		
    				/*if(timespots[itid]==null || typeof(timespots[itid])=="undefined"){
    					new getInstruction("/getBand?appId="+entityLocalReferenceId+"?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?r="+Math.floor(Math.random()*9223372036854775807));
    				}*/
    				
                    setTimeout(function(){new executePreloadTimespot(itid, parentTimespot);}, 0);
                    setTimeout(function(){new executePreloadTimespot(prtid, parentTimespot);}, 0);
                    setTimeout(function(){new executePreloadTimespot(potid, parentTimespot);}, 0);
    				
            		
            		setTimeout(function(){new executeNextPreloadInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);}, 0);
            		break;
        		}
                case IF_EQUAL_TO:
                {
                    var timespotId_ToExecuteIfTrue = instruction.getElementsByTagName("t")[0].firstChild.nodeValue;
                    var timespotId_ToExecuteIfFalse = instruction.getElementsByTagName("f")[0].firstChild.nodeValue;
                    
                    
                    
                    
                    
                    setTimeout(function(){new executePreloadTimespot(timespotId_ToExecuteIfTrue, parentTimespot);}, 0);
                	
                    setTimeout(function(){new executePreloadTimespot(timespotId_ToExecuteIfFalse, parentTimespot);}, 0);
                	
                	
                        
                    setTimeout(function(){new executeNextPreloadInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);}, 0);
                    break;
                }
                case IF_GREATER_THAN:
                {
                	
                    
                    var timespotId_ToExecuteIfTrue = instruction.getElementsByTagName("t")[0].firstChild.nodeValue;
                    var timespotId_ToExecuteIfFalse = instruction.getElementsByTagName("f")[0].firstChild.nodeValue;
                    
                    
                    
                    
                	setTimeout(function(){new executePreloadTimespot(timespotId_ToExecuteIfTrue, parentTimespot);}, 0);
            	
                	setTimeout(function(){new executePreloadTimespot(timespotId_ToExecuteIfFalse, parentTimespot);}, 0);
                	
                        
                    setTimeout(function(){new executeNextPreloadInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);}, 0);
                    break;
                }
                case IF_LESS_THAN:
                {
                    
                    var timespotId_ToExecuteIfTrue = instruction.getElementsByTagName("t")[0].firstChild.nodeValue;
                    var timespotId_ToExecuteIfFalse = instruction.getElementsByTagName("f")[0].firstChild.nodeValue;
                    
                    

                    
                    
                    setTimeout(function(){new executePreloadTimespot(timespotId_ToExecuteIfTrue, parentTimespot);}, 0);
                	
                    setTimeout(function(){new executePreloadTimespot(timespotId_ToExecuteIfFalse, parentTimespot);}, 0);
                	
                        
                    setTimeout(function(){new executeNextPreloadInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);}, 0);
                    break;
                }
    			case ADD_MEDIA:{
    				var preemptiveEntityId = instruction.getElementsByTagName("peid")[0].firstChild.nodeValue;
                    if(globals[preemptiveEntityId]!=null)
                        preemptiveEntityId = globals[preemptiveEntityId];
                    var mediaType = parseInt(instruction.getElementsByTagName("mt")[0].firstChild.nodeValue);
                    var entityName = instruction.getElementsByTagName("en")[0].firstChild.nodeValue;
                    
                    //alert("preload after initialise");
                    if(mediaType==media_jpg || mediaType==media_png || mediaType==media_gif)
                    {
                    	//alert("preload media type checked");
                    	var alreadyInCache = false;
                    	if(images_base64[preemptiveEntityId]!=null && images_base64[preemptiveEntityId]!=""){
                            alreadyInCache = true;
                        }
                        else{
                        	images_base64[preemptiveEntityId] = "";
                        }
                        if(!stop)
                        {
                        	if(alreadyInCache){
                            	setTimeout(function(){new executeNextPreloadInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);}, 0);
                            }
                        	
                        	else{
                            	function addImage2(){
						            var request2 = makeRequest();
						        	
						        	function processResponseTwo(){
						        		if(request2.readyState == 2){
						        			var cl = parseFloat(request2.getResponseHeader("Content-Length"));
							            	if(!isNaN(cl)){
							            	    bandwidthUsed += cl;
							        	    }
						        		    //bandwidthUsed += parseFloat(request2.getResponseHeader("Content-Length"));
					        			}
							            else if(request2.readyState == 4){
							            	var cl = parseFloat(request2.getResponseHeader("Content-Length"));
							            	if(!isNaN(cl)){
							            	    bandwidthUsed -= cl;
							        	    }
							            	//bandwidthUsed -= parseFloat(request2.getResponseHeader("Content-Length"));
							                if (request2.status == 200){
							                	
							                	setTimeout(function(){
							                	    new function(){
									                	var responseString = request2.responseText;
									                	
									                	if(!responseString){
									                		//new addImage2();
									                		setTimeout(function(){new addImage2();}, conn_err_wait);
									                		return;
									                	}
									                	
									                	/*var i = responseString.length;
													    var binaryString = new Array(i);
													    var j = 0;
													    while (i--)
													    {
													        binaryString[j] = String.fromCharCode(responseString.charCodeAt(i) & 0xff);
													        j++;
													    }*/
													    
													    var binaryString = unob(responseString, true);
													    var data = binaryString.join('');
													    
													
													    var base64 = btoa(data);
													    
													    
													
													    var base64MediaHeader = "data:image/jpeg;base64,";
									                	if(mediaType==media_png){
									                		base64MediaHeader = "data:image/png;base64,";
									                	}
									                	else if(mediaType==media_gif){
									                		base64MediaHeader = "data:image/gif;base64,";
									                	}
									                	images_base64[preemptiveEntityId] = base64MediaHeader+base64;
									                	
									                									                	
									                	setTimeout(function(){new executeNextPreloadInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);}, 0);
								                    }
									            }, 0);
							                }
							                else{
							                	//alert("preload non 200");
							                	setTimeout(function(){new addImage2();}, conn_err_wait);
							                	//new addImage2();
							                }
						                }
						                
							        }
							    	
							    	if(request2)
							        {
							        	//request2.ontimeout = function(){
							        	//	setTimeout(function(){new addImage2();}, conn_err_wait);
							        	//};
							        	//request.onerror = function(){
							        	//	setTimeout(function(){new addImage2();}, conn_err_wait);
							        	//};
							            request2.onreadystatechange = processResponseTwo;
							            request2.open("GET", "/addImage?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?imageName="+entityName+"?mediaType="+mediaType, true);
							            request2.overrideMimeType('text/plain; charset=x-user-defined');
							            request2.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
							            request2.send();
							        }
						        }
						        addImage2();
					        }
                        }
                    }
                    else{
                    	setTimeout(function(){new executeNextPreloadInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);}, 0);
                    }
                    break;
    			}
    			default:
                {
                    setTimeout(function(){new executeNextPreloadInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);}, 0);
                    break;
                }
    		}
    	}
    }
    
    /*function setPinchEvent(entityLF){
    	return;
    	
    	var parent = $("div[name='group_"+entityLF+"']");
    	
    	if(checkIfNull(parent) || !checkIfNull(parent.data("bindedPinchEvent"))){
    		return;
    	}
    	
    	var iAndAndroidDevices = false;
        var ua = navigator.userAgent.toLowerCase();
        
        if(ua.indexOf("iphone")!=-1 || ua.indexOf("ipad")!=-1 || ua.indexOf("ipod")!=-1 || ua.indexOf("android")!=-1){
        	iAndAndroidDevices = true;
        }
        
        var pinchEvent = "";
        var endPinchEvent = "";
        
        if(iAndAndroidDevices){
        	pinchEvent = 'touchstart';
        	endPinchEvent = 'touchend';
        }
        else{
            pinchEvent = 'mousedown';
            endPinchEvent = 'mouseup';
        }
    	
        parent.data("bindedPinchEvent", "1");
        
    	parent.bind(pinchEvent, function(e){
    		if(!checkIfNull($(document).data("1stPinchItem"))){
    			if($(document).data("1stPinchItem")!=this){
    				return;
    			}
    		}
    		$(document).data("1stPinchItem", this);
    		
    		
    		//if(this==event.target){
    			//alert(event.target);
    		//}
    		//e.preventDefault();
    		//e.stopPropagation();
    		
    		
            var x = e.pageX || e.screenX;
            var y = e.pageY || e.screenY;
            var parent = $(this);
            parent.data("touchStartTime", new Date().getTime());
            
            parent.bind(endPinchEvent, function(event2){
            	$(document).data("1stPinchItem", null);
            	var parent = $(this);
            	parent.unbind(endPinchEvent);
            	
            	
        		if(new Date().getTime()-parent.data("touchStartTime")>100){
        			
        			if(!checkIfNull($(document).data("dragStarted"))){
    					return;
    				}
        			
        			//it's a pinch
        			var entityLocalReferenceId = entityLF;
                    if(globals[entityLocalReferenceId]!=null)
                    {
                        entityLocalReferenceId = globals[entityLocalReferenceId];
                    }
                    //var occupyPercentage = 1;
                    	                    
                    
              	    var parent = $("div[name='group_"+entityLocalReferenceId+"']");
              	  
              	    
              	    //alert(parent.data("scale"));
              	        
      	      	    tempAccScales = new Array();
                  
                  
                    var accumulatedScale = calAccScale(parent);
                    var accumulatedScale2 = accumulatedScale*$("div[name='group_0']").data("scale");
                    
                    var occupyPercentage = accumulatedScale*parseFloat(((new Date().getTime()-parent.data("touchStartTime"))/1000)+1)*1.5;
                  
                    tempAccScales = new Array();
                  
                    var currentEntity = $("div[name='group_"+entityLocalReferenceId+"']");
                  
                  
                   
                    var tempElementX = parseFloat(currentEntity.offset().left);
                    var tempElementY = parseFloat(currentEntity.offset().top);
                  
                  
                  
                    var width = 1000*accumulatedScale2;
                    var height = 1000*accumulatedScale2;
                  
                    var factor = occupyPercentage/accumulatedScale;
                    //var factor = ($("div[name='group_0']").data("scale")*1000*occupyPercentage)/(1000*accumulatedScale2/currentEntity.data("scale"));
                    
                    //alert(occupyPercentage);
                  
                    //var ratio = 1/accumulatedScale;
                    var base_ratio = camera.getScale();
                    var ratio = factor*base_ratio;
                   
                    
                   
                    var final_ratio = ratio/base_ratio;
                 
                                                 
                  		                                  
                    var x_orig = camera.getX();
                    var y_orig = camera.getY();
                  
                  
                  
                    var x_dist = (($("div[name='group_0']").offset().left+(1000*$("div[name='group_0']").data("scale")/2))-(width/2)-tempElementX)/(accumulatedScale2/accumulatedScale);
                    x_dist = (x_dist)*(final_ratio)+(x_orig*(ratio-base_ratio)/base_ratio);
                  
                    var y_dist = (($("div[name='group_0']").offset().top+(1000*$("div[name='group_0']").data("scale")/2))-(height/2)-tempElementY)/(accumulatedScale2/accumulatedScale);
                    y_dist = (y_dist)*(final_ratio)+(y_orig*(ratio-base_ratio)/base_ratio);

                    
                    camera.moveCamera(ratio, x_dist, y_dist, true, true);
        		}
        	});
		});
    }*/
    
    
    function executeInstruction(timespot_id, instruction, noi, instructionList, parentArray, parentTimespot)
    {
        writeToConsole("executeInstruction "+instruction.getAttribute("ins_id"));
        var stop = false;

        this.stopExecution = function()
        {
            stop=true;
            writeToErrorConsole("Stopping execution of instruction "+ instruction.getAttribute("ins_id") +" timespot id = " + parentTimespot.getId());
        }
        
        
        if(stop){
        	writeToErrorConsole("Stopping execution of instruction2 "+ instruction.getAttribute("ins_id") +" timespot id = " + parentTimespot.getId());
        }
        else{
            switch(parseInt(instruction.getAttribute("ins_id")))
            {
            	case SET_TOP_PRIORITY:{
    				var parentEntityLocalReferenceId = instruction.getElementsByTagName("elf")[0].firstChild.nodeValue;
    				var type = parseInt(instruction.getElementsByTagName("type")[0].firstChild.nodeValue);
    				
    				if(type==TOP_PRIORITY_CONTEXT){
    					
    				}
    				else if(type==TOP_PRIORITY_SERVER){
    					
    				}
    				
            		
            		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
    				break;
    			}
    			case SET_ESCAPE_EVENT_ON_BROWSER_EVENT:{
    				
    				var parentEntityLocalReferenceId = instruction.getElementsByTagName("peid")[0].firstChild.nodeValue;
            	    var childEntityLocalReferenceId = instruction.getElementsByTagName("ceid")[0].firstChild.nodeValue;
            		if(globals[parentEntityLocalReferenceId]!=null)
                    {
                        parentEntityLocalReferenceId = globals[parentEntityLocalReferenceId];
                    }
                    if(globals[childEntityLocalReferenceId]!=null)
                    {
                        childEntityLocalReferenceId = globals[childEntityLocalReferenceId];
                    }
                    var tid = instruction.getElementsByTagName("tid")[0].firstChild.nodeValue;
                    var passUpwards = instruction.getElementsByTagName("pu")[0].firstChild.nodeValue;
                    
                    if(checkIfNull(applications_child_escape[parentEntityLocalReferenceId])){
    					applications_child_escape[parentEntityLocalReferenceId] = new Array();
    				}
    				if(checkIfNull(applications_child_escape[parentEntityLocalReferenceId][childEntityLocalReferenceId])){
    					applications_child_escape[parentEntityLocalReferenceId][childEntityLocalReferenceId] = new Array();
    				}
    				
    				if(applications_child_escape[parentEntityLocalReferenceId][childEntityLocalReferenceId].length==0){
    				    applications_child_escape[parentEntityLocalReferenceId][childEntityLocalReferenceId]['tid'] = tid;
    				    applications_child_escape[parentEntityLocalReferenceId][childEntityLocalReferenceId]['pu'] = passUpwards;
				    }
    				executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    
    				break;
    			}
    			case SET_EVENT_PROPAGATION:{
    				var parentEntityId = instruction.getElementsByTagName("peid")[0].firstChild.nodeValue;
            	    var localReferenceId = instruction.getElementsByTagName("lf")[0].firstChild.nodeValue;
            		if(globals[parentEntityId]!=null)
                    {
                        parentEntityId = globals[parentEntityId];
                    }
                    if(globals[localReferenceId]!=null)
                    {
                        localReferenceId = globals[localReferenceId];
                    }
                    var tid = instruction.getElementsByTagName("tid")[0].firstChild.nodeValue;
                    var eventType = parseInt(instruction.getElementsByTagName("et")[0].firstChild.nodeValue);
                    var passUpwards = parseInt(instruction.getElementsByTagName("pu")[0].firstChild.nodeValue);
                    if(checkIfNull(event_pro[parentEntityId])){
                    	event_pro[parentEntityId] = new Array();
                    }
                    if(checkIfNull(event_pro[parentEntityId][localReferenceId])){
                    	event_pro[parentEntityId][localReferenceId] = new Array();
                    }
                    if(checkIfNull(event_pro[parentEntityId][localReferenceId][eventType])){
                    	event_pro[parentEntityId][localReferenceId][eventType] = new Array();
                    }
                    event_pro[parentEntityId][localReferenceId][eventType]['tid'] = tid;
                    event_pro[parentEntityId][localReferenceId][eventType]['pu'] = passUpwards;
                    //console.error("set prop "+parentEntityId+" "+localReferenceId);
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
    				break;
    			}
            	case SET_RECEIVE_EVENT_FROM_CHILD_ON_BROWSER_EVENT:{
            	    var parentEntityLocalReferenceId = instruction.getElementsByTagName("peid")[0].firstChild.nodeValue;
            	    var childEntityLocalReferenceId = instruction.getElementsByTagName("ceid")[0].firstChild.nodeValue;
            		if(globals[parentEntityLocalReferenceId]!=null)
                    {
                        parentEntityLocalReferenceId = globals[parentEntityLocalReferenceId];
                    }
                    if(globals[childEntityLocalReferenceId]!=null)
                    {
                        childEntityLocalReferenceId = globals[childEntityLocalReferenceId];
                    }
                    var tid = instruction.getElementsByTagName("tid")[0].firstChild.nodeValue;
                    var passUpwards = instruction.getElementsByTagName("pu")[0].firstChild.nodeValue;
                    
                    
                    if(typeof applications_child_event[parentEntityLocalReferenceId] == "undefined" || applications_child_event[parentEntityLocalReferenceId]==null){
    					applications_child_event[parentEntityLocalReferenceId] = new Array();
    				}
    				if(typeof applications_child_event[parentEntityLocalReferenceId][childEntityLocalReferenceId] == "undefined" || applications_child_event[parentEntityLocalReferenceId][childEntityLocalReferenceId]==null){
    					applications_child_event[parentEntityLocalReferenceId][childEntityLocalReferenceId] = new Array();
    				}
    				
    				if(applications_child_event[parentEntityLocalReferenceId][childEntityLocalReferenceId].length==0){
    				    applications_child_event[parentEntityLocalReferenceId][childEntityLocalReferenceId]['tid'] = tid;
    				    applications_child_event[parentEntityLocalReferenceId][childEntityLocalReferenceId]['pu'] = passUpwards;
				    }
    				executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
            		break;	
            	}
            	case SET_RENDER_DEFINITION_BAND:{
            		//alert("set render");
            		var entityLocalReferenceId = instruction.getElementsByTagName("elr")[0].firstChild.nodeValue;
            		if(globals[entityLocalReferenceId]!=null)
                    {
                        entityLocalReferenceId = globals[entityLocalReferenceId];
                    }
                    var newBandLimit_KBpsBandwidth = parseFloat(instruction.getElementsByTagName("b")[0].firstChild.nodeValue);
                    var newBandLimit_averageDeepreachInstructionsPerSecond = parseFloat(instruction.getElementsByTagName("ips")[0].firstChild.nodeValue);
            		var newBandLimit_kilobytesVolatileMemory = parseFloat(instruction.getElementsByTagName("m")[0].firstChild.nodeValue);
            		var newBand_percentageOfScreenAvailable = parseFloat(instruction.getElementsByTagName("p")[0].firstChild.nodeValue);
            		var itid = instruction.getElementsByTagName("itid")[0].firstChild.nodeValue;
            		var prtid = instruction.getElementsByTagName("prtid")[0].firstChild.nodeValue;
            		var atid = instruction.getElementsByTagName("atid")[0].firstChild.nodeValue;
            		var potid = instruction.getElementsByTagName("potid")[0].firstChild.nodeValue;
            		var ctid = instruction.getElementsByTagName("ctid")[0].firstChild.nodeValue;
            		
            		var bandLevel = parseInt(instruction.getElementsByTagName("bl")[0].firstChild.nodeValue);
            		
            		//var atid = instruction.getElementsByTagName("atid")[0].firstChild.nodeValue;
            		
            		if(checkIfNull(restrains[itid])){
        				restrains[itid] = new Array();
        				//restrains[itid]["atid"] = atid;
        				restrains[itid]["elf"] = entityLocalReferenceId;
        				restrains[itid]["prtid"] = prtid;
        				restrains[itid]["atid"] = atid;
        				restrains[itid]["potid"] = potid;
        				restrains[itid]["ctid"] = ctid;
        				restrains[itid]["b"] = newBandLimit_KBpsBandwidth;
        				restrains[itid]["bl"] = bandLevel;
        				restrains[itid]["ips"] = newBandLimit_averageDeepreachInstructionsPerSecond;
        				restrains[itid]["m"] = newBandLimit_kilobytesVolatileMemory;
        				restrains[itid]["p"] = newBand_percentageOfScreenAvailable;
        				restrains[itid]["preloaded"] = false;
        				restrains[itid]["executed"] = false;
        				if(checkIfNull(restraint_groups[entityLocalReferenceId])){
        					restraint_groups[entityLocalReferenceId] = new Array();
        				}
        				
        				restraint_groups[entityLocalReferenceId][bandLevel] = itid;
        				
        				
        				//create hidden group for high bands
        				/*if($("div[name='group_hidden_"+entityLocalReferenceId+"']").length==0){
	        				$("div[name='group_"+entityLocalReferenceId+"']").addGroup("hidden_"+entityLocalReferenceId, {width: full_screen_scale, height: full_screen_scale, preemptiveLocalReferenceId: "hidden_"+entityLocalReferenceId});
	        				
	        				$("div[name='group_hidden_"+entityLocalReferenceId+"']").css("pointer-events", "visible");
	        				$("div[name='group_hidden_"+entityLocalReferenceId+"']").scale(0);
	        				
	        				var pinchEvent = "mousedown";
	        				var hiddenClick = "mouseup";
	        				if(checkIfMobile()){
	        					hiddenClick = "touchend";
	        					pinchEvent = "touchstart";
	        				}
	        				
	        				$("div[name='group_hidden_"+entityLocalReferenceId+"']").bind(pinchEvent, function(e){
	        					$("div[name='group_hidden_"+entityLocalReferenceId+"']").data("touchStartTime", new Date().getTime());
	        					$("div[name='group_hidden_"+entityLocalReferenceId+"']").bind(hiddenClick, function(e2){
	        						$("div[name='group_hidden_"+entityLocalReferenceId+"']").unbind(hiddenClick);
	        						if(new Date().getTime()-$("div[name='group_hidden_"+entityLocalReferenceId+"']").data("touchStartTime")<500){
	        							$("div[name='group_hidden_"+entityLocalReferenceId+"']").scale(0);
	        							camera.zoomIntoApp(entityLocalReferenceId);
	    							}
	    						});
	        				});
	        				
	        				
        				}*/
        				
        				
        				
        				//restraint_groups[entityLocalReferenceId].push(itid);
        				
        				application_timespots[itid] = entityLocalReferenceId;
        				
        				applications[entityLocalReferenceId] = "1";
        				
        				findParent(entityLocalReferenceId);
        				
        				//setPinchEvent(entityLocalReferenceId);
        				if(bandLevel<=1){
        					var nextInsToExe = function(){
        						executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
        					};
        					setTimeout(function(){new checkItem2(entityLocalReferenceId, nextInsToExe);}, 0);
          				    //checkItem2(entityLocalReferenceId, nextInsToExe);
      				    }
      				    else{
        				    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
    				    }
        				
        				
        				
        				/*if(newBandLimit_KBpsBandwidth==0){	
	        				function bandNextIns(noi, instructionList, timespot_id, parentArray, parentTimespot, stop){
	        					this.exeNext = function(){
	        						executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	    						}
	        				}
	        				var nextInsFnc = new bandNextIns(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	        			    
	        				setTimeout(function(){
	        					new checkRenderDefinitionBands(entityLocalReferenceId, true, 0, 0, parentTimespot, nextInsFnc);
	        				}, 0);
        				}
        				else{
        					executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
        				}*/
        				
        				
        				//executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
    				}
    				else{
    					//create hidden group for high bands
        				/*if($("div[name='group_hidden_"+entityLocalReferenceId+"']").length==0){
	        				$("div[name='group_"+entityLocalReferenceId+"']").addGroup("hidden_"+entityLocalReferenceId, {width: full_screen_scale, height: full_screen_scale, preemptiveLocalReferenceId: "hidden_"+entityLocalReferenceId});
	        				
	        				$("div[name='group_hidden_"+entityLocalReferenceId+"']").css("pointer-events", "visible");
	        				$("div[name='group_hidden_"+entityLocalReferenceId+"']").scale(0);
	        				
	        				var pinchEvent = "mousedown";
	        				var hiddenClick = "mouseup";
	        				if(checkIfMobile()){
	        					hiddenClick = "touchend";
	        					pinchEvent = "touchstart";
	        				}
	        				
	        				$("div[name='group_hidden_"+entityLocalReferenceId+"']").bind(pinchEvent, function(e){
	        					$("div[name='group_hidden_"+entityLocalReferenceId+"']").data("touchStartTime", new Date().getTime());
	        					$("div[name='group_hidden_"+entityLocalReferenceId+"']").bind(hiddenClick, function(e2){
	        						//alert("here2");
	        						$("div[name='group_hidden_"+entityLocalReferenceId+"']").unbind(hiddenClick);
	        						if(new Date().getTime()-$("div[name='group_hidden_"+entityLocalReferenceId+"']").data("touchStartTime")<500){
	        							$("div[name='group_hidden_"+entityLocalReferenceId+"']").scale(0);
	        							camera.zoomIntoApp(entityLocalReferenceId);
	    							}
	    						});
	        				});
	        				
	        				
        				}*/
    					
    					
    					applications[entityLocalReferenceId] = "1";
    					findParent(entityLocalReferenceId);
    					
    					//setPinchEvent(entityLocalReferenceId);
    					
    					if(bandLevel<=1){
        					var nextInsToExe = function(){
        						executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
        					};
        					setTimeout(function(){new checkItem2(entityLocalReferenceId, nextInsToExe);}, 0);
          				    //checkItem2(entityLocalReferenceId, nextInsToExe);
      				    }
      				    else{
        				    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
    				    }
    					
    					//if(bandLevel<=1){
    					//    checkItem2(entityLocalReferenceId);
					    //}
        				//executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
    					
    					/*findParent(entityLocalReferenceId);
    					
    					if(newBandLimit_KBpsBandwidth==0){	
	        				function bandNextIns(noi, instructionList, timespot_id, parentArray, parentTimespot, stop){
	        					this.exeNext = function(){
	        						executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	    						}
	        				}
	        				var nextInsFnc = new bandNextIns(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	        			    
	        				setTimeout(function(){
	        					new checkRenderDefinitionBands(entityLocalReferenceId, true, 0, 0, parentTimespot, nextInsFnc);
	        				}, 0);
        				}
        				else{
        					executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
        				}
        			    
        				setTimeout(function(){
        					new checkRenderDefinitionBands(entityLocalReferenceId, true, 0, 0, parentTimespot, nextInsFnc);
        				}, 0);*/
    					
    					//executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
    				}
    				
    				//executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
    				
    				
    				
    				/*var bandFinishTimer = setInterval(function(){
    					if($("div[name='group_"+entityLocalReferenceId+"']").data("bandChecked")!=null || typeof($("div[name='group_"+entityLocalReferenceId+"']").data("bandChecked"))!="undefined"){
    						clearInterval(bandFinishTimer);
    						executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
    					}
    				}, 25);*/
            		
            		//executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
            		break;
            	}
            	case SET_RENDER_SIZE_CHANGE_EVENT_ON_BROWSER:{
            		
            		var entityLocalReferenceId = instruction.getElementsByTagName("elr")[0].firstChild.nodeValue;
            		if(globals[entityLocalReferenceId]!=null)
                    {
                        entityLocalReferenceId = globals[entityLocalReferenceId];
                    }
                    var percentLessThan = instruction.getElementsByTagName("l")[0].firstChild.nodeValue;
                    var percentGreaterThan = instruction.getElementsByTagName("g")[0].firstChild.nodeValue;
            		var timespotIdLessThan = instruction.getElementsByTagName("tidl")[0].firstChild.nodeValue;
            		var timespotIdGreaterThan = instruction.getElementsByTagName("tidg")[0].firstChild.nodeValue;
            		
            		
            		//setTimeout(function(){
            			if(typeof applications[entityLocalReferenceId] != "undefined" && applications[entityLocalReferenceId]!=null){
            				timespots[applications[entityLocalReferenceId]['plt']['t']] = null;
            			    timespots[applications[entityLocalReferenceId]['pgt']['t']] = null;
            			    
            			    applications[entityLocalReferenceId] = new Array();
            			    applications[entityLocalReferenceId]['executing'] = false;
		            		applications[entityLocalReferenceId]['plt'] = new Array();
		            		applications[entityLocalReferenceId]['pgt'] = new Array();
		            		applications[entityLocalReferenceId]['plt']['p'] = percentLessThan;
		            		applications[entityLocalReferenceId]['plt']['t'] = timespotIdLessThan;
		            		applications[entityLocalReferenceId]['pgt']['p'] = percentGreaterThan;
		            		applications[entityLocalReferenceId]['pgt']['t'] = timespotIdGreaterThan;
		            		
		            		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	            		}
	            		else{
            			    
            			    applications[entityLocalReferenceId] = new Array();
            			    applications[entityLocalReferenceId]['executing'] = false;
		            		applications[entityLocalReferenceId]['plt'] = new Array();
		            		applications[entityLocalReferenceId]['pgt'] = new Array();
		            		applications[entityLocalReferenceId]['plt']['p'] = percentLessThan;
		            		applications[entityLocalReferenceId]['plt']['t'] = timespotIdLessThan;
		            		applications[entityLocalReferenceId]['pgt']['p'] = percentGreaterThan;
		            		applications[entityLocalReferenceId]['pgt']['t'] = timespotIdGreaterThan;
		            		
		            		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	            		}
            		//}, 0);
            		break;
            	}
            	case SET_ESCAPE_CONDITION:{
            		
            		/*var timespotId = instruction.getElementsByTagName("tid")[0].firstChild.nodeValue;
            		
            		if($("div[id='back']").length>0 && timespots[timespotId].length>0)
                    {
                    	var iAndAndroidDevices = false;
				        var ua = navigator.userAgent.toLowerCase();
				        
				        if(ua.indexOf("iphone")!=-1 || ua.indexOf("ipad")!=-1 || ua.indexOf("ipod")!=-1 || ua.indexOf("android")!=-1){
				        	iAndAndroidDevices = true;
				        }
	                    
	                    var eventType = 'click';
	                    if(iAndAndroidDevices){
	                    	event_type[preemptiveLocalReferenceId]='touchend';
	                    }
	                    
	                    //if(!camera.hasUndo()){
	                    if(checkIfNull(camera.getLastHistory())){
		                    var aFunction = function(){
			                    $("div[id='back']").unbind(eventType);
			                    
			                    
		                        
		                        $("div[id='back']").bind(eventType, function(e)
		                        {
		                            e.stopPropagation();
		                            
		                            $("div[id='back']").unbind(eventType);
		                            
		                            checkItemLock2 = true;
		                            
		                            
		                            camera.removeLastHistory();
		                            
		                            new executeTimespot(timespotId, parentTimespot);
		                            //alert("exe back");
		                            
		                            setTimeout(function checkEscapeFinish(){
		                            	if(running_timespots[timespotId]==null || typeof(running_timespots[timespotId])=="undefined"){
					        		    	checkItemLock2 = false;
					        		    }
					        		    else{
					        		    	setTimeout(function(){
												checkEscapeFinish();
											}, 200);
					        		    }
		                            }, 200);
		                            
		                            
		                            //setTimeout(function(){new executeTimespot(timespotId, parentTimespot);}, 0);
		                            
		                            
		                            setTimeout(function(){
										new getInstruction("/sendEscapeSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?r="+Math.floor(Math.random()*9223372036854775807));
									}, 0);
		                            
		                        	
		                        });
	                        };
	                        
	                        camera.getHistory().push(aFunction);
	                        //if(camera.getHistory().length<=2){
	                        	aFunction();
	                        //}
                        }
                        
                    }*/
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
            		break;
            	}
            	case LOCAL_REFERNCE_ZOOM:{
            		writeToConsole("calling local reference zoom");
            		
            		var entityLocalReferenceId = instruction.getElementsByTagName("eid")[0].firstChild.nodeValue;
            		if(globals[entityLocalReferenceId]!=null)
                    {
                        entityLocalReferenceId = globals[entityLocalReferenceId];
                    }
            		var localReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
            		if(globals[localReferenceId]!=null)
                    {
                        localReferenceId = globals[localReferenceId];
                    }
                    
                    var offsetX = parseFloat(instruction.getElementsByTagName("ox")[0].firstChild.nodeValue);
                    var offsetY = parseFloat(instruction.getElementsByTagName("oy")[0].firstChild.nodeValue);
                    var occupyPercentage = parseFloat(instruction.getElementsByTagName("op")[0].firstChild.nodeValue);
                    
            		var time = parseFloat(instruction.getElementsByTagName("t")[0].firstChild.nodeValue);
                    
                    var time_allowed = time*1000;
                    
                    

			        var parent = $($("div[id='group_"+entityLocalReferenceId+"']").find("div")[0]);
			        var parent2 = $("div[id='group_"+entityLocalReferenceId+"']");
			        
			        //alert(localReferenceId);
			    	setTimeout(function checkElementExist1(){
                    	if(!checkIfNull(parent)){
                    		//var parentName = parent.attr("name").substring(6, parent.attr("name").length);
                    		
                    		//purgeCalCache(parent, parentName);
                		    
                    		
		            		if(parent.data("awaiting_imgs")!=null){
	                  	        if(parseInt(parent.data("awaiting_imgs"))>0){
	                  	            if(!stop){
	                  	            	setTimeout(function(){
	                  	            		new checkElementExist1();
	                  	            	}, 300);
	                      	        }
	                  	        }
	                  	        else{
		                            parent.data("moving", parentTimespot.getId());
		                            
		                            var currentEntity = $("div[name='group_"+localReferenceId+"']");
		                            
		                            var base_ratio = parent.data("scale");
		                            
		                            var x_orig = parseFloat(parent.css("left"));
		                            var y_orig = parseFloat(parent.css("top"));
		                            
		                            
		                            
		                            
		                            
		                            
		                            
		                            var accumulatedScale = calAccScale2(currentEntity);
		                            var accumulatedScale2 = accumulatedScale*$("div[name='group_0']").data("scale");
		                            
		                            
		                            var accumulatedScale3 = calAccScale2(parent2)*$("div[name='group_0']").data("scale");
		                            
		                            var accumulatedScale4 = calAccScale2(parent)*$("div[name='group_0']").data("scale");
		                            
		                            //var accumulatedScale5 = calAccScale2(currentEntity.parent())*$("div[name='group_0']").data("scale");
		                            
		                            var xCenter = parent2.offset().left+(500*accumulatedScale3);
		                            var yCenter = parent2.offset().top+(500*accumulatedScale3);
		                            //alert(xCenter+" "+yCenter+" "+accumulatedScale3);
		                            
		                            var parentName = currentEntity.parent().attr("name").substring(6, currentEntity.parent().attr("name").length);
		                            purgeCalCache(currentEntity.parent(), parentName);
		                            //tempAccScales = new Array();
		                            
		                            //alert(accumulatedScale+" "+accumulatedScale2+" "+$("div[name='group_0']").data("scale"));		                            
		                            
		                            //var zoomPercentage = base_ratio/accumulatedScale;
		                            //var zoomPercentage = (base_ratio/accumulatedScale)*base_ratio;
		                            var zoomPercentage = (occupyPercentage/accumulatedScale*calAccScale2(parent2));
		                            var ratio = zoomPercentage*base_ratio;
		                            
		                            //alert(ratio);
		                            
		                            //if(base_ratio>1){
		                            //	ratio = base_ratio;
		                            //}
		                            //ratio = 5;
		                            
		                            
		                            
		                            
		                            //var x_dist = (-zoomPercentage*x)-parseFloat(parent.css("left"));
		                            //var y_dist = (-zoomPercentage*y)-parseFloat(parent.css("top"));
			                          
		                            //alert(accumulatedScale+" "+accumulatedScale2);
		                            
		                            //var x_dist = (ratio)*(xCenter-(currentEntity.offset().left+(500*accumulatedScale2)))/$("div[name='group_0']").data("scale");
		                            //var y_dist = (ratio)*(yCenter-(currentEntity.offset().top+(500*accumulatedScale2)))/$("div[name='group_0']").data("scale");
		                            
		                            		                            
		                            //var x_dist = (ratio)*(xCenter-(currentEntity.offset().left+(500*accumulatedScale2)))/$("div[name='group_0']").data("scale");
		                            //var y_dist = (ratio)*(yCenter-(currentEntity.offset().top+(500*accumulatedScale2)))/$("div[name='group_0']").data("scale");
		                            
		                            //var x = -(xCenter-(currentEntity.offset().left+(500*accumulatedScale2)))*(accumulatedScale/accumulatedScale2);
		                            //var y = -(yCenter-(currentEntity.offset().top+(500*accumulatedScale2)))*(accumulatedScale/accumulatedScale2);
		                            
		                            //relative to application
		                            //var x = -(xCenter-(currentEntity.offset().left+(500*accumulatedScale2)))/accumulatedScale4+(offsetX);
		                            //var y = -(yCenter-(currentEntity.offset().top+(500*accumulatedScale2)))/accumulatedScale4+(offsetY);
		                            
		                            //relative to view
		                            //alert(offsetX*accumulatedScale5/accumulatedScale4+" "+accumulatedScale5+" "+accumulatedScale4);
		                            var x = -(xCenter-(currentEntity.offset().left+(500*accumulatedScale2))-(offsetX*accumulatedScale2))/accumulatedScale4;
		                            var y = -(yCenter-(currentEntity.offset().top+(500*accumulatedScale2))-(offsetY*accumulatedScale2))/accumulatedScale4;
		                            
		                            
		                            //alert(x+" "+y+" "+accumulatedScale+" "+accumulatedScale2+" "+accumulatedScale3+" "+accumulatedScale4+" "+accumulatedScale5+" "+$("div[name='group_0']").data("scale"));
		                            
		                            
		                            /*var currentEntity2 = currentEntity;
		                            var x = parseFloat(currentEntity2.css("left")) + currentEntity2.data("scale")*500-500;
		                            var y = parseFloat(currentEntity2.css("top")) + currentEntity2.data("scale")*500-500;
		                            
		                            while(currentEntity2.attr("id").substring(6, currentEntity2.attr("id").length)!=entityLocalReferenceId){
		                            	x += parseFloat(currentEntity2.css("left")) + currentEntity2.data("scale")*500-500;;
		                            	y += parseFloat(currentEntity2.css("top")) + currentEntity2.data("scale")*500-500;
		                            	currentEntity2 = currentEntity2.parent();
		                            }
		                            
		                            alert(x+" "+y+" "+accumulatedScale+" "+accumulatedScale2+" "+accumulatedScale3);*/
		                            
		                            
		                            
		                            //if(ratio=25){
		                            //	ratio = 5;
		                            //}
		                            		                            
		                            var x_dist = (-ratio*x)+(x_orig*(ratio-base_ratio)/base_ratio);
		                            var y_dist = (-ratio*y)+(y_orig*(ratio-base_ratio)/base_ratio);
		                            
		                            //if(ratio==25){
		                            //	y_dist = 0;
		                            //}
		                            
		                            //if(ratio==25){
		                            //	x_dist = 0;
		                            //	y_dist = 0;
		                            //}
		                            
		                            //alert(x_dist+" "+y_dist);
		                            
		                            //x = -400;
		                            //y = -360;
		                            
		                            //alert("ratio "+ratio)
		                            
		                            //var x_dist = (-ratio*x)-parseFloat(parent.css("left"));
		                            //var y_dist = (-ratio*y)-parseFloat(parent.css("top"));
		                            
		                            
		                            
		                            //x_dist = (x_dist)+(x_orig*(ratio-base_ratio)/base_ratio);
		                            //y_dist = (y_dist)+(y_orig*(ratio-base_ratio)/base_ratio);
		                            
		                            
		                            //if(!lastBackPinch){
		                            	if(backStack.length==0 || (backStack.length>0 && !backStack[backStack.length-1].isSwipe()) || (backStack.length==1)){
							        		//lastBackPinch = true;
								        	var backFunc = new function(){
								        		this.isSwipe = function(){
										    		return true;
										    	}
										    	this.isPinch = function(){
										    		return false;
										    	}
										    	this.execBack = function(){
												    backFunction(parent, x_orig, y_orig, base_ratio);
											    }
											};
											backStack.push(backFunc);
										}
									//}
		                            
									
		                            
		                            //lastBackPinch = false;
									//var backFunc = new function(){
									//	backFunction(parent, x_orig, y_orig, base_ratio);
									//};
									//backStack.push(backFunc);
		                            
	
		                            var start = new Date().getTime();
		                          
		                            var hasMoreTime = true;
		                          
		                            pausePreloadBands();
		                            setTimeout(function moveItem4(){
			                            if(hasMoreTime){
			                                var elapsed = new Date().getTime()-start;
			                                if(elapsed<time_allowed)
			                                {	
			                              	    setTimeout(function(){
			                              	    	if(speedUp){
			                              	    		parent.backToNormalTransform(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), x_orig+((elapsed/time_allowed)*x_dist), y_orig+((elapsed/time_allowed)*y_dist));
				                                    
			                                        }
			                                        else{
				                                        parent.css({
														    left: x_orig+((elapsed/time_allowed)*x_dist),
														    top: y_orig+((elapsed/time_allowed)*y_dist)
														});
			                                        }
				                                }, 0);
			                                  
			                                    
			                                  
			                                    setTimeout(function(){
			                                        if(ratio!=base_ratio){
			                                        	if(!speedUp){
				                                            parent.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
			                                            }
				                                      
				                                        parent.data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
				                                        parent.data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='group_"+entityLocalReferenceId+"']").data("original_width"));
				                                        parent.data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='group_"+entityLocalReferenceId+"']").data("original_height"));
				                                        
				                                    }
				                                    
				                                    
				                                    if(parent.data("moving")!= parentTimespot.getId())
				                                    {
				                                        hasMoreTime = false;
									                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
				                                    }
				                                    else{
				                                  	    if(stop){
				                                      	     hasMoreTime = false;
									                    }
									                    else{
									                  	    new moveItem4();
			                                            }
				                                    }
			                                    }, 0);
			                                  
			                                  
			                                }
			                                else
			                                {
			                              	    hasMoreTime = false;
			                              	    
		                              	        setTimeout(function(){
		                              	        	if(speedUp){
		                              	        	    parent.backToNormalTransform(ratio, x_orig+x_dist, y_orig+y_dist);
	                              	        	    }
	                              	        	    else{
				                                        parent.css({
														    left: x_orig+x_dist,
														    top: y_orig+y_dist
														});
	                              	        	    }
				                                    
				                                }, 0);  
			                                  
				                                
			                                  
			                                  
			                                    setTimeout(function(){
			                                  	    if(ratio!=base_ratio){
			                                  	    	if(!speedUp){
				                                  	        parent.scale(ratio);
			                                  	        }
				                                      
				                                        parent.data("scale", ratio);
				                                        parent.data("width", ratio*$("div[name='group_"+entityLocalReferenceId+"']").data("original_width"));
				                                        parent.data("height", ratio*$("div[name='group_"+entityLocalReferenceId+"']").data("original_height"));	
								            			
				                                    }
				                                  
										            resumePreloadBands();
										            
										            
				                                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
			                                    }, 0);
			                                  
			                                }
		                                }
		                            }, 0);
	                            }
	                  	    }
                  	    }
                  	    else{
                  	    	writeToErrorConsole("local reference zoom parent app not found!");
                      	    if(!stop){
                      	    	setTimeout(function(){
                      	    		new checkElementExist1();
                      	    	}, 300);
                      	    	
                  	        }
                  	    }
              	    }, 0);	

            		break;
            	}
            	case XY_CLICK_CENTER:{
            		var entityLocalReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
            		if(globals[entityLocalReferenceId]!=null)
                    {
                        entityLocalReferenceId = globals[entityLocalReferenceId];
                    }
                    
                    
                    
                    
                    var zoomPercentage = parseFloat(instruction.getElementsByTagName("zp")[0].firstChild.nodeValue);
                    
                    
                    var occupyPercentage = calAccScale($("div[name='group_"+entityLocalReferenceId+"']"));
                    var accumulatedScale3 = occupyPercentage*$("div[name='group_0']").data("scale");
                    var halfEdgeLength = full_screen_scale*accumulatedScale3/2;
                    //var height = full_screen_scale*accumulatedScale3;
                    
                    //alrt("x "+clickedChild_xy[1]+" y "+clickedChild_xy[2]);
                    //alert("occupyPercentage "+occupyPercentage);
                    //alert("bg ratio "+$("div[name='group_0']").data("scale"));
                    //alert("accumulatedScale3 "+accumulatedScale3);
                    //alert("ce ratio "+$("div[name='group_"+entityLocalReferenceId+"']").data("scale"));
                    //alert("left "+$("div[name='group_"+entityLocalReferenceId+"']").offset().left);
                    //alert("top "+$("div[name='group_"+entityLocalReferenceId+"']").offset().top);
                    //alert("clickedChild_xy[1] "+clickedChild_xy[1]);
                    //alert("clickedChild_xy[2] "+clickedChild_xy[2]);
                    
                    //try{
                    	//var x = (clickedChild_xy[1]/accumulatedScale3-((parseFloat($("div[name='group_"+entityLocalReferenceId+"']").offset().left)+halfEdgeLength))/accumulatedScale3);
	                    //var y = (clickedChild_xy[2]/accumulatedScale3-((parseFloat($("div[name='group_"+entityLocalReferenceId+"']").offset().top)+halfEdgeLength))/accumulatedScale3);
	                    
	                
	                    var x = (clickedChild_xy[1]-(parseFloat($("div[name='group_"+entityLocalReferenceId+"']").offset().left)+halfEdgeLength))/accumulatedScale3;
	                    var y = (clickedChild_xy[2]-(parseFloat($("div[name='group_"+entityLocalReferenceId+"']").offset().top)+halfEdgeLength))/accumulatedScale3;
            		//}
            		//catch(err){
            		//	alert(err);
            		//}
            		
            		//alert("x "+x+" y "+y);
            		if(!isNaN(x) && !isNaN(y)){
            		
	            		var time = parseFloat(instruction.getElementsByTagName("t")[0].firstChild.nodeValue);
	                    
	                    var time_allowed = time*1000;
	                    
	                    var parent = $("div[name='group_"+entityLocalReferenceId+"']");
	                    
	            		
	                    setTimeout(function checkElementExist1(){
	                    	if(parent.length>0){
	                    		var parentName = parent.attr("name").substring(6, parent.attr("name").length);
	                    		
	                    		purgeCalCache(parent, parentName);
	                    		
	                		    
	                    		
			            		if(parent.data("awaiting_imgs")!=null){
		                  	        if(parseInt(parent.data("awaiting_imgs"))>0){
		                  	            if(!stop){
		                  	            	setTimeout(function(){
		                  	            		new checkElementExist1();
		                  	            	}, 300);
			                      	        //setTimeout(arguments.callee, 300);
		                      	        }
		                  	        }
		                  	        else{
		                  	        	
		                  	        	
		                  	        	
			                            parent.data("moving", parentTimespot.getId());
			                            /*if($("div[name='group_"+entityLocalReferenceId+"']").parent().data("original_width")!=null){
			                                var ratio = radius/full_screen_scale;}
			                            else{
			                                var ratio = (radius/full_screen_scale)*(Math.min(PLAYGROUND_WIDTH,PLAYGROUND_HEIGHT)/full_screen_scale);}
			                            */
			                            
			                            
			
			                            var base_ratio = parent.data("scale");
			                            var ratio = zoomPercentage;
			                            
			                            var x_orig = parseFloat(parent.css("left"));
			                            var y_orig = parseFloat(parent.css("top"));
			                            
			                           
			                            
			                            var x_dist = (-zoomPercentage*x)-parseFloat(parent.css("left"));
			                            var y_dist = (-zoomPercentage*y)-parseFloat(parent.css("top"));
			                          
		
			                            var start = new Date().getTime();
			                          
			                            var hasMoreTime = true;
			                          
			                            pausePreloadBands();
			                            setTimeout(function moveItem4(){
				                            if(hasMoreTime){
				                                var elapsed = new Date().getTime()-start;
				                                if(elapsed<time_allowed)
				                                {
		
				                              	    setTimeout(function(){
				                              	    	if(speedUp){
				                              	    		parent.backToNormalTransform(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), x_orig+((elapsed/time_allowed)*x_dist), y_orig+((elapsed/time_allowed)*y_dist));
					                                    
				                                        }
				                                        else{
				                                        	//$("div[name='group_"+entityLocalReferenceId+"']").css("left", x_orig+((elapsed/time_allowed)*x_dist));
					                                        //$("div[name='group_"+entityLocalReferenceId+"']").css("top", y_orig+((elapsed/time_allowed)*y_dist));
					                                        parent.css({
															    left: x_orig+((elapsed/time_allowed)*x_dist),
															    top: y_orig+((elapsed/time_allowed)*y_dist)
															});
				                                        }
					                                }, 0);
				                                  
				                                    
				                                  
				                                    setTimeout(function(){
				                                        if(ratio!=base_ratio){
				                                        	if(!speedUp){
					                                            parent.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
				                                            }
					                                      
					                                        parent.data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
					                                        parent.data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='group_"+entityLocalReferenceId+"']").data("original_width"));
					                                        parent.data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='group_"+entityLocalReferenceId+"']").data("original_height"));
					                                        
					                                        
					                                        
					                                    }
					                                    
					                                    
					                                    if(parent.data("moving")!= parentTimespot.getId())
					                                    {
					                                        hasMoreTime = false;
					                                        
										                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
					                                    }
					                                    else{
					                                  	    if(stop){
					                                      	     hasMoreTime = false;
										                    }
										                    else{
										                  	    new moveItem4();
				                                            }
					                                    }
				                                    }, 0);
				                                  
				                                  
				                                }
				                                else
				                                {
				                              	    hasMoreTime = false;
				                              	   
				                              	    
			                              	        setTimeout(function(){
			                              	        	if(speedUp){
			                              	        	    parent.backToNormalTransform(ratio, x_orig+x_dist, y_orig+y_dist);
		                              	        	    }
		                              	        	    else{
					                                        parent.css({
															    left: x_orig+x_dist,
															    top: y_orig+y_dist
															});
		                              	        	    }
					                                }, 0);
				                                  
				                                    setTimeout(function(){
				                                  	    if(ratio!=base_ratio){
				                                  	    	if(!speedUp){
					                                  	        parent.scale(ratio);
				                                  	        }
					                                      
					                                        parent.data("scale", ratio);
					                                        parent.data("width", ratio*$("div[name='group_"+entityLocalReferenceId+"']").data("original_width"));
					                                        parent.data("height", ratio*$("div[name='group_"+entityLocalReferenceId+"']").data("original_height"));
					                                    
	
					                                    }
					                                    
					                                    resumePreloadBands();
					                                    //preloadBandQueue = findClosestLookingApps();
					                                    
					                                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
				                                    }, 0);
				                                  
				                                }
			                                }
			                            }, 0);
		                            }
		                  	    }
	                  	    }
	                  	    else{
	                  	    	writeToErrorConsole("xy click center app not found!");
	                      	    if(!stop){
	                      	    	setTimeout(function(){
	                      	    		new checkElementExist1();
	                      	    	}, 300);
	                  	        }
	                  	    }
	              	    }, 0);
              	    }
              	    else{
              	    	executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
              	    }
            		break;
            	}
            	case XY_CENTER:{
            		var entityLocalReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
            		if(globals[entityLocalReferenceId]!=null)
                    {
                        entityLocalReferenceId = globals[entityLocalReferenceId];
                    }
            		var x = parseFloat(instruction.getElementsByTagName("x")[0].firstChild.nodeValue);
            		var y = parseFloat(instruction.getElementsByTagName("y")[0].firstChild.nodeValue);
            		var zoomPercentage = parseFloat(instruction.getElementsByTagName("zp")[0].firstChild.nodeValue);
            		var time = parseFloat(instruction.getElementsByTagName("t")[0].firstChild.nodeValue);
                    
                    var time_allowed = time*1000;
                    
                    var parent = $("div[name='group_"+entityLocalReferenceId+"']");
                    
            		
                    setTimeout(function checkElementExist1(){
                    	if(parent.length>0){
                    		var parentName = parent.attr("name").substring(6, parent.attr("name").length);
                    		
                    		purgeCalCache(parent, parentName);
                		    
                    		
		            		if(parent.data("awaiting_imgs")!=null){
	                  	        if(parseInt(parent.data("awaiting_imgs"))>0){
	                  	            if(!stop){
	                  	            	setTimeout(function(){
	                  	            		new checkElementExist1();
	                  	            	}, 300);
		                      	        //setTimeout(arguments.callee, 300);
	                      	        }
	                  	        }
	                  	        else{
	                  	        	
	                  	        	
	                  	        	
		                            parent.data("moving", parentTimespot.getId());
		                            /*if($("div[name='group_"+entityLocalReferenceId+"']").parent().data("original_width")!=null){
		                                var ratio = radius/full_screen_scale;}
		                            else{
		                                var ratio = (radius/full_screen_scale)*(Math.min(PLAYGROUND_WIDTH,PLAYGROUND_HEIGHT)/full_screen_scale);}
		                            */
		                            
		                            
		
		                            var base_ratio = parent.data("scale");
		                            var ratio = zoomPercentage;
		                            
		                            var x_orig = parseFloat(parent.css("left"));
		                            var y_orig = parseFloat(parent.css("top"));
		                            
		                           
		                            
		                            //var x_dist = (-zoomPercentage*x)-$("div[name='group_"+entityLocalReferenceId+"']").data("cx");
		                            //var y_dist = (-zoomPercentage*y)-$("div[name='group_"+entityLocalReferenceId+"']").data("cy");
		                            
		                            var x_dist = (-zoomPercentage*x)-parseFloat(parent.css("left"));
		                            var y_dist = (-zoomPercentage*y)-parseFloat(parent.css("top"));
		                            
		                            
		                            //lastBackPinch = false;
									var backFunc = new function(){
										this.isSwipe = function(){
								    		return false;
								    	}
								    	this.isPinch = function(){
								    		return false;
								    	}
								    	this.execBack = function(){
										    backFunction(parent, x_orig, y_orig, base_ratio);
									    }
									};
									backStack.push(backFunc);
		                            
		                            
		                            //alert(x_dist+" "+y_dist);
		                          
		                            /*var accumulatedScale = 1;
	                                var currentEntity = $("div[name='group_"+entityLocalReferenceId+"']");
	                              
	                                while(true){
                                	    
                                	    accumulatedScale = accumulatedScale * currentEntity.data("scale");
                                	    
                                	    if(currentEntity.attr("id")=="group_0"){
                                		    break;
                                	    }
                                	  
                                	    currentEntity = currentEntity.parent();
                                    }
                                    
	                                
	                                accumulatedScale2 = accumulatedScale;*/
		                          
	                                
		                          
	
		                            var start = new Date().getTime();
		                          
		                            var hasMoreTime = true;
		                          
		                            pausePreloadBands();
		                            setTimeout(function moveItem4(){
			                            if(hasMoreTime){
			                                var elapsed = new Date().getTime()-start;
			                                if(elapsed<time_allowed)
			                                {
			                                  
			                                	//$("div[name='group_"+entityLocalReferenceId+"']").transform2(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), ((elapsed/time_allowed)*x_dist/ratio), ((elapsed/time_allowed)*y_dist/ratio));
			                                	
			                                	
			                                	
			                              	    setTimeout(function(){
			                              	    	if(speedUp){
			                              	    		parent.backToNormalTransform(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), x_orig+((elapsed/time_allowed)*x_dist), y_orig+((elapsed/time_allowed)*y_dist));
				                                    
			                                        }
			                                        else{
			                                        	//$("div[name='group_"+entityLocalReferenceId+"']").css("left", x_orig+((elapsed/time_allowed)*x_dist));
				                                        //$("div[name='group_"+entityLocalReferenceId+"']").css("top", y_orig+((elapsed/time_allowed)*y_dist));
				                                        parent.css({
														    left: x_orig+((elapsed/time_allowed)*x_dist),
														    top: y_orig+((elapsed/time_allowed)*y_dist)
														});
			                                        }
				                                }, 0);
			                                  
			                                    
			                                  
			                                    setTimeout(function(){
			                                        if(ratio!=base_ratio){
			                                        	if(!speedUp){
				                                            parent.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
			                                            }
				                                      
				                                        parent.data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
				                                        parent.data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='group_"+entityLocalReferenceId+"']").data("original_width"));
				                                        parent.data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='group_"+entityLocalReferenceId+"']").data("original_height"));
				                                        
				                                        
				                                        /*render size change event on browser
				                                    	var parent = $("div[name='group_"+entityLocalReferenceId+"']");
				                                    	var children = new Array();
				                                    	
				                                    	for (var item in applications) {
				                                    		var foundChildren = parent.find("div[name='group_"+item+"']");
									            			if(foundChildren.length>0){
									            				
									            				var occupyPercentage = 1;
							                                    currentEntity = $("div[name='group_"+item+"']");
							                                  
							                                    while(true){
							                                	    if(currentEntity.attr("id")=="group_0"){
							                                		    break;
							                                	    }
							                                	    occupyPercentage = occupyPercentage * currentEntity.data("scale");
							                                	    currentEntity = currentEntity.parent();
							                                    }
							                                    if(occupyPercentage<applications[item]['plt']['p'] && ratio<base_ratio){
							                                    	children[item] = applications[item]['plt']['t'];
							                                    }
							                                    else if(occupyPercentage>applications[item]['pgt']['p'] && ratio>base_ratio){
							                                    	children[item] = applications[item]['pgt']['t'];
							                                    }
									            			}
								            			}
								            			for (var itemToExecute in children) {
								            				if(!applications[itemToExecute]['executing']){
								            					//alert("reset");
								            					applications[itemToExecute]['executing'] = true;
									            			    new executeTimespot(children[itemToExecute], parentTimespot);
								            			    }
								            			    //else{
								            			    	//alert("not reset");
								            			    //}
								            			}
					                                    //render size change event on browser */
				                                        
				                                    }
				                                    
				                                    
				                                    if(parent.data("moving")!= parentTimespot.getId())
				                                    {
				                                        hasMoreTime = false;
				                                        //clearInterval(at);
				                                        //if(stop){
				                                      	//     hasMoreTime = false;
									                    //}
									                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
				                                    }
				                                    else{
				                                  	    if(stop){
				                                      	     hasMoreTime = false;
				                                      	     //clearInterval(at);
									                    }
									                    else{
									                    	//requestAnimationFrame(moveItem3());
									                    	
									                    	//setTimeout(function(){
						              	            		//    new moveItem3();
						              	            	    //}, 0);
									                    	//new moveItem3();
									                  	    //setTimeout(function(){new moveItem4();}, 0);
									                  	    new moveItem4();
				                                          //setTimeout(arguments.callee, 0);
			                                            }
				                                    }
			                                    }, 0);
			                                  
			                                  
			                                }
			                                else
			                                {
			                              	    hasMoreTime = false;
			                              	    //clearInterval(at);
			                              	    
			                              	    
			                              	    
			                              	    //$("div[name='group_"+entityLocalReferenceId+"']").css("visibility", "hidden");
			                              	    
		                              	        setTimeout(function(){
		                              	        	if(speedUp){
		                              	        	    parent.backToNormalTransform(ratio, x_orig+x_dist, y_orig+y_dist);
	                              	        	    }
	                              	        	    else{
	                              	        	    	//$("div[name='group_"+entityLocalReferenceId+"']").css("left", x_orig+x_dist);
				                                        //$("div[name='group_"+entityLocalReferenceId+"']").css("top", y_orig+y_dist);
				                                        parent.css({
														    left: x_orig+x_dist,
														    top: y_orig+y_dist
														});
	                              	        	    }
				                                    
				                                    //$("div[name='group_"+entityLocalReferenceId+"']").data("cx", parseFloat($("div[name='group_"+entityLocalReferenceId+"']").css("left")));
				                                    //$("div[name='group_"+entityLocalReferenceId+"']").data("cy", parseFloat($("div[name='group_"+entityLocalReferenceId+"']").css("top")));
				                                }, 0);  
			                                  
				                                //$("div[name='group_"+entityLocalReferenceId+"']").scale(ratio);
			                                  
			                                  
			                                    setTimeout(function(){
			                                  	    if(ratio!=base_ratio){
			                                  	    	if(!speedUp){
				                                  	        parent.scale(ratio);
			                                  	        }
				                                      
				                                        parent.data("scale", ratio);
				                                        parent.data("width", ratio*$("div[name='group_"+entityLocalReferenceId+"']").data("original_width"));
				                                        parent.data("height", ratio*$("div[name='group_"+entityLocalReferenceId+"']").data("original_height"));
				                                    //}
				                                    
				                                    
				                                    
				                                    
				                                    //render size change event on browser
				                                    //if(ratio!=base_ratio){
				                                    	
				                                    	
				                                    	//setTimeout(function(){
				                                    	  //  new checkRenderDefinitionBands(entityLocalReferenceId, false, ratio, base_ratio, parentTimespot);
			                                    	    //}, 0);
			                                    	    
				                                    	
				                                    	/*var parent = $("div[name='group_"+entityLocalReferenceId+"']");
				                                    	var children = new Array();
				                                    	
				                                    	for(var item in restraint_groups){
				                                    		var foundChildren = parent.find("div[name='group_"+item+"']");
				                                    		if(foundChildren.length>0){
				                                    			
				                                    			var occupyPercentage = 1;
							                                    var currentEntity = $("div[name='group_"+item+"']");
							                                    var baseGroup = $("div[name='group_0']");
							                                  
							                                    var withinDisplay = false;
							                                    var accumulatedScale = 1;
							                                    var accumulatedScale2 = 1;
							                                    
							                                    while(true){
							                                	    if(currentEntity.attr("id")=="group_0"){
							                                	    	accumulatedScale2 = accumulatedScale;
	                                	  	                            accumulatedScale2 = accumulatedScale2 * currentEntity.data("scale");
							                                		    break;
							                                	    }
							                                	    accumulatedScale = accumulatedScale * currentEntity.data("scale");
							                                	    occupyPercentage = occupyPercentage * currentEntity.data("scale");
							                                	    currentEntity = currentEntity.parent();
							                                    }
							                                    var width = full_screen_scale*accumulatedScale2/2;
	                                                            var height = full_screen_scale*accumulatedScale2/2;
	                                                            currentEntity = $("div[name='group_"+item+"']");
							                                    if((parseFloat(currentEntity.offset().left)+width>=parseFloat(baseGroup.offset().left)  && parseFloat(currentEntity.offset().left+width)<=(parseFloat(baseGroup.offset().left)+(baseGroup.data("scale")*full_screen_scale))) && (parseFloat(currentEntity.offset().top+height)>=parseFloat(baseGroup.offset().top)  && parseFloat(currentEntity.offset().top+height)<=(parseFloat(baseGroup.offset().top)+(baseGroup.data("scale")*full_screen_scale)))){
	                                                            	withinDisplay = true;
	                                                            }
							                                    
							                                    
							                                    if(withinDisplay || ratio<base_ratio){
							                                    	var tids = restraint_groups[item];
						                                    		var temparray = new Array();
						                                    		var cpuArray = new Array();
						                                    		var bandwidthArray = new Array();
						                                    		for(var item2 in tids){
							                                    		temparray.push(restrains[tids[item2]]["p"]);
							                                    		cpuArray.push(restrains[tids[item2]]["ips"]);
							                                    		bandwidthArray.push(restrains[tids[item2]]["b"]);
						                                    		}
						                                    		temparray.sort(function(a,b){return a-b});
						                                    		cpuArray.sort(function(a,b){return a-b});
						                                    		bandwidthArray.sort(function(a,b){return a-b});
						                                    		
						                                    		var currentP;
						                                    		var currentCPU;
						                                    		var currentBandwidth;
						                                    		if(temparray.length==1){
						                                    			currentP = temparray[0];
						                                    			currentCPU = cpuArray[0];
						                                    			currentBandwidth = bandwidthArray[0];
						                                    		}
						                                    		else{
						                                    			for (var i = 0; i < temparray.length; i++) {
						                                    				if(i==0){
																			    if(occupyPercentage<(temparray[i]+temparray[i+1])/2){
																		    		currentP = temparray[0];
																		    	}
																		    	else{
									                                    			currentP = temparray[1];
									                                    		}
									                                    		if(cpuSpeed<(cpuArray[i]+cpuArray[i+1])/2){
																		    		currentCPU = cpuArray[0];
																		    	}
																		    	else{
									                                    			currentCPU = cpuArray[1];
									                                    		}
									                                    		if(bandwidthSpeed<(bandwidthArray[i]+bandwidthArray[i+1])/2){
																		    		currentBandwidth = bandwidthArray[0];
																		    	}
																		    	else{
									                                    			currentBandwidth = bandwidthArray[1];
									                                    		}
																	    	}
																	    	else{
																	    		if(i+1<temparray.length){
																	    			if(occupyPercentage<(temparray[i]+temparray[i+1])/2){
																			    		currentP = temparray[i];
																			    	}
																			    	else{
																			    		currentP = temparray[i+1];
																			    	}
																			    	if(cpuSpeed<(cpuArray[i]+cpuArray[i+1])/2){
																			    		currentCPU = cpuArray[i];
																			    	}
																			    	else{
																			    		currentCPU = cpuArray[i+1];
																			    	}
																			    	if(bandwidthSpeed<(bandwidthArray[i]+bandwidthArray[i+1])/2){
																			    		currentBandwidth = bandwidthArray[i];
																			    	}
																			    	else{
																			    		currentBandwidth = bandwidthArray[i+1];
																			    	}
																	    		}
																	    	}
																		}
						                                    		}
						                                    		
						                                    		var closeness = new Array();
						                                    		var max = 0;
						                                    		var closerTid = 0;
						                                    		
						                                    		for(var item2 in tids){
						                                    			closeness[tids[item2]] = 0;
						                                    			if(restrains[tids[item2]]["p"]==currentP){
						                                    				closeness[tids[item2]] += 40;
						                                    			}
						                                    			if(restrains[tids[item2]]["ips"]==currentCPU){
						                                    				closeness[tids[item2]] += 20;
						                                    			}
						                                    			if(restrains[tids[item2]]["b"]==currentBandwidth){
						                                    				closeness[tids[item2]] += 20;
						                                    			}
						                                    			
						                                    			if(closeness[tids[item2]]>max){
						                                    				max = closeness[tids[item2]];
						                                    				closerTid = tids[item2];
						                                    			}
						                                    			
						                                    			
						                                    			
						                                    			//if(restrains[tids[item2]]["p"]==currentP && restrains[tids[item2]]["ips"]==currentCPU){
						                                    			//	children[item] = tids[item2];
						                                    			//}
					                                    			}
					                                    			children[item] = closerTid;
					                                    			
					                                    			
					                                    			
							                                    }
			                                    			}
				                                    	}
				                                    	for (var itemToExecute in children) {
								            				//if(!applications[itemToExecute]['executing']){
									            			    new executeTimespot(children[itemToExecute], parentTimespot);
								            			    //}
									            			//applications[itemToExecute]['executing'] = false;
									            			
								            			}*/
				                                    	
				                                    	/*for (var item in applications) {
				                                    		var foundChildren = parent.find("div[name='group_"+item+"']");
									            			if(foundChildren.length>0){
									            				var occupyPercentage = 1;
							                                    var currentEntity = $("div[name='group_"+item+"']");
							                                    var baseGroup = $("div[name='group_0']");
							                                  
							                                    var withinDisplay = false;
							                                    var accumulatedScale = 1;
							                                    var accumulatedScale2 = 1;
							                                    
							                                    while(true){
							                                	    if(currentEntity.attr("id")=="group_0"){
							                                	    	accumulatedScale2 = accumulatedScale;
	                                	  	                            accumulatedScale2 = accumulatedScale2 * currentEntity.data("scale");
							                                		    break;
							                                	    }
							                                	    accumulatedScale = accumulatedScale * currentEntity.data("scale");
							                                	    occupyPercentage = occupyPercentage * currentEntity.data("scale");
							                                	    currentEntity = currentEntity.parent();
							                                    }
							                                    var width = full_screen_scale*accumulatedScale2/2;
	                                                            var height = full_screen_scale*accumulatedScale2/2;
	                                                            currentEntity = $("div[name='group_"+item+"']");
							                                    if((parseFloat(currentEntity.offset().left)+width>=parseFloat(baseGroup.offset().left)  && parseFloat(currentEntity.offset().left+width)<=(parseFloat(baseGroup.offset().left)+(baseGroup.data("scale")*full_screen_scale))) && (parseFloat(currentEntity.offset().top+height)>=parseFloat(baseGroup.offset().top)  && parseFloat(currentEntity.offset().top+height)<=(parseFloat(baseGroup.offset().top)+(baseGroup.data("scale")*full_screen_scale)))){
	                                                            	withinDisplay = true;
	                                                            }
							                                    
							                                    
							                                    
							                                    if(withinDisplay || ratio<base_ratio){
								                                    if(occupyPercentage<applications[item]['plt']['p'] && ratio<base_ratio){
								                                    	children[item] = applications[item]['plt']['t'];
								                                    }
								                                    else if(occupyPercentage>applications[item]['pgt']['p'] && ratio>base_ratio){
								                                    	children[item] = applications[item]['pgt']['t'];
								                                    }
							                                    }
									            			}
								            			}*/
								            			
								            			
								            			
				                                    }
				                                    //render size change event on browser
				                                    
				                                  
				                                    /*var occupyPercentage = 1;
				                                    currentEntity = $("div[name='group_"+entityLocalReferenceId+"']");
				                                  
				                                    while(true){
				                                	    if(currentEntity.attr("id")=="group_0"){
				                                		    break;
				                                	    }
				                                	    occupyPercentage = occupyPercentage * currentEntity.data("scale");
				                                	    currentEntity = currentEntity.parent();
				                                    }
				                                  
				                                  
				                                    setTimeout(function centerSendSignal(){
						                                //Testing only
						                                var test_request = false;
						
											            try
											            {
											                test_request = new XMLHttpRequest();
											            }
											            catch (error)
											            {
											                try
											                {
											                    test_request = new ActiveXObject("Microsoft.XMLHTTP");
											                }
											                catch (error)
											                {
											                    alert('there is an unknown error occurred');
											                }
											            }
												        
											    	    if(test_request)
											            {
											                test_request.onreadystatechange = function(){
										            	        if (test_request.readyState == 4){
										            	    	    if (test_request.status == 200){
										            	    	  	    if(test_request.getResponseHeader("Content-Length")=="0"){
										            	    			    setTimeout(function(){new getInstruction();}, refresh_rate);
										            	    		    }
										            	    		    else{
										            	    			    new centerSendSignal();
										            	    		    }
									            	    		    }
									            	    		    else{
									            	    			    new centerSendSignal();
									            	    		    }
									            	    	    }
											                };
											                test_request.open("GET", "/sendCenterSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+entityLocalReferenceId+"?initialSize="+accumulatedScale.toFixed(1)+"?finalSize="+occupyPercentage.toFixed(1)+"?time="+time.toFixed(1), true);
											            
											                test_request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
											                test_request.send();
											            //setTimeout(function(){new getInstruction();}, refresh_rate);
											            }
										            }, 0);*/
				                                  
										            resumePreloadBands();
										            //preloadBandQueue = findClosestLookingApps();
										            
				                                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
			                                    }, 0);
			                                  
			                                }
		                                }
		                            }, 0);
	                            }
	                  	    }
                  	    }
                  	    else{
                  	    	writeToErrorConsole("xy center app not found!");
                      	    if(!stop){
                      	    	setTimeout(function(){
                      	    		new checkElementExist1();
                      	    	}, 300);
                      	    	
                      	        //setTimeout(arguments.callee, 300);
                  	        }
                  	    }
              	    }, 0);
            		break;
            	}
            	case CENTER_SCREEN:{
            		var entityLocalReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                    if(globals[entityLocalReferenceId]!=null)
                    {
                        entityLocalReferenceId = globals[entityLocalReferenceId];
                    }
                    var occupyPercentage = parseFloat(instruction.getElementsByTagName("op")[0].firstChild.nodeValue);
                    	                    
                    var time = parseFloat(instruction.getElementsByTagName("t")[0].firstChild.nodeValue);
                    time = parseFloat(time);
                    
                    var time_allowed = time*1000;
                    
                    var centerSignalSent = false;
                    
                    
            		
                    setTimeout(function checkElementExist2(){
                    	if($("div[name='group_"+entityLocalReferenceId+"']").length>0)
	                      {
	                      	  if($("div[name='group_"+entityLocalReferenceId+"']").data("awaiting_imgs")!=null){
	                      	      if(parseInt($("div[name='group_"+entityLocalReferenceId+"']").data("awaiting_imgs"))>0){
	                      	          if(!stop){
	                      	          	  setTimeout(function(){
	                  	            		  new checkElementExist2();
	                  	            	  }, 300);
			                      	      //setTimeout(arguments.callee, 300);
		                      	      }
	                      	      }
	                      	      else{
			                          var ratio;
		                        	  if($("div[name='group_"+entityLocalReferenceId+"']").parent().data("original_width")!=null){
		                                  ratio = radius/full_screen_scale;
	                                  }
		                              else{
		                                  ratio = (radius/full_screen_scale)*(Math.min(PLAYGROUND_WIDTH,PLAYGROUND_HEIGHT)/full_screen_scale);
	                                  }
	                                  
	                                  
	                                  var accumulatedScale = 1;
	                                  var accumulatedScale2 = 1;
	                                  
	                                  
	                                  
	                                  var currentEntity = $("div[name='group_"+entityLocalReferenceId+"']");
	                                  
	                                  
	                                  
	                                  var entityToZoom = $($("div[name='group_0']").find("div")[0]);
	                                  
	                                  var tempElementX = parseFloat(currentEntity.offset().left);
                                      var tempElementY = parseFloat(currentEntity.offset().top);
                                      
                                      
	                                  
	                                  while(true){
	                                  	  
	                                	  if(currentEntity.attr("id")=="group_0"){
	                                	  	  accumulatedScale2 = accumulatedScale;
	                                	  	  accumulatedScale2 = accumulatedScale2 * currentEntity.data("scale");
	                                		  break;
	                                	  }
	                                	  accumulatedScale = accumulatedScale * currentEntity.data("scale");
	                                	  
	                                	  currentEntity = currentEntity.parent();
	                                  }
	                                  
	                                  /*setTimeout(function centerSendSignal(){
			                            //Testing only
			                            var test_request = false;
			
								        try
								        {
								           test_request = new XMLHttpRequest();
								        }
								        catch (error)
								        {
								            try
								            {
								                test_request = new ActiveXObject("Microsoft.XMLHTTP");
								            }
								            catch (error)
								            {
								                alert('there is an unknown error occurred');
								            }
								        }
									        
								    	if(test_request)
								        {
								            test_request.onreadystatechange = function(){
								            	    if (test_request.readyState == 4){
								            	    	if (test_request.status == 200){
								            	    		if(test_request.getResponseHeader("Content-Length")=="0"){
								            	    			centerSignalSent = true;
								            	    			setTimeout(function(){new getInstruction();}, refresh_rate);
								            	    		}
								            	    		else{
								            	    			new centerSendSignal();
								            	    		}
							            	    		}
							            	    		else{
							            	    			new centerSendSignal();
							            	    		}
							            	    	}
								            	};
								            test_request.open("GET", "/sendCenterSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+entityLocalReferenceId+"?initialSize="+accumulatedScale.toFixed(1)+"?finalSize="+occupyPercentage.toFixed(1)+"?time="+time.toFixed(1), true);
								            
								            test_request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
								            test_request.send();
								            //setTimeout(function(){new getInstruction();}, refresh_rate);
								        }
							        }, 0);*/
	                                  
	                                  
	                                  
	                                  var width = 1000*accumulatedScale2;
	                                  var height = 1000*accumulatedScale2;
	                                  
	                                  
	                                  var factor = ($("div[name='group_0']").data("scale")*1000*occupyPercentage)/(1000*accumulatedScale2);
	                                  
	                                  
	                                  //var ratio = 1/accumulatedScale;
	                                  var base_ratio = entityToZoom.data("scale");
	                                  var ratio = factor*base_ratio;
	                                   
	                                  //entityToZoom.scale(1/accumulatedScale);
	                                   
	                                  var final_ratio = ratio/base_ratio;
	                                 
	                                                                 
	                                  		                                  
                                      var x_orig = parseFloat(entityToZoom.css("left"));
		                              var y_orig = parseFloat(entityToZoom.css("top"));
		                              
		                              
		                              
		                              var x_dist = (($("div[name='group_0']").offset().left+(1000*$("div[name='group_0']").data("scale")/2))-(width/2)-tempElementX)/(accumulatedScale2/accumulatedScale);
		                              x_dist = (x_dist)*(final_ratio)+(x_orig*(ratio-base_ratio)/base_ratio);
		                              
			                          var y_dist = (($("div[name='group_0']").offset().top+(1000*$("div[name='group_0']").data("scale")/2))-(height/2)-tempElementY)/(accumulatedScale2/accumulatedScale);
			                          y_dist = (y_dist)*(final_ratio)+(y_orig*(ratio-base_ratio)/base_ratio);
		                              
		                              
				                      
				                      
				                      entityToZoom.data("moving", parentTimespot.getId());
				                      
				                      var start = new Date().getTime();
			                          
			                          var hasMoreTime = true;
			                          
			                          
			                          setTimeout(function moveItem4(){
				                          if(hasMoreTime){
				                              var elapsed = new Date().getTime()-start;
				                              if(elapsed<time_allowed)
				                              {
				                                  
				                              	  setTimeout(function(){
					                                  entityToZoom.css("left", x_orig+((elapsed/time_allowed)*x_dist));
					                                  entityToZoom.css("top", y_orig+((elapsed/time_allowed)*y_dist));
					                              }, 0);
				                                  
				                                  
				                                  
				                                  setTimeout(function(){
				                                      if(ratio!=base_ratio){
					                                      entityToZoom.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
					                                      
					                                      entityToZoom.data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
					                                      entityToZoom.data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*entityToZoom.data("original_width"));
					                                      entityToZoom.data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*entityToZoom.data("original_height"));
					                                  }
					                                  if(entityToZoom.data("moving")!= parentTimespot.getId())
					                                  {
					                                      hasMoreTime = false;
					                                      executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
					                                  }
					                                  else{
					                                  	  if(stop){
					                                      	   hasMoreTime = false;
										                  }
										                  else{
										                  	  setTimeout(moveItem4(), 0);
				                                          }
					                                  }
				                                  }, 0);
				                              }
				                              else
				                              {
				                              	  hasMoreTime = false;
				                              	  setTimeout(function(){
					                                  entityToZoom.css("left", x_orig+x_dist);
					                                  entityToZoom.css("top", y_orig+y_dist);
					                              }, 0);  
				                                  
				                                  
				                                  
				                                  setTimeout(function(){
				                                  	  if(ratio!=base_ratio){
					                                  	  entityToZoom.scale(ratio);
					                                      
					                                      entityToZoom.data("scale", ratio);
					                                      entityToZoom.data("width", ratio*entityToZoom.data("original_width"));
					                                      entityToZoom.data("height", ratio*entityToZoom.data("original_height"));
					                                  }
					                                  
					                                  executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
				                                  }, 0);
				                                  
				                              }
			                              }
			                          }, 0);
				                      	
				                      
				                      
	                                  
		                          }
	                      	  }
	                      }
	                      else{
	                      	  if(!stop){
	                      	  	  setTimeout(function(){
                      	    		  new checkElementExist2();
                      	    	  }, 300);
	                      	      //setTimeout(arguments.callee, 300);
                      	      }
                      	  }
                    }, 0);
                    
            		
            		break;
            	}
            	case GET_BINARY_FILE:{
            		var lf = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
            		var sLF = instruction.getElementsByTagName("slr")[0].firstChild.nodeValue;
            		
            		function sendBinaryFile(){
                        //Testing only
                        var test_request = makeRequest();
					        
				    	if(test_request)
				        {
				        	//test_request.ontimeout = function(){
				        	//	setTimeout(function(){new sendFormValues();}, conn_err_wait);
				        	//};
				        	//test_request.onerror = function(){
				        	//	setTimeout(function(){new sendFormValues();}, conn_err_wait);
				        	//};
				            test_request.onreadystatechange = function(){
				            	    if(test_request.readyState == 2){
				            	    	var cl = parseFloat(test_request.getResponseHeader("Content-Length"));
						            	if(!isNaN(cl)){
						            	    bandwidthUsed += cl;
						        	    }
					        		    //bandwidthUsed += parseFloat(test_request.getResponseHeader("Content-Length"));
				        			}
				            	    else if(test_request.readyState == 4){
				            	    	var cl = parseFloat(test_request.getResponseHeader("Content-Length"));
						            	if(!isNaN(cl)){
						            	    bandwidthUsed -= cl;
						        	    }
				            	    	//bandwidthUsed -= parseFloat(test_request.getResponseHeader("Content-Length"));
				            	    	if (test_request.status == 200 || test_request.status == 0){
				            	    		setTimeout(function(){new getInstruction("");}, refresh_rate);
				            	    		/*if(test_request.getResponseHeader("Content-Length")){
				            	    			if(parseFloat(test_request.getResponseHeader("Content-Length"))>=0){
				            	    				//setTimeout(function(){canClick[localReferenceId] = true;}, 2000);
				            	    				alert("here");
				            	    				setTimeout(function(){
				            	    					var parser = new DOMParser();
				            	    					var t = unob(test_request.responseText, false);
				            	    					storeTimespot(parser.parseFromString(t, "text/xml"));
				            	    				}, 0);
				            	    			}
				            	    		}
				            	    		else{
				            	    			setTimeout(function(){new sendBinaryFile();}, conn_err_wait);
				            	    		}*/
			            	    		}
			            	    		else{
			            	    			setTimeout(function(){new sendBinaryFile();}, conn_err_wait);
			            	    		}
			            	    	}
				            	};
				            	
				            
				            //$("input[name='binary_"+lf+"']").remove();
				            	
				            var files = $("input[name='binary_"+lf+"']")[0].files;
				            var fileData = new FormData();
				            if(!checkIfNull(files)){
				            	if(files.length==1){
				            		fileData.append('file', files[0]);
				            		
				            		test_request.open("POST", "/sendBinary?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+sLF+"?eventType="+EVENT_TYPE_FORM_SUBMIT, true);
						            //test_request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
						            test_request.setRequestHeader("X-Content-Length", files[0].size);
						            
						            
						            test_request.send(fileData);
						            
						            //var fr = new FileReader();
						            //fr.onload = sendOff; 
						            
						            //fr.readAsArrayBuffer(files[0]);
						            //fr.readAsText(files[0]);
						            
						            function sendOff(evt){
						            	function arrayBufferToBase64( buffer ) {
										    var binary = ''
										    var bytes = new Uint8Array( buffer )
										    var len = bytes.byteLength;
										    for (var i = 0; i < len; i++) {
										        binary += String.fromCharCode( bytes[ i ] )
										    }
										    return window.btoa( binary );
										}
										
										//var feawfew = arrayBufferToBase64(evt.target.result);
						            	
						            	test_request.setRequestHeader("X-Content-Length", evt.target.result.length);
						            	test_request.send(evt.target.result);
						            }
						            
				            	}
				            }
				            	
				            //var params = formComponentsArray[0]+"="+$("input[name='input_"+formComponentsArray[0]+"']").val()+"&"+formComponentsArray[1]+"="+$("input[name='input_"+formComponentsArray[1]+"']").val()+"\nEND\n";
				            
				            	
				            
				            //setTimeout(function(){new getInstruction();}, refresh_rate);
				        }
			        }
			        sendBinaryFile();
            		
            		
            		
            		
            		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
            		
            		break;
            	}
            	case GET_FORM_VALUES:{
            		var formComponentsString = instruction.getElementsByTagName("lrs")[0].firstChild.nodeValue;
            		var formComponentsArray = formComponentsString.split(" ");
            		var localReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
            		
            		
            		
            		function sendFormValues(){
                        //Testing only
                        var test_request = makeRequest();
					        
				    	if(test_request)
				        {
				        	//test_request.ontimeout = function(){
				        	//	setTimeout(function(){new sendFormValues();}, conn_err_wait);
				        	//};
				        	//test_request.onerror = function(){
				        	//	setTimeout(function(){new sendFormValues();}, conn_err_wait);
				        	//};
				            test_request.onreadystatechange = function(){
				            	    if(test_request.readyState == 2){
				            	    	var cl = parseFloat(test_request.getResponseHeader("Content-Length"));
						            	if(!isNaN(cl)){
						            	    bandwidthUsed += cl;
						        	    }
					        		    //bandwidthUsed += parseFloat(test_request.getResponseHeader("Content-Length"));
				        			}
				            	    else if(test_request.readyState == 4){
				            	    	var cl = parseFloat(test_request.getResponseHeader("Content-Length"));
						            	if(!isNaN(cl)){
						            	    bandwidthUsed -= cl;
						        	    }
				            	    	//bandwidthUsed -= parseFloat(test_request.getResponseHeader("Content-Length"));
				            	    	if (test_request.status == 200 || test_request.status == 0){
				            	    		setTimeout(function(){new getInstruction("");}, refresh_rate);
			            	    		}
			            	    		else{
			            	    			setTimeout(function(){new sendFormValues();}, conn_err_wait);
			            	    		}
				            	    	
			            	    	}
				            	};
				            	
				            //var params = "_start_\n"+$("textarea[name='textarea_"+formComponentsArray[0]+"']").val()+"\n"+$("input[name='input_"+formComponentsArray[1]+"']").val()+"\n_end_\n";
				            
				            var params = "_start_\n"+$("textarea[name='textarea_"+formComponentsArray[0]+"']").val()+"\n_end_\n";
				            	
				            //var params = formComponentsArray[0]+"="+$("input[name='input_"+formComponentsArray[0]+"']").val()+"&"+formComponentsArray[1]+"="+$("input[name='input_"+formComponentsArray[1]+"']").val()+"\nEND\n";
				            
				            	
				            test_request.open("POST", "/sendSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+localReferenceId+"?eventType="+EVENT_TYPE_FORM_SUBMIT, true);
				            
				            test_request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
				            test_request.send(params);
				            //setTimeout(function(){new getInstruction();}, refresh_rate);
				        }
			        }
			        sendFormValues();
            		
            		
            		
            		
            		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
            		break;
            	}
            	case ADD_FORM:{
            		var preemptiveEntityId = instruction.getElementsByTagName("peid")[0].firstChild.nodeValue;
            		if(globals[preemptiveEntityId]!=null){
                        preemptiveEntityId = globals[preemptiveEntityId];
                    }
            		var parentEntityId = instruction.getElementsByTagName("pid")[0].firstChild.nodeValue;
            		if(globals[parentEntityId]!=null){
                        parentEntityId = globals[parentEntityId];
                    }
            		var preemptiveLocalReferenceId = instruction.getElementsByTagName("plr")[0].firstChild.nodeValue;
            		if(globals[preemptiveLocalReferenceId]!=null){
                        preemptiveLocalReferenceId = globals[preemptiveLocalReferenceId];
                    }
            		var x = instruction.getElementsByTagName("x")[0].firstChild.nodeValue;
            		x = parseFloat(x);
            		var y = instruction.getElementsByTagName("y")[0].firstChild.nodeValue;
            		y = parseFloat(y);
            		var radius = instruction.getElementsByTagName("rad")[0].firstChild.nodeValue;
            		radius = parseFloat(radius);
            		
            		if($("div[id='group_"+parentEntityId+"']").length>0){
            			$("div[id='group_"+parentEntityId+"']").addGroup("group_"+preemptiveEntityId, {width: full_screen_scale, height: full_screen_scale, preemptiveLocalReferenceId: preemptiveLocalReferenceId});
            			
            			$("div[name='group_"+preemptiveLocalReferenceId+"']").css("left", x+($("#group_"+parentEntityId).data("original_width")/2)-(full_screen_scale/2));
	                    $("div[name='group_"+preemptiveLocalReferenceId+"']").css("top", y+($("#group_"+parentEntityId).data("original_height")/2)-(full_screen_scale/2));
	                    $("div[name='group_"+preemptiveLocalReferenceId+"']").data("awaiting_imgs", 0);
                        $("div[name='group_"+preemptiveLocalReferenceId+"']").data("original_width",full_screen_scale);
                        $("div[name='group_"+preemptiveLocalReferenceId+"']").data("original_height",full_screen_scale);
                        $("div[name='group_"+preemptiveLocalReferenceId+"']").data("width",full_screen_scale);
                        $("div[name='group_"+preemptiveLocalReferenceId+"']").data("height",full_screen_scale);
                        var ratio = radius/full_screen_scale;
                        $("div[name='group_"+preemptiveLocalReferenceId+"']").scale(ratio);
                        
                        $("div[name='group_"+preemptiveLocalReferenceId+"']").css("visibility", "inherit");
                        $("div[name='group_"+preemptiveLocalReferenceId+"']").data("scale", ratio);
                        $("div[name='group_"+preemptiveLocalReferenceId+"']").data("width",ratio*full_screen_scale);
                        $("div[name='group_"+preemptiveLocalReferenceId+"']").data("height",ratio*full_screen_scale);
            		}
            		
            		
            		
            		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
            		break;
            	}
            	case ADD_FORM_COMPONENT:{
            		var componentType = instruction.getElementsByTagName("ct")[0].firstChild.nodeValue;
            		var preemptiveEntityId = instruction.getElementsByTagName("pid")[0].firstChild.nodeValue;
            		if(globals[preemptiveEntityId]!=null){
                        preemptiveEntityId = globals[preemptiveEntityId];
                    }
            		var formEntityId = instruction.getElementsByTagName("fid")[0].firstChild.nodeValue;
            		if(globals[formEntityId]!=null){
                        formEntityId = globals[formEntityId];
                    }
            		var preemptiveLocalReferenceId = instruction.getElementsByTagName("plr")[0].firstChild.nodeValue;
            		if(globals[preemptiveLocalReferenceId]!=null){
                        preemptiveLocalReferenceId = globals[preemptiveLocalReferenceId];
                    }
            		var x = instruction.getElementsByTagName("x")[0].firstChild.nodeValue;
            		x = parseFloat(x);
            		var y = instruction.getElementsByTagName("y")[0].firstChild.nodeValue;
            		y = parseFloat(y);
            		var width = instruction.getElementsByTagName("w")[0].firstChild.nodeValue;
            		width = parseFloat(width)*full_screen_scale;
            		var height = instruction.getElementsByTagName("h")[0].firstChild.nodeValue;
            		height = parseFloat(height)*full_screen_scale;
            		
            		
            		if(componentType==form_binary){
            			if($("div[id='group_"+formEntityId+"']").length>0){
            				$("div[id='group_"+formEntityId+"']").addBinaryUpload("binary_"+preemptiveEntityId, {x:x+($("#group_"+formEntityId).data("original_width")/2)-(width/2), y:y+($("#group_"+formEntityId).data("original_height")/2)-(height/2), width:width, height:height, preemptiveLocalReferenceId:preemptiveLocalReferenceId} );
            				
            				
            			}
            		}
            		else if(componentType==form_input_text || componentType==form_input_password){
            			if($("div[id='group_"+formEntityId+"']").length>0){
            				$("div[id='group_"+formEntityId+"']").addInputText("input_"+preemptiveEntityId, {x:x+($("#group_"+formEntityId).data("original_width")/2)-(width/2), y:y+($("#group_"+formEntityId).data("original_height")/2)-(height/2), width:width, height:height, preemptiveLocalReferenceId:preemptiveLocalReferenceId, hideText:componentType==form_input_password?true:false});
            				
            				var iAndAndroidDevices = false;
					        var ua = navigator.userAgent.toLowerCase();
					        var eventType = '';
					        
					        if(ua.indexOf("iphone")!=-1 || ua.indexOf("ipad")!=-1 || ua.indexOf("ipod")!=-1 || ua.indexOf("android")!=-1){
					        	iAndAndroidDevices = true;
					        }
		                    if(iAndAndroidDevices){
		                    	eventType = 'touchend';
		                    }
		                    else{
		                        eventType = 'click';
	                        }
	                        $("input[name='input_"+preemptiveLocalReferenceId+"']").bind(eventType, function(e)
                            {
                            	if($(document).data("draggingElement")==null){
                                    e.stopPropagation();
                                    this.focus();
                                }
                            });
                        
            			}
            		}
            		else if(componentType==form_textarea){
            			if($("div[id='group_"+formEntityId+"']").length>0){
            				$("div[id='group_"+formEntityId+"']").addTextArea("textarea_"+preemptiveEntityId, {x:x+($("#group_"+formEntityId).data("original_width")/2)-(width/2), y:y+($("#group_"+formEntityId).data("original_height")/2)-(height/2), width:width, height:height, preemptiveLocalReferenceId:preemptiveLocalReferenceId});
            				
            				var iAndAndroidDevices = false;
					        var ua = navigator.userAgent.toLowerCase();
					        var eventType = '';
					        
					        if(ua.indexOf("iphone")!=-1 || ua.indexOf("ipad")!=-1 || ua.indexOf("ipod")!=-1 || ua.indexOf("android")!=-1){
					        	iAndAndroidDevices = true;
					        }
		                    if(iAndAndroidDevices){
		                    	eventType = 'touchstart';
		                    }
		                    else{
		                        eventType = 'mousedown';
	                        }
	                        $("textarea[name='textarea_"+preemptiveLocalReferenceId+"']").bind(eventType, function(e){
	                        	e.stopPropagation();
	                        });
            			}
            		}
            		
            		
            		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
            		break;
            	}
            	case CLIPPING:{
            		var preemptiveLocalReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
            		var enabled = instruction.getElementsByTagName("e")[0].firstChild.nodeValue;
            		if($("div[name='group_"+preemptiveLocalReferenceId+"']").length>0){
            			if(enabled=="1"){
	            			$("div[name='group_"+preemptiveLocalReferenceId+"']").css("clip", "rect(0px "+full_screen_scale+"px "+full_screen_scale+"px 0px);");
	            		}
	            		else{
	            			$("div[name='group_"+preemptiveLocalReferenceId+"']").css("clip", "");
	            		}
            		}
            		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
            		break;
            	}
            	case SET_DRAG_CONDITION:
            	{
            		writeToConsole("calling set drag");
            		
            		var preemptiveLocalReferenceId = instruction.getElementsByTagName("plr")[0].firstChild.nodeValue;
            		if(globals[preemptiveLocalReferenceId]!=null){
                        preemptiveLocalReferenceId = globals[preemptiveLocalReferenceId];
                    }
            		var localReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
            		if(globals[localReferenceId]!=null){
                        localReferenceId = globals[localReferenceId];
                    }
                    event_element[preemptiveLocalReferenceId] = localReferenceId;
                    event_type[preemptiveLocalReferenceId] = "drag";
                    
            		var dragPlane = instruction.getElementsByTagName("dp")[0].firstChild.nodeValue;
            		//if(dragPlane==2){
            			//console.error("set vertical drag");
            		//}
            		var isStationary = instruction.getElementsByTagName("is")[0].firstChild.nodeValue;
            		var leftUpRightDown = parseInt(instruction.getElementsByTagName("lurd")[0].firstChild.nodeValue);
            		var isEdgeLockedToBoundary = instruction.getElementsByTagName("ie")[0].firstChild.nodeValue;
            		var minDragDistanceForTrigger = parseFloat(instruction.getElementsByTagName("min")[0].firstChild.nodeValue);
            		var maxDragDistanceForTrigger = parseFloat(instruction.getElementsByTagName("max")[0].firstChild.nodeValue);
            		var timespot_to_execute = instruction.getElementsByTagName("tid")[0].firstChild.nodeValue;
            		//var x = parseFloat(instruction.getElementsByTagName("x")[0].firstChild.nodeValue);
            		//var y = parseFloat(instruction.getElementsByTagName("y")[0].firstChild.nodeValue);
            		//var finalZoom = parseFloat(instruction.getElementsByTagName("fz")[0].firstChild.nodeValue);
            		
            		//alert(mLocalReferenceId);
            		
            		if($("div[name='image_"+localReferenceId+"']").length>0){
            			if($("div[name='image_"+localReferenceId+"']").data("pp-ppdrag")==null || typeof($("div[name='image_"+localReferenceId+"']").data("pp-ppdrag"))=="undefined"){
	            			$("div[name='image_"+localReferenceId+"']").css("pointer-events", "visible");
	            			if(dragPlane=="1"){
	            				//$("div[name='image_"+localReferenceId+"']").draggable({ axis: 'x' });
	            				$("div[name='image_"+localReferenceId+"']").ppdrag({ er: preemptiveLocalReferenceId, axis: 'x', isStationary: isStationary=="1"?true:false, isEdgeLockedToBoundary: isEdgeLockedToBoundary=="1"?true:false });
	            				$("div[name='image_"+localReferenceId+"']").css("cursor","e-resize");
	            			}
	            			else if(dragPlane=="2"){
	            				//$("div[name='image_"+localReferenceId+"']").draggable({ axis: 'y' });
	            				
                                $("div[name='image_"+localReferenceId+"']").ppdrag({ er: preemptiveLocalReferenceId, axis: 'y', isStationary: isStationary=="1"?true:false, isEdgeLockedToBoundary: isEdgeLockedToBoundary=="1"?true:false });
                                $("div[name='image_"+localReferenceId+"']").css("cursor","n-resize");
	            			}
	            			else if(dragPlane=="3"){
	            				//$("div[name='image_"+localReferenceId+"']").draggable();
	            				$("div[name='image_"+localReferenceId+"']").ppdrag({ er: preemptiveLocalReferenceId, isStationary: isStationary=="1"?true:false, isEdgeLockedToBoundary: isEdgeLockedToBoundary=="1"?true:false });
	            				$("div[name='image_"+localReferenceId+"']").css("cursor","move");
	            			}
	            			
	            			if(isEdgeLockedToBoundary){
	            				
	            				event_type_function[preemptiveLocalReferenceId] = function(event, param1, param2, param3, param4) {
	        						if(leftUpRightDown==6){
	        							//left
	        							var diff = (param2-param1)/($("div[name='group_0']").data("scale")*1000);
	        							if(diff>=minDragDistanceForTrigger && diff<=maxDragDistanceForTrigger){
	        								
	        								//findParentAndZoom($("div[name='image_"+localReferenceId+"']"), x, y, finalZoom);
	        								new executeTimespot(timespot_to_execute, parentTimespot);
	        								checkReceiveEventFromChild2($("div[name='image_"+localReferenceId+"']"), parentTimespot, EVENT_TYPE_SWIPE_LEFT, param1, param3, param2, param4);
	        							}
	        							
	        						}
	        						else if(leftUpRightDown==7){
	        							//right
	        							var diff = (param1-param2)/($("div[name='group_0']").data("scale")*1000);
	        							
	        							if(diff>=minDragDistanceForTrigger && diff<=maxDragDistanceForTrigger){
	        								
	        								//findParentAndZoom($("div[name='image_"+localReferenceId+"']"), x, y, finalZoom);
	        								new executeTimespot(timespot_to_execute, parentTimespot);
	        								checkReceiveEventFromChild2($("div[name='image_"+localReferenceId+"']"), parentTimespot, EVENT_TYPE_SWIPE_RIGHT, param1, param3, param2, param4);
	        							}
	        							else{
	        								return true;
	        							}
	        						}
	        						else if(leftUpRightDown==8){
	        							//up
	        							var diff = (param4-param3)/($("div[name='group_0']").data("scale")*1000);
	        							if(diff>=minDragDistanceForTrigger && diff<=maxDragDistanceForTrigger){
	        								
	        								//findParentAndZoom($("div[name='image_"+localReferenceId+"']"), x, y, finalZoom);
	        								new executeTimespot(timespot_to_execute, parentTimespot);
	        								checkReceiveEventFromChild2($("div[name='image_"+localReferenceId+"']"), parentTimespot, EVENT_TYPE_SWIPE_UP, param1, param3, param2, param4);
	        							}
	        							else{
	        								return true;
	        							}
	        						}
	        						else if(leftUpRightDown==9){
	        							//down
	        							var diff = (param3-param4)/($("div[name='group_0']").data("scale")*1000);
	        							if(diff>=minDragDistanceForTrigger && diff<=maxDragDistanceForTrigger){
	        								
	        								//findParentAndZoom($("div[name='image_"+localReferenceId+"']"), x, y, finalZoom);
	        								new executeTimespot(timespot_to_execute, parentTimespot);
	        								checkReceiveEventFromChild2($("div[name='image_"+localReferenceId+"']"), parentTimespot, EVENT_TYPE_SWIPE_DOWN, param1, param3, param2, param4);
	        							}
	        							else{
	        								return true;
	        							}
	        						}
	        						
	        						return false;
	    						};
	            				
		            			$("div[name='image_"+localReferenceId+"']").bind("swipe", event_type_function[preemptiveLocalReferenceId]);
    						}
    						
	            			
	            			$("div[name='image_"+localReferenceId+"']").bind("custom", function() {
							    if(!stop){
		                        	//new executeTimespot(timespot_to_execute, parentTimespot);
		                        	//setTimeout(function(){new executeTimespot(timespot_to_execute, parentTimespot);}, 0);
		                        	new executeTimespot(timespot_to_execute, parentTimespot);
		                        	
		                        	function imgDragSendSignal(){
		                                var drag_signal = makeRequest();
									        
								    	if(drag_signal)
								        {
								        	//drag_signal.ontimeout = function(){
								        	//	setTimeout(function(){new imgDragSendSignal();}, conn_err_wait);
								        	//};
								        	//drag_signal.onerror = function(){
								        	//	setTimeout(function(){new imgDragSendSignal();}, conn_err_wait);
								        	//};
								            drag_signal.onreadystatechange = function(){
								            	    if(drag_signal.readyState == 2){
								            	    	var cl = parseFloat(drag_signal.getResponseHeader("Content-Length"));
										            	if(!isNaN(cl)){
										            	    bandwidthUsed += cl;
										        	    }
									        		    //bandwidthUsed += parseFloat(drag_signal.getResponseHeader("Content-Length"));
								        			}
								            	    else if(drag_signal.readyState == 4){
								            	    	var cl = parseFloat(drag_signal.getResponseHeader("Content-Length"));
										            	if(!isNaN(cl)){
										            	    bandwidthUsed -= cl;
										        	    }
								            	    	//bandwidthUsed -= parseFloat(drag_signal.getResponseHeader("Content-Length"));
								            	    	if (drag_signal.status == 200){
								            	    		if(drag_signal.getResponseHeader("Content-Length")=="0"){
								            	    			setTimeout(function(){new getInstruction("");}, refresh_rate);
								            	    		}
								            	    		else{
								            	    			setTimeout(function(){new imgDragSendSignal();}, conn_err_wait);
								            	    			//new imgDragSendSignal();
								            	    		}
							            	    		}
							            	    		else{
							            	    			setTimeout(function(){new imgDragSendSignal();}, conn_err_wait);
							            	    			//new imgDragSendSignal();
							            	    		}
							            	    	}
								            	};
								            drag_signal.open("GET", "/sendSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+localReferenceId+"?eventType="+EVENT_TYPE_DRAG, true);
								            
								            drag_signal.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
								            drag_signal.send();
								            //setTimeout(function(){new getInstruction();}, refresh_rate);
								        }
							        }
							        imgDragSendSignal();
		                        }
		                        else
		                            writeToConsole("Stopping EXECUTE_TIMESPOT");
							});
        			    }
            		}
            		else if($("div[name='group_"+localReferenceId+"']").length>0){
            			//if($("div[name='group_"+localReferenceId+"']").data("pp-ppdrag")==null || typeof($("div[name='group_"+localReferenceId+"']").data("pp-ppdrag"))=="undefined"){
	            			$("div[name='group_"+localReferenceId+"']").css("pointer-events", "visible");
	            			if(dragPlane=="1"){
	            				$("div[name='group_"+localReferenceId+"']").ppdrag({ er: preemptiveLocalReferenceId, axis: 'x', isStationary: isStationary=="1"?true:false, isEdgeLockedToBoundary: isEdgeLockedToBoundary=="1"?true:false });
	            				$("div[name='group_"+localReferenceId+"']").css("cursor","e-resize");
	            			}
	            			else if(dragPlane=="2"){
            				    $("div[name='group_"+localReferenceId+"']").ppdrag({ er: preemptiveLocalReferenceId, axis: 'y', isStationary: isStationary=="1"?true:false, isEdgeLockedToBoundary: isEdgeLockedToBoundary=="1"?true:false });
            				    $("div[name='group_"+localReferenceId+"']").css("cursor","n-resize");
	            			}
	            			else if(dragPlane=="3"){
	            				$("div[name='group_"+localReferenceId+"']").ppdrag({ er: preemptiveLocalReferenceId, isStationary: isStationary=="1"?true:false, isEdgeLockedToBoundary: isEdgeLockedToBoundary=="1"?true:false });
	            				$("div[name='group_"+localReferenceId+"']").css("cursor","move");
            				}
            				
            				$("div[name='group_"+localReferenceId+"']").bind("checkRender"+localReferenceId, function() {
            					//appBandCheckedTracer = new Array();
            					resumePreloadBands();
    	                        //preloadBandQueue = findClosestLookingApps();
            					startPreloadingBands();
            					
            					
            					//setTimeout(function(){
            					//    new checkRenderDefinitionBands(localReferenceId, false, $("div[name='group_"+localReferenceId+"']").data("scale"), $("div[name='group_"+localReferenceId+"']").data("scale"), parentTimespot);
        					    //}, 0);
        					    
        					});
        					
        					if(isEdgeLockedToBoundary){
        						
    							event_type_function[preemptiveLocalReferenceId] = function(event, param1, param2, param3, param4) {
        						
	        						if(leftUpRightDown==6){
	        							//left
	        							var diff = (param2-param1)/($("div[name='group_0']").data("scale")*1000);
	        							if(diff>=minDragDistanceForTrigger && diff<=maxDragDistanceForTrigger){
	        								
	        								//findParentAndZoom($("div[name='group_"+localReferenceId+"']"), x, y, finalZoom);
	        								
	        								//camera.centerIntoLF(mLocalReferenceId);
	        								new executeTimespot(timespot_to_execute, parentTimespot);
	        								checkReceiveEventFromChild2($("div[name='group_"+localReferenceId+"']"), parentTimespot, EVENT_TYPE_SWIPE_LEFT, param1, param3, param2, param4);
	        							}
	        							else{
	        								return true;
	        							}
	        							
	        						}
	        						else if(leftUpRightDown==7){
	        							//right
	        							var diff = (param1-param2)/($("div[name='group_0']").data("scale")*1000);
	        							
	        							if(diff>=minDragDistanceForTrigger && diff<=maxDragDistanceForTrigger){
	        								//alert("right");
	        								
	        								//findParentAndZoom($("div[name='group_"+localReferenceId+"']"), x, y, finalZoom);
	        								new executeTimespot(timespot_to_execute, parentTimespot);
	        								checkReceiveEventFromChild2($("div[name='group_"+localReferenceId+"']"), parentTimespot, EVENT_TYPE_SWIPE_RIGHT, param1, param3, param2, param4);
	        							}
	        							else{
	        								return true;
	        							}
	        							
	        						}
	        						else if(leftUpRightDown==8){
	        							//up
	        							var diff = (param4-param3)/($("div[name='group_0']").data("scale")*1000);
	        							if(diff>=minDragDistanceForTrigger && diff<=maxDragDistanceForTrigger){
	        								//alert("up");
	        								
	        								//findParentAndZoom($("div[name='group_"+localReferenceId+"']"), x, y, finalZoom);
	        								new executeTimespot(timespot_to_execute, parentTimespot);
	        								checkReceiveEventFromChild2($("div[name='group_"+localReferenceId+"']"), parentTimespot, EVENT_TYPE_SWIPE_UP, param1, param3, param2, param4);
	        							}
	        							else{
	        								return true;
	        							}
	        							
	        						}
	        						else if(leftUpRightDown==9){
	        							//down
	        							var diff = (param3-param4)/($("div[name='group_0']").data("scale")*1000);
	        							if(diff>=minDragDistanceForTrigger && diff<=maxDragDistanceForTrigger){
	        								//alert("down");
	        								
	        								//findParentAndZoom($("div[name='group_"+localReferenceId+"']"), x, y, finalZoom);
	        								//camera.centerIntoLF(mLocalReferenceId);
	        								new executeTimespot(timespot_to_execute, parentTimespot);
	        								checkReceiveEventFromChild2($("div[name='group_"+localReferenceId+"']"), parentTimespot, EVENT_TYPE_SWIPE_DOWN, param1, param3, param2, param4);
	        							}
	        							else{
	        								return true;
	        							}
	        						}
	        						return false;
        						};
        						
        						
        						
        						
        						
	        					$("div[name='group_"+localReferenceId+"']").bind("swipe", event_type_function[preemptiveLocalReferenceId]
	        						
	    						);
    						}
    						
            				
            				$("div[name='group_"+localReferenceId+"']").bind("custom", function() {
            					
							    if(!stop){
		                        	//new executeTimespot(timespot_to_execute, parentTimespot);
		                        	//setTimeout(function(){new executeTimespot(timespot_to_execute, parentTimespot);}, 0);
		                        	new executeTimespot(timespot_to_execute, parentTimespot);
		                        	
		                        	function grpDragSendSignal(){
		                                var drag_signal = makeRequest();
									        
								    	if(drag_signal)
								        {
								        	//drag_signal.ontimeout = function(){
								        	//	setTimeout(function(){new grpDragSendSignal();}, conn_err_wait);
								        	//};
								        	//drag_signal.onerror = function(){
								        	//	setTimeout(function(){new grpDragSendSignal();}, conn_err_wait);
								        	//};
								            drag_signal.onreadystatechange = function(){
								            	    if(drag_signal.readyState == 2){
								            	    	var cl = parseFloat(drag_signal.getResponseHeader("Content-Length"));
										            	if(!isNaN(cl)){
										            	    bandwidthUsed += cl;
										        	    }
									        		    //bandwidthUsed += parseFloat(drag_signal.getResponseHeader("Content-Length"));
								        			}
								            	    else if(drag_signal.readyState == 4){
								            	    	var cl = parseFloat(drag_signal.getResponseHeader("Content-Length"));
										            	if(!isNaN(cl)){
										            	    bandwidthUsed -= cl;
										        	    }
								            	    	//bandwidthUsed -= parseFloat(drag_signal.getResponseHeader("Content-Length"));
								            	    	if (drag_signal.status == 200){
								            	    		if(drag_signal.getResponseHeader("Content-Length")=="0"){
								            	    			setTimeout(function(){new getInstruction("");}, refresh_rate);
								            	    		}
								            	    		else{
								            	    			setTimeout(function(){new grpDragSendSignal();}, conn_err_wait);
								            	    			//new grpDragSendSignal();
								            	    		}
							            	    		}
							            	    		else{
							            	    			setTimeout(function(){new grpDragSendSignal();}, conn_err_wait);
							            	    			//new grpDragSendSignal();
							            	    		}
							            	    	}
								            	};
								            drag_signal.open("GET", "/sendSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+localReferenceId+"?eventType="+EVENT_TYPE_DRAG, true);
								            
								            drag_signal.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
								            drag_signal.send();
								            //setTimeout(function(){new getInstruction();}, refresh_rate);
								        }
							        }
							        grpDragSendSignal();
		                        }
		                        else{
		                            writeToConsole("Stopping EXECUTE_TIMESPOT");
	                            }
	                            
							});
        			    //}
            		}
            		else{
            			writeToErrorConsole("SET drag event failed, no image or group found");
            		}
            		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
            		
            		break;
            	}
            	case ADD_ACTIVE_DEEPREACH:{
                	var preemptiveEntityId = instruction.getElementsByTagName("peid")[0].firstChild.nodeValue;
                	//var activeDeepreach = instruction.getElementsByTagName("t")[0].firstChild.nodeValue;
                	
                	var activeDeepreach = "";
                	for(var i=0; i<instruction.getElementsByTagName("t")[0].childNodes.length; i++){
                		activeDeepreach += instruction.getElementsByTagName("t")[0].childNodes[i].nodeValue;
                	}
                	
                	//if($("canvas[id='group_"+preemptiveEntityId+"']").length>0){
                		
                		
                		
                		
                		//var sc = $('<div/>').html(activeDeepreach).text();
                		
                		
                		
                		//writeToErrorConsole("sc "+sc);
                		
                		//writeToErrorConsole("activeDeeprach "+activeDeepreach);
                		//var scString = unescape(activeDeepreach);
                		
                		
                		var script   = document.createElement("script");
                		script.type  = "text/javascript";
                		script.text  = $('<div/>').html(activeDeepreach).text();
                		//script.text  = scString;
                		
                		//var scString2 = scString+"end";
                		
                		//writeToErrorConsole("scstring "+scString2);
                		//alert(script.text);
                		
                		
                		//setTimeout(function(){
                			$("div[id='group_"+preemptiveEntityId+"']").append(script);
                		
                		    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                		//}, 1000);
                		
                		
                		
                		//eval(sc);
                		
                		
                		
            		//}
            		
            		
            		
                	break;
                }
                case ADD_MEDIA:
                {
                	//var start = new Date().getTime();
                    var preemptiveEntityId = instruction.getElementsByTagName("peid")[0].firstChild.nodeValue;
                    if(globals[preemptiveEntityId]!=null)
                        preemptiveEntityId = globals[preemptiveEntityId];
                    var mediaType = parseInt(instruction.getElementsByTagName("mt")[0].firstChild.nodeValue);
                    
                    
                    var html_code = "";
                    if(mediaType==media_htmlcode){
                    	html_code = instruction.getElementsByTagName("c")[0].firstChild.nodeValue;
                    }
                    
                    
                    var entityName = instruction.getElementsByTagName("en")[0].firstChild.nodeValue;
                    //var imageData = timespot_to_execute.getElementsByTagName("instruction")[i].getElementsByTagName("imageData")[0].firstChild.nodeValue;
                    var parentEntityId = instruction.getElementsByTagName("pid")[0].firstChild.nodeValue;
                    if(globals[parentEntityId]!=null)
                        parentEntityId = globals[parentEntityId];
                    var preemptiveLocalReferenceId = instruction.getElementsByTagName("plr")[0].firstChild.nodeValue;
                    if(globals[preemptiveLocalReferenceId]!=null)
                        preemptiveLocalReferenceId = globals[preemptiveLocalReferenceId];
                    var x = instruction.getElementsByTagName("x")[0].firstChild.nodeValue;
                    x = parseFloat(x);
                    /*if(floats[x]!=null)
                    {
                        x = floats[x];
                    }
                    else{
                    	x = parseFloat(x);
                    }*/
                    var y = instruction.getElementsByTagName("y")[0].firstChild.nodeValue;
                    y = parseFloat(y);
                    /*if(floats[y]!=null)
                    {
                        y = floats[y];
                    }
                    else{
                    	y = parseFloat(y);
                    }*/
                    
                    var radius = instruction.getElementsByTagName("r")[0].firstChild.nodeValue;
                    radius = parseFloat(radius);
                    /*if(floats[radius]!=null)
                    {
                        radius = floats[radius];
                    }
                    else{
                    	radius = parseFloat(radius);
                    }*/
                    
                    var mediaWidth = parseFloat(instruction.getElementsByTagName("mw")[0].firstChild.nodeValue);
                    var mediaHeight = parseFloat(instruction.getElementsByTagName("mh")[0].firstChild.nodeValue);
                    writeToConsole("add image with local ref: "+preemptiveLocalReferenceId+" with radius " + radius + " width = " + mediaWidth + " height = " + mediaHeight);
                    
                    var parent = $("div[id='group_"+parentEntityId+"']");
                    
                    if(mediaType==media_htmlcode){
                    	var ratio=radius/Math.max(mediaWidth, mediaHeight);
                    	
                    	//alert(ratio+" "+radius+" "+mediaWidth+" "+mediaHeight);
                    	
                    	if(!stop){
                    		if(parent.length>0){
                    			if(parent.find("div[name='group_"+preemptiveLocalReferenceId+"']").length==0){
                    				
                    				
                    				parent.addGroup("group_"+preemptiveEntityId, {width: mediaWidth, height: mediaHeight, preemptiveLocalReferenceId: preemptiveLocalReferenceId});
                    				
	                                var addedHtmlGroup = $("div[name='group_"+preemptiveLocalReferenceId+"']");
                    				
	                                addedHtmlGroup.css("left", x+($("#group_"+parentEntityId).data("original_width")/2)-(mediaWidth/2));
	                                addedHtmlGroup.css("top", y+($("#group_"+parentEntityId).data("original_height")/2)-(mediaHeight/2));
	                                
	                                addedHtmlGroup.css("clip", "");

	                                addedHtmlGroup.css("pointer-events", "visible");
	                                addedHtmlGroup.data("awaiting_imgs", 0);
	                                addedHtmlGroup.data("original_width",mediaWidth);
	                                addedHtmlGroup.data("original_height",mediaHeight);
	                                addedHtmlGroup.data("width",mediaWidth);
	                                addedHtmlGroup.data("height",mediaHeight);

	                                addedHtmlGroup.scale(ratio);
	                                
	                                addedHtmlGroup.css("visibility", "inherit");
	                                addedHtmlGroup.data("scale", ratio);
	                                addedHtmlGroup.data("width",ratio*mediaWidth);
	                                addedHtmlGroup.data("height",ratio*mediaHeight);
	                                
	                                addedHtmlGroup.append($('<div/>').html(html_code).text());
	                                
	                                timespotImgsLoaded[timespot_id] = 1;
	                                
	                            	executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
		                            
                				}
                				else{
                					timespotImgsLoaded[timespot_id] = 1;
                					executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                				}
                    		}
                    	}
                    }
                    if(mediaType==media_jpg || mediaType==media_png || mediaType==media_gif)
                    {
                    	var alreadyInCache = false;
                    	if(images_base64[preemptiveEntityId]!=null && images_base64[preemptiveEntityId]!=""){
                            alreadyInCache = true;
                        }
                        else{
                        	images_base64[preemptiveEntityId] = "";
                        }
                        
                        /*if(images[preemptiveEntityId]==null){
                            images[preemptiveEntityId] = new $.gameQuery.Animation({imageURL: "/addImage?sessionId="+sessionId+"?imageName="+entityName+"?mediaType="+mediaType});
                        }
                        else{
                        	alreadyInCache = true;
                        }*/
                        var ratio=radius/Math.max(mediaWidth, mediaHeight);
                        //var ratio = radius/full_screen_scale;
                        if(!stop)
                        {
                            if(parent.length>0)
                            {
                            	if(parent.find("div[name='image_"+preemptiveLocalReferenceId+"']").length==0){
                            		parent.data("awaiting_imgs", parent.data("awaiting_imgs")==null?1:parseInt(parent.data("awaiting_imgs"))+1);
                            		
	                                parent.addSprite("image_"+preemptiveEntityId, {posx: x+($("#group_"+parentEntityId).data("original_width")/2)-(mediaWidth/2), posy: y+($("#group_"+parentEntityId).data("original_height")/2)-(mediaHeight/2), width: mediaWidth, height: mediaHeight, preemptiveLocalReferenceId: preemptiveLocalReferenceId});
	                                
	                                var addedImage = $("div[name='image_"+preemptiveLocalReferenceId+"']");
	                                
	                                addedImage.css("left", x+($("#group_"+parentEntityId).data("original_width")/2)-(mediaWidth/2));
	                                addedImage.css("top", y+($("#group_"+parentEntityId).data("original_height")/2)-(mediaHeight/2));
	                                
	                                
	                                addedImage.data("image", "");
	                                //$("div[name='image_"+preemptiveLocalReferenceId+"']").data("image", images[preemptiveEntityId]);
	                                addedImage.css("background-color", "#ffffff");
	                                addedImage.data("original_width", mediaWidth);
	                                addedImage.data("original_height", mediaHeight);
	                                addedImage.data("width", mediaWidth);
	                                addedImage.data("height", mediaHeight);
	                                if(ratio!=1)
	                                {
	                                	addedImage.scale(ratio);
	                                    
	                                    addedImage.data("scale", ratio);
	                                    addedImage.data("width", ratio*mediaWidth);
	                                    addedImage.data("height", ratio*mediaHeight);
	                                }
	                                
	                                /*var addImgInNewThread = false;
	                                for(var next_noi = noi+1; next_noi<instructionList.length; next_noi++){
	                                	var next_instruction = instructionList[next_noi];
	                                	var next_ins_code = parseInt(next_instruction.getAttribute("ins_id"));
	                                	if(next_ins_code==ADD_MEDIA){
	                                		addImgInNewThread = true;
	                                		break;
	                                	}
	                                }*/
	                                
	                                if(alreadyInCache){
	                                	timespotImgsLoaded[timespot_id] = 1;
	                                	parent.data("awaiting_imgs", parseInt(parent.data("awaiting_imgs"))-1);
	                                	addedImage.data("image", "ok");
		                            	addedImage.css("background-color", "");
		                            	
		                            	//grab base64 image from cache
		                            	addedImage.css("background", "url("+images_base64[preemptiveEntityId]+")");
		                            	//alert(new Date().getTime()-start);
		                            	executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
		                            }
	                                
	                                
	                                
	                                /*images[preemptiveEntityId].image.onerror = function ttt(){
	                                	images[preemptiveEntityId] = new $.gameQuery.Animation({imageURL: "/addImage?sessionId="+sessionId+"?imageName="+entityName+"?mediaType="+mediaType});
                                	};*/
                                	
                                	else{
	                                	//test
	                                	function addImage(){
								            var request2 = makeRequest();
								        	
								        	function processResponseTwo(){
								        		if(request2.readyState == 2){
								        			var cl = parseFloat(request2.getResponseHeader("Content-Length"));
									            	if(!isNaN(cl)){
									            	    bandwidthUsed += cl;
									        	    }
								        		    //bandwidthUsed += parseFloat(request2.getResponseHeader("Content-Length"));
							        			}
									            else if(request2.readyState == 4){
									            	var cl = parseFloat(request2.getResponseHeader("Content-Length"));
									            	if(!isNaN(cl)){
									            	    bandwidthUsed -= cl;
									        	    }
									            	//bandwidthUsed -= parseFloat(request2.getResponseHeader("Content-Length"));
									                if (request2.status == 200){
									                	//setTimeout(function(){
									                		
									                	    //new function(){
									                	    //function decodeImage(){
											                	var responseString = request2.responseText;
											                	
											                	if(!responseString){
											                		setTimeout(function(){new addImage();}, conn_err_wait);
											                		//new addImage();
											                		return;
											                	}
											                	
											                	/*var i = responseString.length;
															    var binaryString = new Array(i);
															    var j = 0;
															    while (i--)
															    {
															        binaryString[j] = String.fromCharCode(responseString.charCodeAt(i) & 0xff);
															        j++;
															    }*/
															    
															    var binaryString = unob(responseString, true);
															    
															    var data = binaryString.join('');
															    
															
															    var base64 = btoa(data);
															    
															    
															
															    var base64MediaHeader = "data:image/jpeg;base64,";
											                	if(mediaType==media_png){
											                		base64MediaHeader = "data:image/png;base64,";
											                	}
											                	else if(mediaType==media_gif){
											                		base64MediaHeader = "data:image/gif;base64,";
											                	}
											                	
											                	images_base64[preemptiveEntityId] = base64MediaHeader+base64;
											                	
											                	addedImage.css("background", "url("+images_base64[preemptiveEntityId]+")");
											                	parent.data("awaiting_imgs", parseInt(parent.data("awaiting_imgs"))-1);
											                	addedImage.data("image", "ok");
											                	//alert(new Date().getTime()-start);
											                	
											                	timespotImgsLoaded[timespot_id] = 1;
											                	executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
										                	//}
									                	//}, 0);
									                }
									                else{
									                	setTimeout(function(){new addImage();}, conn_err_wait);
									                	//new addImage();
									                }
								                }
								                
								                /*function base64encode(binary) {
								                	var data = btoa(unescape(encodeURIComponent(binary)));
								                	//var data = btoa(unescape(encodeURIComponent(binary)));
								                	var base64MediaHeader = "data:image/jpeg;base64,";
								                	if(mediaType==media_png){
								                		base64MediaHeader = "data:image/png;base64,";
								                	}
								                	else if(mediaType==media_gif){
								                		base64MediaHeader = "data:image/gif;base64,";
								                	}
								                	$("div[name='image_"+preemptiveLocalReferenceId+"']").css("background", "url("+base64MediaHeader+data+")");
								                	
												    //return btoa(unescape(encodeURIComponent(binary)));
												}*/
									        }
									        /*function dataToBinary(data){
									        	var data_string = "";
											    for(var i=0; i<data.length; i++){
											        data_string += String.fromCharCode(data[i].charCodeAt(0) & 0xff);
											    }
											    return data_string;
									        }*/
									    	
									    	if(request2)
									        {
									        	//request2.ontimeout = function(){
									        	//	setTimeout(function(){new addImage();}, conn_err_wait);
									        	//};
									        	//request2.onerror = function(){
									        	//	setTimeout(function(){new addImage();}, conn_err_wait);
									        	//};
									            request2.onreadystatechange = processResponseTwo;
									            /*if(typeof(request2.onerror)!="undefined"){
									            	request2.onerror = function(){
									            		new addImage();
								            		}
									            }*/
									            request2.open("GET", "/addImage?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?imageName="+entityName+"?mediaType="+mediaType, true);
									            request2.overrideMimeType('text/plain; charset=x-user-defined');
									            //request2.responseType = 'arraybuffer';
									            request2.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
									            request2.send();
									        }
								        }
								        //setTimeout(function(){new addImage()}, 0);
								        addImage();
							        }
                                	
                                	
                                	/*images[preemptiveEntityId].image.onload = function sss(){
								        if(stop){
	                                	    //do nothing
                                		}
					                    else{
					                    	$("div[name='image_"+preemptiveLocalReferenceId+"']").css("background-color", "");
						                    noi++;
						                    if(noi<instructionList.length){	
						                    	var currentInstruction = new executeInstruction(instructionList[noi], noi, instructionList, parentArray, parentTimespot);
						                        parentArray.push(currentInstruction);
						                    }
					                    }
								        
                                	};*/
                                	
	                                
	                                
	                                
	                                
	                                /*if(addImgInNewThread){
	                                	if(stop){
		                                	break;
                                		}
					                    else{
					                    	images[preemptiveEntityId].image.onload = 
					                    	function sss(){
					                    		$("div[name='image_"+preemptiveLocalReferenceId+"']").css("background-color", "");
					                    	};
						                    noi++;
						                    if(noi<instructionList.length){	
						                    	var currentInstruction = new executeInstruction(instructionList[noi], noi, instructionList, parentArray, parentTimespot);
						                        parentArray.push(currentInstruction);
						                    }
					                    }
	                                }
	                                else{
	                                	
	                                	images[preemptiveEntityId].image.onload = 
	                                	function sss(){
	                                		if(stop){
		                                	    //do nothing
	                                		}
						                    else{
						                    	$("div[name='image_"+preemptiveLocalReferenceId+"']").css("background-color", "");
							                    noi++;
							                    if(noi<instructionList.length){	
							                    	var currentInstruction = new executeInstruction(instructionList[noi], noi, instructionList, parentArray, parentTimespot);
							                        parentArray.push(currentInstruction);
							                    }
						                    }
	                                	};
	                                }*/
                                }
                                else{
                                	timespotImgsLoaded[timespot_id] = 1;
                                	executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                                }
                            }
                            else{
                                writeToErrorConsole('Add image error: The parent group with parentEntityId '+parentEntityId+" that you want to add an image to does not exist");}
                        }
                        else {
                            writeToConsole("Stopping ADD_MEDIA: ");
                        }
                    }
                    else if(mediaType>=media_flv && mediaType<=media_wmv)
                    {
                    	var ratio=radius/Math.max(mediaWidth, mediaHeight);
                        if(!stop)
                        {
                        	//alert("add video!!!");
                        	
                            if(parent.length>0){
                            	parent.addVideo2("video_"+preemptiveEntityId, {posx: x+($("#group_"+parentEntityId).data("original_width")/2)-(mediaWidth/2), posy: y+($("#group_"+parentEntityId).data("original_height")/2)-(mediaHeight/2), width: mediaWidth, height: mediaHeight, preemptiveLocalReferenceId: preemptiveLocalReferenceId, mediaType: mediaType, videoUrl: "/addVideo2?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?videoName="+entityName})
                            	
                                //parent.addVideo("video_"+preemptiveEntityId, {posx: x+($("#group_"+parentEntityId).data("original_width")/2)-(mediaWidth/2), posy: y+($("#group_"+parentEntityId).data("original_height")/2)-(mediaHeight/2), width: mediaWidth, height: mediaHeight, preemptiveLocalReferenceId: preemptiveLocalReferenceId, mediaType: mediaType, videoName: entityName, videoUrl: "/addVideo?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?videoName="+entityName+"?mediaType="+mediaType+"?part=1"})
                                
                                var addedVideo = $("div[name='video_"+preemptiveLocalReferenceId+"']");
                                
                                addedVideo.css("background-color", "#ffffff");
                                addedVideo.data("original_width", mediaWidth);
                                addedVideo.data("original_height", mediaHeight);
                                addedVideo.data("width", mediaWidth);
                                addedVideo.data("height", mediaHeight);
                                addedVideo.data("finished", false);
                                
                                var video = addedVideo.find("video")[0];
                                
                                video.addEventListener('click',function(){
								    video.play();
								},false);
                                
                                if(ratio!=1)
                                {
                                	addedVideo.scale(ratio);
                                    
                                    addedVideo.data("scale", ratio);
                                    addedVideo.data("width", ratio*mediaWidth);
                                    addedVideo.data("height", ratio*mediaHeight);
                                }
                                
                                //$("div[name='video_"+preemptiveLocalReferenceId+"']").find("video")[0].addEventListener('ended', function(){
                                //	$("div[name='video_"+preemptiveLocalReferenceId+"']").remove();
                                //}, false);
                                
                                //$("div[name='video_"+preemptiveLocalReferenceId+"']").find("video")[0].addEventListener('pause', function(){
                                //	$("div[name='video_"+preemptiveLocalReferenceId+"']").remove();
                                //}, false);
                                
                                
                                
                                
                        		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                            }
                            else{
                                writeToErrorConsole('Add video error: The parent group with parentEntityId '+parentEntityId+" that you want to add a video to does not exist");}
                        }
                        else
                            writeToConsole("Stopping ADD_MEDIA: ");
                    }
                    
                    break;
                }
                case MEDIA_CANCEL_DOWNLOAD:{
                	var preemptiveLocalReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                	if(globals[preemptiveLocalReferenceId]!=null){
                        preemptiveLocalReferenceId = globals[preemptiveLocalReferenceId];
                    }
                    $("div[name='video_"+preemptiveLocalReferenceId+"']").cancelDownload();
                    running_timespots[$("div[name='video_"+preemptiveLocalReferenceId+"']").data("tid")] = null;
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                	break;
                }
                case MEDIA_PAUSE:
                {
                	var preemptiveLocalReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                	if(globals[preemptiveLocalReferenceId]!=null){
                        preemptiveLocalReferenceId = globals[preemptiveLocalReferenceId];
                    }
                	var isPaused = instruction.getElementsByTagName("ip")[0].firstChild.nodeValue;
                	if($("div[name='video_"+preemptiveLocalReferenceId+"']").length>0){
                		if(isPaused=="1"){
                			$("div[name='video_"+preemptiveLocalReferenceId+"']").data("notPaused", false);
                			//$("div[name='video_"+preemptiveLocalReferenceId+"']").pause();
                		    //$("div[name='video_"+preemptiveLocalReferenceId+"']").find("video")[0].pause();
            		    }
            		    //else if(isPaused=="0"){
            		    //	$("div[name='video_"+preemptiveLocalReferenceId+"']").find("video")[0].play();
            		    //}
                	}
                	executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                	break;
                }
                case MEDIA_PLAY_BYTES:
                {
                	var preemptiveLocalReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                	if(globals[preemptiveLocalReferenceId]!=null){
                        preemptiveLocalReferenceId = globals[preemptiveLocalReferenceId];
                    }
                	var buffering = instruction.getElementsByTagName("bf")[0].firstChild.nodeValue;
                	
                	if($("div[name='video_"+preemptiveLocalReferenceId+"']").length>0){
                		$("div[name='video_"+preemptiveLocalReferenceId+"']").data("notPaused", true);
                		$("div[name='video_"+preemptiveLocalReferenceId+"']").data("tid", timespot_id);
                		$("div[name='video_"+preemptiveLocalReferenceId+"']").play2();
                	}
                	setTimeout(function checkVidFinish(){
                		if($("div[name='video_"+preemptiveLocalReferenceId+"']").data("finished")){
                			executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                		}
                		else{
                			setTimeout(function(){
                  	    		new checkVidFinish();
                  	    	}, 300);
                			//setTimeout(arguments.callee, 300);
                		}
                	}, 300);
                	
                	
                	//if($("div[name='video_"+preemptiveLocalReferenceId+"']").length>0){
                	//	$("div[name='video_"+preemptiveLocalReferenceId+"']").find("video")[0].play();
                	//}
                	break;
                }
                case ADD_POLYGON:
                {
                    var preemptiveEntityId = instruction.getElementsByTagName("peid")[0].firstChild.nodeValue;
                    if(globals[preemptiveEntityId]!=null)
                        preemptiveEntityId = globals[preemptiveEntityId];
                    var polygonPoints = instruction.getElementsByTagName("pps")[0].firstChild.nodeValue;
                    var bar = polygonPoints.split("|");
                    var jsPoints = new Array();
                    for(var k = 0; k<bar.length; k++)
                    {
                        bar[k] = bar[k].split(",");
                        bar[k][0] = parseFloat(bar[k][0]+(full_screen_scale/2));
                        bar[k][1] = parseFloat(bar[k][1]+(full_screen_scale/2));
                        jsPoints.push(bar[k]);
                    }
                    var lineColour_Red = instruction.getElementsByTagName("lcR")[0].firstChild.nodeValue;
                    var lineColour_Green = instruction.getElementsByTagName("lcG")[0].firstChild.nodeValue;
                    var lineColour_Blue = instruction.getElementsByTagName("lcB")[0].firstChild.nodeValue;
                    var fillColour_Red = instruction.getElementsByTagName("fcR")[0].firstChild.nodeValue;
                    var fillColour_Green = instruction.getElementsByTagName("fcG")[0].firstChild.nodeValue;
                    var fillColour_Blue = instruction.getElementsByTagName("fcB")[0].firstChild.nodeValue;
                    var lineThickness = parseFloat(instruction.getElementsByTagName("lt")[0].firstChild.nodeValue);
                    var parentEntityId = instruction.getElementsByTagName("pid")[0].firstChild.nodeValue;
                    if(globals[parentEntityId]!=null)
                        parentEntityId = globals[parentEntityId];
                    var preemptiveLocalReferenceId = instruction.getElementsByTagName("plr")[0].firstChild.nodeValue;
                    if(globals[preemptiveLocalReferenceId]!=null){
                        preemptiveLocalReferenceId = globals[preemptiveLocalReferenceId];}
                    var x_float = instruction.getElementsByTagName("x")[0].firstChild.nodeValue;
                    
                    
                    if(floats[x_float]==null)
                    {
                        writeToErrorConsole("add polygon Ins Error: xRelativeToParent_FloatId is pointing to null, now put 0 into it");
                        floats[x_float] = 0;
                    }
                    var x = floats[x_float];
                    var y_float = instruction.getElementsByTagName("y")[0].firstChild.nodeValue;
                    if(floats[y_float]==null)
                    {
                        writeToErrorConsole("add polygon Ins Error: yRelativeToParent_FloatId is pointing to null, now put 0 into it");
                        floats[y_float] = 0;
                    }
                    var y = floats[y_float];
                    var radius_float = instruction.getElementsByTagName("rad")[0].firstChild.nodeValue;
                    if(floats[radius_float]==null)
                    {
                        writeToErrorConsole("add polygon Ins Error: radiusRelativeToParent_FloatId is pointing to null, now put 0 into it");
                        floats[radius_float] = 0;
                    }
                    var radius = floats[radius_float];
                    if($("div[id='image_"+parentEntityId+"']").length>0)
                    {
                        writeToConsole("Draw polygon to image parentEntityId: "+parentEntityId+", polygon preemptiveEntityId: "+preemptiveEntityId+" polygon preemptiveLocalReferenceId "+preemptiveLocalReferenceId);
                        $("div[id='image_"+parentEntityId+"']").addCanvas("group_"+preemptiveEntityId, {posx: x+($("#image_"+parentEntityId).data("original_width")/2)-(canvas_size/2), posy: y+($("#image_"+parentEntityId).data("original_height")/2)-(canvas_size/2), width: canvas_size, height: canvas_size, preemptiveLocalReferenceId: preemptiveLocalReferenceId});
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").css("visibility", "hidden");
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("original_width", canvas_size);
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("original_height", canvas_size);
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("width", canvas_size);
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("height", canvas_size);
                        var length = $("canvas[name='group_"+preemptiveLocalReferenceId+"']").length;
                        for(var k=0; k<length; k++)
                        {
                            var example = $("canvas[name='group_"+preemptiveLocalReferenceId+"']").get(k);
                            if(example.getContext)
                            {
                                var context = example.getContext('2d');
                                context.fillStyle = "rgb("+fillColour_Red+","+fillColour_Green+","+fillColour_Blue+")";
                                context.strokeStyle = "rgb("+lineColour_Red+","+lineColour_Green+","+lineColour_Blue+")";
                                context.lineWidth = lineThickness;
                                context.beginPath();
                                for(var m=0; m<jsPoints.length; m++)
                                {
                                    if(m!=0){context.lineTo(jsPoints[m][0], jsPoints[m][1]);}
                                    else{context.moveTo(jsPoints[m][0], jsPoints[m][1]);}
                                }
                                context.closePath();
                                context.stroke();
                                context.fill();
                                $(example).data("canvas", context.getImageData(0, 0, canvas_size, canvas_size));
                            }
                        }
                        var ratio = radius/canvas_size;
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").scale(ratio);
                        
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").css("visibility", "inherit");
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("scale", ratio);
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("width", ratio*canvas_size);
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("height", ratio*canvas_size);
                    }
                    else if($("div[id='group_"+parentEntityId+"']").length>0)
                    {
                        writeToConsole("Draw polygon to group parentEntityId: "+parentEntityId+", polygon preemptiveEntityId: "+preemptiveEntityId+" polygon preemptiveLocalReferenceId "+preemptiveLocalReferenceId);
                        $("div[id='group_"+parentEntityId+"']").addCanvas("group_"+preemptiveEntityId, {posx: x+($("#group_"+parentEntityId).data("original_width")/2)-(canvas_size/2), posy: y+($("#group_"+parentEntityId).data("original_height")/2)-(canvas_size/2), width: canvas_size, height: canvas_size, preemptiveLocalReferenceId: preemptiveLocalReferenceId});
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").css("visibility", "hidden");
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("original_width", canvas_size);
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("original_height", canvas_size);
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("width", canvas_size);
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("height", canvas_size);
                        var length = $("canvas[name='group_"+preemptiveLocalReferenceId+"']").length;
                        for(var k=0; k<length; k++)
                        {
                            var example = $("canvas[name='group_"+preemptiveLocalReferenceId+"']").get(k);
                            if(example.getContext)
                            {
                                var context = example.getContext('2d');
                                context.fillStyle = "rgb("+fillColour_Red+","+fillColour_Green+","+fillColour_Blue+")";
                                context.strokeStyle = "rgb("+lineColour_Red+","+lineColour_Green+","+lineColour_Blue+")";
                                context.lineWidth = lineThickness;
                                context.beginPath();
                                for(var m=0; m<jsPoints.length; m++)
                                {
                                    if(m!=0){
                                        context.lineTo(jsPoints[m][0], jsPoints[m][1]);}
                                    else{
                                        context.moveTo(jsPoints[m][0], jsPoints[m][1]);}
                                }
                                context.closePath();
                                context.stroke();
                                context.fill();
                                $(example).data("canvas", context.getImageData(0, 0, canvas_size, canvas_size));
                            }
                        }
                        var ratio = radius/canvas_size;
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").scale(ratio);
                        
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").css("visibility", "inherit");
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("scale", ratio);
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("width", ratio*canvas_size);
                        $("canvas[name='group_"+preemptiveLocalReferenceId+"']").data("height", ratio*canvas_size);
                    }
                    else{writeToErrorConsole("The entity with parentEntityId: "+parentEntityId+" that you wanted to draw a polygon to does not exist!");}
                    
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case ADD_TO_ENTITY:
                {
                    var childEntityId = instruction.getElementsByTagName("ceid")[0].firstChild.nodeValue;
                    if(globals[childEntityId]!=null)
                        childEntityId = globals[childEntityId];
                    var parentEntityId = instruction.getElementsByTagName("pid")[0].firstChild.nodeValue;
                    if(globals[parentEntityId]!=null)
                        parentEntityId = globals[parentEntityId];
                    var preemptiveLocalReferenceId = instruction.getElementsByTagName("plr")[0].firstChild.nodeValue;
                    if(globals[preemptiveLocalReferenceId]!=null)
                        preemptiveLocalReferenceId = globals[preemptiveLocalReferenceId];
                    var x = instruction.getElementsByTagName("x")[0].firstChild.nodeValue;
                    
                    x = parseFloat(x);
                    /*if(floats[x]!=null)
                    {
                        x = floats[x];
                    }
                    else{
                    	x = parseFloat(x);
                    }*/
                    
                    var y = instruction.getElementsByTagName("y")[0].firstChild.nodeValue;
                    y = parseFloat(y);
                    /*if(floats[y]!=null)
                    {
                        y = floats[y];
                    }
                    else{
                    	y = parseFloat(y);
                    }*/
                    
                    var radius = instruction.getElementsByTagName("rad")[0].firstChild.nodeValue;
                    radius = parseFloat(radius);
                    /*if(floats[radius]!=null)
                    {
                        radius = floats[radius];
                    }
                    else{
                    	radius = parseFloat(radius);
                    }*/
                    if(!stop)
                    {
                        if($("div[id='group_"+childEntityId+"']").length>0)
                        {
                            if($("div[id='group_"+parentEntityId+"']").length>0)
                            {
                                writeToConsole("add to entity called, clone group childEntityId "+childEntityId+" to parent group parentEntityId: "+parentEntityId);
                                var clonedElement_deep = jQuery.extend(true, {}, $("#group_"+childEntityId));
                                var clonedElement = clonedElement_deep.clone(true);
                                clonedElement.attr("name", "group_"+preemptiveLocalReferenceId);
                                clonedElement.css("left", x+($("#group_"+parentEntityId).data("original_width")/2)-($("#group_"+childEntityId).data("original_width")/2));
                                clonedElement.css("top", y+($("#group_"+parentEntityId).data("original_height")/2)-($("#group_"+childEntityId).data("original_height")/2));
                                if(!stop)
                                {
                                    $("div[id='group_"+parentEntityId+"']").append(clonedElement);
                                    $("div[id='group_"+parentEntityId+"']").pushStack(clonedElement);
                                    if(clonedElement.find("canvas").length>0)
                                    {
                                        var foundCanvas = clonedElement.find("canvas");
                                        var length = foundCanvas.length;
                                        for(var k=0; k<length; k++)
                                        {
                                            if($(foundCanvas.get(k)).data("canvas")!='undefined' || $(foundCanvas.get(k)).data("canvas")!=null){
                                                foundCanvas.get(k).getContext("2d").putImageData($(foundCanvas.get(k)).data("canvas"), 0, 0);}
                                            else{
                                                writeToErrorConsole('add to entity: error copying canvas when adding cloned element to parent group');}
                                        }
                                    }
                                    var ratio = radius/full_screen_scale;
//                                    var ratio = (radius/full_screen_scale)*(Math.min(PLAYGROUND_WIDTH,PLAYGROUND_HEIGHT)/full_screen_scale);
                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").scale(ratio);
                                    
                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").css("visibility", "inherit");
                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").data("scale", ratio);
                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").data("width", ratio*full_screen_scale);
                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").data("height", ratio*full_screen_scale);
                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").data("original_width", full_screen_scale);
                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").data("original_height", full_screen_scale);
                                }
                                else
                                    writeToConsole("Stopping ADD_TO_ENTITY: ");
                            }
                            else{writeToErrorConsole('Add to entity error: The parent group that you want a group to be added to does not exist!');}
                        }
//                        add image(s) to group(s)
                        else if($("div[id='image_"+childEntityId+"']").length>0)
                        {
                            if($("div[id='group_"+parentEntityId+"']").length>0)
                            {
                                writeToConsole("add to entity called, clone image entityId: "+childEntityId+" to parent group parentEntityId: "+parentEntityId);
                                var clonedElement_deep = jQuery.extend(true, {}, $("#image_"+childEntityId));
                                var clonedElement = clonedElement_deep.clone(true);
                                clonedElement.attr("name", "image_"+preemptiveLocalReferenceId);
                                clonedElement.css("left", x+($("#group_"+parentEntityId).data("original_width")/2)-($("#image_"+childEntityId).data("original_width")/2));
                                clonedElement.css("top", y+($("#group_"+parentEntityId).data("original_height")/2)-($("#image_"+childEntityId).data("original_height")/2));
                                if(!stop)
                                {
                                    $("div[id='group_"+parentEntityId+"']").append(clonedElement);
                                    $("div[id='group_"+parentEntityId+"']").pushStack(clonedElement);
                                    if(clonedElement.find("canvas").length>0)
                                    {
                                        var foundCanvas = clonedElement.find("canvas");
                                        var length = foundCanvas.length;
                                        for(var k=0; k<length; k++)
                                        {
                                            if($(foundCanvas.get(k)).data("canvas")!='undefined' || $(foundCanvas.get(k)).data("canvas")!=null){
                                                foundCanvas.get(k).getContext("2d").putImageData($(foundCanvas.get(k)).data("canvas"), 0, 0);}
                                            else{
                                                writeToErrorConsole('add to entity: error copying canvas when adding cloned element to parent group');}
                                        }
                                    }
                                    var ratio = radius/Math.max($("#image_"+childEntityId).data("original_width"),$("#image_"+childEntityId).data("original_height"));
                                    $("div[name='image_"+preemptiveLocalReferenceId+"']").scale(ratio);
                                    
                                    $("div[name='image_"+preemptiveLocalReferenceId+"']").css("visibility", "inherit");
                                    $("div[name='image_"+preemptiveLocalReferenceId+"']").data("scale", ratio);
                                    $("div[name='image_"+preemptiveLocalReferenceId+"']").data("width", $("#image_"+childEntityId).data("original_width")*ratio);
                                    $("div[name='image_"+preemptiveLocalReferenceId+"']").data("height", $("#image_"+childEntityId).data("original_height")*ratio);
                                    $("div[name='image_"+preemptiveLocalReferenceId+"']").data("original_width", $("#image_"+childEntityId).data("original_width"));
                                    $("div[name='image_"+preemptiveLocalReferenceId+"']").data("original_height", $("#image_"+childEntityId).data("original_height"));
                                }
                                else
                                    writeToConsole("Stopping ADD_TO_ENTITY: ");
                            }
                            else{writeToErrorConsole('Add to entity error: The parent group that you want an image to be added to does not exist!');}
                        }
                        else{writeToErrorConsole('Add to entity error: The image/group that you wanted to add to a group does not exist!');}
                    }
                    else
                        writeToConsole("Stopping ADD_TO_ENTITY: ");
                        
                        
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case BRING_TO_FRONT:
                {
                    var localReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                    if(globals[localReferenceId]!=null)
                    {
                        localReferenceId = globals[localReferenceId];
                        writeToConsole("Bring to front: put global id "+localReferenceId+" into localReferenceId");
                    }
                    
                    setTimeout(function checkElementExist3(){
                    	if($("div[name='image_"+localReferenceId+"']").length>0)
	                    {
	                    	if($("div[name='image_"+localReferenceId+"']").data("image")!=""){
	                    	//if($("div[name='image_"+localReferenceId+"']").data("image").image.complete){
                    			writeToConsole("Bring image "+localReferenceId+" to front");
		                        $("div[name='image_"+localReferenceId+"']").css("z-index", bringtofront_index++);
		                        executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	                    	}
	                    	else{
                    			if(!stop){
                    				setTimeout(function(){
              	            		    new checkElementExist3();
              	            	    }, 300);
		                      	    //setTimeout(arguments.callee, 300);
	                      	    }
                    		}
	                    }
	                    else if($("div[name='group_"+localReferenceId+"']").length>0)
	                    {
	                    	if($("div[name='group_"+localReferenceId+"']").data("awaiting_imgs")!=null){
	                    	    if(parseInt($("div[name='group_"+localReferenceId+"']").data("awaiting_imgs"))>0){
	                    	        if(!stop){
	                    	        	setTimeout(function(){
                  	            		    new checkElementExist3();
                  	            	    }, 300);
			                      	    //setTimeout(arguments.callee, 300);
		                      	    }
                    	    	}
                    	    	else{
                    	    		writeToConsole("Bring group "+localReferenceId+" to front");
			                        $("div[name='group_"+localReferenceId+"']").css("z-index", bringtofront_index++);
			                        executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    	    	}
                    		}
	                    }
	                    else if($("div[name='video_"+localReferenceId+"']").length>0)
	                    {
	                        writeToConsole("Bring video "+localReferenceId+" to front");
	                        $("div[name='video_"+localReferenceId+"']").css("z-index", bringtofront_index++);
	                        executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	                    }
	                    else{
	                    	if(!stop){
	                    		setTimeout(function(){
          	            		    new checkElementExist3();
          	            	    }, 300);
	                    		//setTimeout(arguments.callee, 300);
	                    	}
	                    	writeToErrorConsole("The group/image/video that you want to bring to the front does not exist");
                    	}
	                    
                    }, 0);
                    
                    break;
                }
                case CENTRE_CAMERA:
                {
                    var entityId = instruction.getElementsByTagName("eid")[0].firstChild.nodeValue;
                    if(globals[entityId]!=null)
                        entityId = globals[entityId];
                    var localReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                    if(globals[localReferenceId]!=null)
                    {
                        localReferenceId = globals[localReferenceId];
                        writeToConsole("Centre camera: put global id "+localReferenceId+" into localReferenceId");
                    }
                    var radius_float = instruction.getElementsByTagName("rad")[0].firstChild.nodeValue;
                    if(floats[radius_float]==null)
                    {
                        writeToErrorConsole("centre camera Ins Error: radiusRelativeToLocalReference_FloatId is pointing to null, now put 0 into it");
                        floats[radius_float] = 0;
                    }
                    var radius = floats[radius_float];
                    var time_float = instruction.getElementsByTagName("sec")[0].firstChild.nodeValue;
                    if(floats[time_float]==null)
                    {
                        writeToErrorConsole("centre camera Ins Error: secondsToCompleteMigration_FloatId is pointing to null, now put 0 into it");
                        floats[time_float] = 0;
                    }
                    var time_allowed = floats[time_float]*1000;
                    if($("div[id='image_"+entityId+"']").length>0)
                    {}
                    else if($("div[id='group_"+entityId+"']").length>0)
                    {
                        var start = new Date().getTime();
                        $("div[id='group_"+entityId+"']").each(function()
                        {
                            var current_element = $(this);
                            if(current_element.css("visibility")=="hidden"){
                                return;}
                            var imageChilds = current_element.find("div[name='image_"+localReferenceId+"']");
                            var groupChilds = current_element.find("div[name='group_"+localReferenceId+"']");
                            if(imageChilds.length>1 || groupChilds.length>1)
                            {
                                if(debug_mode==true){
                                    alert("Centre camera error: localRef "+localReferenceId+" is repeated in one or more instances of entityId "+entityId);}
                                return;
                            }
                            else if(imageChilds.length==0 && groupChilds.length==0)
                            {
                                if(debug_mode==true){
                                    alert("Centre camera error: localRef "+localReferenceId+" does not exist in one or more instances of entityId "+entityId);}
                                return;
                            }
                            else
                            {
                                current_element.data("moving", parentTimespot.getId());
                                var base_ratio=current_element.data("scale");
                                var x_orig = parseFloat(current_element.offset().left);
                                var y_orig = parseFloat(current_element.offset().top);
//                                ratio = 1;
                                var x = 0;
                                var y = 0;
                                var x2 = 0;
                                var y2 = 0;
                                if(imageChilds.length==1){
                                    writeToConsole('Centre camera: move group entityId '+entityId+" to image localReferenceId "+localReferenceId+" radius: "+radius);}
                                else if(groupChilds.length==1)
                                {
                                    writeToConsole('Centre camera: move group entityId '+entityId+" to group localReferenceId "+localReferenceId+" radius: "+radius);
                                    var groupChildsTemp = groupChilds;
                                    var x_orig_child = parseFloat(groupChilds.offset().left);
                                    var y_orig_child = parseFloat(groupChilds.offset().top);
                                    var ratio = radius/(groupChilds.data("width"));
                                    var ratio2 = 1;
                                    var cond = true;
                                    while(groupChildsTemp.attr("id")!="sceengraph")
                                    {
                                        if(groupChildsTemp.attr("id")==current_element.attr("id")){
                                            cond = false;}
                                        if(cond==true){
                                            ratio = ratio/groupChildsTemp.data("scale");}
                                        ratio2 = ratio2*groupChildsTemp.data("scale");
                                        groupChildsTemp = groupChildsTemp.parent();
                                    }
                                    if(ratio2*groupChilds.data("original_width")>Math.min(PLAYGROUND_WIDTH, PLAYGROUND_HEIGHT))
                                    {
                                        x = ((ratio2*groupChilds.data("original_width"))-PLAYGROUND_WIDTH)/2;
                                        y = ((ratio2*groupChilds.data("original_height"))-PLAYGROUND_HEIGHT)/2;
                                    }
                                    else
                                    {
                                        x = (PLAYGROUND_WIDTH-(ratio2*groupChilds.data("original_width")))/2;
                                        y = (PLAYGROUND_HEIGHT-(ratio2*groupChilds.data("original_height")))/2;
                                    }
                                    x_dist = (x-x_orig_child)*(1/ratio2);
                                    y_dist = (y-y_orig_child)*(1/ratio2);

                                    x_orig = parseFloat(current_element.css("left"));
                                    y_orig = parseFloat(current_element.css("top"));
                                }
                                var timerTemp = setInterval(function()
                                {
                                    var elapsed = new Date().getTime()-start;
                                    if(elapsed<time_allowed)
                                    {
//                                        current_element.offset({top: y_orig+((elapsed/time_allowed)*y_dist), left: x_orig+((elapsed/time_allowed)*x_dist)});
                                        current_element.css("left", x_orig+((elapsed/time_allowed)*x_dist));
                                        current_element.css("top", y_orig+((elapsed/time_allowed)*y_dist));
                                        
                                        
                                        if(ratio!=base_ratio)
                                        {
//                                            if(Math.abs(ratio-base_ratio)>0.1){
	                                        current_element.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
                                            
                                            current_element.data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
                                            current_element.data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*current_element.data("original_width"));
                                            current_element.data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*current_element.data("original_height"));
                                        }
                                        if(stop)
                                        {
                                            clearInterval(timerTemp);
                                            return;
                                        }
                                        if(current_element.data("moving")!=parentTimespot.getId())
                                        {
                                            clearInterval(timerTemp);
                                            return;
                                        }
                                    }
                                    else
                                    {
//                                        current_element.offset({top: y_orig+y_dist, left: x_orig+x_dist});
                                        current_element.css("left", x_orig+x_dist);
                                        current_element.css("top", y_orig+y_dist);
                                        
                                        
                                        if(ratio!=base_ratio)
                                        {
                                        	current_element.scale(ratio);
                                            
                                            current_element.data("scale", ratio);
                                            current_element.data("width", ratio*current_element.data("original_width"));
                                            current_element.data("height", ratio*current_element.data("original_height"));
                                        }
                                        clearInterval(timerTemp);
                                    }
                                }, refresh_rate);
                            }
                        });
                    }
                    else
                    {
                        if(debug_mode==true){
                            alert("Centre camera error: entityId "+entityId+" does not exist");}
                    }
                    
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case COMPILE_AND_ADD_ENTITY:
                {
                	writeToConsole("compile_and_add_entity");
                	var isCanvas = instruction.getElementsByTagName("ic")[0].firstChild.nodeValue;
                    var preemptiveEntityId = instruction.getElementsByTagName("peid")[0].firstChild.nodeValue;
                    if(globals[preemptiveEntityId]!=null)
                        preemptiveEntityId = globals[preemptiveEntityId];
                    var parentEntityId = instruction.getElementsByTagName("pid")[0].firstChild.nodeValue;
                    if(globals[parentEntityId]!=null)
                        parentEntityId = globals[parentEntityId];
                    var preemptiveLocalReferenceId = instruction.getElementsByTagName("plr")[0].firstChild.nodeValue;
                    if(globals[preemptiveLocalReferenceId]!=null)
                    {
                        preemptiveLocalReferenceId = globals[preemptiveLocalReferenceId];
                        writeToConsole("Compile and add: put global id "+preemptiveLocalReferenceId+" into preemptiveLocalReferenceId");
                    }
                    
                    if(parentEntityId=="0"){
                    	parentEntityId = "00";
                    	$("div[name='group_0']").addGroup("group_00", {width: full_screen_scale, height: full_screen_scale, preemptiveLocalReferenceId: "00"});
                    	
                    	var addedGroup = $("div[name='group_00']");
		                                
                    	addedGroup.css("left", 0);
                        addedGroup.css("top", 0);
                                                
                        addedGroup.css("clip", "");
                        addedGroup.data("awaiting_imgs", 0);
                        addedGroup.data("original_width",full_screen_scale);
                        addedGroup.data("original_height",full_screen_scale);
                        addedGroup.data("width",full_screen_scale);
                        addedGroup.data("height",full_screen_scale);
                        var ratio = 1;
                        addedGroup.scale(ratio);
                        
                        addedGroup.css("visibility", "inherit");
                        addedGroup.data("scale", ratio);
                        addedGroup.data("width",ratio*full_screen_scale);
                        addedGroup.data("height",ratio*full_screen_scale);
                        
                        if(checkIfNull(camera)){
                        	camera = new globalCamera();
                        }
                    }
                    
                    var x = instruction.getElementsByTagName("x")[0].firstChild.nodeValue;
                    x = parseFloat(x);
                    /*if(floats[x]!=null)
                    {
                        x = floats[x];
                    }
                    else{
                    	x = parseFloat(x);
                    }*/
                    
                    var y = instruction.getElementsByTagName("y")[0].firstChild.nodeValue;
                    y = parseFloat(y);
                    /*if(floats[y]!=null)
                    {
                        y = floats[y];
                    }
                    else{
                    	y = parseFloat(y);
                    }*/
                    
                    var radius = instruction.getElementsByTagName("rad")[0].firstChild.nodeValue;
                    radius = parseFloat(radius);
                    /*if(floats[radius]!=null)
                    {
                        radius = floats[radius];
                    }
                    else{
                    	radius = parseFloat(radius);
                    }*/
                    
                    var parent = $("div[id='group_"+parentEntityId+"']");
                    
                    if(!stop)
                    {
                        if(parent.length>0)
                        {
                            writeToConsole("Compile and add entity to group parentEntityId: "+parentEntityId+" called, create group with entity id: "+preemptiveEntityId+" local ref: "+preemptiveLocalReferenceId);
                            
                            //if(isCanvas=="0"){
	                            if(parent.find("div[name='group_"+preemptiveLocalReferenceId+"']").length==0){
		                            if($("div[id='group_"+preemptiveEntityId+"']").length>0)
		                            {
		                                var clonedElement_deep = jQuery.extend(true, {}, $("#group_"+preemptiveEntityId));
		                                var clonedElement = clonedElement_deep.clone(true);
		                                clonedElement.attr("name", "group_"+preemptiveLocalReferenceId);
		                                clonedElement.css("left", x+($("#group_"+parentEntityId).data("original_width")/2)-(full_screen_scale/2));
		                                clonedElement.css("top", y+($("#group_"+parentEntityId).data("original_height")/2)-(full_screen_scale/2));
		                                if(!stop)
		                                {
		                                    $("div[id='group_"+parentEntityId+"']").append(clonedElement);
		                                    $("div[id='group_"+parentEntityId+"']").pushStack(clonedElement);
		                                    if(clonedElement.find("canvas").length>0)
		                                    {
		                                        var foundCanvas = clonedElement.find("canvas");
		                                        var length = foundCanvas.length;
		                                        for(var k=0; k<length; k++)
		                                        {
		                                            if($(foundCanvas.get(k)).data("canvas")!='undefined' || $(foundCanvas.get(k)).data("canvas")!=null){
		                                                foundCanvas.get(k).getContext("2d").putImageData($(foundCanvas.get(k)).data("canvas"), 0, 0);}
		                                            else{
		                                                writeToErrorConsole("compile and add: error copying canvas when adding cloned element to parent group");}
		                                        }
		                                    }
		                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").data("original_width", full_screen_scale);
		                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").data("original_height", full_screen_scale);
		                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").data("width", full_screen_scale);
		                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").data("height", full_screen_scale);
		                                    var ratio = radius/full_screen_scale;
		                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").scale(ratio);
		                                    
		                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").css("visibility", "inherit");
		                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").data("scale", ratio);
		                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").data("width", full_screen_scale*ratio);
		                                    $("div[name='group_"+preemptiveLocalReferenceId+"']").data("height", full_screen_scale*ratio);
		                                }
		                                else
		                                    writeToConsole("Stopping COMPILE_AND_ADD_ENTITY: ");
		                            }
		                            else
		                            {
		                                parent.addGroup("group_"+preemptiveEntityId, {width: full_screen_scale, height: full_screen_scale, preemptiveLocalReferenceId: preemptiveLocalReferenceId});
		                                
		                                var addedGroup = $("div[name='group_"+preemptiveLocalReferenceId+"']");
		                                
		                                addedGroup.css("left", x+($("#group_"+parentEntityId).data("original_width")/2)-(full_screen_scale/2));
		                                addedGroup.css("top", y+($("#group_"+parentEntityId).data("original_height")/2)-(full_screen_scale/2));
		                                
		                                
		                                
		                                
		                                
		                                addedGroup.data("awaiting_imgs", 0);
		                                addedGroup.data("original_width",full_screen_scale);
		                                addedGroup.data("original_height",full_screen_scale);
		                                addedGroup.data("width",full_screen_scale);
		                                addedGroup.data("height",full_screen_scale);
		                                var ratio = radius/full_screen_scale;
		                                addedGroup.scale(ratio);
		                                
		                                addedGroup.css("visibility", "inherit");
		                                addedGroup.data("scale", ratio);
		                                addedGroup.data("width",ratio*full_screen_scale);
		                                addedGroup.data("height",ratio*full_screen_scale);
		                            }
	                            }
                            //}
                            /*else if(isCanvas=="1"){
                            	$("div[id='group_"+parentEntityId+"']").addCanvas("group_"+preemptiveEntityId, {width: full_screen_scale, height: full_screen_scale, preemptiveLocalReferenceId: preemptiveLocalReferenceId});
		                                
                                $("div[name='group_"+preemptiveLocalReferenceId+"']").css("left", x+($("#group_"+parentEntityId).data("original_width")/2)-(full_screen_scale/2));
                                $("div[name='group_"+preemptiveLocalReferenceId+"']").css("top", y+($("#group_"+parentEntityId).data("original_height")/2)-(full_screen_scale/2));
                                
                                $("div[name='group_"+preemptiveLocalReferenceId+"']").data("awaiting_imgs", 0);
                                $("div[name='group_"+preemptiveLocalReferenceId+"']").data("original_width",full_screen_scale);
                                $("div[name='group_"+preemptiveLocalReferenceId+"']").data("original_height",full_screen_scale);
                                $("div[name='group_"+preemptiveLocalReferenceId+"']").data("width",full_screen_scale);
                                $("div[name='group_"+preemptiveLocalReferenceId+"']").data("height",full_screen_scale);
                                var ratio = radius/full_screen_scale;
                                $("div[name='group_"+preemptiveLocalReferenceId+"']").scale(ratio);
                                
                                $("div[name='group_"+preemptiveLocalReferenceId+"']").css("visibility", "inherit");
                                $("div[name='group_"+preemptiveLocalReferenceId+"']").data("scale", ratio);
                                $("div[name='group_"+preemptiveLocalReferenceId+"']").data("width",ratio*full_screen_scale);
                                $("div[name='group_"+preemptiveLocalReferenceId+"']").data("height",ratio*full_screen_scale);
                            }*/
                        }
                        else{writeToErrorConsole('Compile and add entity error: Parent group does not exist');}
                    }
                    else
                        writeToConsole("Stopping COMPILE_AND_ADD_ENTITY: ");
                        
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case COMPILE_ENTITY:
                {
                    var preemptiveEntityId = instruction.getElementsByTagName("peid")[0].firstChild.nodeValue;
                    if(globals[preemptiveEntityId]!=null)
                        preemptiveEntityId = globals[preemptiveEntityId];
                    writeToConsole("Caching entity with EntityId: "+preemptiveEntityId);
                    $.playground().addGroup("group_"+preemptiveEntityId, {width: full_screen_scale, height: full_screen_scale});
                    
                    
                    $("div[id='group_"+preemptiveEntityId+"']").data("awaiting_imgs", 0);
                    $("div[id='group_"+preemptiveEntityId+"']").scale(0);
                    
                    $("div[id='group_"+preemptiveEntityId+"']").data("scale", 0);
                    $("div[id='group_"+preemptiveEntityId+"']").data("original_width", full_screen_scale);
                    $("div[id='group_"+preemptiveEntityId+"']").data("original_height", full_screen_scale);
                    $("div[id='group_"+preemptiveEntityId+"']").data("width",0);
                    $("div[id='group_"+preemptiveEntityId+"']").data("height",0);
                    
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case EXECUTE_GLOBAL:
                {
                    var localReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                    var globals_localReferenceId = globals[localReferenceId];
                    if(globals_localReferenceId!=null)
                    {
                    	function checkGid(){
		                    if(!stop){
	                        	//new executeTimespot(tid, parentTimespot);
	                        	
	                        	if(timespots[globals_localReferenceId]==null || typeof(timespots[globals_localReferenceId])=="undefined"){
	                        		setTimeout(function(){
	                        			new checkGid();
	                        		}, 300);
	                        	}
	                        	else{
	                        		new executeTimespot(globals_localReferenceId, parentTimespot);
	                        		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	                        		
	                        	    //setTimeout(function(){new executeTimespot(globals_localReferenceId, parentTimespot);}, 0);
	                        	    //executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                        	    }
	                        }
	                        else
	                            writeToConsole("Stopping EXECUTE_TIMESPOT");
                            }
                    }
                    checkGid();
                    
                    break;
                }
                case EXECUTE_TIMESPOT:
                {
                    var tid = instruction.getElementsByTagName("etid")[0].firstChild.nodeValue;
                    
                    
                    function checkTid(){
	                    if(!stop){
                        	//new executeTimespot(tid, parentTimespot);
                        	
                        	if(timespots[tid]==null || typeof(timespots[tid])=="undefined"){
                        		setTimeout(function(){
                        			new checkTid();
                        		}, 300);
                        	}
                        	else{
                        		new executeTimespot(tid, parentTimespot);
                        		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                        	    //setTimeout(function(){new executeTimespot(tid, parentTimespot);}, 0);
                        	    //executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    	    }
                        }
                        else
                            writeToConsole("Stopping EXECUTE_TIMESPOT");
	                    
                    }
                    checkTid();
                    break;
                }
                case FLOAT_MINUS:
                {
                    var assignmentToValue_FloatId = instruction.getElementsByTagName("av")[0].firstChild.nodeValue;
                    var firstTerm_FloatId = instruction.getElementsByTagName("ft")[0].firstChild.nodeValue;
                    var minusSecondTerm_FloatId = instruction.getElementsByTagName("st")[0].firstChild.nodeValue;
                    if(floats[firstTerm_FloatId]==null)
                    {
                        writeToErrorConsole("float minus Ins Error: firstTerm_FloatId is pointing to null, now put 0 into it");
                        floats[firstTerm_FloatId] = 0;
                    }
                    if(floats[minusSecondTerm_FloatId]==null)
                    {
                        writeToErrorConsole("float minus Ins Error: minusSecondTerm_FloatId is pointing to null, now put 0 into it");
                        floats[minusSecondTerm_FloatId] = 0;
                    }
                    writeToConsole('Float minus instruction executed taking away '+floats[plusSecondTerm_FloatId]+ " from "+ floats[firstTerm_FloatId]+ " assigning to " + assignmentToValue_FloatId);
                    floats[assignmentToValue_FloatId] = floats[firstTerm_FloatId]-floats[minusSecondTerm_FloatId];
                    
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case FLOAT_PLUS:
                {
                    var assignmentToValue_FloatId = instruction.getElementsByTagName("av")[0].firstChild.nodeValue;
                    var firstTerm_FloatId = instruction.getElementsByTagName("ft")[0].firstChild.nodeValue;
                    var plusSecondTerm_FloatId = instruction.getElementsByTagName("st")[0].firstChild.nodeValue;
                    if(floats[firstTerm_FloatId]==null)
                    {
                        writeToErrorConsole("float plus Ins Error: firstTerm_FloatId is pointing to null, now put 0 into it");
                        floats[firstTerm_FloatId] = 0;
                    }
                    if(floats[plusSecondTerm_FloatId]==null)
                    {
                        writeToErrorConsole("float plus Ins Error: plusSecondTerm_FloatId is pointing to null, now put 0 into it");
                        floats[plusSecondTerm_FloatId] = 0;
                    }
                    writeToConsole('Float plus instruction executed adding '+floats[plusSecondTerm_FloatId]+ " to "+ floats[firstTerm_FloatId]+ " assigning to " + assignmentToValue_FloatId);
                    floats[assignmentToValue_FloatId] = floats[firstTerm_FloatId]+floats[plusSecondTerm_FloatId];
                    
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case IF_EQUAL_TO:
                {
                    var leftValue_FloatId = instruction.getElementsByTagName("lv")[0].firstChild.nodeValue;
                    var rightValue_FloatId = instruction.getElementsByTagName("rv")[0].firstChild.nodeValue;
                    var timespotId_ToExecuteIfTrue = instruction.getElementsByTagName("t")[0].firstChild.nodeValue;
                    var timespotId_ToExecuteIfFalse = instruction.getElementsByTagName("f")[0].firstChild.nodeValue;
                    var tid;
                    if(floats[leftValue_FloatId]==null)
                    {
                        writeToErrorConsole("if equal to Ins Error: leftValue_FloatId is pointing to null, now put 0 into it");
                        floats[leftValue_FloatId] = 0;
                    }
                    if(floats[rightValue_FloatId]==null)
                    {
                        writeToErrorConsole("if equal to Ins Error: rightValue_FloatId is pointing to null, now put 0 into it");
                        floats[rightValue_FloatId] = 0;
                    }
                    writeToConsole('If equal to instruction executed left value in ' + leftValue_FloatId+ ' = '+ floats[leftValue_FloatId] + " right value = "+ floats[rightValue_FloatId]);
                    
                    
                    if(floats[leftValue_FloatId]==floats[rightValue_FloatId]){
                        tid = timespotId_ToExecuteIfTrue;
                    }
                    else{
                        tid = timespotId_ToExecuteIfFalse;
                    }
                    if(tid!="0"){
                    	//new executeTimespot(tid, parentTimespot);
                    	//setTimeout(function(){new executeTimespot(tid, parentTimespot);}, 0);
                    	new executeTimespot(tid, parentTimespot);
                	}
                	
                        
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case IF_GREATER_THAN:
                {
                    var leftValue_FloatId = instruction.getElementsByTagName("lv")[0].firstChild.nodeValue;
                    var rightValue_FloatId = instruction.getElementsByTagName("rv")[0].firstChild.nodeValue;
                    var timespotId_ToExecuteIfTrue = instruction.getElementsByTagName("t")[0].firstChild.nodeValue;
                    var timespotId_ToExecuteIfFalse = instruction.getElementsByTagName("f")[0].firstChild.nodeValue;
                    var tid;
                    if(floats[leftValue_FloatId]==null)
                    {
                        writeToErrorConsole("if greater than Ins Error: leftValue_FloatId is pointing to null, now put 0 into it");
                        floats[leftValue_FloatId] = 0;
                    }
                    if(floats[rightValue_FloatId]==null)
                    {
                        writeToErrorConsole("if greater than Ins Error: rightValue_FloatId is pointing to null, now put 0 into it");
                        floats[rightValue_FloatId] = 0;
                    }
                    writeToConsole('If greater than instruction executed left value = '+ floats[leftValue_FloatId] + " right value = "+ floats[rightValue_FloatId]);
                    if(floats[leftValue_FloatId]>floats[rightValue_FloatId]){
                        tid = timespotId_ToExecuteIfTrue;
                    }
                    else{
                        tid = timespotId_ToExecuteIfFalse;}
                    if(tid!="0"){
                        //new executeTimespot(tid, parentTimespot);
                        //setTimeout(function(){new executeTimespot(tid, parentTimespot);}, 0);
                        new executeTimespot(tid, parentTimespot);
                    }
                        
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case IF_LESS_THAN:
                {
                    var leftValue_FloatId = instruction.getElementsByTagName("lv")[0].firstChild.nodeValue;
                    var rightValue_FloatId = instruction.getElementsByTagName("rv")[0].firstChild.nodeValue;
                    var timespotId_ToExecuteIfTrue = instruction.getElementsByTagName("t")[0].firstChild.nodeValue;
                    var timespotId_ToExecuteIfFalse = instruction.getElementsByTagName("f")[0].firstChild.nodeValue;
                    var tid;
                    if(floats[leftValue_FloatId]==null)
                    {
                        writeToErrorConsole("if less than Ins Error: leftValue_FloatId is pointing to null, now put 0 into it");
                        floats[leftValue_FloatId] = 0;
                    }
                    if(floats[rightValue_FloatId]==null)
                    {
                        writeToErrorConsole("if less than Ins Error: rightValue_FloatId is pointing to null, now put 0 into it");
                        floats[rightValue_FloatId] = 0;
                    }
                    writeToConsole('If less than instruction executed left value = '+ floats[leftValue_FloatId] + " right value = "+ floats[rightValue_FloatId]);
                    if(floats[leftValue_FloatId]<floats[rightValue_FloatId]){
                        tid = timespotId_ToExecuteIfTrue;}
                    else{
                        tid = timespotId_ToExecuteIfFalse;}
                    if(tid!="0"){
                        //new executeTimespot(tid, parentTimespot);
                        //setTimeout(function(){new executeTimespot(tid, parentTimespot);}, 0);
                        new executeTimespot(tid, parentTimespot);
                    }
                        
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case MOVE_LOCAL_REFERENCE:
                {
                    var preemptiveLocalReferenceId = instruction.getElementsByTagName("plr")[0].firstChild.nodeValue;
                    if(globals[preemptiveLocalReferenceId]!=null)
                    {
                        preemptiveLocalReferenceId = globals[preemptiveLocalReferenceId];
                        writeToConsole("Move local ref: put global id "+preemptiveLocalReferenceId+" into preemptiveLocalReferenceId");
                    }
                    //writeToErrorConsole("Move local ref: "+preemptiveLocalReferenceId);
                    var x = instruction.getElementsByTagName("x")[0].firstChild.nodeValue;
                    x = parseFloat(x);
                    /*if(floats[x]!=null)
                    {
                        x = floats[x];
                    }
                    else{
                    	x = parseFloat(x);
                    }*/
                    
                    var y = instruction.getElementsByTagName("y")[0].firstChild.nodeValue;
                    y = parseFloat(y);
                    /*if(floats[y]!=null)
                    {
                        y = floats[y];
                    }
                    else{
                    	y = parseFloat(y);
                    }*/
                    
                    var radius = instruction.getElementsByTagName("rad")[0].firstChild.nodeValue;
                    radius = parseFloat(radius);
                    /*if(floats[radius]!=null)
                    {
                        radius = floats[radius];
                    }
                    else{
                    	radius = parseFloat(radius);
                    }*/
                    
                    var time = instruction.getElementsByTagName("sec")[0].firstChild.nodeValue;
                    time = parseFloat(time);
                    
                    var skip = false;
                    if(instruction.getElementsByTagName("sk").length>0){
                    	if(instruction.getElementsByTagName("sk")[0].firstChild.nodeValue=="1"){
                    		skip = true;
                    	}
                    	else{
                    		skip = false;
                    	}
                    }
                    
                    /*if(floats[time]!=null)
                    {
                        time = floats[time];
                    }
                    else{
                    	time = parseFloat(time);
                    }*/
                    var time_allowed = time*1000;
                    writeToConsole('move ref in '+ time_allowed + "s " + preemptiveLocalReferenceId + " x: " + x + " y: " + y + " radius: " + radius);
                    
                    
                    
                    setTimeout(function checkElementExist4(){
                    	if($("div[name='image_"+preemptiveLocalReferenceId+"']").length>0)
	                    {
	                    	if($("div[name='image_"+preemptiveLocalReferenceId+"']").data("image")!=""){
	                    	//if($("div[name='image_"+preemptiveLocalReferenceId+"']").data("image").image.complete){
	                    		$("div[name='image_"+preemptiveLocalReferenceId+"']").data("moving", parentTimespot.getId());
		                        var ratio=radius/Math.max($("div[name='image_"+preemptiveLocalReferenceId+"']").data("original_width"), $("div[name='image_"+preemptiveLocalReferenceId+"']").data("original_height"));
		                        //var base_ratio=$("div[name='image_"+preemptiveLocalReferenceId+"']").data("width")/$("div[name='image_"+preemptiveLocalReferenceId+"']").data("original_width");
		                          var base_ratio=$("div[name='image_"+preemptiveLocalReferenceId+"']").data("scale");
		                          
		                          //$("div[name='image_"+preemptiveLocalReferenceId+"']").offset({ top:$("div[name='image_"+preemptiveLocalReferenceId+"']").offset().top, left:$("div[name='image_"+preemptiveLocalReferenceId+"']").offset().left });
		                          var x_orig = parseFloat($("div[name='image_"+preemptiveLocalReferenceId+"']").css("left"));
		                          var y_orig = parseFloat($("div[name='image_"+preemptiveLocalReferenceId+"']").css("top"));
		                          var x_dist = x+($("div[name='image_"+preemptiveLocalReferenceId+"']").parent().data("original_width")==null?PLAYGROUND_WIDTH/2:$("div[name='image_"+preemptiveLocalReferenceId+"']").parent().data("original_width")/2)-($("div[name='image_"+preemptiveLocalReferenceId+"']").data("original_width")/2)-x_orig;
		                          var y_dist = y+($("div[name='image_"+preemptiveLocalReferenceId+"']").parent().data("original_height")==null?PLAYGROUND_HEIGHT/2:$("div[name='image_"+preemptiveLocalReferenceId+"']").parent().data("original_height")/2)-($("div[name='image_"+preemptiveLocalReferenceId+"']").data("original_height")/2)-y_orig;
		                          var largest_edge = Math.max($("div[name='image_"+preemptiveLocalReferenceId+"']").data("width"), $("div[name='image_"+preemptiveLocalReferenceId+"']").data("height"));
		                          var start = new Date().getTime();
		                          
		                          
		                          var hasMoreTime = true;
		                          
		                          
		                          setTimeout(function moveItem1(){
			                          if(hasMoreTime){
			                          	  var elapsed = new Date().getTime()-start;
			                              if(elapsed<time_allowed){
			                              	  //console.log("move image");
			                              	  
			                              	  
			                              	  
			                              	  setTimeout(function(){
			                              	  	  if(speedUp){
			                              	  	      $("div[name='image_"+preemptiveLocalReferenceId+"']").backToNormalTransform(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), x_orig+((elapsed/time_allowed)*x_dist), y_orig+((elapsed/time_allowed)*y_dist));
		                              	  	      }
		                              	  	      else{
		                              	  	      	  //$("div[name='image_"+preemptiveLocalReferenceId+"']").css("left", x_orig+((elapsed/time_allowed)*x_dist));
				                                      //$("div[name='image_"+preemptiveLocalReferenceId+"']").css("top", y_orig+((elapsed/time_allowed)*y_dist));
				                                      $("div[name='image_"+preemptiveLocalReferenceId+"']").css({
													      left: x_orig+((elapsed/time_allowed)*x_dist),
													      top: y_orig+((elapsed/time_allowed)*y_dist)
													  });
		                              	  	      }
			                                  }, 0);
			                                  

			                                  setTimeout(function(){
			                                      if(radius!=largest_edge){
			                                      	  if(!speedUp){
			                                      		  $("div[name='image_"+preemptiveLocalReferenceId+"']").scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
			                                      	  }
				                                      
				                                      
				                                       
				                                      $("div[name='image_"+preemptiveLocalReferenceId+"']").data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
				                                      $("div[name='image_"+preemptiveLocalReferenceId+"']").data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='image_"+preemptiveLocalReferenceId+"']").data("original_width"));
				                                  $("div[name='image_"+preemptiveLocalReferenceId+"']").data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='image_"+preemptiveLocalReferenceId+"']").data("original_height"));
				                                  }
				                                  
				                                  if($("div[name='image_"+preemptiveLocalReferenceId+"']").data("moving")!= parentTimespot.getId())
				                                  {
				                                      hasMoreTime = false;
				                                      //clearInterval(at);
				                                      //if(stop){
				                                      //	   hasMoreTime = false;
									                  //}
									                  executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
				                                  }
				                                  else{
				                                  	  if(stop){
				                                      	   hasMoreTime = false;
				                                      	   //clearInterval(at);
									                  }
									                  else{
									                      //setTimeout(function(){
						              	            	  //    new moveItem1();
						              	            	  //}, 0);
						              	            	  //new moveItem1();
									                  	  //setTimeout(function(){new moveItem1();}, 0);
									                  	  //setTimeout(moveItem1(), 0);
									                  	  moveItem1();
				                                          //setTimeout(arguments.callee, 0);
			                                          }
				                                  }
			                                  }, 0);
			                              }
			                              else
			                              {
			                              	  //clearInterval(at);
			                              	  hasMoreTime = false;
			                              	  
			                              	  
			                              	  
			                              	  setTimeout(function(){
			                              	  	  if(speedUp){
			                              	  	  	  $("div[name='image_"+preemptiveLocalReferenceId+"']").backToNormalTransform(ratio, x_orig+x_dist, y_orig+y_dist);
			                              	  	  }
			                              	  	  else{
			                              	  	  	  //$("div[name='image_"+preemptiveLocalReferenceId+"']").css("left", x_orig+x_dist);
				                                      //$("div[name='image_"+preemptiveLocalReferenceId+"']").css("top", y_orig+y_dist);
				                                      $("div[name='image_"+preemptiveLocalReferenceId+"']").css({
													      left: x_orig+x_dist,
													      top: y_orig+y_dist
													  });
			                              	  	  }
				                              	  
			                                  }, 0);
			                                  
			                                  
			                                  
			                                  setTimeout(function(){
			                                  	  if(!speedUp){
			                                  	  	  $("div[name='image_"+preemptiveLocalReferenceId+"']").scale(ratio);
			                                  	  }
			                                      
				                                  if(radius!=largest_edge){
					                                  $("div[name='image_"+preemptiveLocalReferenceId+"']").data("scale", ratio);
					                                  $("div[name='image_"+preemptiveLocalReferenceId+"']").data("width", ratio*$("div[name='image_"+preemptiveLocalReferenceId+"']").data("original_width"));
					                                  $("div[name='image_"+preemptiveLocalReferenceId+"']").data("height", ratio*$("div[name='image_"+preemptiveLocalReferenceId+"']").data("original_height"));
					                                  //hasMoreTime = false;
				                                  }
					                                  
									              executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
			                                  }, 0);
			                                  
			                              }
			                          }
		                          }, 0);
                    		}
                    		else{
                    			if(!stop){
                    				setTimeout(function(){
              	            		    new checkElementExist4();
              	            	    }, 300);
                      	            //setTimeout(arguments.callee, 300);
                  	            }
                  	        }
	                    }
	                    else if($("div[name='video_"+preemptiveLocalReferenceId+"']").length>0)
	                    {
	                        $("div[name='video_"+preemptiveLocalReferenceId+"']").data("moving", parentTimespot.getId());
	                        var ratio=radius/Math.max($("div[name='video_"+preemptiveLocalReferenceId+"']").data("original_width"), $("div[name='video_"+preemptiveLocalReferenceId+"']").data("original_height"));
	                        //var base_ratio=$("div[name='image_"+preemptiveLocalReferenceId+"']").data("width")/$("div[name='image_"+preemptiveLocalReferenceId+"']").data("original_width");
	                          var base_ratio=$("div[name='video_"+preemptiveLocalReferenceId+"']").data("scale");
	                          var x_orig = parseFloat($("div[name='video_"+preemptiveLocalReferenceId+"']").css("left"));
	                          var y_orig = parseFloat($("div[name='video_"+preemptiveLocalReferenceId+"']").css("top"));
	                          var x_dist = x+($("div[name='video_"+preemptiveLocalReferenceId+"']").parent().data("original_width")==null?PLAYGROUND_WIDTH/2:$("div[name='video_"+preemptiveLocalReferenceId+"']").parent().data("original_width")/2)-($("div[name='video_"+preemptiveLocalReferenceId+"']").data("original_width")/2)-x_orig;
	                          var y_dist = y+($("div[name='video_"+preemptiveLocalReferenceId+"']").parent().data("original_height")==null?PLAYGROUND_HEIGHT/2:$("div[name='video_"+preemptiveLocalReferenceId+"']").parent().data("original_height")/2)-($("div[name='video_"+preemptiveLocalReferenceId+"']").data("original_height")/2)-y_orig;
	                          var largest_edge = Math.max($("div[name='video_"+preemptiveLocalReferenceId+"']").data("width"), $("div[name='video_"+preemptiveLocalReferenceId+"']").data("height"));
	                          var start = new Date().getTime();
	                          
	                          var hasMoreTime = true;
	                          
	                          
	                          setTimeout(function moveItem2(){
		                          if(hasMoreTime){
		                          	  var elapsed = new Date().getTime()-start;
		                              if(elapsed<time_allowed){
		                              	  //console.log("move video");
		                              	  
		                              	  
		                              	  
		                              	  setTimeout(function(){
		                              	  	  if(speedUp){
		                              	  	  	  $("div[name='video_"+preemptiveLocalReferenceId+"']").backToNormalTransform(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), x_orig+((elapsed/time_allowed)*x_dist), y_orig+((elapsed/time_allowed)*y_dist));
		                              	  	  }
		                              	  	  else{
		                              	  	  	  //$("div[name='video_"+preemptiveLocalReferenceId+"']").css("left", x_orig+((elapsed/time_allowed)*x_dist));
			                                      //$("div[name='video_"+preemptiveLocalReferenceId+"']").css("top", y_orig+((elapsed/time_allowed)*y_dist));
			                                      $("div[name='video_"+preemptiveLocalReferenceId+"']").css({
												      left: x_orig+((elapsed/time_allowed)*x_dist),
												      top: y_orig+((elapsed/time_allowed)*y_dist)
												  });
		                              	  	  }
			                              	  
		                                  }, 0);
		                                  
		                                  
		                                  setTimeout(function(){
		                                      if(radius!=largest_edge){
		                                      	  if(!speedUp){
		                                      	  	  $("div[name='video_"+preemptiveLocalReferenceId+"']").scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
		                                      	  }
			                                  	  
			                                      $("div[name='video_"+preemptiveLocalReferenceId+"']").data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
			                                      $("div[name='video_"+preemptiveLocalReferenceId+"']").data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='video_"+preemptiveLocalReferenceId+"']").data("original_width"));
			                                      $("div[name='video_"+preemptiveLocalReferenceId+"']").data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='video_"+preemptiveLocalReferenceId+"']").data("original_height"));
			                                  }
			                                  
			                                  if($("div[name='video_"+preemptiveLocalReferenceId+"']").data("moving")!= parentTimespot.getId())
			                                  {
			                                      hasMoreTime = false;
			                                      //clearInterval(at);
			                                      //if(stop){
			                                      //	   hasMoreTime = false;
								                  //}
								                  executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
			                                  }
			                                  else{
			                                  	  if(stop){
			                                      	   hasMoreTime = false;
			                                      	   //clearInterval(at);
								                  }
								                  else{
								                  	  //setTimeout(function(){
						              	              //    new moveItem2();
						              	              //}, 0);
						              	              //new moveItem2();
								                  	  //setTimeout(function(){new moveItem2();}, 0);
								                  	  //setTimeout(moveItem2(), 0);
								                  	  moveItem2();
			                                          //setTimeout(arguments.callee, 0);
		                                          }
			                                  }
		                                  }, 0);
		                                  
		                                  
		                              }
		                              else
		                              {
		                              	  //clearInterval(at);
		                              	  hasMoreTime = false;
		                              	  
		                              	  
		                              	  setTimeout(function(){
		                              	  	  if(speedUp){
		                              	  	  	  $("div[name='video_"+preemptiveLocalReferenceId+"']").backToNormalTransform(ratio, x_orig+x_dist, y_orig+y_dist);
		                              	  	  }
		                              	  	  else{
		                              	  	  	  //$("div[name='video_"+preemptiveLocalReferenceId+"']").css("left", x_orig+x_dist);
			                                      //$("div[name='video_"+preemptiveLocalReferenceId+"']").css("top", y_orig+y_dist);
			                                      $("div[name='video_"+preemptiveLocalReferenceId+"']").css({
												      left: x_orig+x_dist,
												      top: y_orig+y_dist
												  });
		                              	  	  }
			                              }, 0);
		                                  
		                                  
		                                  setTimeout(function(){
			                                  if(radius!=largest_edge){
			                                  	  if(!speedUp){
			                                  	  	  $("div[name='video_"+preemptiveLocalReferenceId+"']").scale(ratio);
			                                  	  }
			                                      
			                                  
				                                  $("div[name='video_"+preemptiveLocalReferenceId+"']").data("scale", ratio);
				                                  $("div[name='video_"+preemptiveLocalReferenceId+"']").data("width", ratio*$("div[name='video_"+preemptiveLocalReferenceId+"']").data("original_width"));
				                                  $("div[name='video_"+preemptiveLocalReferenceId+"']").data("height", ratio*$("div[name='video_"+preemptiveLocalReferenceId+"']").data("original_height"));
				                                  //hasMoreTime = false;
			                                  }
								              
			                                  executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
		                                  }, 0);
		                                  
		                                  
		                              }
		                          }
	                          }, 0);
	                      }
	                      
	                      else if($("div[name='group_"+preemptiveLocalReferenceId+"']").length>0)
	                      {
	                      	  var parent = $("div[name='group_"+preemptiveLocalReferenceId+"']");
	                      	  var parentName = parent.attr("name").substring(6, parent.attr("name").length);
                    		
	                      	  purgeCalCache(parent, parentName);
                		    
	                      	
	                      	
	                      	  if(parent.data("awaiting_imgs")!=null){
	                      	      /*if(parseInt($("div[name='group_"+preemptiveLocalReferenceId+"']").data("awaiting_imgs"))>0 || ($("div[name='group_"+preemptiveLocalReferenceId+"']").find("*").length<=1 && $("div[name='group_"+preemptiveLocalReferenceId+"']").find("p").length==0)){
	                      	      
	                      	      	
	                      	      
	                      	      //if(parseInt($("div[name='group_"+preemptiveLocalReferenceId+"']").data("awaiting_imgs"))>0){
	                      	          if(!stop){
	                      	          	  //writeToErrorConsole("group images not finished loaded");
	                      	          	  setTimeout(function(){
                  	            		      new checkElementExist4();
                  	            	      }, 300);
			                      	      //setTimeout(arguments.callee, 300);
		                      	      }
	                      	      }*/
	                      	      //else{
	                      	      	
	                      	      	  //appBandCheckedTracer = new Array();
	                      	      	
			                          parent.data("moving", parentTimespot.getId());
			                          if(parent.parent().data("original_width")!=null){
			                              var ratio = radius/Math.max(parent.data("original_width"), parent.data("original_height"));}
			                          else{
			                              var ratio = (radius/full_screen_scale)*(Math.min(PLAYGROUND_WIDTH,PLAYGROUND_HEIGHT)/full_screen_scale);}
			
			                          var base_ratio=parent.data("scale");
			                          var x_orig = parseFloat(parent.css("left"));
			                          var y_orig = parseFloat(parent.css("top"));
			                          var x_dist = (x+(parent.parent().data("original_width")==null?PLAYGROUND_WIDTH/2:parent.parent().data("original_width")/2)-(parent.data("original_width")/2)-x_orig);
			                          var y_dist = (y+(parent.parent().data("original_height")==null?PLAYGROUND_HEIGHT/2:parent.parent().data("original_height")/2)-(parent.data("original_height")/2)-y_orig);
			                          
			                          /*var accumulatedScale = 1;
	                                  var currentEntity = $("div[name='group_"+preemptiveLocalReferenceId+"']").parent();
	                              
	                                  while(true){
                                	      accumulatedScale = accumulatedScale * currentEntity.data("scale");
                                	      if(currentEntity.attr("id")=="group_0"){
                                		      break;
                                	      }
                                	      currentEntity = currentEntity.parent();
                                      }*/
                                    
			                          
		
			                          var start = new Date().getTime();
			                          
			                          var hasMoreTime = true;
			                          
			                          
			                          setTimeout(function moveItem3(){
				                          if(hasMoreTime){
				                              var elapsed = new Date().getTime()-start;
				                              if(elapsed<time_allowed)
				                              {
				                                  
				                              	  //$("div[name='group_"+preemptiveLocalReferenceId+"']").transform2(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), ((elapsed/time_allowed)*x_dist/ratio), ((elapsed/time_allowed)*y_dist/ratio));
				                              	  
				                              	  
				                              	
				                              	  setTimeout(function(){
				                              	  	  if(speedUp){
				                              	  	  	  parent.backToNormalTransform(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), x_orig+((elapsed/time_allowed)*x_dist), y_orig+((elapsed/time_allowed)*y_dist));
				                              	  	  }
				                              	  	  else{
				                              	  	  	  //$("div[name='group_"+preemptiveLocalReferenceId+"']").css("left", x_orig+((elapsed/time_allowed)*x_dist));
					                                      //$("div[name='group_"+preemptiveLocalReferenceId+"']").css("top", y_orig+((elapsed/time_allowed)*y_dist));
					                                      parent.css({
														      left: x_orig+((elapsed/time_allowed)*x_dist),
														      top: y_orig+((elapsed/time_allowed)*y_dist)
														  });
				                              	  	  }
					                              }, 0);
				                                  
				                                  
				                                  
				                                  setTimeout(function(){
				                                      if(ratio!=base_ratio){
				                                      	  if(!speedUp){
				                                      	  	  parent.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
				                                      	  }
					                                      
					                                      
					                                      parent.data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
					                                      parent.data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='group_"+preemptiveLocalReferenceId+"']").data("original_width"));
					                                      parent.data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='group_"+preemptiveLocalReferenceId+"']").data("original_height"));
					                                      
					                                      /*render size change event on browser
					                                      var parent = $("div[name='group_"+preemptiveLocalReferenceId+"']");
					                                      var children = new Array();
					                                    	
					                                      for (var item in applications) {
					                                    	  var foundChildren = parent.find("div[name='group_"+item+"']");
										            		  if(foundChildren.length>0){
										            		  	
										            			  var occupyPercentage = 1;
								                                  currentEntity = $("div[name='group_"+item+"']");
								                                  
								                                  while(true){
								                                	  if(currentEntity.attr("id")=="group_0"){
								                                		  break;
								                                	  }
								                                	  occupyPercentage = occupyPercentage * currentEntity.data("scale");
								                                	  currentEntity = currentEntity.parent();
								                                  }
								                                  if(occupyPercentage<applications[item]['plt']['p'] && ratio<base_ratio){
								                                      children[item] = applications[item]['plt']['t'];
								                                  }
								                                  else if(occupyPercentage>applications[item]['pgt']['p'] && ratio>base_ratio){
								                                      children[item] = applications[item]['pgt']['t'];
								                                  }
										            		  }
									            		  }
									            		  for (var itemToExecute in children) {
									            		  	  if(!applications[itemToExecute]['executing']){
									            		  	  	  applications[itemToExecute]['executing'] = true;
										            		      new executeTimespot(children[itemToExecute], parentTimespot);
									            		      }
									            		  }
					                                   	   //render size change event on browser */
					                                  }
					                                  
					                                  
					                                  
					                                  if(parent.data("moving")!= parentTimespot.getId())
					                                  {
					                                      hasMoreTime = false;
					                                      //clearInterval(at);
					                                      //if(stop){
					                                      //	   hasMoreTime = false;
										                  //}
										                  executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
					                                  }
					                                  else{
					                                  	  if(stop){
					                                      	   hasMoreTime = false;
					                                      	   //clearInterval(at);
										                  }
										                  else{
										                  	  //requestAnimationFrame(moveItem3());
										                  	  //setTimeout(function(){
								              	              //    new moveItem3();
								              	              //}, 0);
								              	              //new moveItem3();
										                  	  //setTimeout(function(){new moveItem3();}, 0);
										                  	  //setTimeout(new moveItem3(), 0);
										                  	  moveItem3();
					                                          //setTimeout(arguments.callee, 0);
				                                          }
					                                  }
				                                  }, 0);
				                                  
				                                  
				                              }
				                              else
				                              {
				                              	  hasMoreTime = false;
				                              	  //clearInterval(at);
				                              	  //setTimeout(function(){
					                                  
					                              //}, 0);  
					                              
					                              
					                              //$("div[name='group_"+preemptiveLocalReferenceId+"']").css("visibility", "hidden");
				                                  
					                              setTimeout(function(){
					                              	  if(speedUp){
					                              	  	  parent.backToNormalTransform(ratio, x_orig+x_dist, y_orig+y_dist);
					                              	  }
					                              	  else{
					                              	  	  //$("div[name='group_"+preemptiveLocalReferenceId+"']").css("left", x_orig+x_dist);
					                                      //$("div[name='group_"+preemptiveLocalReferenceId+"']").css("top", y_orig+y_dist);
					                                      parent.css({
														      left: x_orig+x_dist,
														      top: y_orig+y_dist
														  });
					                              	  }
					                                  
					                              }, 0);
				                                  
					                              //$("div[name='group_"+preemptiveLocalReferenceId+"']").scale(ratio);
				                                  
				                                  setTimeout(function(){
				                                  	  if(ratio!=base_ratio){
				                                  	  	  if(!speedUp){
				                                  	  	  	  parent.scale(ratio);
				                                  	  	  }
					                                      
					                                      parent.data("scale", ratio);
					                                      parent.data("width", ratio*$("div[name='group_"+preemptiveLocalReferenceId+"']").data("original_width"));
					                                      parent.data("height", ratio*$("div[name='group_"+preemptiveLocalReferenceId+"']").data("original_height"));
					                                  //}
					                                  
					                                  //$("div[name='group_"+preemptiveLocalReferenceId+"']").css("visibility", "inherit");
					                                  
					                                  //render size change event on browser
				                                    //if(ratio!=base_ratio){
				                                    	
				                                    	//setTimeout(function(){
				                                    	//    new checkRenderDefinitionBands(preemptiveLocalReferenceId, false, ratio, base_ratio, parentTimespot);
			                                    	    //}, 0);
			                                    	    
			                                    	    
				                                    	
				                                    	/*var parent = $("div[name='group_"+preemptiveLocalReferenceId+"']");
				                                    	var children = new Array();
				                                    	
				                                    	for(var item in restraint_groups){
				                                    		var foundChildren = parent.find("div[name='group_"+item+"']");
				                                    		if(foundChildren.length>0){
				                                    			
				                                    			var occupyPercentage = 1;
							                                    var currentEntity = $("div[name='group_"+item+"']");
							                                    var baseGroup = $("div[name='group_0']");
							                                  
							                                    var withinDisplay = false;
							                                    var accumulatedScale = 1;
							                                    var accumulatedScale2 = 1;
							                                    
							                                    while(true){
							                                	    if(currentEntity.attr("id")=="group_0"){
							                                	    	accumulatedScale2 = accumulatedScale;
	                                	  	                            accumulatedScale2 = accumulatedScale2 * currentEntity.data("scale");
							                                		    break;
							                                	    }
							                                	    accumulatedScale = accumulatedScale * currentEntity.data("scale");
							                                	    occupyPercentage = occupyPercentage * currentEntity.data("scale");
							                                	    currentEntity = currentEntity.parent();
							                                    }
							                                    var width = full_screen_scale*accumulatedScale2/2;
	                                                            var height = full_screen_scale*accumulatedScale2/2;
	                                                            currentEntity = $("div[name='group_"+item+"']");
							                                    if((parseFloat(currentEntity.offset().left)+width>=parseFloat(baseGroup.offset().left)  && parseFloat(currentEntity.offset().left+width)<=(parseFloat(baseGroup.offset().left)+(baseGroup.data("scale")*full_screen_scale))) && (parseFloat(currentEntity.offset().top+height)>=parseFloat(baseGroup.offset().top)  && parseFloat(currentEntity.offset().top+height)<=(parseFloat(baseGroup.offset().top)+(baseGroup.data("scale")*full_screen_scale)))){
	                                                            	withinDisplay = true;
	                                                            }
							                                    
							                                    
							                                    if(withinDisplay || ratio<base_ratio){
							                                    	var tids = restraint_groups[item];
						                                    		var temparray = new Array();
						                                    		var cpuArray = new Array();
						                                    		var bandwidthArray = new Array();
						                                    		for(var item2 in tids){
							                                    		temparray.push(restrains[tids[item2]]["p"]);
							                                    		cpuArray.push(restrains[tids[item2]]["ips"]);
							                                    		bandwidthArray.push(restrains[tids[item2]]["b"]);
						                                    		}
						                                    		temparray.sort(function(a,b){return a-b});
						                                    		cpuArray.sort(function(a,b){return a-b});
						                                    		bandwidthArray.sort(function(a,b){return a-b});
						                                    		
						                                    		var currentP;
						                                    		var currentCPU;
						                                    		var currentBandwidth;
						                                    		if(temparray.length==1){
						                                    			currentP = temparray[0];
						                                    			currentCPU = cpuArray[0];
						                                    			currentBandwidth = bandwidthArray[0];
						                                    		}
						                                    		else{
						                                    			for (var i = 0; i < temparray.length; i++) {
						                                    				if(i==0){
																			    if(occupyPercentage<(temparray[i]+temparray[i+1])/2){
																		    		currentP = temparray[0];
																		    	}
																		    	else{
									                                    			currentP = temparray[1];
									                                    		}
									                                    		if(cpuSpeed<(cpuArray[i]+cpuArray[i+1])/2){
																		    		currentCPU = cpuArray[0];
																		    	}
																		    	else{
									                                    			currentCPU = cpuArray[1];
									                                    		}
									                                    		if(bandwidthSpeed<(bandwidthArray[i]+bandwidthArray[i+1])/2){
																		    		currentBandwidth = bandwidthArray[0];
																		    	}
																		    	else{
									                                    			currentBandwidth = bandwidthArray[1];
									                                    		}
																	    	}
																	    	else{
																	    		if(i+1<temparray.length){
																	    			if(occupyPercentage<(temparray[i]+temparray[i+1])/2){
																			    		currentP = temparray[i];
																			    	}
																			    	else{
																			    		currentP = temparray[i+1];
																			    	}
																			    	if(cpuSpeed<(cpuArray[i]+cpuArray[i+1])/2){
																			    		currentCPU = cpuArray[i];
																			    	}
																			    	else{
																			    		currentCPU = cpuArray[i+1];
																			    	}
																			    	if(bandwidthSpeed<(bandwidthArray[i]+bandwidthArray[i+1])/2){
																			    		currentBandwidth = bandwidthArray[i];
																			    	}
																			    	else{
																			    		currentBandwidth = bandwidthArray[i+1];
																			    	}
																	    		}
																	    	}
																		}
						                                    		}
						                                    		
						                                    		var closeness = new Array();
						                                    		var max = 0;
						                                    		var closerTid = 0;
						                                    		
						                                    		for(var item2 in tids){
						                                    			closeness[tids[item2]] = 0;
						                                    			if(restrains[tids[item2]]["p"]==currentP){
						                                    				closeness[tids[item2]] += 40;
						                                    			}
						                                    			if(restrains[tids[item2]]["ips"]==currentCPU){
						                                    				closeness[tids[item2]] += 20;
						                                    			}
						                                    			if(restrains[tids[item2]]["b"]==currentBandwidth){
						                                    				closeness[tids[item2]] += 20;
						                                    			}
						                                    			
						                                    			if(closeness[tids[item2]]>max){
						                                    				max = closeness[tids[item2]];
						                                    				closerTid = tids[item2];
						                                    			}
					                                    			}
					                                    			children[item] = closerTid;
							                                    }
			                                    			}
				                                    	}
				                                    	for (var itemToExecute in children) {
									            			new executeTimespot(children[itemToExecute], parentTimespot);
								            			}*/
				                                    	
				                                    	/*for (var item in applications) {
				                                    		var foundChildren = parent.find("div[name='group_"+item+"']");
									            			if(foundChildren.length>0){
									            				var occupyPercentage = 1;
							                                    var currentEntity = $("div[name='group_"+item+"']");
							                                    var baseGroup = $("div[name='group_0']");
							                                  
							                                    var withinDisplay = false;
							                                    var accumulatedScale = 1;
							                                    var accumulatedScale2 = 1;
							                                    
							                                    while(true){
							                                	    if(currentEntity.attr("id")=="group_0"){
							                                	    	accumulatedScale2 = accumulatedScale;
	                                	  	                            accumulatedScale2 = accumulatedScale2 * currentEntity.data("scale");
							                                		    break;
							                                	    }
							                                	    accumulatedScale = accumulatedScale * currentEntity.data("scale");
							                                	    occupyPercentage = occupyPercentage * currentEntity.data("scale");
							                                	    currentEntity = currentEntity.parent();
							                                    }
							                                    var width = full_screen_scale*accumulatedScale2/2;
	                                                            var height = full_screen_scale*accumulatedScale2/2;
	                                                            currentEntity = $("div[name='group_"+item+"']");
							                                    if((parseFloat(currentEntity.offset().left)+width>=parseFloat(baseGroup.offset().left)  && parseFloat(currentEntity.offset().left+width)<=(parseFloat(baseGroup.offset().left)+(baseGroup.data("scale")*full_screen_scale))) && (parseFloat(currentEntity.offset().top+height)>=parseFloat(baseGroup.offset().top)  && parseFloat(currentEntity.offset().top+height)<=(parseFloat(baseGroup.offset().top)+(baseGroup.data("scale")*full_screen_scale)))){
	                                                            	withinDisplay = true;
	                                                            }
							                                    
							                                    
							                                    
							                                    if(withinDisplay || ratio<base_ratio){
								                                    if(occupyPercentage<applications[item]['plt']['p'] && ratio<base_ratio){
								                                    	children[item] = applications[item]['plt']['t'];
								                                    }
								                                    else if(occupyPercentage>applications[item]['pgt']['p'] && ratio>base_ratio){
								                                    	children[item] = applications[item]['pgt']['t'];
								                                    }
							                                    }
									            			}
								            			}*/
								            			
								            			
								            			
				                                    }
				                                    //if(!checkIfNull(applications[preemptiveLocalReferenceId])){
				                                    	//getNewPreloadBandQueue();
				                                    //}
					                                  
					                                  
					                                   /*render size change event on browser
					                                   if(ratio!=base_ratio){
					                                       var parent = $("div[name='group_"+preemptiveLocalReferenceId+"']");
					                                       var children = new Array();
					                                    	
					                                       for (var item in applications) {
					                                    	   var foundChildren = parent.find("div[name='group_"+item+"']");
										            		   if(foundChildren.length>0){
										            			   var occupyPercentage = 1;
								                                   currentEntity = $("div[name='group_"+item+"']");
								                                  
								                                   while(true){
								                                	   if(currentEntity.attr("id")=="group_0"){
								                                		   break;
								                                	   }
								                                	   occupyPercentage = occupyPercentage * currentEntity.data("scale");
								                                	   currentEntity = currentEntity.parent();
								                                   }
								                                   if(occupyPercentage<applications[item]['plt']['p'] && ratio<base_ratio){
								                                       children[item] = applications[item]['plt']['t'];
								                                   }
								                                   else if(occupyPercentage>applications[item]['pgt']['p'] && ratio>base_ratio){
								                                       children[item] = applications[item]['pgt']['t'];
								                                   }
										            		   }
									            		   }
									            		   for (var itemToExecute in children) {
									            		   	   if(!applications[itemToExecute]['executing']){
										            		       new executeTimespot(children[itemToExecute], parentTimespot);
									            		       }
										            		   applications[itemToExecute]['executing'] = false;
									            		   }
					                                   }
					                                   //render size change event on browser*/
					                                  
					                                  
					                                  /*var occupyPercentage = 1;
					                                  currentEntity = $("div[name='group_"+preemptiveLocalReferenceId+"']");
					                                  
					                                  while(true){
					                                	  if(currentEntity.attr("id")=="group_0"){
					                                		  break;
					                                	  }
					                                	  occupyPercentage = occupyPercentage * currentEntity.data("scale");
					                                	  currentEntity = currentEntity.parent();
					                                  }
					                                  
					                                  
					                                  setTimeout(function centerSendSignal(){
							                            //Testing only
							                            var test_request = false;
							
												        try
												        {
												           test_request = new XMLHttpRequest();
												        }
												        catch (error)
												        {
												            try
												            {
												                test_request = new ActiveXObject("Microsoft.XMLHTTP");
												            }
												            catch (error)
												            {
												                alert('there is an unknown error occurred');
												            }
												        }
													        
												    	if(test_request)
												        {
												            test_request.onreadystatechange = function(){
												            	    if (test_request.readyState == 4){
												            	    	if (test_request.status == 200){
												            	    		if(test_request.getResponseHeader("Content-Length")=="0"){
												            	    			centerSignalSent = true;
												            	    			setTimeout(function(){new getInstruction();}, refresh_rate);
												            	    		}
												            	    		else{
												            	    			new centerSendSignal();
												            	    		}
											            	    		}
											            	    		else{
											            	    			new centerSendSignal();
											            	    		}
											            	    	}
												            	};
												            test_request.open("GET", "/sendCenterSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+preemptiveLocalReferenceId+"?initialSize="+accumulatedScale.toFixed(1)+"?finalSize="+occupyPercentage.toFixed(1)+"?time="+time.toFixed(1), true);
												            
												            test_request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
												            test_request.send();
												            //setTimeout(function(){new getInstruction();}, refresh_rate);
												        }
											        }, 0);*/
					                                  
					                                executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
				                                  }, 0);
				                                  
				                              }
			                              }
			                          }, 0);
		                          //}
	                      	  }
	                      }
	                      else{
	                      	  if(!stop){
	                      	  	  if(skip){
	                      	  	  	  executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	                      	  	  }
	                      	  	  else{
	                      	  	      setTimeout(function(){
          	            		          new checkElementExist4();
          	            	          }, 300);
      	            	          }
	                      	      //setTimeout(arguments.callee, 300);
                      	      }
	                      	  //writeToErrorConsole('The entity that you wanted to move does not exist! ref: '+preemptiveLocalReferenceId);	
                      	  }
                    }, 0);
                    

                    break;
                }
                case REMOVE_CONDITION:
                {
                	
                    var event_reference = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                    if(globals[event_reference]!=null){
                        event_reference = globals[event_reference];
                    }
                    if(event_element[event_reference]!=null && event_type[event_reference]!=null)
                    {	
                        var localRefToElement = event_element[event_reference];
                        if(event_type[event_reference]=="drag"){
                        	//console.error("remove drag");
                        	if($("div[name='image_"+localRefToElement+"']").length>0){
                        		$("div[name='image_"+localRefToElement+"']").css("cursor","");
                        		$("div[name='image_"+localRefToElement+"']").ppdrag({ cer: event_reference, destroy: 'yes'});
                        		$("div[name='image_"+localRefToElement+"']").unbind("checkRender"+localRefToElement);
                        		$("div[name='image_"+localRefToElement+"']").unbind("custom");
                        		
                        		if(!checkIfNull(event_type_function[event_reference])){
	                            	$("div[name='image_"+localRefToElement+"']").unbind("swipe", event_type_function[event_reference]);
	                            	event_type_function[event_reference] = null;
	                            }
                        		
                        		//$("div[name='image_"+localRefToElement+"']").unbind("swipe");
                        	}
                        	else if($("div[name='group_"+localRefToElement+"']").length>0)
	                        {
	                            $("div[name='group_"+localRefToElement+"']").css("cursor","");
	                            $("div[name='group_"+localRefToElement+"']").ppdrag({ cer: event_reference, destroy: 'yes'});
	                            $("div[name='group_"+localRefToElement+"']").unbind("checkRender"+localRefToElement);
	                            $("div[name='group_"+localRefToElement+"']").unbind("custom");
	                            
	                            if(!checkIfNull(event_type_function[event_reference])){
	                            	$("div[name='group_"+localRefToElement+"']").unbind("swipe", event_type_function[event_reference]);
	                            	event_type_function[event_reference] = null;
	                            }
	                            //$("div[name='group_"+localRefToElement+"']").unbind("swipe");
	                        }
                        }
                        else{
	                        if($("div[name='image_"+localRefToElement+"']").length>0)
	                        {
	                            writeToConsole("remove condition "+event_reference+" from image with local ref: "+localRefToElement);
	                            $("div[name='image_"+localRefToElement+"']").data("hasClick", null);
	                            $("div[name='image_"+localRefToElement+"']").unbind(event_type[event_reference]);
	                        }
	                        else if($("div[name='group_"+localRefToElement+"']").length>0)
	                        {
	                            writeToConsole("remove condition "+event_reference+" from group with local ref: "+localRefToElement);
	                            $("div[name='group_"+localRefToElement+"']").data("hasClick", null);
	                            $("div[name='group_"+localRefToElement+"']").unbind(event_type[event_reference]);
	                            $("div[name='group_"+localRefToElement+"']").css("pointer-events", "none");
	                        }
	                        else if($("div[name='video_"+localRefToElement+"']").length>0)
	                        {
	                            writeToConsole("remove condition "+event_reference+" from video with local ref: "+localRefToElement);
	                            $("div[name='video_"+localRefToElement+"']").data("hasClick", null);
	                            $("div[name='video_"+localRefToElement+"']").unbind(event_type[event_reference]);
	                        }
	                        event_type[event_reference]=null;
	                        event_element[event_reference]=null;
                        }
                    }
                    else{writeToConsole('The event with ref: ' + event_reference + ' that you wanted to remove does not exist!!');}
                    
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case REMOVE_FROM_AND_ADD_TO_ENTITY:
                {
                    var localReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                    if(globals[localReferenceId]!=null)
                    {
                        localReferenceId = globals[localReferenceId];
                        writeToConsole("Remove from and add to entity: put global id "+localReferenceId+" into localReferenceId");
                    }
                    var parentEntityId = instruction.getElementsByTagName("pid")[0].firstChild.nodeValue;
                    if(globals[parentEntityId]!=null)
                        parentEntityId = globals[parentEntityId];
                    //var x = instruction.getElementsByTagName("x")[0].firstChild.nodeValue;
                    //x = parseFloat(x);
                    /*if(floats[x]!=null)
                    {
                        x = floats[x];
                    }
                    else{
                    	x = parseFloat(x);
                    }*/
                    
                    //var y = instruction.getElementsByTagName("y")[0].firstChild.nodeValue;
                    //y = parseFloat(y);
                    
                    /*if(floats[y]!=null)
                    {
                        y = floats[y];
                    }
                    else{
                    	y = parseFloat(y);
                    }*/
                    
                    //var radius = instruction.getElementsByTagName("rad")[0].firstChild.nodeValue;
                    //radius = parseFloat(radius);
                    /*if(floats[radius]!=null)
                    {
                        radius = floats[radius];
                    }
                    else{
                    	radius = parseFloat(radius);
                    }*/
                    
                    //var time = instruction.getElementsByTagName("sec")[0].firstChild.nodeValue;
                    //time = parseFloat(time);
                    /*if(floats[time]!=null)
                    {
                        time = floats[time];
                    }
                    else{
                    	time = parseFloat(time);
                    }*/
                    //var time_allowed = time*1000;
                    writeToConsole("REMOVE_FROM_AND_ADD_TO_ENTITY: up to line 1089 ");
                    if($("div[name='image_"+localReferenceId+"']").length>0)
                    {
                        if($("div[id='group_"+parentEntityId+"']").length>0)
                        {
                        	
                        	
                        	
                            $("div[name='image_"+localReferenceId+"']").data("moving", parentTimespot.getId());
                            writeToConsole("remove image localReferenceId: "+localReferenceId+" to parent parentEntityId: "+parentEntityId);
                            var ratio=radius/Math.max($("div[name='image_"+localReferenceId+"']").data("original_width"), $("div[name='image_"+localReferenceId+"']").data("original_height"));
                            var base_ratio=$("div[name='image_"+localReferenceId+"']").data("scale");
                            var tempElementX = parseFloat($("div[name='image_"+localReferenceId+"']").offset().left);
                            var tempElementY = parseFloat($("div[name='image_"+localReferenceId+"']").offset().top);
                            var tempElement = $("div[name='image_"+localReferenceId+"']").detach();
                            $("div[id='group_"+parentEntityId+"']").append(tempElement);
                            
                            //make image's parent top z-index
                            $("div[name='image_"+localReferenceId+"']").parent().css("z-index", bringtofront_index++);
                        	$("div[name='image_"+localReferenceId+"']").css("z-index", bringtofront_index++);
                        	
                            
                            /*alert($("div[name='image_"+localReferenceId+"']").css("left"));
                            alert(tempElementX);
                            $("div[name='image_"+localReferenceId+"']").offset({ top: tempElementY, left: tempElementX });
                            alert($("div[name='image_"+localReferenceId+"']").css("left"));
                            alert($("div[name='image_"+localReferenceId+"']").offset().left);*/
                            
                            $("div[name='image_"+localReferenceId+"']").offset({top: tempElementY, left: tempElementX});
                            
                            /*while($("div[name='image_"+localReferenceId+"']").offset().left!=tempElementX || $("div[name='image_"+localReferenceId+"']").offset().top!=tempElementY)
                            {
                                var currentTop = $("div[name='image_"+localReferenceId+"']").offset().top;
                                var currentLeft = $("div[name='image_"+localReferenceId+"']").offset().left;
//                                
                                $("div[name='image_"+localReferenceId+"']").offset({top: tempElementY, left: tempElementX});
                                if($("div[name='image_"+localReferenceId+"']").offset().top == currentTop && $("div[name='image_"+localReferenceId+"']").offset().left == currentLeft)
                                {
//                                    writeToErrorConsole("REMOVE_FROM_AND_ADD_TO_ENTITY : no change in top and left after call to offset" );
                                    break;
                                }
                            }*/
                            
                            
                            //while($("div[name='image_"+localReferenceId+"']").offset().left!=tempElementX && $("div[name='image_"+localReferenceId+"']").offset().top!=tempElementY)
                            //{
                            //    $("div[name='image_"+localReferenceId+"']").offset({top: tempElementY, left: tempElementX});
                            //}
                            
                            
                            
                            var x_orig = parseFloat($("div[name='image_"+localReferenceId+"']").css("left"));
                            var y_orig = parseFloat($("div[name='image_"+localReferenceId+"']").css("top"));
                            var x_dist = x+($("#group_"+parentEntityId).data("original_width")==null?PLAYGROUND_WIDTH/2:$("#group_"+parentEntityId).data("original_width")/2)-($("div[name='image_"+localReferenceId+"']").data("original_width")/2)-x_orig;
                            var y_dist = y+($("#group_"+parentEntityId).data("original_height")==null?PLAYGROUND_HEIGHT/2:$("#group_"+parentEntityId).data("original_height")/2)-($("div[name='image_"+localReferenceId+"']").data("original_height")/2)-y_orig;
                            var largest_edge = Math.max($("div[name='image_"+localReferenceId+"']").data("width"), $("div[name='image_"+localReferenceId+"']").data("height"));
                            
                            
                            var start = new Date().getTime();
                          
                            
	                            
                          
	                        var hasMoreTime = true;
	                          
	                          
	                        setTimeout(function moveItem(){
		                        if(hasMoreTime){
		                          	var elapsed = new Date().getTime()-start;
		                            if(elapsed<time_allowed){
		                              	//console.log("move image");
		                              	$("div[name='image_"+localReferenceId+"']").css("left", x_orig+((elapsed/time_allowed)*x_dist));
		                                $("div[name='image_"+localReferenceId+"']").css("top", y_orig+((elapsed/time_allowed)*y_dist));
		                              	  
		                                
		                              	
		                                  
		                                  
		                                if(radius!=largest_edge)
		                                {
		                                	setTimeout(function(){$("div[name='image_"+localReferenceId+"']").scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));}, 0);
		                                    
		                                      
		                                       
		                                      
		                                    $("div[name='image_"+localReferenceId+"']").data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
		                                  }
		                                $("div[name='image_"+localReferenceId+"']").data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='image_"+localReferenceId+"']").data("original_width"));
		                                $("div[name='image_"+localReferenceId+"']").data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='image_"+localReferenceId+"']").data("original_height"));
		                                if($("div[name='image_"+localReferenceId+"']").data("moving")!= parentTimespot.getId())
		                                {
		                                    hasMoreTime = false;
		                                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
		                                }
		                                else{
		                                  	if(stop){
		                                      	 hasMoreTime = false;
							                }
							                else{
							                	setTimeout(function(){
			              	            		    new moveItem();
			              	            	    }, 0);
		                                        //setTimeout(arguments.callee, 0);
	                                        }
		                                }
		                            }
		                            else
		                            {
		                              	$("div[name='image_"+localReferenceId+"']").css("left", x_orig+x_dist);
		                                $("div[name='image_"+localReferenceId+"']").css("top", y_orig+y_dist);
		                                
		                                
		                                
		                                setTimeout(function(){$("div[name='image_"+localReferenceId+"']").scale(ratio);}, 0);
		                                
		                                $("div[name='image_"+localReferenceId+"']").data("scale", ratio);
		                                $("div[name='image_"+localReferenceId+"']").data("width", ratio*$("div[name='image_"+localReferenceId+"']").data("original_width"));
		                                $("div[name='image_"+localReferenceId+"']").data("height", ratio*$("div[name='image_"+localReferenceId+"']").data("original_height"));
		                                hasMoreTime = false;
		                                  
						                    
						                executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
		                            }
		                        }
	                        }, 0);
                        }
                        else{writeToErrorConsole("remove from and add to entity failed: parent group parentEntityId: "+parentEntityId+" does not exist");}
                    }
                    else if($("div[name='group_"+localReferenceId+"']").length>0)
                    {
                        if($("div[id='group_"+parentEntityId+"']").length>0)
                        {
                        	//new
                        	$("div[name='group_"+localReferenceId+"']").data("moving", parentTimespot.getId());
                        	var ratio;
                        	if($("div[name='group_"+localReferenceId+"']").parent().data("original_width")!=null){
                                ratio = radius/full_screen_scale;
                            }
                            else{
                                ratio = (radius/full_screen_scale)*(Math.min(PLAYGROUND_WIDTH,PLAYGROUND_HEIGHT)/full_screen_scale);
                            }
                            
                            var accumulatedScale = 1;
                            var currentEntity = $("div[name='group_"+localReferenceId+"']");
                            while(true){
                            	accumulatedScale = accumulatedScale * currentEntity.data("scale");
                            	if(currentEntity.attr("id")=="group_0"){
                            		break;
                            	}
                            	currentEntity = currentEntity.parent();
                            }
                            
                            var parentAccumulatedScale = 1;
                            var parentCurrentEntity = $("div[id='group_"+parentEntityId+"']");
                            while(true){
                            	parentAccumulatedScale = parentAccumulatedScale * parentCurrentEntity.data("scale");
                            	if(parentCurrentEntity.attr("id")=="group_0"){
                            		break;
                            	}
                            	parentCurrentEntity = parentCurrentEntity.parent();
                            }
                            var base_ratio = accumulatedScale / parentAccumulatedScale;
                            //alert(base_ratio);
                            
                            //var base_ratio=$("div[name='group_"+localReferenceId+"']").data("scale");
                            var tempElementX = parseInt($("div[name='group_"+localReferenceId+"']").offset().left);
                            var tempElementY = parseInt($("div[name='group_"+localReferenceId+"']").offset().top);
                            var tempElement = $("div[name='group_"+localReferenceId+"']").detach();
                            $("div[id='group_"+parentEntityId+"']").append(tempElement);
                            
                            //make group's parent top z-index
                            //$("div[name='group_"+localReferenceId+"']").parent().css("z-index", bringtofront_index++);
                        	//$("div[name='group_"+localReferenceId+"']").css("z-index", bringtofront_index++);
                        	
                        	
                        	$("div[name='group_"+localReferenceId+"']").scale(base_ratio);
                        	
                            //alert("original: "+tempElementX+" "+tempElementY);
                            var counter = 0;
                            while(counter<10){
                            	$("div[name='group_"+localReferenceId+"']").offset({top: tempElementY, left: tempElementX});
                            	if(tempElementX==parseInt($("div[name='group_"+localReferenceId+"']").offset().left) && tempElementY==parseInt($("div[name='group_"+localReferenceId+"']").offset().top)){
                            		break;
                            	}
                            	counter++;
                            }
                            
                            executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                            
                           
                            
                            /*var x_orig = parseFloat($("div[name='group_"+localReferenceId+"']").css("left"));
                            var y_orig = parseFloat($("div[name='group_"+localReferenceId+"']").css("top"));
                            var x_dist = (x+($("div[name='group_"+localReferenceId+"']").parent().data("original_width")==null?PLAYGROUND_WIDTH/2:$("div[name='group_"+localReferenceId+"']").parent().data("original_width")/2)-($("div[name='group_"+localReferenceId+"']").data("original_width")/2)-x_orig);
                            var y_dist = (y+($("div[name='group_"+localReferenceId+"']").parent().data("original_height")==null?PLAYGROUND_HEIGHT/2:$("div[name='group_"+localReferenceId+"']").parent().data("original_height")/2)-($("div[name='group_"+localReferenceId+"']").data("original_height")/2)-y_orig);

                            var start = new Date().getTime();
                          
                            var hasMoreTime = true;
                          
                          
                            setTimeout(function moveItem(){
	                            if(hasMoreTime){
	                                var elapsed = new Date().getTime()-start;
	                                if(elapsed<time_allowed)
	                                {
	                                    $("div[name='group_"+localReferenceId+"']").css("left", x_orig+((elapsed/time_allowed)*x_dist));
	                                    $("div[name='group_"+localReferenceId+"']").css("top", y_orig+((elapsed/time_allowed)*y_dist));
	                                    
	                                    
	                                    
	                                    
	                                    if(ratio!=base_ratio)
	                                    {
	                                    	setTimeout(function(){$("div[name='group_"+localReferenceId+"']").scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));}, 0);
	                                        
	                                        $("div[name='group_"+localReferenceId+"']").data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
	                                        $("div[name='group_"+localReferenceId+"']").data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='group_"+localReferenceId+"']").data("original_width"));
	                                        $("div[name='group_"+localReferenceId+"']").data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='group_"+localReferenceId+"']").data("original_height"));
	                                    }
	                                    if($("div[name='group_"+localReferenceId+"']").data("moving")!= parentTimespot.getId())
	                                    {
	                                        hasMoreTime = false;
	                                        if(stop){
	                                      	     hasMoreTime = false;
						                    }
						                    else{
							                    noi++;
							                    if(noi<instructionList.length){	
								                    var currentInstruction = new executeInstruction(instructionList[noi], noi, instructionList, parentArray, parentTimespot);
								                  //if(currentInstruction.checkIsOnTimer())
								                        parentArray.push(currentInstruction);
							                    }
						                    }
	                                    }
	                                    else{
	                                    	if(stop){
	                                        	   hasMoreTime = false;
						                    }
						                    else{
	                                            setTimeout(arguments.callee, 0);
                                            }
	                                    }
	                                }
	                                else
	                                {
	                              	    hasMoreTime = false;
	                                    $("div[name='group_"+localReferenceId+"']").css("left", x_orig+x_dist);
	                                    $("div[name='group_"+localReferenceId+"']").css("top", y_orig+y_dist);
	                                    
	                                    
	                                    
	                                    
	                                    if(ratio!=base_ratio)
	                                    {
	                                    	setTimeout(function(){$("div[name='group_"+localReferenceId+"']").scale(ratio);}, 0);
	                                        
	                                        $("div[name='group_"+localReferenceId+"']").data("scale", ratio);
	                                        $("div[name='group_"+localReferenceId+"']").data("width", ratio*$("div[name='group_"+localReferenceId+"']").data("original_width"));
	                                        $("div[name='group_"+localReferenceId+"']").data("height", ratio*$("div[name='group_"+localReferenceId+"']").data("original_height"));
	                                    }
	                                  
	                                    noi++;
					                    if(noi<instructionList.length){	
						                    var currentInstruction = new executeInstruction(instructionList[noi], noi, instructionList, parentArray, parentTimespot);
					                        parentArray.push(currentInstruction);  
				                        }
	                                }
                                }
                            }, 0);*/
                        	
                        	
                        	/*//old
                            $("div[name='group_"+localReferenceId+"']").data("moving", parentTimespot.getId());
                            var ratio = radius/full_screen_scale;
                            var base_ratio=$("div[name='group_"+localReferenceId+"']").data("scale");
                            var tempElementX = parseFloat($("div[name='group_"+localReferenceId+"']").offset().left);
                            var tempElementY = parseFloat($("div[name='group_"+localReferenceId+"']").offset().top);

                            var tempElement = $("div[name='group_"+localReferenceId+"']").detach();
                            $("div[id='group_"+parentEntityId+"']").append(tempElement);
                            
                            $("div[name='group_"+localReferenceId+"']").offset({top: tempElementY, left: tempElementX});

                            
                            var x_orig = parseFloat($("div[name='group_"+localReferenceId+"']").css("left"));
                            var y_orig = parseFloat($("div[name='group_"+localReferenceId+"']").css("top"));
                            var x_dist = (x+($("#group_"+parentEntityId).data("original_width")==null?PLAYGROUND_WIDTH/2:$("#group_"+parentEntityId).data("original_width")/2)-($("div[name='group_"+localReferenceId+"']").data("original_width")/2)-x_orig);
                            var y_dist = (y+($("#group_"+parentEntityId).data("original_height")==null?PLAYGROUND_HEIGHT/2:$("#group_"+parentEntityId).data("original_height")/2)-($("div[name='group_"+localReferenceId+"']").data("original_height")/2)-y_orig);
                            var start = new Date().getTime();
                            atimer = setInterval(function()
                            {
                                var elapsed = new Date().getTime()-start;
                                if(elapsed<time_allowed)
                                {
                                    $("div[name='group_"+localReferenceId+"']").css("left", x_orig+((elapsed/time_allowed)*x_dist));
                                    $("div[name='group_"+localReferenceId+"']").css("top", y_orig+((elapsed/time_allowed)*y_dist));
                                    if(ratio!=base_ratio)
                                    {
                                        $("div[name='group_"+localReferenceId+"']").scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
                                        $("div[name='group_"+localReferenceId+"']").data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
                                        $("div[name='group_"+localReferenceId+"']").data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='group_"+localReferenceId+"']").data("original_width"));
                                        $("div[name='group_"+localReferenceId+"']").data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*$("div[name='group_"+localReferenceId+"']").data("original_height"));
                                    }
                                }
                                else
                                {
                                    $("div[name='group_"+localReferenceId+"']").css("left", x_orig+x_dist);
                                    $("div[name='group_"+localReferenceId+"']").css("top", y_orig+y_dist);
                                    if(ratio!=base_ratio)
                                    {
                                        $("div[name='group_"+localReferenceId+"']").scale(ratio);
                                        $("div[name='group_"+localReferenceId+"']").data("scale", ratio);
                                        $("div[name='group_"+localReferenceId+"']").data("width", ratio*$("div[name='group_"+localReferenceId+"']").data("original_width"));
                                        $("div[name='group_"+localReferenceId+"']").data("height", ratio*$("div[name='group_"+localReferenceId+"']").data("original_height"));
                                    }
                                    clearInterval(atimer);
                                }
                            }, refresh_rate);*/
                        }
                        else{writeToErrorConsole("remove from and add to entity failed: parent group parentEntityId: "+parentEntityId+" does not exist");}
                    }
//                    Move video
                    else if($("div[name='video_"+localReferenceId+"']").length>0)
                    {
                        var x_orig = parseFloat($("div[name='video_"+localReferenceId+"']").css("left"));
                        var y_orig = parseFloat($("div[name='video_"+localReferenceId+"']").css("top"));
                        var orig_width=$("div[name='video_"+localReferenceId+"']").width();
                        var orig_height=$("div[name='video_"+localReferenceId+"']").height();
                        var largest_edge=Math.max($("div[name='video_"+localReferenceId+"']").width(),$("div[name='video_"+localReferenceId+"']").height());
                        var ratio=radius/largest_edge;
                        var final_width=ratio*orig_width;
                        var final_height=ratio*orig_height;
                        var tempElementX = parseFloat($("div[name='video_"+localReferenceId+"']").offset().left);
                        var tempElementY = parseFloat($("div[name='video_"+localReferenceId+"']").offset().top);
                        var tempElement = $("div[name='video_"+localReferenceId+"']").detach();
                        $("#group_"+parentEntityId).append(tempElement);
                        while($("div[name='video_"+localReferenceId+"']").offset().left!=tempElementX && $("div[name='video_"+localReferenceId+"']").offset().top!=tempElementY)
                        {
                            $("div[name='video_"+localReferenceId+"']").offset({top: tempElementY, left: tempElementX});
                        }
                        var x_orig = parseFloat($("div[name='video_"+localReferenceId+"']").css("left"));
                        var y_orig = parseFloat($("div[name='video_"+localReferenceId+"']").css("top"));
                        var x_dist = x+(PLAYGROUND_WIDTH/2)-(final_width/2)-x_orig;
                        var y_dist = y+(PLAYGROUND_HEIGHT/2)-(final_height/2)-y_orig;
                        var start = new Date().getTime();
                        atimer = setInterval(function()
                        {
                            var elapsed = new Date().getTime()-start;
                            if(elapsed<time_allowed)
                            {
                                $("div[name='video_"+localReferenceId+"']").css("left", x_orig+((elapsed/time_allowed)*x_dist));
                                $("div[name='video_"+localReferenceId+"']").css("top", y_orig+((elapsed/time_allowed)*y_dist));
                                
                                
                                
                                
                                
                                if(radius!=largest_edge)
                                {
                                    $("div[name='video_"+localReferenceId+"']").width(orig_width+((elapsed/time_allowed)*(final_width-orig_width)));
                                    $("div[name='video_"+localReferenceId+"']").height(orig_height+((elapsed/time_allowed)*(final_height-orig_height)));
                                }
                            }
                            else
                            {
                                $("div[name='video_"+localReferenceId+"']").width(final_width);
                                $("div[name='video_"+localReferenceId+"']").height(final_height);
                                $("div[name='video_"+localReferenceId+"']").css("left", x_orig+x_dist);
                                $("div[name='video_"+localReferenceId+"']").css("top", y_orig+y_dist);
                                
                                
                                clearInterval(atimer);
                            }
                        }, refresh_rate);
                    }
                    else{writeToConsole('The image/group/video that you wanted to remove and add to a group does not exist! ref: '+localReferenceId);}
                    
                    /*if(stop)
                        break;
                    
                    
                    noi++;
                    if(noi<instructionList.length){	
	                    var currentInstruction = new executeInstruction(instructionList[noi], noi, instructionList, parentArray, parentTimespot);
	                    //if(currentInstruction.checkIsOnTimer())
	                       parentArray.push(currentInstruction);
                    }*/
                    break;
                }
                case REMOVE_FROM_ENTITY:
                {	
                    var localReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                    if(globals[localReferenceId]!=null)
                    {
                        localReferenceId = globals[localReferenceId];
                        writeToConsole("Remove from entity: put global id "+localReferenceId+" into localReferenceId");
                    }
                    if(!stop)
                    {
                        if($("div[name='image_"+localReferenceId+"']").length>0)
                        {
                            writeToConsole('remove image with ref ' + localReferenceId);
                            $("div[name='image_"+localReferenceId+"']").remove();
                        }
                        else if($("div[name='group_"+localReferenceId+"']").length>0)
                        {
                            writeToConsole('remove group with ref ' + localReferenceId);
                            
                            //check if its an app
                            if(!checkIfNull(applications[localReferenceId]) && !checkIfNull(restraint_groups[localReferenceId])){
                            	for(var item in restraint_groups[localReferenceId]){
                            		try{
	                            		var itid = restraint_groups[localReferenceId][item];
	                            		delete restrains[itid];
	                            		delete application_timespots[itid];
	                            		delete restraint_groups[localReferenceId];
	                            		delete applications[localReferenceId];
	                            		removeFromParentApp(localReferenceId);
                            		}
                            		catch(err){
                            			
                            		}
                            	}
                            }
                            
                            $("div[name='group_"+localReferenceId+"']").remove();
                        }
                        else if($("div[name='video_"+localReferenceId+"']").length>0)
                        {
                            writeToConsole('remove video with ref ' + localReferenceId);
                            $("div[name='video_"+localReferenceId+"']").remove();
                        }
                        else{writeToConsole('The image/group/video that you wanted to remove does not exist!');}
                    }
                    else
                        writeToConsole("Stopping REMOVE_FROM_ENTITY: ");
                        
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case SET_FLOAT:
                {
                    var preemptive_FloatId = instruction.getElementsByTagName("pfid")[0].firstChild.nodeValue;
                    var value = parseFloat(instruction.getElementsByTagName("v")[0].firstChild.nodeValue);
                    
                    
                    
                    writeToConsole('Set float instruction executed');
                    floats[preemptive_FloatId] = value;
                    
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case SET_GET_FLOAT:
                {
                    var preemptive_FloatId = instruction.getElementsByTagName("pfid")[0].firstChild.nodeValue;
                    var floatId = instruction.getElementsByTagName("fid")[0].firstChild.nodeValue;
                    if(floats[floatId]==null)
                    {
                        writeToErrorConsole("set get float Ins Error: floatId is pointing to null, now put 0 into it");
                        floats[floatId] = 0;
                    }
                    floats[preemptive_FloatId] = floats[floatId];
                    writeToConsole("setting float " + preemptive_FloatId +" to " + floats[floatId]);
                    
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case SET_GET_GLOBAL:
                {
                    var localReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                    var globals_localReferenceId = globals[localReferenceId];
                    var preemptiveLocalReferenceId = instruction.getElementsByTagName("plr")[0].firstChild.nodeValue;
                    writeToConsole("SET_GET_GLOBAL instruction called, copy local ref "+localReferenceId+" to preempt ref "+preemptiveLocalReferenceId);
                    if(globals_localReferenceId==null)
                    {
                        writeToErrorConsole("Global timespot  "+localReferenceId+" that you want to put into global timespot "+preemptiveLocalReferenceId+" does not exist!!! This is skipped");
                        globals[preemptiveLocalReferenceId]=null;
                    }
                    else
                    {
                        if(!stop)
                            globals[preemptiveLocalReferenceId]=globals_localReferenceId;
                        else
                            writeToLong("Stopping SET_GET_GLOBAL: ");
                    }
                    
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case SET_GLOBAL:
                {
                    var preemptiveLocalReferenceId = instruction.getElementsByTagName("plr")[0].firstChild.nodeValue;
                    var valueId = instruction.getElementsByTagName("vtid")[0].firstChild.nodeValue;
                    writeToConsole("set global instruction called, putting value "+valueId+" into global ref "+preemptiveLocalReferenceId);
                    if(!stop)
                        globals[preemptiveLocalReferenceId]=valueId;
                    else
                        writeToConsole("Stopping SET_GLOBAL: ");
                        
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case SET_GLOBAL_NULL:
                {
                    var localReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                    writeToConsole("SET_GLOBAL_NULL "+localReferenceId);
                    if(globals[localReferenceId]!=null)
                    {
                        if(!stop)
                            globals[localReferenceId]=null;
                        else
                            writeToConsole("Stopping SET_GLOBAL_NULL: ");
                    }
                    else{writeToConsole("The global timespot "+localReferenceId+" that you want to set null does not exist!!");}
                    
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case SET_TOUCH_CONDITION:
                {
                    var preemptiveLocalReferenceId = instruction.getElementsByTagName("plr")[0].firstChild.nodeValue;
                    var localReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                    if(globals[preemptiveLocalReferenceId]!=null){
                        preemptiveLocalReferenceId = globals[preemptiveLocalReferenceId];}
                    if(globals[localReferenceId]!=null)
                        localReferenceId = globals[localReferenceId];
                    writeToConsole('set touch on element with ref ' + localReferenceId);
                    var tid = instruction.getElementsByTagName("tid")[0].firstChild.nodeValue;
                    var sendSignal = instruction.getElementsByTagName("ss")[0].firstChild.nodeValue;
                    
                    
                    event_element[preemptiveLocalReferenceId]=localReferenceId;
                    
                    
                    
                    /*if(navigator.platform.indexOf("iPhone")!=-1 || navigator.platform.indexOf("iPod")!=-1 || navigator.platform.indexOf("iPad")!=-1 || navigator.platform.indexOf("Android")!=-1){
                    	event_type[preemptiveLocalReferenceId]='touchend';
                    }*/
                    
                    var pinchEvent = "";
                    var moveEvent = "";
                    
                    if(checkIfMobile()){
                    	pinchEvent = 'touchstart';
                    	event_type[preemptiveLocalReferenceId]='touchend';
                    	moveEvent = "touchmove";
                    }
                    else{
                        //event_type[preemptiveLocalReferenceId]='click';
                        pinchEvent = 'mousedown';
                        event_type[preemptiveLocalReferenceId]='mouseup';
                        moveEvent = "mousemove";
                    }
                    //event_type[preemptiveLocalReferenceId]='click';
                    
                    if(instruction.getElementsByTagName("tlX").length>0)
                    {
                        var topleftx_float = instruction.getElementsByTagName("tlX")[0].firstChild.nodeValue;
                        if(floats[topleftx_float]==null)
                        {
                            writeToErrorConsole("boundary set touch Ins Error: topLeftX_FloatId is pointing to null, now put 0 into it");
                            floats[topleftx_float] = 0;
                        }
                        var topLeftX = floats[topleftx_float];
                        var toplefty_float = instruction.getElementsByTagName("tlY")[0].firstChild.nodeValue;
                        if(floats[toplefty_float]==null)
                        {
                            writeToErrorConsole("boundary set touch Ins Error: topLeftY_FloatId is pointing to null, now put 0 into it");
                            floats[toplefty_float] = 0;
                        }
                        var topLeftY = floats[toplefty_float];
                        var bottomrightx_float = instruction.getElementsByTagName("brX")[0].firstChild.nodeValue;
                        if(floats[bottomrightx_float]==null)
                        {
                            writeToErrorConsole("boundary set touch Ins Error: bottomRightX_FloatId is pointing to null, now put 0 into it");
                            floats[bottomrightx_float] = 0;
                        }
                        var bottomRightX = floats[bottomrightx_float];
                        var bottomrighty_float = instruction.getElementsByTagName("brY")[0].firstChild.nodeValue;
                        if(floats[bottomrighty_float]==null)
                        {
                            writeToErrorConsole("boundary set touch Ins Error: bottomRightY_FloatId is pointing to null, now put 0 into it");
                            floats[bottomrighty_float] = 0;
                        }
                        var bottomRightY = floats[bottomrighty_float];
                        if($("div[name='image_"+localReferenceId+"']").length>0)
                        {
                            $("div[name='image_"+localReferenceId+"']").bind(event_type[preemptiveLocalReferenceId], function(e)
                            {
    //                            if the click is within the boundary, we execute the associated timespot
    //                            if(e.pageX>=((parseFloat($("div[name='image_"+localReferenceId+"']").css("left"))+($("div[name='image_"+localReferenceId+"']").width()/2))-topLeftX) && e.pageX<=((parseFloat($("div[name='image_"+localReferenceId+"']").css("left"))+($("div[name='image_"+localReferenceId+"']").width()/2))+bottomRightX) && e.pageY>=((parseFloat($("div[name='image_"+localReferenceId+"']").css("top"))+($("div[name='image_"+localReferenceId+"']").height()/2))-topLeftY) && e.pageY<=((parseFloat($("div[name='image_"+localReferenceId+"']").css("top"))+($("div[name='image_"+localReferenceId+"']").height()/2))+bottomRightY)){
                                e.stopPropagation();
                                var ratio = $(this).data("scale");
                                if(e.pageX>=(parseFloat($(this).offset().left)+($(this).data("width")/2)+(ratio*topLeftX)) && e.pageX<=(parseFloat($(this).offset().left)+($(this).data("width")/2)+(ratio*bottomRightX)) && e.pageY>=(parseFloat($(this).offset().top)+($(this).data("height")/2)+(ratio*topLeftY)) && e.pageY<=(parseFloat($(this).offset().top)+($(this).data("height")/2)+(ratio*bottomRightY)))
                                {
                                    alert("trigger boundary click on image ref "+localReferenceId);
                                    setTimerToExecuteTimespot("SET_TOUCH_CONDITION : image got touch, triggering timespot ref: ",tid);
                                }
                            });
                        }
                        else if($("div[name='group_"+localReferenceId+"']").length>0)
                        {
                            $("div[name='group_"+localReferenceId+"']").bind(event_type[preemptiveLocalReferenceId], function(e)
                            {
    //                            if the click is within the boundary, we execute the associated timespot
    //                            if(e.pageX>=((parseFloat($("div[name='group_"+localReferenceId+"']").css("left"))+($("div[name='group_"+localReferenceId+"']").width()/2))-topLeftX) && e.pageX<=((parseFloat($("div[name='group_"+localReferenceId+"']").css("left"))+($("div[name='group_"+localReferenceId+"']").width()/2))+bottomRightX) && e.pageY>=((parseFloat($("div[name='group_"+localReferenceId+"']").css("top"))+($("div[name='group_"+localReferenceId+"']").height()/2))-topLeftY) && e.pageY<=((parseFloat($("div[name='group_"+localReferenceId+"']").css("top"))+($("div[name='group_"+localReferenceId+"']").height()/2))+bottomRightY)){
                                e.stopPropagation();
                                var ratio = $(this).data("scale");
                                if(e.pageX>=(parseFloat($(this).offset().left)+($(this).data("width")/2)+(ratio*topLeftX)) && e.pageX<=(parseFloat($(this).offset().left)+($(this).data("width")/2)+(ratio*bottomRightX)) && e.pageY>=(parseFloat($(this).offset().top)+($(this).data("height")/2)+(ratio*topLeftY)) && e.pageY<=(parseFloat($(this).offset().top)+($(this).data("height")/2)+(ratio*bottomRightY)))
                                {
   //                                alert("trigger boundary click on group ref "+localReferenceId);
                                    setTimerToExecuteTimespot("SET_TOUCH_CONDITION : group got touch, triggering timespot ref: ",tid);
                                }
                            });
                        }
                        else if($("div[name='video_"+localReferenceId+"']").length>0)
                        {
                            $("div[name='video_"+localReferenceId+"']").bind(event_type[preemptiveLocalReferenceId], function(e)
                            {
    //                            if the click is within the boundary, we execute the associated timespot
                                if(e.pageX>=((parseFloat($("div[name='video_"+localReferenceId+"']").css("left"))+($("div[name='video_"+localReferenceId+"']").width()/2))-topLeftX) && e.pageX<=((parseFloat($("div[name='video_"+localReferenceId+"']").css("left"))+($("div[name='video_"+localReferenceId+"']").width()/2))+bottomRightX) && e.pageY>=((parseFloat($("div[name='video_"+localReferenceId+"']").css("top"))+($("div[name='video_"+localReferenceId+"']").height()/2))-topLeftY) && e.pageY<=((parseFloat($("div[name='video_"+localReferenceId+"']").css("top"))+($("div[name='video_"+localReferenceId+"']").height()/2))+bottomRightY))
                                {
                                    setTimerToExecuteTimespot("SET_TOUCH_CONDITION : video got touch, triggering timespot ref: ",tid);
                                }
                            });
                        }
                        else{writeToConsole('The image/group/video that you wanted to set a touch event does not exist!');}
                    }
                    else
                    {
                    	setTimeout(function checkElementExist5(){
	                        if($("div[name='image_"+localReferenceId+"']").length>0)
	                        {
	                        	if(!checkIfNull($("div[name='image_"+localReferenceId+"']").data("hasClick"))){
	                        		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	                        		return;
	                        	}
	                        	
	                        	$("div[name='image_"+localReferenceId+"']").css("pointer-events", "visible");
	                        	
	                        	$("div[name='image_"+localReferenceId+"']").data("hasClick", "1");
	                        	
	                        	
	                        	$("div[name='image_"+localReferenceId+"']").bind(pinchEvent, function(e){
	                        		
                        		    //e.preventDefault();
                        		    //e.stopPropagation();
                        		    
                        		    //alert("callback");
                        			
	                        		//e.preventDefault();
    		 						//e.stopPropagation();
	                        		var x = e.pageX || e.screenX;
		                            var y = e.pageY || e.screenY;
		                            
		                            //var x;
		                            //var y;
		                            
		                            var originalEvent = e.originalEvent;
		                            if(!checkIfNull(originalEvent)){
		                            	if(!checkIfNull(originalEvent.touches)){
		                            		if(originalEvent.touches.length>=2){
		                            			return;
		                            		}
		                            	}
		                            }
		                            
		                            		                            
		                            //$("div[name='image_"+localReferenceId+"']").data("touchStartTime", new Date().getTime());
		                            
		                            
	                        		$("div[name='image_"+localReferenceId+"']").bind(event_type[preemptiveLocalReferenceId], function(event2){
	                        			event2.preventDefault();
    		 						    event2.stopPropagation();
	                        			
	                        			$("div[name='image_"+localReferenceId+"']").unbind(event_type[preemptiveLocalReferenceId]);
	                        			
	                        		    
	                        			//if(new Date().getTime()-$("div[name='image_"+localReferenceId+"']").data("touchStartTime")<100){
	                        				
	                        				
	                        				
	                        				var endPinchEvent = "";
            
								            if(checkIfMobile()){
								            	endPinchEvent = 'touchend';
								            }
								            else{
								                endPinchEvent = 'mouseup';
								            }
											var anevent = new jQuery.Event(endPinchEvent);
											$(window).trigger(anevent);
											
											if(!checkIfNull($(window).data("pinching"))){
	                        					return;
	                        				}
	                        				
	                        				//It's a click!!
	                        				startPreloadingBands();
			                                //e.stopPropagation();
			                                //var x = e.pageX || e.screenX;
				                            //var y = e.pageY || e.screenY;
				                            //alert("image push null");
				                            
				                            
				                            if(checkIfNull(x) || checkIfNull(y)){
				                            	x = $(document).data("clickX");
				                            	y = $(document).data("clickY");
				                            }
				                            
				                            $(document).data("clickX", null);
				                            $(document).data("clickY", null);
				                            
				                            
				                            var anevent = null;
				                            
				                            var iAndAndroidDevices = false;
									        var ua = navigator.userAgent.toLowerCase();
									        
									        if(ua.indexOf("iphone")!=-1 || ua.indexOf("ipad")!=-1 || ua.indexOf("ipod")!=-1 || ua.indexOf("android")!=-1){
									        	iAndAndroidDevices = true;
									        }
				                            
								            
							            	if(iAndAndroidDevices){
							            		anevent = new jQuery.Event('touchend');
							            	}
							            	else{
							            		anevent = new jQuery.Event('mouseup');
							            	}
							            	
							            	anevent.pageX = x;
				            				anevent.pageY = y;
				            				$(document).trigger(anevent);
				            				
				            				
			                                
			                                
			                                var executeOrNot = true;
			                                //if($(document).data("draggingElement")!=null || canClick[localReferenceId]==false){
			                                if((!checkIfNull($(document).data("draggingElement"))) || canClick[localReferenceId]==false){
			                                	executeOrNot = false;
			                                	//if($(document).data("draggingElement")==this){
			                                	//	executeOrNot = false;
			                                	//}
			                                	//else if($($(document).data("draggingElement")).find("div[name='image_"+localReferenceId+"']").length>0){
			                                	//	executeOrNot = false;
			                                	//}
			                                }
			                                if(executeOrNot){
			                                	//camera.getHistory().push(null);
			                                	//lastBackPinch = false;
			                                	var backFunc = new function(){
			                                		this.isSwipe = function(){
											    		return false;
											    	}
											    	this.isPinch = function(){
											    		return false;
											    	}
											    	this.execBack = function(){
				                                		escapeItem = $("div[name='image_"+localReferenceId+"']");
										            	checkEscape();
									            	}
			                                	};
			                                	backStack.push(backFunc);
			                                	
			                                	
			                                	canClick[localReferenceId] = false;
			                                	
			                                	//setTimeout(function(){new checkReceiveEventFromChild($("div[name='image_"+localReferenceId+"']"), parentTimespot);}, 0);
			                                	var execFunc = function(){new executeTimespot(tid, parentTimespot)};
			                                	camera.backToCenter(execFunc);
			                                	//checkReceiveEventFromChild($("div[name='image_"+localReferenceId+"']"), parentTimespot, x, y);
			                                	checkReceiveEventFromChild2($("div[name='image_"+localReferenceId+"']"), parentTimespot, EVENT_TYPE_CLICK, x, y, x, y);
			                                	
			                                	/*check event from child
			                                	var currentParent = null;
			                                	var currentEntity = $("div[name='image_"+localReferenceId+"']");
			                                	var timespotsToExecute = new Array();
			                                	var initialAppArray = new Array();
			                                	var notEndSearch = true;
			                                	
			                                	while(notEndSearch){
			                                		var currentEntityString = currentEntity.attr("id");
			                                		var currentEntityId = currentEntityString.substring(6, currentEntity.attr("id").length);
			                                		
			                                		if(currentEntityString=="group_0"){
			                                		    break;
			                                	    }
			                                	    else{
			                                	    	if(typeof applications_child_event[currentEntityId] == "undefined" || applications_child_event[currentEntityId]==null){
			                                	    		if(currentParent==null){
			                                	    			initialAppArray.push(currentEntityId);
			                                	    		}
								        					currentEntity = currentEntity.parent();
								        				}
								        				else{
								        					if(currentParent==null){
								        						for(var i=0; i<initialAppArray.length; i++){
								        							if(applications_child_event[currentEntityId][initialAppArray[i]]!="undefined" && applications_child_event[currentEntityId][initialAppArray[i]]!=null){
								        								timespotsToExecute.push(applications_child_event[currentEntityId][initialAppArray[i]]['tid']);
								        								if(applications_child_event[currentEntityId][initialAppArray[i]]['pu']=="0"){
								        									notEndSearch = false;
								        								}
								        								break;
								        							}
								        						}
								        						currentParent = currentEntityId;
								        						currentEntity = currentEntity.parent();
								        					}
								        					else{
								        						if(applications_child_event[currentEntityId][currentParent]!="undefined" && applications_child_event[currentEntityId][currentParent]!=null){
							        								timespotsToExecute.push(applications_child_event[currentEntityId][currentParent]['tid']);
							        								if(applications_child_event[currentEntityId][currentParent]['pu']=="0"){
							        									notEndSearch = false;
							        								}
							        								break;
							        							}
							        							currentParent = currentEntityId;
								        						currentEntity = currentEntity.parent();
								        					}
								        				}
			                                	    }
			                                	}
			                                	
			                                	for (var i = 0; i< timespotsToExecute.length; i++) {
							            			new executeTimespot(timespotsToExecute[i], parentTimespot);
						            			}
			                                	
			                                	
			                                	*/
			                                	
			                                	
				                                //new executeTimespot(tid, parentTimespot);
				                                //setTimeout(function(){new executeTimespot(tid, parentTimespot);}, 0);
				                                //new executeTimespot(tid, parentTimespot);
				                                
				                                function imgClickSendSignal(){
					                                //Testing only
					                                var test_request = makeRequest();
												        
											    	if(test_request)
											        {
											            test_request.onreadystatechange = function(){
											            	    if(test_request.readyState == 2){
											            	    	var cl = parseFloat(test_request.getResponseHeader("Content-Length"));
													            	if(!isNaN(cl)){
													            	    bandwidthUsed += cl;
													        	    }
												        		    //bandwidthUsed += parseFloat(test_request.getResponseHeader("Content-Length"));
											        			}
											            	    else if(test_request.readyState == 4){
											            	    	var cl = parseFloat(test_request.getResponseHeader("Content-Length"));
													            	if(!isNaN(cl)){
													            	    bandwidthUsed -= cl;
													        	    }
											            	    	//bandwidthUsed -= parseFloat(test_request.getResponseHeader("Content-Length"));
											            	    	if (test_request.status == 200){
											            	    		if(test_request.getResponseHeader("Content-Length")){
											            	    			if(parseFloat(test_request.getResponseHeader("Content-Length"))>=0){
											            	    				//setTimeout(function(){canClick[localReferenceId] = true;}, 2000);
											            	    				setTimeout(function(){
											            	    					var parser = new DOMParser();
											            	    					var t = unob(test_request.responseText, false);
											            	    					storeTimespot(parser.parseFromString(t, "text/xml"));
											            	    					//storeTimespot(parser.parseFromString(test_request.responseText, "text/xml"));
											            	    				}, 0);
											            	    			}
											            	    		}
											            	    		//if(test_request.getResponseHeader("Content-Length")=="0"){
											            	    		//	setTimeout(function(){canClick[localReferenceId] = true;}, 2000);
											            	    		//	setTimeout(function(){new getInstruction("");}, refresh_rate);
											            	    		//}
											            	    		else{
											            	    			setTimeout(function(){new imgClickSendSignal();}, conn_err_wait);
											            	    			//new imgClickSendSignal();
											            	    		}
										            	    		}
										            	    		else{
										            	    			setTimeout(function(){new imgClickSendSignal();}, conn_err_wait);
										            	    			//new imgClickSendSignal();
										            	    		}
										            	    	}
											            	};
											            	
											            //test_request.ontimeout = function(){
											        	//	setTimeout(function(){new imgClickSendSignal();}, conn_err_wait);
											        	//};
											        	//test_request.onerror = function(){
											        	//	setTimeout(function(){new imgClickSendSignal();}, conn_err_wait);
											        	//};
											            
											            test_request.open("GET", "/sendSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+localReferenceId+"?eventType="+EVENT_TYPE_CLICK, true);
											            test_request.overrideMimeType('text/plain; charset=x-user-defined');
											            test_request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
											            test_request.send();
											            //setTimeout(function(){new getInstruction();}, refresh_rate);
											        }
										        }
										        if(sendSignal=="1"){
										        	//setTimeout(function(){new getInstruction("/sendSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+localReferenceId+"?eventType="+EVENT_TYPE_CLICK);}, 0);
										            imgClickSendSignal();
									            }
									            setTimeout(function(){canClick[localReferenceId] = true;}, 2000);
			                                }
	                        			//}
	                        			/*else{
	                        				if(!checkIfNull($(document).data("dragStarted"))){
	                        					return;
	                        				}
	                        				
	                        				//It's a pinch!!
	                        				var entityLocalReferenceId = localReferenceId;
						                    if(globals[entityLocalReferenceId]!=null)
						                    {
						                        entityLocalReferenceId = globals[entityLocalReferenceId];
						                    }
						                    
						                    //var occupyPercentage = 1;
						                    
						                    
						                    
						                    
						                    //remember previous camera location!
						                    
						                    
						            		
						                    
					                    	var parentTest = $("div[name='image_"+entityLocalReferenceId+"']").parent();
					                    	while(true){
					                    		var parentEntityLF = parentTest.attr("name").substring(6, parentTest.attr("name").length);
					                    		
					                    		if(!checkIfNull(applications[parentEntityLF])){
					                    			entityLocalReferenceId = parentEntityLF;
					                    			break;
					                    		}
					                    		parentTest = parentTest.parent();
					                    	}
					                    	
					                    	
				                      	    var parent = $("div[name='group_"+entityLocalReferenceId+"']");
				                      	    
				                      	    //var occupyPercentage = camera.getScale()*parseFloat((new Date().getTime()-$("div[name='image_"+localReferenceId+"']").data("touchStartTime"))/1000);
				                      	    
				                      	    
				                      	  
				                      	    if(parent.data("awaiting_imgs")!=null){
				                      	        if(parseInt(parent.data("awaiting_imgs"))>0){
				                      	            if(!stop){
				                      	          	    setTimeout(function(){
				                  	            		    new checkPinchElementExist();
				                  	            	    }, 300);
					                      	        }
				                      	        }
				                      	        else{
				                      	      	    tempAccScales = new Array();
				                      	      	
				                                  
				                                    var accumulatedScale = calAccScale(parent);
				                                    var accumulatedScale2 = accumulatedScale*$("div[name='group_0']").data("scale");
				                                    
				                                    var occupyPercentage = accumulatedScale*parseFloat(((new Date().getTime()-$("div[name='image_"+localReferenceId+"']").data("touchStartTime"))/1000)+1)*1.5;
				                                  
				                                    tempAccScales = new Array();
				                                  
				                                    var currentEntity = $("div[name='group_"+entityLocalReferenceId+"']");
				                                  
				                                    
				                                  
				                                    var tempElementX = parseFloat(currentEntity.offset().left);
			                                        var tempElementY = parseFloat(currentEntity.offset().top);
			                                      
			                                      
				                                  
				                                    var width = 1000*accumulatedScale2;
				                                    var height = 1000*accumulatedScale2;
				                                  
				                                    var factor = occupyPercentage/accumulatedScale;
				                                    //var factor = ($("div[name='group_0']").data("scale")*1000*occupyPercentage)/(1000*accumulatedScale2/currentEntity.data("scale"));
				                                  
				                                  
				                                    //var gc = new globalCamera();
				                                    var base_ratio = camera.getScale();
				                                    
				                                    var ratio = factor*base_ratio;
				                                   
				                                    //entityToZoom.scale(1/accumulatedScale);
				                                   
				                                    var final_ratio = ratio/base_ratio;
				                                 
				                                      
				                                    var x_orig = camera.getX();
				                                    var y_orig = camera.getY();
					                                
					                              
					                                var x_dist = (($("div[name='group_0']").offset().left+(1000*$("div[name='group_0']").data("scale")/2))-(width/2)-tempElementX)/(accumulatedScale2/accumulatedScale);
					                                x_dist = (x_dist)*(final_ratio)+(x_orig*(ratio-base_ratio)/base_ratio);
					                              
						                            var y_dist = (($("div[name='group_0']").offset().top+(1000*$("div[name='group_0']").data("scale")/2))-(height/2)-tempElementY)/(accumulatedScale2/accumulatedScale);
						                            y_dist = (y_dist)*(final_ratio)+(y_orig*(ratio-base_ratio)/base_ratio);
					                              
					                              
							                        //alert("width "+width+" tempElementX "+tempElementX+" accumulatedScale2 "+accumulatedScale2+" accumulatedScale "+accumulatedScale);
							                      
							                      //entityToZoom.data("moving", parentTimespot.getId());
							                      
							                      
							                     new executeTimespot(tid, parentTimespot);
							                      
							                      
							                     function imgClickSendSignal(){
					                                //Testing only
					                                var test_request = makeRequest();
												        
											    	if(test_request)
											        {
											            test_request.onreadystatechange = function(){
											            	    if(test_request.readyState == 2){
												        		    bandwidthUsed += parseFloat(test_request.getResponseHeader("Content-Length"));
											        			}
											            	    else if(test_request.readyState == 4){
											            	    	bandwidthUsed -= parseFloat(test_request.getResponseHeader("Content-Length"));
											            	    	if (test_request.status == 200){
											            	    		if(test_request.getResponseHeader("Content-Length")){
											            	    			if(parseFloat(test_request.getResponseHeader("Content-Length"))>=0){
											            	    				setTimeout(function(){
											            	    					var parser = new DOMParser();
											            	    					storeTimespot(parser.parseFromString(test_request.responseText, "text/xml"));
											            	    				}, 0);
											            	    			}
											            	    		}
											            	    		else{
											            	    			new imgClickSendSignal();
											            	    		}
										            	    		}
										            	    		else{
										            	    			new imgClickSendSignal();
										            	    		}
										            	    	}
											            	};
											            test_request.onerror = function(){
											            	new imgClickSendSignal();
											        	};
											            test_request.open("GET", "/sendSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+localReferenceId+"?eventType="+EVENT_TYPE_CLICK, true);
											            
											            test_request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
											            test_request.send();
											            //setTimeout(function(){new getInstruction();}, refresh_rate);
											        }
										        }
										        if(sendSignal=="1"){
										        	//setTimeout(function(){new getInstruction("/sendSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+localReferenceId+"?eventType="+EVENT_TYPE_CLICK);}, 0);
										            imgClickSendSignal();
									            }
							                      
							                      
							                      // fix here
							                      camera.moveCamera(ratio, x_dist, y_dist, true, true);
					                          }
				                      	  }
						                      
					                    	
					                    	
					                    	
					                      	if($("div[name='image_"+entityLocalReferenceId+"']").length>0)
						                      {
						                      	  
						                      	
						                      	
						                      	
						                      	
						                      	  var parent = $("div[name='image_"+entityLocalReferenceId+"']");
						                      	  
						                      	  
						                      	      
				                      	      	  tempAccScales = new Array();
				                      	      	  								            		    	  
				                      	      	
						                          //var ratio;
					                        	  //if(parent.parent().data("original_width")!=null){
					                              //    ratio = radius/full_screen_scale;
				                                  //}
					                              //else{
					                              //    ratio = (radius/full_screen_scale)*(Math.min(PLAYGROUND_WIDTH,PLAYGROUND_HEIGHT)/full_screen_scale);
				                                  //}
				                                  
				                                  
				                                  var accumulatedScale = calAccScale(parent);
				                                  var accumulatedScale2 = accumulatedScale*$("div[name='group_0']").data("scale");
				                                  
				                                  tempAccScales = new Array();
				                                  
				                                  var currentEntity = $("div[name='image_"+entityLocalReferenceId+"']");
				                                  
				                                  
				                                  var entityToZoom = $("div[name='group_00']");
				                                  //var entityToZoom = $($("div[name='group_0']").find("div")[0]);
				                                  
				                                  var tempElementX = parseFloat(currentEntity.offset().left);
			                                      var tempElementY = parseFloat(currentEntity.offset().top);
			                                      
			                                      
				                                  
				                                  var width = currentEntity.data("original_width")*accumulatedScale2;
				                                  var height = currentEntity.data("original_height")*accumulatedScale2;
				                                  
				                                  
				                                  
				                                  
				                                  var factor = ($("div[name='group_0']").data("scale")*1000*occupyPercentage)/(1000*accumulatedScale2/parent.data("scale"));
				                                  
				                                  
				                                  //var ratio = 1/accumulatedScale;
				                                  var base_ratio = entityToZoom.data("scale");
				                                  var ratio = factor*base_ratio;
				                                   
				                                  //entityToZoom.scale(1/accumulatedScale);
				                                   
				                                  var final_ratio = ratio/base_ratio;
				                                 
				                                  //alert("factor "+factor+" base_ratio "+base_ratio+" final_ratio "+final_ratio);                                
				                                  		                                  
			                                      var x_orig = parseFloat(entityToZoom.css("left"));
					                              var y_orig = parseFloat(entityToZoom.css("top"));
					                              
					                              
					                              
					                              var x_dist = (($("div[name='group_0']").offset().left+(1000*$("div[name='group_0']").data("scale")/2))-(width/2)-tempElementX)/(accumulatedScale2/accumulatedScale);
					                              x_dist = (x_dist)*(final_ratio)+(x_orig*(ratio-base_ratio)/base_ratio);
					                              
						                          var y_dist = (($("div[name='group_0']").offset().top+(1000*$("div[name='group_0']").data("scale")/2))-(height/2)-tempElementY)/(accumulatedScale2/accumulatedScale);
						                          y_dist = (y_dist)*(final_ratio)+(y_orig*(ratio-base_ratio)/base_ratio);
					                              
					                              
							                      
							                      
							                      entityToZoom.data("moving", parentTimespot.getId());
							                      
							                      var start = new Date().getTime();
						                          
						                          var hasMoreTime = true;
						                          
						                          
						                          setTimeout(function pinchItem(){
							                          if(hasMoreTime){
							                              var elapsed = new Date().getTime()-start;
							                              if(elapsed<time_allowed)
							                              {
							                                  
							                              	  setTimeout(function(){
							                              	  	if(speedUp){
						                              	    		entityToZoom.backToNormalTransform(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)), x_orig+((elapsed/time_allowed)*x_dist), y_orig+((elapsed/time_allowed)*y_dist));
							                                    
						                                        }
						                                        else{
								                                    entityToZoom.css({
																	    left: x_orig+((elapsed/time_allowed)*x_dist),
																	    top: y_orig+((elapsed/time_allowed)*y_dist)
																	});
							                                    }
								                              }, 0);
							                                  
							                                  
							                                  
							                                  setTimeout(function(){
							                                      if(ratio!=base_ratio){
							                                      	  if(!speedUp){
							                                              entityToZoom.scale(base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
						                                              }
							                                      	
								                                      
								                                      
								                                      entityToZoom.data("scale", base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)));
								                                      entityToZoom.data("width", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*entityToZoom.data("original_width"));
								                                      entityToZoom.data("height", (base_ratio+((elapsed/time_allowed)*(ratio-base_ratio)))*entityToZoom.data("original_height"));
								                                  }
								                                  if(entityToZoom.data("moving")!= parentTimespot.getId())
								                                  {
								                                      hasMoreTime = false;
								                                  }
								                                  else{
								                                  	  if(stop){
								                                      	   hasMoreTime = false;
													                  }
													                  else{
													                  	  new pinchItem();
							                                          }
								                                  }
							                                  }, 0);
							                              }
							                              else
							                              {
							                              	  hasMoreTime = false;
							                              	  setTimeout(function(){
							                              	  	  if(speedUp){
					                              	        	      entityToZoom.backToNormalTransform(ratio, x_orig+x_dist, y_orig+y_dist);
				                              	        	      }
				                              	        	      else{
				                              	        	      	  entityToZoom.css({
																	      left: x_orig+x_dist,
																	      top: y_orig+y_dist
																	  });
								                                  }
								                              }, 0);  
							                                  
							                                  
							                                  
							                                  setTimeout(function(){
							                                  	  if(ratio!=base_ratio){
							                                  	  	  if(!speedUp){
							                                  	          entityToZoom.scale(ratio);
						                                  	          }
								                                  	  
								                                      
								                                      entityToZoom.data("scale", ratio);
								                                      entityToZoom.data("width", ratio*entityToZoom.data("original_width"));
								                                      entityToZoom.data("height", ratio*entityToZoom.data("original_height"));
								                                  }
							                                  }, 0);
							                                  
							                              }
						                              }
						                          }, 0);
						                      	  
						                      }
						                      
						                      else{
					                      	  	  setTimeout(function(){
				                      	    		  new checkPinchElementExist();
				                      	    	  }, 300);
					                      	  }
						                    
	                        			}*/
                        			});
                        			
                        			
                        			
                        			/*if(checkIfMobile()){
							    		$(window).bind("touchstart", {test: af}, function(te){
							    			$(window).bind("touchend", function(touchEndEvent){
							    				$(window).unbind("touchstart");
							    				$(window).unbind("touchend");
							    			});
							    			
							    			
							    			
							    			if(checkIfNull(te.originalEvent)){
							    				te.data.test(te);
							    			}
							    			else{
								    			if(te.originalEvent.touches.length==2){
								    				te.data.test(te);
								    			}
							    			}
							    		});
							    	}
							    	else{
							    		$(window).bind("mousedown", {test: af}, function(me){
							    			$(window).unbind("mousedown");
						    				me.data.test(me);
							    		});
							    	}
							    	
							    	
	                        		if(checkIfNull(ee.originalEvent)){
	                        		    $(window).trigger(pinchEvent);
                        		    }
                        		    else{
                        		    	if(checkIfNull(ee.originalEvent.touches)){
                        		    		$(window).trigger(pinchEvent);
                        		    	}
                        		    	else{
							    			if(ee.originalEvent.touches.length==1){
							    				$(window).trigger(pinchEvent);
							    			}
						    			}
					    			}
	                        	});*/
		                        
		                        
	                        	

	                            /*$("div[name='image_"+localReferenceId+"']").bind(event_type[preemptiveLocalReferenceId], function(e)
	                            {
	                            	
	                            	startPreloadingBands();
	                                e.stopPropagation();
	                                var x = e.pageX || e.screenX;
		                            var y = e.pageY || e.screenY;
		                            
		                            if(checkIfNull(x) || checkIfNull(y)){
		                            	x = $(document).data("clickX");
		                            	y = $(document).data("clickY");
		                            }
		                            
		                            $(document).data("clickX", null);
		                            $(document).data("clickY", null);
		                            
		                            var anevent = null;
		                            
		                            var iAndAndroidDevices = false;
							        var ua = navigator.userAgent.toLowerCase();
							        
							        if(ua.indexOf("iphone")!=-1 || ua.indexOf("ipad")!=-1 || ua.indexOf("ipod")!=-1 || ua.indexOf("android")!=-1){
							        	iAndAndroidDevices = true;
							        }
		                            
					            	if(iAndAndroidDevices){
					            		anevent = new jQuery.Event('touchend');
					            	}
					            	else{
					            		anevent = new jQuery.Event('mouseup');
					            	}
					            	
					            	anevent.pageX = x;
		            				anevent.pageY = y;
		            				$(document).trigger(anevent);
		            				
		            				
	                                
	                                
	                                var executeOrNot = true;
	                                if($(document).data("draggingElement")!=null || canClick[localReferenceId]==false){
	                                	executeOrNot = false;
	                                }
	                                if(executeOrNot){
	                                	canClick[localReferenceId] = false;
	                                	
	                                	
	                                	new checkReceiveEventFromChild($("div[name='image_"+localReferenceId+"']"), parentTimespot, x, y);
	                                	
	                                	
		                                new executeTimespot(tid, parentTimespot);
		                                
		                                function imgClickSendSignal(){
			                                //Testing only
			                                var test_request = makeRequest();
										        
									    	if(test_request)
									        {
									            test_request.onreadystatechange = function(){
									            	    if(test_request.readyState == 2){
										        		    bandwidthUsed += parseFloat(test_request.getResponseHeader("Content-Length"));
									        			}
									            	    else if(test_request.readyState == 4){
									            	    	bandwidthUsed -= parseFloat(test_request.getResponseHeader("Content-Length"));
									            	    	if (test_request.status == 200){
									            	    		if(test_request.getResponseHeader("Content-Length")){
									            	    			if(parseFloat(test_request.getResponseHeader("Content-Length"))>=0){
									            	    				setTimeout(function(){canClick[localReferenceId] = true;}, 2000);
									            	    				setTimeout(function(){
									            	    					var parser = new DOMParser();
									            	    					storeTimespot(parser.parseFromString(test_request.responseText, "text/xml"));
									            	    				}, 0);
									            	    			}
									            	    		}
									            	    		else{
									            	    			new imgClickSendSignal();
									            	    		}
								            	    		}
								            	    		else{
								            	    			new imgClickSendSignal();
								            	    		}
								            	    	}
									            	};
									            test_request.onerror = function(){
									            	new imgClickSendSignal();
									        	};
									            test_request.open("GET", "/sendSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+localReferenceId+"?eventType="+EVENT_TYPE_CLICK, true);
									            
									            test_request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
									            test_request.send();
									        }
								        }
								        if(sendSignal=="1"){
								            imgClickSendSignal();
							            }
	                                }
	                                
	                            }); */
	                            });
	                            executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	                        }
	                        else if($("div[name='group_"+localReferenceId+"']").length>0)
	                        {
	                        	if(!($("div[name='group_"+localReferenceId+"']").data("hasClick")==null || typeof($("div[name='group_"+localReferenceId+"']").data("hasClick"))=="undefined")){
	                        		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	                        		return;
	                        	}
	                        	
	                        	$("div[name='group_"+localReferenceId+"']").css("pointer-events", "visible");
	                        	$("div[name='group_"+localReferenceId+"']").data("hasClick", "1");
	                        	
	                        	
	                        	$("div[name='group_"+localReferenceId+"']").bind(pinchEvent, function(e){
	                        		//e.preventDefault();
    		 						//e.stopPropagation();
    		 						
    		                        var x = e.pageX || e.screenX;
		                            var y = e.pageY || e.screenY;
    		                        //$("div[name='group_"+localReferenceId+"']").data("touchStartTime", new Date().getTime());
    		                        
    		                        $("div[name='group_"+localReferenceId+"']").bind(event_type[preemptiveLocalReferenceId], function(event2){
    		                        	event2.preventDefault();
    		 						    event2.stopPropagation();
    		                        	$("div[name='group_"+localReferenceId+"']").unbind(event_type[preemptiveLocalReferenceId]);
    		                        	
    		                        	
    		                        	
    		                        	//if(new Date().getTime()-$("div[name='group_"+localReferenceId+"']").data("touchStartTime")<10000){
    		                        		
	                        				
	                        				var endPinchEvent = "";
            
								            if(checkIfMobile()){
								            	endPinchEvent = 'touchend';
								            }
								            else{
								                endPinchEvent = 'mouseup';
								            }
											var anevent = new jQuery.Event(endPinchEvent);
											$(window).trigger(anevent);
											
											if(!checkIfNull($(window).data("pinching"))){
	                        					return;
	                        				}
	                        				
    		                        		
    		                        		startPreloadingBands();
    		                        		//alert("group push null");
    		                        		
    		                        		
    		                        		if(checkIfNull(x) || checkIfNull(y)){
						                    	x = $(document).data("clickX");
						                    	y = $(document).data("clickY");
						                    }
						                    $(document).data("clickX", null);
                                            $(document).data("clickY", null);
                                            
                                            var anevent = null;
		                            
				                            var iAndAndroidDevices = false;
									        var ua = navigator.userAgent.toLowerCase();
									        
									        if(ua.indexOf("iphone")!=-1 || ua.indexOf("ipad")!=-1 || ua.indexOf("ipod")!=-1 || ua.indexOf("android")!=-1){
									        	iAndAndroidDevices = true;
									        }
				                            
								            
							            	if(iAndAndroidDevices){
							            		anevent = new jQuery.Event('touchend');
							            	}
							            	else{
							            		anevent = new jQuery.Event('mouseup');
							            	}
							            	anevent.pageX = x;
				            				anevent.pageY = y;
				            				$(document).trigger(anevent);
				            				
				            				var executeOrNot = true;
				            				/*if(canClick[localReferenceId]==false){
			                                	executeOrNot = false;
			                                }
			                                
			                                if($(document).data("draggingElement")!=null){
			                                	if($(document).data("draggingElement")==this){
			                                		executeOrNot = false;
			                                	}
			                                	else if($($(document).data("draggingElement")).find("div[name='group_"+localReferenceId+"']").length>0){
			                                		executeOrNot = false;
			                                	}
			                                }*/
			                                
			                                if((!checkIfNull($(document).data("draggingElement"))) || canClick[localReferenceId]==false){
			                                	executeOrNot = false;
		                                	}
			                                
		                                	
			                                if(executeOrNot){
			                                	//camera.getHistory().push(null);
			                                	//lastBackPinch = false;
			                                	var backFunc = new function(){
			                                		this.isSwipe = function(){
											    		return false;
											    	}
											    	this.isPinch = function(){
											    		return false;
											    	}
											    	this.execBack = function(){
				                                		escapeItem = $("div[name='group_"+localReferenceId+"']");
										            	checkEscape();
									            	}
			                                	};
			                                	backStack.push(backFunc);
			                                	
			                                	/*$("div[id='back']").unbind("click");
	        
									            $("div[id='back']").bind("click", function(e){
									            	e.preventDefault();
									            	e.stopPropagation();
									                            
									                $("div[id='back']").unbind("click");
									            	
									                escapeItem = $("div[name='group_"+localReferenceId+"']");
									            	checkEscape();
									            });*/
									            
			                                	canClick[localReferenceId] = false;
			                                	
			                                	//setTimeout(function(){new checkReceiveEventFromChild($("div[name='group_"+localReferenceId+"']"), parentTimespot);}, 0);
			                                	var execFunc = function(){new executeTimespot(tid, parentTimespot)};
			                                	camera.backToCenter(execFunc);
			                                	//checkReceiveEventFromChild($("div[name='group_"+localReferenceId+"']"), parentTimespot, x, y);
			                                	checkReceiveEventFromChild2($("div[name='group_"+localReferenceId+"']"), parentTimespot, EVENT_TYPE_CLICK, x, y, x, y);
			                                	
				                                //new executeTimespot(tid, parentTimespot);
				                                //setTimeout(function(){new executeTimespot(tid, parentTimespot);}, 0);
				                                
				                                //new executeTimespot(tid, parentTimespot);
				                                
				                                function grpClickSendSignal(){
					                                //Testing only
					                                var test_request = makeRequest();
			
											        
											    	if(test_request)
											        {
											            test_request.onreadystatechange = function(){
											            	    if(test_request.readyState == 2){
											            	    	var cl = parseFloat(test_request.getResponseHeader("Content-Length"));
													            	if(!isNaN(cl)){
													            	    bandwidthUsed += cl;
													        	    }
												        		    //bandwidthUsed += parseFloat(test_request.getResponseHeader("Content-Length"));
											        			}
											            	    else if(test_request.readyState == 4){
											            	    	var cl = parseFloat(test_request.getResponseHeader("Content-Length"));
													            	if(!isNaN(cl)){
													            	    bandwidthUsed -= cl;
													        	    }
											            	    	//bandwidthUsed -= parseFloat(test_request.getResponseHeader("Content-Length"));
											            	    	if (test_request.status == 200){
											            	    		if(test_request.getResponseHeader("Content-Length")){
											            	    			if(parseFloat(test_request.getResponseHeader("Content-Length"))>=0){
											            	    				//setTimeout(function(){canClick[localReferenceId] = true;}, 2000);
											            	    				setTimeout(function(){
											            	    					var parser = new DOMParser();
											            	    					var t = unob(test_request.responseText, false);
											            	    					storeTimespot(parser.parseFromString(t, "text/xml"));
											            	    					//storeTimespot(parser.parseFromString(test_request.responseText, "text/xml"));
											            	    				}, 0);
										            	    				}
									            	    				}
											            	    		
											            	    		//if(test_request.getResponseHeader("Content-Length")=="0"){
											            	    		//	setTimeout(function(){canClick[localReferenceId] = true;}, 2000);
											            	    		//	setTimeout(function(){new getInstruction("");}, refresh_rate);
											            	    		//}
											            	    		else{
											            	    			setTimeout(function(){new grpClickSendSignal();}, conn_err_wait);
											            	    			//new grpClickSendSignal();
											            	    		}
										            	    		}
										            	    		else{
										            	    			setTimeout(function(){new grpClickSendSignal();}, conn_err_wait);
										            	    			//new grpClickSendSignal();
										            	    		}
										            	    	}
											            	};
											            	
											            //test_request.ontimeout = function(){
											        	//	setTimeout(function(){new grpClickSendSignal();}, conn_err_wait);
											        	//};
											        	//test_request.onerror = function(){
											        	//	setTimeout(function(){new grpClickSendSignal();}, conn_err_wait);
											        	//};
											            
											            test_request.open("GET", "/sendSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+localReferenceId+"?eventType="+EVENT_TYPE_CLICK, true);
											            test_request.overrideMimeType('text/plain; charset=x-user-defined');
											            test_request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
											            test_request.send();
											            //setTimeout(function(){new getInstruction();}, refresh_rate);
											        }
										        }
										        if(sendSignal=="1"){
										            grpClickSendSignal();
									            }
									            setTimeout(function(){canClick[localReferenceId] = true;}, 2000);
								        	}
		                        		//}
		                        		/*else{
		                        			if(!checkIfNull($(document).data("dragStarted"))){
	                        					return;
	                        				}
		                        			
		                        			//it's a pinch
		                        			var entityLocalReferenceId = localReferenceId;
						                    if(globals[entityLocalReferenceId]!=null)
						                    {
						                        entityLocalReferenceId = globals[entityLocalReferenceId];
						                    }
						                    //var occupyPercentage = 1;
						                    	                    
						                    
						                    
						                    
						                    	
				                      	    var parent = $("div[name='group_"+entityLocalReferenceId+"']");
				                      	  
				                      	    //var occupyPercentage = camera.getScale()*parseFloat((new Date().getTime()-parent.data("touchStartTime"))/1000);
				                      	    
				                      	        
		                      	      	    tempAccScales = new Array();
		                                  
		                                  
		                                    var accumulatedScale = calAccScale(parent);
		                                    var accumulatedScale2 = accumulatedScale*$("div[name='group_0']").data("scale");
		                                  
		                                    var occupyPercentage = accumulatedScale*parseFloat(((new Date().getTime()-parent.data("touchStartTime"))/1000)+1)*1.5;
		                                    
		                                    tempAccScales = new Array();
		                                  
		                                    var currentEntity = $("div[name='group_"+entityLocalReferenceId+"']");
		                                  
		                                  
		                                   
		                                    var tempElementX = parseFloat(currentEntity.offset().left);
	                                        var tempElementY = parseFloat(currentEntity.offset().top);
	                                      
	                                      
		                                  
		                                    var width = 1000*accumulatedScale2;
		                                    var height = 1000*accumulatedScale2;
		                                  
		                                    var factor = occupyPercentage/accumulatedScale;
		                                    //var factor = ($("div[name='group_0']").data("scale")*1000*occupyPercentage)/(1000*accumulatedScale2/currentEntity.data("scale"));
		                                  
		                                  
		                                    //var ratio = 1/accumulatedScale;
		                                    var base_ratio = camera.getScale();
		                                    var ratio = factor*base_ratio;
		                                   
		                                    
		                                   
		                                    var final_ratio = ratio/base_ratio;
		                                 
		                                                                 
		                                  		                                  
	                                        var x_orig = camera.getX();
		                                    var y_orig = camera.getY();
			                              
			                              
			                              
			                                var x_dist = (($("div[name='group_0']").offset().left+(1000*$("div[name='group_0']").data("scale")/2))-(width/2)-tempElementX)/(accumulatedScale2/accumulatedScale);
			                                x_dist = (x_dist)*(final_ratio)+(x_orig*(ratio-base_ratio)/base_ratio);
			                              
				                            var y_dist = (($("div[name='group_0']").offset().top+(1000*$("div[name='group_0']").data("scale")/2))-(height/2)-tempElementY)/(accumulatedScale2/accumulatedScale);
				                            y_dist = (y_dist)*(final_ratio)+(y_orig*(ratio-base_ratio)/base_ratio);
			                              
				                            
				                            new executeTimespot(tid, parentTimespot);
				                            
				                            function grpClickSendSignal(){
				                                //Testing only
				                                var test_request = makeRequest();
		
										        
										    	if(test_request)
										        {
										            test_request.onreadystatechange = function(){
										            	    if(test_request.readyState == 2){
											        		    bandwidthUsed += parseFloat(test_request.getResponseHeader("Content-Length"));
										        			}
										            	    else if(test_request.readyState == 4){
										            	    	bandwidthUsed -= parseFloat(test_request.getResponseHeader("Content-Length"));
										            	    	if (test_request.status == 200){
										            	    		if(test_request.getResponseHeader("Content-Length")){
										            	    			if(parseFloat(test_request.getResponseHeader("Content-Length"))>=0){
										            	    				setTimeout(function(){
										            	    					var parser = new DOMParser();
										            	    					storeTimespot(parser.parseFromString(test_request.responseText, "text/xml"));
										            	    				}, 0);
									            	    				}
								            	    				}
										            	    		
										            	    		//if(test_request.getResponseHeader("Content-Length")=="0"){
										            	    		//	setTimeout(function(){canClick[localReferenceId] = true;}, 2000);
										            	    		//	setTimeout(function(){new getInstruction("");}, refresh_rate);
										            	    		//}
										            	    		else{
										            	    			new grpClickSendSignal();
										            	    		}
									            	    		}
									            	    		else{
									            	    			new grpClickSendSignal();
									            	    		}
									            	    	}
										            	};
										            	
										            test_request.onerror = function(){
										            	new grpClickSendSignal();
										        	};
										            test_request.open("GET", "/sendSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+localReferenceId+"?eventType="+EVENT_TYPE_CLICK, true);
										            
										            test_request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
										            test_request.send();
										            //setTimeout(function(){new getInstruction();}, refresh_rate);
										        }
									        }
									        if(sendSignal=="1"){
									           grpClickSendSignal();
								            }
				                            
			                              
					                        camera.moveCamera(ratio, x_dist, y_dist, true, true);
		                        		}*/
		                        	});
                        		});
	                        	
	                        	
	                        	
	                            /*$("div[name='group_"+localReferenceId+"']").bind(event_type[preemptiveLocalReferenceId], function(e)
	                            //$("div[name='group_"+localReferenceId+"']").click(function(e)
	                            {
	                            	startPreloadingBands();
	                                e.stopPropagation();
	                                
	                                
	                                
	                                var x = e.pageX || e.screenX;
		                            var y = e.pageY || e.screenY;
		                            if(checkIfNull(x) || checkIfNull(y)){
		                            	x = $(document).data("clickX");
		                            	y = $(document).data("clickY");
		                            }
		                            
		                            
		                            $(document).data("clickX", null);
		                            $(document).data("clickY", null);
		                            
		                            var anevent = null;
		                            
		                            var iAndAndroidDevices = false;
							        var ua = navigator.userAgent.toLowerCase();
							        
							        if(ua.indexOf("iphone")!=-1 || ua.indexOf("ipad")!=-1 || ua.indexOf("ipod")!=-1 || ua.indexOf("android")!=-1){
							        	iAndAndroidDevices = true;
							        }
		                            
						            
					            	if(iAndAndroidDevices){
					            		anevent = new jQuery.Event('touchend');
					            	}
					            	else{
					            		anevent = new jQuery.Event('mouseup');
					            	}
					            	anevent.pageX = x;
		            				anevent.pageY = y;
		            				$(document).trigger(anevent);
					            	
	                                
	//                                alert("trigger whole element click on group ref "+localReferenceId);
	
	                                var executeOrNot = true;
	                                if(canClick[localReferenceId]==false){
	                                	executeOrNot = false;
	                                }
	                                if($(document).data("draggingElement")!=null){
	                                	if($(document).data("draggingElement")==this){
	                                		executeOrNot = false;
	                                	}
	                                	else if($($(document).data("draggingElement")).find("div[name='group_"+localReferenceId+"']").length>0){
	                                		executeOrNot = false;
	                                	}
	                                }
	                                if(executeOrNot){
	                                	canClick[localReferenceId] = false;
	                                	
	                                	//setTimeout(function(){new checkReceiveEventFromChild($("div[name='group_"+localReferenceId+"']"), parentTimespot);}, 0);
	                                	new checkReceiveEventFromChild($("div[name='group_"+localReferenceId+"']"), parentTimespot, x, y);
	                                	
		                                //new executeTimespot(tid, parentTimespot);
		                                //setTimeout(function(){new executeTimespot(tid, parentTimespot);}, 0);
		                                new executeTimespot(tid, parentTimespot);
		                                
		                                function grpClickSendSignal(){
			                                //Testing only
			                                var test_request = makeRequest();
	
									        
									    	if(test_request)
									        {
									            test_request.onreadystatechange = function(){
									            	    if(test_request.readyState == 2){
										        		    bandwidthUsed += parseFloat(test_request.getResponseHeader("Content-Length"));
									        			}
									            	    else if(test_request.readyState == 4){
									            	    	bandwidthUsed -= parseFloat(test_request.getResponseHeader("Content-Length"));
									            	    	if (test_request.status == 200){
									            	    		if(test_request.getResponseHeader("Content-Length")){
									            	    			if(parseFloat(test_request.getResponseHeader("Content-Length"))>=0){
									            	    				setTimeout(function(){canClick[localReferenceId] = true;}, 2000);
									            	    				setTimeout(function(){
									            	    					var parser = new DOMParser();
									            	    					storeTimespot(parser.parseFromString(test_request.responseText, "text/xml"));
									            	    				}, 0);
								            	    				}
							            	    				}
									            	    		
									            	    		//if(test_request.getResponseHeader("Content-Length")=="0"){
									            	    		//	setTimeout(function(){canClick[localReferenceId] = true;}, 2000);
									            	    		//	setTimeout(function(){new getInstruction("");}, refresh_rate);
									            	    		//}
									            	    		else{
									            	    			new grpClickSendSignal();
									            	    		}
								            	    		}
								            	    		else{
								            	    			new grpClickSendSignal();
								            	    		}
								            	    	}
									            	};
									            	
									            test_request.onerror = function(){
									            	new grpClickSendSignal();
									        	};
									            test_request.open("GET", "/sendSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+localReferenceId+"?eventType="+EVENT_TYPE_CLICK, true);
									            
									            test_request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
									            test_request.send();
									            //setTimeout(function(){new getInstruction();}, refresh_rate);
									        }
								        }
								        if(sendSignal=="1"){
								            grpClickSendSignal();
							            }
						        	}
	                                
	                            });*/
	                            executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	                        }
	                        else if($("div[name='video_"+localReferenceId+"']").length>0)
	                        {
	                        	if(!($("div[name='video_"+localReferenceId+"']").data("hasClick")==null || typeof($("div[name='video_"+localReferenceId+"']").data("hasClick"))=="undefined")){
	                        		executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	                        		return;
	                        	}
	                        	
	                        	$("div[name='video_"+localReferenceId+"']").css("pointer-events", "visible");
	                        	$("div[name='video_"+localReferenceId+"']").data("hasClick", "1");
	                        	
	                        	
	                        	$("div[name='video_"+localReferenceId+"']").bind(event_type[preemptiveLocalReferenceId], function(e)
	                            {
	                            	startPreloadingBands();
	                                e.stopPropagation();
	                                var x = e.pageX || e.screenX;
		                            var y = e.pageY || e.screenY;
		                            
		                            
		                            var endPinchEvent = "";
            
						            if(checkIfMobile()){
						            	endPinchEvent = 'touchend';
						            }
						            else{
						                endPinchEvent = 'mouseup';
						            }
									var anevent = new jQuery.Event(endPinchEvent);
									$(window).trigger(anevent);
									
									if(!checkIfNull($(window).data("pinching"))){
                    					return;
                    				}
		                            
		                            
					            	
					            	
					            	
					            	
					            	if(iAndAndroidDevices){
					            		anevent = new jQuery.Event('touchend');
					            	}
					            	else{
					            		anevent = new jQuery.Event('mouseup');
					            	}
					            	anevent.pageX = x;
		            				anevent.pageY = y;
		            				$(document).trigger(anevent);
		            				
		            				
	                                
	                                
	                                var executeOrNot = true;
	                                if(canClick[localReferenceId]==false){
	                                	executeOrNot = false;
	                                }
	                                if($(document).data("draggingElement")!=null){
	                                	if($(document).data("draggingElement")==this){
	                                		executeOrNot = false;
                                		}
                                		else if($($(document).data("draggingElement")).find("div[name='video_"+localReferenceId+"']").length>0){
	                                	    executeOrNot = false;
                                	    }
	                                }
	                                if(executeOrNot){
	                                	//lastBackPinch = false;
	                                	var backFunc = new function(){
	                                		this.isSwipe = function(){
									    		return false;
									    	}
									    	this.isPinch = function(){
									    		return false;
									    	}
									    	this.execBack = function(){
		                                		escapeItem = $("div[name='video_"+localReferenceId+"']");
								            	checkEscape();
							            	}
	                                	};
	                                	backStack.push(backFunc);
	                                	
	                                	canClick[localReferenceId] = false;
	                                	
	                                	//setTimeout(function(){new checkReceiveEventFromChild($("div[name='video_"+localReferenceId+"']"), parentTimespot);}, 0);
	                                	//checkReceiveEventFromChild($("div[name='video_"+localReferenceId+"']"), parentTimespot, x, y);
	                                	checkReceiveEventFromChild2($("div[name='video_"+localReferenceId+"']"), parentTimespot, EVENT_TYPE_CLICK, x, y, x, y);
	                                	
		                                //new executeTimespot(tid, parentTimespot);
		                                //setTimeout(function(){new executeTimespot(tid, parentTimespot);}, 0);
		                                new executeTimespot(tid, parentTimespot);
		                                
		                                function vidClickSendSignal(){
			                                //Testing only
			                                var test_request = makeRequest();
										        
									    	if(test_request)
									        {
									            test_request.onreadystatechange = function(){
									            	    if(test_request.readyState == 2){
									            	    	var cl = parseFloat(test_request.getResponseHeader("Content-Length"));
											            	if(!isNaN(cl)){
											            	    bandwidthUsed += cl;
											        	    }
										        		    //bandwidthUsed += parseFloat(test_request.getResponseHeader("Content-Length"));
									        			}
									            	    else if(test_request.readyState == 4){
									            	    	var cl = parseFloat(test_request.getResponseHeader("Content-Length"));
											            	if(!isNaN(cl)){
											            	    bandwidthUsed -= cl;
											        	    }
									            	    	//bandwidthUsed -= parseFloat(test_request.getResponseHeader("Content-Length"));
									            	    	if (test_request.status == 200){
									            	    		if(test_request.getResponseHeader("Content-Length")){
									            	    			if(parseFloat(test_request.getResponseHeader("Content-Length"))>=0){
									            	    				setTimeout(function(){canClick[localReferenceId] = true;}, 2000);
									            	    				setTimeout(function(){
									            	    					var parser = new DOMParser();
									            	    					var t = unob(test_request.responseText, false);
											            	    			storeTimespot(parser.parseFromString(t, "text/xml"));
									            	    					//storeTimespot(parser.parseFromString(test_request.responseText, "text/xml"));
									            	    				}, 0);
								            	    				}
							            	    				}
									            	    		
									            	    		//if(test_request.getResponseHeader("Content-Length")=="0"){
									            	    		//	setTimeout(function(){canClick[localReferenceId] = true;}, 2000);
									            	    		//	setTimeout(function(){new getInstruction("");}, refresh_rate);
									            	    		//}
									            	    		else{
									            	    			setTimeout(function(){new vidClickSendSignal();}, conn_err_wait);
									            	    			//new vidClickSendSignal();
									            	    		}
								            	    		}
								            	    		else{
								            	    			setTimeout(function(){new vidClickSendSignal();}, conn_err_wait);
								            	    			//new vidClickSendSignal();
								            	    		}
								            	    	}
									            	};
									            	
									            //test_request.ontimeout = function(){
									        	//	setTimeout(function(){new vidClickSendSignal();}, conn_err_wait);
									        	//};
									        	//test_request.onerror = function(){
									        	//	setTimeout(function(){new vidClickSendSignal();}, conn_err_wait);
									        	//};
									            
									            test_request.open("GET", "/sendSignal?sessionId="+sessionId+"?new_id="+new_id+"?old_id="+old_id+"?localRef="+localReferenceId+"?eventType="+EVENT_TYPE_CLICK, true);
									            test_request.overrideMimeType('text/plain; charset=x-user-defined');
									            test_request.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
									            test_request.send();
									        }
								        }
								        if(sendSignal=="1"){
								            vidClickSendSignal();
							            }
	                                }
	                            });
	                        	executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
	                        	
	                            //$("div[name='video_"+localReferenceId+"']").bind(event_type[preemptiveLocalReferenceId], setTimerToExecuteTimespot("SET_TOUCH_CONDITION : whole video got touch, triggering timespot ref: ",timespot_id));
	                        }
	                        else{
	                            writeToErrorConsole('The image/group that you wanted to set a touch event on the whole element does not exist!');
	                            if(!stop){
                    				setTimeout(function(){
              	            		    new checkElementExist5();
              	            	    }, 250);
                  	            }
                            }
                        }, 0);
                    }
                    //executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case SLEEP:
                {
                    var seconds = instruction.getElementsByTagName("sec")[0].firstChild.nodeValue;
                    seconds = parseFloat(seconds);
                    /*if(floats[seconds]!=null)
                    {
                        seconds = floats[seconds];
                    }
                    else{
                    	seconds = parseFloat(seconds);
                    }*/
                    
                    writeToConsole("SLEEP: timestarted = " +new Date().getTime());
                    
                    atimer = setTimeout(function()
                    {
                        writeToConsole("SLEEP: timestopped = " +new Date().getTime());
                        executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    }, seconds*1000);
//                    seconds = seconds * 1000;
//                    var sleeping = true;
//                    var startingMSeconds = new Date().getTime();
//                    while(sleeping)
//                    {
//                        if(new Date().getTime() - startingMSeconds > seconds){
//                            sleeping = false; }
//                    }
                    break;
                }
                case STOP_GLOBAL:
                {
                    var localReferenceId = instruction.getElementsByTagName("lr")[0].firstChild.nodeValue;
                    if(globals[localReferenceId]!=null){
                        var globals_localReferenceId = globals[localReferenceId];}
                    else{
                        var globals_localReferenceId = localReferenceId;}
                    writeToErrorConsole("STOP_GLOBAL: "+localReferenceId+ " / " +globals_localReferenceId+" called by " + parentTimespot.getId());
                    
                    stopTimespot(globals_localReferenceId);
                    
                    /*if(globals_localReferenceId!=null)
                    {
                        if(running_timespots[globals_localReferenceId]!=null)
                        {
                            while(running_timespots[globals_localReferenceId].length>0){
                                running_timespots[globals_localReferenceId].pop().stopExecution();
                            }
                            running_timespots[globals_localReferenceId] = null;
                        }
                    }
                    else{
                    	writeToConsole("you are trying to stop global timespot "+localReferenceId+" which does not exist");
                	}*/
                    
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                case STOP_TIMESPOT:
                {
                    var tid = instruction.getElementsByTagName("tid")[0].firstChild.nodeValue;
                    writeToErrorConsole("STOP_TIMESPOT: "+tid +" called by " + parentTimespot.getId());
                    
                    
                    stopTimespot(tid);
                    
                    /*if(running_timespots[tid]!=null)
                    {
                        while(running_timespots[tid].length>0)
                        {
                            running_timespots[tid].pop().stopExecution();
                        }
                        running_timespots[tid] = null;
                    }*/
                    
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
                default:
                {
                    writeToErrorConsole("wrong instruction code"+ instruction.getAttribute("ins_id"));
                    executeNextInstruction(noi, instructionList, timespot_id, parentArray, parentTimespot, stop);
                    break;
                }
            }
        }
    }
    
});



//ppdrag
(function($) {
	
	function calAccScale(element){
		//if(element.attr("id")=="group_0"){
		//	return element.data("scale");
		//}
		var parent = element.parent();
		
		if(parent.attr("id")=="group_0"){
			return element.data("scale");
		}
		else{
			var parentName = parent.attr("name").substring(6, parent.attr("name").length);
			if(tempAccScales[parentName]!=null || typeof(tempAccScales[parentName])!="undefined"){
				return tempAccScales[parentName];
			}
		    else{
			    return element.data("scale")*calAccScale(element.parent());
		    }
		}
	}
	
	function checkIfMobile(){
        var ua = navigator.userAgent.toLowerCase();
        
        if(ua.indexOf("iphone")!=-1 || ua.indexOf("ipad")!=-1 || ua.indexOf("ipod")!=-1 || ua.indexOf("android")!=-1){
        	return true;
        }
        return false;
    }
	
	$.fn.ppdrag = function(options) {
		options = $.extend({
			er:                       "0",
			cer:                      "-1",
            axis:		              "xy",
            isStationary:             true,
            isEdgeLockedToBoundary:   false,
            destroy:                  "no"
        }, options);
        
        
        var iAndAndroidDevices = false;
        var ua = navigator.userAgent.toLowerCase();
        
        
        
        if(ua.indexOf("iphone")!=-1 || ua.indexOf("ipad")!=-1 || ua.indexOf("ipod")!=-1 || ua.indexOf("android")!=-1){
        	iAndAndroidDevices = true;
        }
       
		
		
		//if (typeof options == 'string') {
	    if(options.destroy=="yes" && options.er==options.cer){ return this.each(function() {
			//if (options == 'destroy') return this.each(function() {
				
				/*if(navigator.platform.indexOf("iPhone")!=-1 || navigator.platform.indexOf("iPod")!=-1 || navigator.platform.indexOf("iPad")!=-1 || navigator.platform.indexOf("Android")!=-1){
				    $(this).unbind('touchstart', $.ppdrag.start);
				}*/
				if(iAndAndroidDevices){
					$(this).unbind('touchstart', $.ppdrag.start);
				}
				else{
				    $(this).unbind('mousedown', $.ppdrag.start);
				}
				
				//$.ppdrag.removeEvent(this, 'mousedown', $.ppdrag.start, false);
				$(this).data('pp-ppdrag', null);
			});
		}
		return this.each(function() {
			$(this).data('pp-ppdrag', { options: $.extend({}, options) });


			/*if(navigator.platform.indexOf("iPhone")!=-1 || navigator.platform.indexOf("iPod")!=-1 || navigator.platform.indexOf("iPad")!=-1 || navigator.platform.indexOf("Android")!=-1){
			    $(this).bind('touchstart.', {element: this}, $.ppdrag.start);
			}*/
			
			
			
			
			if(iAndAndroidDevices){
				$(this).bind('touchstart.', {element: this}, $.ppdrag.start);
			}
			else{
			    $(this).bind('mousedown', {element: this}, $.ppdrag.start);
			}
			
			//custom event
			//$(this).bind('mydrag_start', {element: this}, $.ppdrag.start);

			
			//$.ppdrag.addEvent(this, 'mousedown', $.ppdrag.start, false);
		});
	};
	
	$.ppdrag = {
		start: function(event) {
			
			
			
			if(event.originalEvent.touches!=null && typeof(event.originalEvent.touches)!="undefined"){
				if(event.originalEvent.touches.length>1){
	        	    return;
        	    }
	        }
	        
	        /*if($(document).data("draggingElement")!=null && typeof($(document).data("draggingElement"))!="undefined"){
	        	var anevent;
				if(checkIfMobile()){
					anevent = new jQuery.Event('touchend');
				}
				else{
					anevent = new jQuery.Event('mouseup');
				}
				$(window).trigger(anevent);
	        	return;
	        }*/
	        
			
	        //$(document).data("dragStarted", 1);
	        
			
			var e = null;
			if(event.originalEvent){
				if(event.originalEvent.targetTouches){
					if(event.originalEvent.targetTouches[0]){
				        e = event.originalEvent.targetTouches[0];
			        }
			        else{
			        	e = event;
			        }
			    }
			    else{
			    	e = event;
			    }
			}
			else{
				e = event;
			}
			
			var current = event.data.element;
			
			$(current).data("dragStartTime", new Date().getTime());
			$(document).data("clickX", e.pageX || e.screenX);
			$(document).data("clickY", e.pageY || e.screenY);
			
			
			
			
			$(current).data("dragNotLock", true);
			$(document).data("noOfMoves", 0);
			
			
			$(current).data("aleft", parseFloat($(current).offset().left));
			$(current).data("atop", parseFloat($(current).offset().top));
			
			$(current).data("oleft", parseInt(current.style.left) || 0);
			$(current).data("otop",  parseInt(current.style.top) || 0);
			$(current).data("ox",  e.pageX || e.screenX);
			$(current).data("oy",  e.pageY || e.screenY);
			$(current).data("lastox",  $(event.data.element).data("ox"));
			$(current).data("lastoy",  $(event.data.element).data("oy"));
			
			
			//$.fn.el = event.data.element;
	        //$.fn.oleft = parseInt(event.data.element.style.left) || 0;
	        //$.fn.otop = parseInt(event.data.element.style.top) || 0;
	        //$.fn.ox = e.pageX || e.screenX;
	        //$.fn.oy = e.pageY || e.screenY;
			
			
			/*if($(document).data("dragStack")==null){
				//console.log("#GNWBFIEBWFEBWUFEW");
				
				$(document).data("dragStack", new Array());
				$(event.data.element).data("visibility", $(event.data.element).css("visibility"));
                $(event.data.element).css("visibility", "hidden");
                $(document).data("dragStack").push(event.data.element);
                var just_a_cond = true;
                
                
	            
	            
                
                while(just_a_cond){
	                //console.log("#GRWGEWRGHERG");
	                var x = e.pageX || e.screenX;
		            var y = e.pageY || e.screenY;
		            var anevent = null;
		            if(navigator.platform.indexOf("iPhone")!=-1 || navigator.platform.indexOf("Android")!=-1){
		            	anevent = new jQuery.Event('touchstart');
	            	}
	            	else{
	            		anevent = new jQuery.Event('mousedown');
	            	}
		            
		            
		            anevent.pageX = x;
		            anevent.pageY = y;
		            
		            var element = document.elementFromPoint(x, y);
		            
		            
		            
		            if(element!=null && typeof element !="undefined")
		            {
		                if(element.getAttribute("id")!='playground' && element.getAttribute("id")!='startbutton' && element.getAttribute("id")!='sceengraph')
		                {
		                	//console.log("#GHEIURGEHRGER");
		                    
		                    
		                    $(element).data("visibility", $(element).css("visibility"));
			                $(element).css("visibility", "hidden");
			                $(document).data("dragStack").push(element);
		                    //$(document.getElementById('playground')).trigger(anevent);
		                    //$.playground().trigger(anevent);
		                }
		                else{
		                	//console.log("#NIHUNERGBNERBN");
		                	//$(document.getElementById('playground')).trigger(anevent);
		                	var dragEventStack = $(document).data("dragStack");
				        	if(dragEventStack!=null){
					            while(dragEventStack.length>0)
					            {
					                var popElement = dragEventStack.pop();
					                //console.log("#FFFFFF "+$(popElement).data("visibility"));
					                $(popElement).css("visibility", $(popElement).data("visibility"));
					                if(dragEventStack.length>0){
					                	$(popElement).trigger(anevent);
					                }
					                else{
					                	//do nothing
					                }
					                
					                
					            }
					            $(document).data("dragStack", null);
					            just_a_cond = false;
					            //console.log("#NUHFBEWBFEWBUFE");
				            }
		                }
		            }
	            }
			}*/
			
			
			//console.log("#NMBGBNERGNERGER");
			
			
			
			var data = $(current).data('pp-ppdrag');
			
			
			
			/*if(navigator.platform.indexOf("iPhone")!=-1 || navigator.platform.indexOf("iPod")!=-1 || navigator.platform.indexOf("iPad")!=-1 || navigator.platform.indexOf("Android")!=-1){
				$(document).bind('touchend', {element: event.data.element}, $.ppdrag.stop);
				$(document).bind('touchmove', {element: event.data.element}, $.ppdrag.drag);
				//$(current).bind('touchend', {element: event.data.element}, $.ppdrag.stop);
				//$(current).bind('touchmove', {element: event.data.element}, $.ppdrag.drag);
			}*/
			if(checkIfMobile()){
				$(document).bind('touchend', {element: event.data.element}, $.ppdrag.stop);
				$(document).bind('touchmove', {element: event.data.element}, $.ppdrag.drag);
			}
			else{
			    $(document).bind('mouseup', {element: event.data.element}, $.ppdrag.stop);
			    $(document).bind('mousemove', {element: event.data.element}, $.ppdrag.drag);
			    //$(current).bind('mouseup', {element: event.data.element}, $.ppdrag.stop);
			    //$(current).bind('mousemove', {element: event.data.element}, $.ppdrag.drag);
		    }
				
				
			
			
			
			/*if (!$.ppdrag.current) {
				$.ppdrag.current = { 
					el: this,
					oleft: parseInt(this.style.left) || 0,
					otop: parseInt(this.style.top) || 0,
					ox: e.pageX || e.screenX,
					oy: e.pageY || e.screenY
				};
				var current = $.ppdrag.current;
				var data = $.data(current.el, 'pp-ppdrag');
				if (data.options.zIndex) {
					current.zIndex = current.el.style.zIndex;
					current.el.style.zIndex = data.options.zIndex;
				}
				
				
				if(navigator.platform.indexOf("iPhone")!=-1 || navigator.platform.indexOf("Android")!=-1){
					$(document).bind('touchend', {element: event.data.element}, $.ppdrag.stop);
					$(document).bind('touchmove', {element: event.data.element}, $.ppdrag.drag);
				}
				else{
				    $(document).bind('mouseup', {element: event.data.element}, $.ppdrag.stop);
				    $(document).bind('mousemove', {element: event.data.element}, $.ppdrag.drag);
			    }
				
				
				//$.ppdrag.addEvent(document, 'mouseup', $.ppdrag.stop, true);
				//$.ppdrag.addEvent(document, 'mousemove', $.ppdrag.drag, true);
			}*/
			
			
			//if(!data.options.isStationary){
			    //if (event.stopPropagation) 
			      //  event.stopPropagation();
		    //}
			if (event.preventDefault)
			    event.preventDefault();
			    
			
			//$(window).trigger(event);    
			
			//if(!data.options.isStationary){
			//	$(window).trigger(event);    
			    //return false;
		    //}
		    //return false;
		},
		
		drag: function(event) {
			
			function interruptDrag(){
				var iAndAndroidDevices = false;
		        var ua = navigator.userAgent.toLowerCase();
		        
		        
		        if(ua.indexOf("iphone")!=-1 || ua.indexOf("ipad")!=-1 || ua.indexOf("ipod")!=-1 || ua.indexOf("android")!=-1){
		        	iAndAndroidDevices = true;
		        }
				
				
				var current = event.data.element;
				var data = $(current).data('pp-ppdrag');
				
				
				if(iAndAndroidDevices){
					$(document).unbind('touchmove');
					$(document).unbind('touchend');
				}
				else{
				    $(document).unbind('mousemove');
				    $(document).unbind('mouseup');
			    }
				
				
				if (data.options.stop) {
					data.options.stop.apply(current, [ current ]);
				}
				
				
				$(document).data("draggingElement", null);
		        //$(document).data("dragStarted", null);
				
				
				if (event.stopPropagation) 
				    event.stopPropagation();
				
				if (event.preventDefault)
				   event.preventDefault();
	            
	            $(document).data("clickX", null);
				$(document).data("clickY", null);
	            $(document).data("noOfMoves", null);
			}
			var current = event.data.element;
			var originalEvent = event.originalEvent;
			
		    if($(window).data("pinching")!=null && typeof($(window).data("pinching"))!="undefined"){
		    	interruptDrag();
			    return;
		    }		
			if(originalEvent){
				if(originalEvent.touches){
					if(originalEvent.touches.length>=2){
						//stop drag here
						interruptDrag();
			            return;
					}
				}
			}
			
			$(window).data("notStopPreloadBand", false);
			
		    var noOfMoves = $(document).data("noOfMoves");
		    noOfMoves++;
		    
			$(document).data("noOfMoves", noOfMoves);
			
			if(noOfMoves>=3){
				if($(document).data("draggingElement")==null || typeof($(document).data("draggingElement"))=="undefined"){
				    $(document).data("draggingElement", current);
			    }
			    //}
				/*var anevent;
				if(checkIfMobile()){
					anevent = new jQuery.Event('touchend');
				}
				else{
					anevent = new jQuery.Event('mouseup');
				}
				$(window).trigger(anevent);*/
			}
			
			if (!event) var event = window.event;
			
			
			var e = null;
			if(originalEvent){
				if(originalEvent.targetTouches){
					if(originalEvent.targetTouches[0]){
				        e = originalEvent.targetTouches[0];
			        }
			        else{
			        	e = event;
			        }
			    }
			    else{
			    	e = event;
			    }
			}
			else{
				e = event;
			}
			
			var current = event.data.element;
			var data = $(current).data('pp-ppdrag');
			
			//if($(current).data("dragStart")==null || $(current).data("dragStart")=="undefined"){
			//	$(current).data("dragStart", new Date().getTime())
			//}
			
			//var current = $.ppdrag.current;
			
			//var data = $.data(current.el, 'pp-ppdrag');
			
			
			
			//if($(document).data("draggingElement")==null){
			  //  $(document).data("draggingElement", current);
		    //}
		    
		    
		    /*var accumulatedScale = 1;
            var currentEntity = $(current);
          
            while(true){
        	    
        	    accumulatedScale = accumulatedScale * currentEntity.data("scale");
        	    
        	    if(currentEntity.attr("id")=="group_0"){
        		    break;
        	    }
        	    
        	  
        	    currentEntity = currentEntity.parent();
            }*/
            
		    //alert(accumulatedScale);
		    
		    //if(new Date().getTime()-$(current).data("dragStart")>100){
		    	
		    $(current).data("lastox",  e.pageX || e.screenX);
		    $(current).data("lastoy",  e.pageY || e.screenY);
		    	
		    if(!data.options.isStationary){
			    if(data.options.axis == 'x'){
			    	/*if(speedUp){
			    		//current.style.left = ($(current).data("oleft") + (e.pageX || e.screenX) - $(current).data("ox")) + 'px';
			    		if($(current).data("dragNotLock")){
			    			$(current).data("dragNotLock", false);
			    		    $(current).offset({left: $(current).data("aleft") + (((e.pageX || e.screenX) - $(current).data("ox"))/$(current).parent().data("scale")) });
			    		    $(current).data("dragNotLock", true);
		    		    }
		    		}*/
		    		//else{
		    			if($(current).data("dragNotLock")){
		    				//appBandCheckedTracer = new Array();
			    			setTimeout(function(){
			    				
			    					$(current).data("dragNotLock", false);
			    				    //$(current).offset({left: $(current).data("aleft") + (((e.pageX || e.screenX) - $(current).data("ox"))/$(current).parent().data("scale")) });
			    				    var baseGroup =  $("div[name='group_0']");
					    		    var ratio = calAccScale($(current).parent());
					    		    
					    		    
					    		    $(current).css({
									    left: $(current).data("oleft") + ((e.pageX || e.screenX) - $(current).data("ox"))/ratio/baseGroup.data("scale")
									});
			    				    $(current).data("dragNotLock", true);
		    				    
			    				//current.style.left = ($(current).data("oleft") + (e.pageX || e.screenX) - $(current).data("ox")) + 'px';
			    			}, 0);
			    		}
		    		//}
			    }
			    else if(data.options.axis == 'y'){
			    	/*if(speedUp){
			    		//current.style.top = ($(current).data("otop") + (e.pageY || e.screenY) - $(current).data("oy")) + 'px';
			    		if($(current).data("dragNotLock")){
			    			$(current).data("dragNotLock", false);
			    		    $(current).offset({top: $(current).data("atop") + (((e.pageY || e.screenY) - $(current).data("oy"))/$(current).parent().data("scale")) });
			    		    $(current).data("dragNotLock", true);
			    		}
			    		
			    	}*/
			    	//else{
			    		if($(current).data("dragNotLock")){
			    			//appBandCheckedTracer = new Array();
				    		setTimeout(function(){
					    		//current.style.top = ($(current).data("otop") + (e.pageY || e.screenY) - $(current).data("oy")) + 'px';
					    		
					    			$(current).data("dragNotLock", false);
					    		    //$(current).offset({top: $(current).data("atop") + (((e.pageY || e.screenY) - $(current).data("oy"))/$(current).parent().data("scale")) });
					    		    var baseGroup =  $("div[name='group_0']");
					    		    var ratio = calAccScale($(current).parent());
					    		    
					    		    
					    		    $(current).css({
									    top: $(current).data("otop") + ((e.pageY || e.screenY) - $(current).data("oy"))/ratio/baseGroup.data("scale")
									});
					    		    $(current).data("dragNotLock", true);
					    		
					    		//$(current).transform2($(current).data("scale"), 0, ((e.pageY || e.screenY) - $(current).data("oy"))/accumulatedScale);
					    		
					    	}, 0);
					    }
			    	//}
			    	
			    }
			    else{
			    	/*if(!speedUp){
			    		if($(current).data("dragNotLock")){
			    			$(current).data("dragNotLock", false);
			    		    $(current).offset({top: $(current).data("atop") + (((e.pageY || e.screenY) - $(current).data("oy"))/$(current).parent().data("scale")) , left: $(current).data("aleft") + (((e.pageX || e.screenX) - $(current).data("ox"))/$(current).parent().data("scale")) });
			    		    $(current).data("dragNotLock", true);
			    		}
			    		
			    		//$(current).css({
						//    left: $(current).data("oleft") + (e.pageX || e.screenX) - $(current).data("ox"),
						//    top: $(current).data("otop") + (e.pageY || e.screenY) - $(current).data("oy")
						//});
		    		}*/
		    		//else{
		    			if($(current).data("dragNotLock")){
		    				//appBandCheckedTracer = new Array();
					    	setTimeout(function(){
					    		
					    			$(current).data("dragNotLock", false);
					    		    //$(current).offset({top: $(current).data("atop") + (((e.pageY || e.screenY) - $(current).data("oy"))/$(current).parent().data("scale")) , left: $(current).data("aleft") + (((e.pageX || e.screenX) - $(current).data("ox"))/$(current).parent().data("scale")) });
					    		    
					    		    
					    		    var baseGroup =  $("div[name='group_0']");
					    		    var ratio = calAccScale($(current).parent());
					    		    //current.style.left = ($(current).data("oleft") + ((e.pageX || e.screenX) - $(current).data("ox"))/ratio/eee.data("scale")) + 'px';
					    		    //current.style.top = ($(current).data("otop") + ((e.pageY || e.screenY) - $(current).data("oy"))/ratio/eee.data("scale")) + 'px';
					    		    
					    		    $(current).css({
									    left: $(current).data("oleft") + ((e.pageX || e.screenX) - $(current).data("ox"))/ratio/baseGroup.data("scale"),
									    top: $(current).data("otop") + ((e.pageY || e.screenY) - $(current).data("oy"))/ratio/baseGroup.data("scale")
									});
					    		    
					    		    $(current).data("dragNotLock", true);
				    		    
					    		
						    	//current.style.left = ($(current).data("oleft") + (e.pageX || e.screenX) - $(current).data("ox")) + 'px';
						    	//current.style.top = ($(current).data("otop") + (e.pageY || e.screenY) - $(current).data("oy")) + 'px';
					    	}, 0);
					    }
			    	//}
			    }
			    //$(current).data("dragStart", null);
		    }
		    
		    
		    
		    
		    /*if(data.options.axis == 'x'){
			    $(current).el.style.left = ($(current).oleft + (e.pageX || e.screenX) - $(current).ox) + 'px';
		    }
		    else if(data.options.axis == 'y'){
		    	$(current).el.style.top = ($(current).otop + (e.pageY || e.screenY) - $(current).oy) + 'px';
		    }
		    else{
		    	$(current).el.style.left = ($(current).oleft + (e.pageX || e.screenX) - $(current).ox) + 'px';
		    	$(current).el.style.top = ($(current).otop + (e.pageY || e.screenY) - $(current).oy) + 'px';
		    }*/
	        
			
			
			/*if(data.options.axis == 'x'){
			    current.el.style.left = (current.oleft + (e.pageX || e.screenX) - current.ox) + 'px';
		    }
		    else if(data.options.axis == 'y'){
		    	current.el.style.top = (current.otop + (e.pageY || e.screenY) - current.oy) + 'px';
		    }
		    else{
		    	current.el.style.left = (current.oleft + (e.pageX || e.screenX) - current.ox) + 'px';
		    	current.el.style.top = (current.otop + (e.pageY || e.screenY) - current.oy) + 'px';
		    }*/

			
			
			
			if (event.stopPropagation) 
			event.stopPropagation();
			if (event.preventDefault)
			event.preventDefault();
			return false;
		},
		
		stop: function(event) {
			
			
			var endPinchEvent = "";
            
            if(checkIfMobile()){
            	endPinchEvent = 'touchend';
            }
            else{
                endPinchEvent = 'mouseup';
            }
			var anevent = new jQuery.Event(endPinchEvent);
			$(window).trigger(anevent);
			
			var endTime = new Date().getTime();
			
			
			
			$(document).data("dragEndTime", endTime);
			
						
			
			var current = event.data.element;
			var data = $(current).data('pp-ppdrag');
			
			//$(current).data("dragStart", null);
			
			
			/*var tempElementX = parseInt($(current).offset().left);
	        var tempElementY = parseInt($(current).offset().top);
			$(current).transform2($(current).data("scale"), 0, 0);
			$(current).offset({top: tempElementY, left: tempElementX});*/
			
			
			
			
			
			//var current = $.ppdrag.current;
			//var data = $.data(current.el, 'pp-ppdrag');
			
			
			
			
			/*if(navigator.platform.indexOf("iPhone")!=-1 || navigator.platform.indexOf("iPod")!=-1 || navigator.platform.indexOf("iPad")!=-1 || navigator.platform.indexOf("Android")!=-1){
				$(document).unbind('touchmove');
				$(document).unbind('touchend');
			}*/
			if(checkIfMobile()){
				$(document).unbind('touchmove');
				$(document).unbind('touchend');
			}
			else{
			    $(document).unbind('mousemove');
			    $(document).unbind('mouseup');
		    }
			
			
			//$.ppdrag.removeEvent(document, 'mousemove', $.ppdrag.drag, true);
			//$.ppdrag.removeEvent(document, 'mouseup', $.ppdrag.stop, true);
			
			
			
			//if (data.options.zIndex) {
			//	current.el.style.zIndex = current.zIndex;
			//}
			
			if (data.options.stop) {
				data.options.stop.apply(current, [ current ]);
			}
			
			/*if (data.options.stop) {
				data.options.stop.apply(current.el, [ current.el ]);
			}*/
			
			
			//$.ppdrag.current = null;
			
			
			//if($(document).data("draggingElement")!=null){
            //    $(current).trigger('checkRender'+$(current).attr("name").substring(6, $(current).attr("name").length));
            //}
			
			//if($(document).data("draggingElement")!=null){
	            setTimeout(function(){
	                $(document).data("draggingElement", null);
	                $(current).trigger('checkRender'+$(current).attr("name").substring(6, $(current).attr("name").length));
	            }, 300);
            //}
			
			
			if (event.stopPropagation) 
			    event.stopPropagation();
			
			if (event.preventDefault)
			   event.preventDefault();


            

            
            
            

            /*if(endTime-$(current).data("dragStartTime")<100 || $(document).data("noOfMoves")<3){
            	var anevent = null;
            	if(checkIfMobile()){
            		anevent = new jQuery.Event('touchend');
        		}
        		else{
        			anevent = new jQuery.Event('mouseup');
        		}
            	//anevent.pageX = $(current).data("clickX");
		        //anevent.pageY = $(current).data("clickY");
		        //$(event.target).trigger(event.originalEvent || event);
		        //$(event.target).trigger("mouseup");
		        //alert("original x "+$(current).data("clickX"));
		        $(event.target).trigger(anevent);
		        
            }
            else{
            	$(document).data("clickX", null);
			    $(document).data("clickY", null);
			    
			    
			    //if(data.options.isEdgeLockedToBoundary){
	            	//$(event.target).remove();
	            	var swipeEvent = new jQuery.Event('swipe');
	            	$(current).trigger(swipeEvent, [$(current).data("ox"), $(current).data("lastox"), $(current).data("oy"), $(current).data("lastoy")]);
	        	//}
            }*/
            
            
            //$(document).data("dragStarted", null);
            //$(document).data("clickX", null);
			//$(document).data("clickY", null);
			
			
			if($(document).data("noOfMoves")>=3){
				var swipeEvent = new jQuery.Event('swipe');
	            $(current).trigger(swipeEvent, [$(current).data("ox"), $(current).data("lastox"), $(current).data("oy"), $(current).data("lastoy")]);
			}
            
            
            
            $(document).data("noOfMoves", null);
            
            
            
            
            //$(current).data("clickX", null);
            //$(current).data("clickY", null);
			
			return false;
		}
		
		/*addEvent: function(obj, type, fn, mode) {
			if (obj.addEventListener)
				obj.addEventListener(type, fn, mode);
			else if (obj.attachEvent) {
				obj["e"+type+fn] = fn;
				obj[type+fn] = function() { return obj["e"+type+fn](window.event); }
				obj.attachEvent("on"+type, obj[type+fn]);
			}
		},
		
		removeEvent: function(obj, type, fn, mode) {
			if (obj.removeEventListener)
				obj.removeEventListener(type, fn, mode);
			else if (obj.detachEvent) {
				obj.detachEvent("on"+type, obj[type+fn]);
				obj[type+fn] = null;
				obj["e"+type+fn] = null;
			}
		}*/
		
	};

})(jQuery);