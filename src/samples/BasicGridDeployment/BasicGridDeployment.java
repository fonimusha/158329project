package samples.BasicGridDeployment;

import FieldenNode.BasicFieldenNode;
import Functions.Implementations.Deepreach.*;
import BasicGrid.NodeTypes.BasicEscrowProducerFunction;
import GridConfigurations.PathIPPort ;
import BasicGrid.AgentTypes.BasicEscrowAgent;
import BasicGrid.AgentTypes.BasicLogonAgent;

public class BasicGridDeployment
{
    private String LOGON_AGENT_ID = "Sample Logon Agent" ;
    
    
    public BasicGridDeployment( DeepreachService logonDeepreachService,
                                String [] args)
    {             
        PathIPPort pathIPPort = new PathIPPort(args) ;
        new BasicGridDeployment(    logonDeepreachService,
                                    pathIPPort.ip,
                                    pathIPPort.port,
                                    pathIPPort.path) ;
    }
    
    public BasicGridDeployment( DeepreachService logonDeepreachService,
                                String ip,
                                int port,
                                String path)
    {
        //**********
        //Basic Grid
        //Smallest Grid is Node hosting a Logon Agent and another Node hosting an Escrow Consumer Agent
        
        ///////////////////
        //Create Logon Node
        BasicFieldenNode logonBasicFieldenNode = new BasicFieldenNode("Logon Node", path) ;
        Deepreach_WebAccessPoint_FunctionImplementation001 deepreach_WebAccessPoint_FunctionImplementation001
        = new Deepreach_WebAccessPoint_FunctionImplementation001(   logonBasicFieldenNode,
                                                                    Deepreach_WebAccessPoint_FunctionImplementation001.DEEPREACH_WEB_ACCESS_POINT_ID,
                                                                    ip, 
                                                                    port,
                                                                    path);

        logonBasicFieldenNode.addFunction(  deepreach_WebAccessPoint_FunctionImplementation001) ;      
        
        BasicLogonAgent basicLogonAgent = new BasicLogonAgent(logonDeepreachService,LOGON_AGENT_ID) ;
        logonBasicFieldenNode.addAgent(basicLogonAgent) ;
        
        try
        {
            Thread.sleep(3000);
        } catch(InterruptedException e)
        {
        }

        /////////////////////
        //Create Escrow Node
        BasicFieldenNode escrowBasicFieldenNode = new BasicFieldenNode("Escrow Node", path) ;
        BasicEscrowProducerFunction basicEscrowProducerFunction = new BasicEscrowProducerFunction(escrowBasicFieldenNode) ;
        escrowBasicFieldenNode.addFunction(basicEscrowProducerFunction);
        BasicEscrowAgent basicEscrowAgent = new BasicEscrowAgent("Basic Escrow Agent") ;
        escrowBasicFieldenNode.addAgent(basicEscrowAgent);
        
        // Register Fraglets with Escrow Consumer
        //
        //
        //
        //
           
        
        //Temporary wire up
        //
        basicLogonAgent.deepreachService.escrowApplicationConsumerService.tempBasicEscrowAgent = basicEscrowAgent ;
        
        
        // Only one Node needs to establish the connection
        basicLogonAgent.connect(escrowBasicFieldenNode) ;
        
        
        //Start Nodes
        System.out.println("Starting all Nodes...") ;
        logonBasicFieldenNode.start(); 
        escrowBasicFieldenNode.start();
        
        
        
        
        
        
        
        
        
        
        
        //old
//        DeepreachSingleNode deepreachSingleNode = new DeepreachSingleNode("192.168.1.64",80) ;
//        ClickEventAgent clickEventAgent = new ClickEventAgent(deepreachSingleNode.basicFieldenNode) ;
//        deepreachSingleNode.activate(clickEventAgent) ;
    }
}
