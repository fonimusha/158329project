package samples.BasicGridDeployment;

import Functions.Implementations.Deepreach.*;
import Functions.Interfaces.DataService.DataState;
import Functions.Interfaces.Deepreach.FragletInterface;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.EntityInterface;
import Functions.Interfaces.Deepreach.LocalReference;

public class ChildFraglet extends BasicFraglet
{
    public void newSession(DeepreachSessionHandlerInterface dsh, DataState dataState) 
    {
        System.out.println("Fraglet has received Session event") ;
        
        DeepreachThreadInterface mainDeepreachThread = dsh.getDeepreachThread();
        EntityInterface mainEntity = dsh.getEntity();
        
        String HTMLString = "<font size=\"3\" color=\"red\">And this is just from the Fraglet" ;
        Media html = new Media(HTMLString, 100,100);
        
        LocalReference logoLF = mainEntity.addMedia(    mainDeepreachThread, 
                                                        html, 
                                                        new SpaceImplementation001(0, 0.5, 0.4));
        
        
    }
}
