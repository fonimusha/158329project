package samples.BasicGridDeployment;

import Functions.Implementations.Deepreach.Space;
import GridConfigurations.PathIPPort;

public class Sample_FragletWithoutAuthorisation 
{
    public static void main(String[] args) 
    {
        PathIPPort pathIPPort = new PathIPPort(args) ;
        
        ChildFraglet basicFraglet = new ChildFraglet() ;
        
        new BasicGridDeployment(    new Sample_LogonDeepreachService(  basicFraglet,
                                                                new Space(0, 0.2, 0.8)),  
                                    pathIPPort.ip,
                                    pathIPPort.port,
                                    pathIPPort.path) ; 
    }
    
}
