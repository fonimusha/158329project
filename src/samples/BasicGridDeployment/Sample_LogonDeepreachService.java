package samples.BasicGridDeployment;

import Functions.Implementations.Deepreach.* ;
import Functions.Interfaces.DataService.DataState;
import Functions.Interfaces.Deepreach.FragletInterface;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.EntityInterface;
import Functions.Interfaces.Deepreach.LocalReference;

public class Sample_LogonDeepreachService extends DeepreachService
{
    private FragletInterface authorisedFraglet ;
    private Space launchSpace ;
    public Sample_LogonDeepreachService(FragletInterface aF, Space lS)
    {
        authorisedFraglet = aF ;
        launchSpace = lS ;
    }
        
    public void newSession( DeepreachSessionHandlerInterface deepreachSessionHandler, 
                            DataState dataState2) 
    {
        System.out.println("Deepreach Service : Incoming session...") ;
        
        DeepreachThreadInterface mainDeepreachThread = deepreachSessionHandler.getDeepreachThread();
        
        EntityInterface mainEntity = deepreachSessionHandler.getEntity();

        String HTMLString = "<font size=\"3\" color=\"yellow\">Logon Agent is bypassing authorisation and is launching Fraglet (BELOW)</font>" ;
        Media html = new Media(HTMLString, 100, 100);
        
        LocalReference logoLF = mainEntity.addMedia(    mainDeepreachThread, 
                                                        html, 
                                                        new SpaceImplementation001(-300, -300, 0.2));
        
        
        deepreachSessionHandler.launchApplication(null, mainDeepreachThread, mainEntity, launchSpace, authorisedFraglet) ;
    }
}