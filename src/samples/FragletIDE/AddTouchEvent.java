package samples.FragletIDE;


import Functions.Implementations.Deepreach.BasicEvent;
import Functions.Implementations.Deepreach.BasicFraglet;
import Functions.Implementations.Deepreach.Space;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.LocalReference;
import java.util.Dictionary;
import java.util.Vector;
import samples.FragletIDE.ReferenceInfo;
import samples.FragletIDE.UploadButtonTouchEvent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author HP User
 */
public class AddTouchEvent extends BasicEvent{
    UploadButtonTouchEvent event;
    Dictionary<LocalReference, ReferenceInfo> dict;
    DeepreachSessionHandlerInterface dsh;
    BasicFraglet fraglet;
    DeepreachThreadInterface mainDeepreachThread;

    public AddTouchEvent(LocalReference lF, DeepreachSessionHandlerInterface _dsh, UploadButtonTouchEvent _event,
            BasicFraglet _fraglet, DeepreachThreadInterface _mainDeepreachThread) {
        super(lF, _dsh);
        event = _event;
        dsh = _dsh;
        fraglet = _fraglet;
        mainDeepreachThread = _mainDeepreachThread;
    }
    
    public void eventOnServer(Vector parameters, DeepreachThreadInterface onServerEventDeepreachThread){
        LocalReference lf = event.getUploadedReference();
        FlyEvent fly = new FlyEvent(lf, dsh);
        fraglet.addTouchEvent(onServerEventDeepreachThread, fly);
    }
    
}
