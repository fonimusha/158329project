/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package samples.FragletIDE;

import FieldenData.Interfaces.ColumnType;
import FieldenData.Interfaces.Record;
import FieldenData.Interfaces.Table;
import FieldenData.Interfaces.TableSchema;

/**
 *
 * @author HP User
 */
public class ButtonTable {
    Table ButtonTable;
    
    public ButtonTable()
    {
        TableSchema tableButtonSchema = new TableSchema("Button");
        
        tableButtonSchema.AppendSchema("Name", new ColumnType(ColumnType.BINARY));
        tableButtonSchema.AppendSchema("Content", new ColumnType(ColumnType.BINARY));
        tableButtonSchema.AppendSchema("Space_x", new ColumnType(ColumnType.DECIMAL));
        tableButtonSchema.AppendSchema("Space_y", new ColumnType(ColumnType.DECIMAL));
        tableButtonSchema.AppendSchema("Space_siz", new ColumnType(ColumnType.DECIMAL));
        tableButtonSchema.AppendSchema("width", new ColumnType(ColumnType.INTEGER));
        tableButtonSchema.AppendSchema("height", new ColumnType(ColumnType.INTEGER));
        
        String startButtonHtml = "<button type=\"button\">Start</button>";
        String uploadButtonHtml = "<button type=\"button\">Upload</button>";
        
        String moveLeftButtonHtml = "<button type=\"button\">Left</button>";
        String moveRightButtonHtml = "<button type=\"button\">Right</button";
        String moveDownButtonHtml = "<button type=\"button\">Down</button";
        String moveUpButtonHtml = "<button type=\"button\">Up</button";
        String plusButtonHtml = "<button type=\"button\">+</button";
        String minusButtonHtml = "<button type=\"button\">-</button";
        String addEventButtonHtml = "<button type=\"button\">Add Fly Event</button";
        
        ButtonTable = new Table("Button", tableButtonSchema);
        ButtonTable.InsertRecord(makeRecord("start", startButtonHtml, 0.4, -0.4, 0.2, 70, 30));
        ButtonTable.InsertRecord(makeRecord("upload", uploadButtonHtml, 0.4, -0.3, 0.2, 70, 30));
        ButtonTable.InsertRecord(makeRecord("moveLeft", moveLeftButtonHtml, 0.4, -0.2, 0.2, 70, 30));
        ButtonTable.InsertRecord(makeRecord("moveRight", moveRightButtonHtml, 0.4, -0.1, 0.2, 70, 30));
        ButtonTable.InsertRecord(makeRecord("moveDown", moveDownButtonHtml, 0.4, 0, 0.2, 70, 30));
        ButtonTable.InsertRecord(makeRecord("moveUp", moveUpButtonHtml, 0.4, 0.1, 0.2, 70, 30));
        ButtonTable.InsertRecord(makeRecord("plus", plusButtonHtml, 0.4, 0.2, 0.2, 70, 30));
        ButtonTable.InsertRecord(makeRecord("minus", minusButtonHtml, 0.4, 0.3, 0.2, 70, 30));
        ButtonTable.InsertRecord(makeRecord("addEvent", addEventButtonHtml, 0.4, 0.4, 0.2, 70, 30));
    }
    
    public Table getTable()
    {
        return ButtonTable;
    }

    private Record makeRecord(String name, String content, double x, double y, double size, int width, int height) {
        Record record = new Record();
        record.SetBinary("Name", name);
        record.SetBinary("Content", content);
        record.SetDecimal("Space_x", x);
        record.SetDecimal("Space_y", y);
        record.SetDecimal("Space_size", size);
        record.SetInteger("width", width);
        record.SetInteger("height", height);
        return record;
    }
}
