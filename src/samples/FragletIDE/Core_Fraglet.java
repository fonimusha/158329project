package samples.FragletIDE;

import Functions.Implementations.Deepreach.BasicFraglet;
import Functions.Implementations.Deepreach.Media;
import Functions.Implementations.Deepreach.Space;
import Functions.Interfaces.DataService.DataState;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.EntityInterface;
import Functions.Interfaces.Deepreach.LocalReference;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import samples.SwipeEvent.*;

public class Core_Fraglet extends BasicFraglet
{
    EntityInterface fragletUniverseEntity;
    DeepreachThreadInterface mainDeepreachThread;
    Map<String, LocalReference> collection = new HashMap<String, LocalReference>();
    Dictionary<LocalReference, ReferenceInfo> localReferenceTable = new Hashtable<LocalReference, ReferenceInfo>();
    float button_x = -0.4f;
    
    
    @Override
    public void newSession(DeepreachSessionHandlerInterface dsh, DataState dataState) 
    {
        System.out.println("Fraglet IDE has received Session event") ;
        
        //////
        //#1 -  Acquire the foundation object
        
        //      Obtain the main thread - all instructions will be built on to this and pushed to the mobile
        mainDeepreachThread = dsh.getDeepreachThread();
        
        //      Obtain the "universe" or "space" on which to work 
       fragletUniverseEntity = dsh.getEntity();
        
        //////
        //#2 -  Create the objects that will go onto the universe
        
        //Create some simple HTML to display
        //String HTMLString = "<font size=\"3\" color=\"red\">Test Swipe DOWN</font>"          
        String designFrameHtml = "<div style=\"border-style:groove;border-color:white;width:200px;height:200px;color:white\">Design Area</div>";
        String startButtonHtml = "<button type=\"button\">Start</button>";
        String uploadButtonHtml = "<button type=\"button\">Upload</button>";
        
        String moveLeftButtonHtml = "<button type=\"button\">Left</button>";
        String moveRightButtonHtml = "<button type=\"button\">Right</button";
        String moveDownButtonHtml = "<button type=\"button\">Down</button";
        String moveUpButtonHtml = "<button type=\"button\">Up</button";
        String plusButtonHtml = "<button type=\"button\">+</button";
        String minusButtonHtml = "<button type=\"button\">-</button";
        String addEventButtonHtml = "<button type=\"button\">Add Fly Event</button";
        //Create it on a square Embedded HTML Browser of size 100x100 pixels
        Media designFrameMedia = new Media(designFrameHtml, 100,100);
        button_x = -0.4f;
        addButton("start", addButtonMedia(startButtonHtml));
        addButton("upload", addButtonMedia(uploadButtonHtml));
        addButton("moveLeft", addButtonMedia(moveLeftButtonHtml));
        addButton("moveRight", addButtonMedia(moveRightButtonHtml));
        addButton("moveDown", addButtonMedia(moveDownButtonHtml));
        addButton("moveUp", addButtonMedia(moveUpButtonHtml));
        addButton("plus", addButtonMedia(plusButtonHtml));
        addButton("minus", addButtonMedia(minusButtonHtml));
        addButton("addEvent", addButtonMedia(addEventButtonHtml));

        //Add them both to the Universe
        LocalReference designFrameReference = fragletUniverseEntity.addMedia(mainDeepreachThread, designFrameMedia, new Space(-0.3, -0.3, 0.3));
        // new Space(-0.3, -0.3, 0.3) -> new Space(-0.2, -0.2, 0.45)

        //////
        //#3 -  Create interactive events that will move these objects
        collection.put("designFrame", designFrameReference);
        
        StartButtonTouchEvent startButton = new StartButtonTouchEvent(getButton("start"), dsh, collection.values(), localReferenceTable, this);
        UploadButtonTouchEvent uploadButton = new UploadButtonTouchEvent(getButton("upload"), dsh, localReferenceTable);
        MoveLeftEvent moveLeftButton = new MoveLeftEvent(getButton("moveLeft"), dsh, uploadButton, localReferenceTable);
        ResizeBiggerEvent plusButton = new ResizeBiggerEvent(getButton("plus"), dsh, uploadButton, localReferenceTable);
        AddTouchEvent addEventButton = new AddTouchEvent(getButton("addEvent"), dsh, uploadButton, this, mainDeepreachThread);
        //FlyEvent addEventButton = new FlyEvent(getButton("addEvent"), dsh);

        addTouchEvent(mainDeepreachThread, startButton);
        addTouchEvent(mainDeepreachThread, uploadButton);
        addTouchEvent(mainDeepreachThread, moveLeftButton);
        addTouchEvent(mainDeepreachThread, plusButton);
        addTouchEvent(mainDeepreachThread, addEventButton);

        
        //addSwipeEvent(mainDeepreachThread, sampleEvent1, SWIPE_DOWN, false) ;
    }
    
    public Media addButtonMedia(String html){
        Media btn = new Media(html, 70, 30);
        return btn;
    }
    
    public void addButton(String key, Media button){
        LocalReference value = fragletUniverseEntity.addMedia(mainDeepreachThread, button, new Space(0.4, button_x, 0.2));
        button_x += 0.1f;
        collection.put(key, value);
    }
    
    public LocalReference getButton(String key){
        return collection.get(key);
    }
}
