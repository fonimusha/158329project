/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package samples.FragletIDE;

import Functions.Implementations.Deepreach.BasicEvent;
import Functions.Implementations.Deepreach.Space;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.LocalReference;
import java.util.Vector;

/**
 *
 * @author HP User
 */
public class FlyEvent extends BasicEvent{
    LocalReference localReference = null ;
    DeepreachSessionHandlerInterface dsh = null ;

    public FlyEvent(LocalReference lF, DeepreachSessionHandlerInterface _dsh) {
        super(lF, _dsh);
        localReference = lF ;
        dsh = _dsh ;
    }
    
    public void eventOnBrowser(DeepreachThreadInterface onBrowserEventDeepreachThread){
        localReference.moveLocalReference(onBrowserEventDeepreachThread, new Space(1, -1, 0), 2);
    }
    
}
