/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package samples.FragletIDE;

import Functions.Implementations.Deepreach.BasicEvent;
import Functions.Implementations.Deepreach.Space;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.LocalReference;
import java.util.Dictionary;
import java.util.Vector;

/**
 *
 * @author HP User
 */
public class MoveLeftEvent extends BasicEvent{
    UploadButtonTouchEvent event;
    Dictionary<LocalReference, ReferenceInfo> dict;

    public MoveLeftEvent(LocalReference lF, DeepreachSessionHandlerInterface _dsh, UploadButtonTouchEvent _event, Dictionary<LocalReference, ReferenceInfo> _dict) {
        super(lF, _dsh);
        event = _event;
        dict = _dict;
    }
    
    public void eventOnServer(Vector parameters, DeepreachThreadInterface onServerEventDeepreachThread){
        LocalReference lf = event.getUploadedReference();
        ReferenceInfo info = dict.get(lf);
        float x = info.getX() - 0.05f;
        info.setX(x);
        lf.moveLocalReference(onServerEventDeepreachThread, new Space(info.getX(), info.getY(), info.getSize()), 0);
    }
}
