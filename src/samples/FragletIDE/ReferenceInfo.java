/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package samples.FragletIDE;

import Functions.Implementations.Deepreach.BasicEvent;

/**
 *
 * @author HP User
 */
class ReferenceInfo {
    float coordinate_x;
    float coordinate_y;
    float size;
    BasicEvent event;
    
    
    public ReferenceInfo(float _x, float _y, float _size){
        coordinate_x = _x;
        coordinate_y = _y;
        size = _size;
    }
    
    public float getX(){
        return coordinate_x;
    }
    
    public void setX(float x){
        coordinate_x = x;
    }
    
    public float getY(){
        return coordinate_y;
    }
    
    public float getSize(){
        return size;
    }
    
    public void setSize(float s){
        size = s;
    }
    
    public void setEvent(BasicEvent e){
        event = e;
    }
    
    public BasicEvent getEvent(){
        return event;
    }
    
    public Boolean hasEvent(){
        if (event != null)
           return true;
        return false;
    }
}
