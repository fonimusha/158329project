package samples.FragletIDE;

import BasicGrid.Artifacts.BasicLogonDeepreachService;
import samples.BasicGridDeployment.BasicGridDeployment;

public class Sample_FragletIDE 
{
    public static void main(String[] args) 
    {
        Core_Fraglet core_Fraglet = new Core_Fraglet() ;
        
        BasicGridDeployment basicGridDeployment 
                = new BasicGridDeployment(  new BasicLogonDeepreachService(  core_Fraglet), 
                                            args) ;
    }
}
