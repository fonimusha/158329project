/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package samples.FragletIDE;

import Functions.Implementations.Deepreach.BasicEvent;
import Functions.Implementations.Deepreach.BasicFraglet;
import Functions.Implementations.Deepreach.Space;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.LocalReference;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Vector;

/**
 *
 * @author HP User
 */
public class StartButtonTouchEvent extends BasicEvent{
    LocalReference button = null ;
    DeepreachSessionHandlerInterface dsh = null ;
    Collection<LocalReference> buttons;
    LocalReference lf = null;
    Dictionary<LocalReference, ReferenceInfo> localReferenceTable;
    BasicFraglet fraglet;

    public StartButtonTouchEvent(LocalReference btn, DeepreachSessionHandlerInterface _dsh, Collection<LocalReference> btns, Dictionary<LocalReference, ReferenceInfo> dict,
             BasicFraglet _fraglet){
        super(btn, _dsh);
        dsh = _dsh ;
        button = btn;
        buttons = btns;
        localReferenceTable = dict;
        fraglet = _fraglet;
    }
    
    public StartButtonTouchEvent(LocalReference btn, DeepreachSessionHandlerInterface _dsh, LocalReference _lf){
        super(btn, _dsh);
        dsh = _dsh ;
        button = btn;
        lf = _lf;
    }
    
    public void eventOnBrowser(DeepreachThreadInterface onBrowserEventDeepreachThread)
    {
        for(LocalReference button : buttons){
            button.moveLocalReference(onBrowserEventDeepreachThread, new Space(2, 2, 0), 0);
        }
        //lf.moveLocalReference(this.eventDeepreachThread, new Space(2, 2, 0), 0);
        //replaced.moveLocalReference(this.eventDeepreachThread, new Space(-0.3, -0.3, 0.3), 0);
    }
    
    public void eventOnServer(Vector parameters, DeepreachThreadInterface onServerEventDeepreachThread){
        Enumeration<LocalReference> localReferences = localReferenceTable.keys();
        while(localReferences.hasMoreElements()){
            LocalReference lf = localReferences.nextElement();
            ReferenceInfo info = localReferenceTable.get(lf);
            lf.moveLocalReference(onServerEventDeepreachThread, new Space(info.getX()+0.1f, info.getY()+0.1f, info.getSize()+0.2f), 0);
            if (info.hasEvent()){
                fraglet.addTouchEvent(onServerEventDeepreachThread, info.getEvent());
            }
        }
    }

}
