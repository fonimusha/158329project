package samples.FragletIDE;

import samples.SwipeEvent.*;
import Functions.Implementations.Deepreach.BasicEvent;
import Functions.Implementations.Deepreach.SpaceImplementation001;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.LocalReference;

public class TestSwipeEvent extends BasicEvent
{
    LocalReference localReference = null ;
    DeepreachSessionHandlerInterface dsh = null ;
    
    public TestSwipeEvent(LocalReference lR, DeepreachSessionHandlerInterface _dsh) 
    {
        super(lR, _dsh) ;
        localReference = lR ;
        dsh = _dsh ;
    }
    
    public void eventOnBrowser(DeepreachThreadInterface onBrowserEventDeepreachThread)
    {
        System.out.println("This Swipe (#1) execution path was reached. You can do anything here") ;
        
        localReference.moveLocalReference(  this.eventDeepreachThread, 
                                            new SpaceImplementation001(-0.3, 1, 0), 
                                            1);
        //
        //
        // Additional event stuff can be handled here
    }
    
}
