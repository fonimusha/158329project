/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package samples.FragletIDE;

import Functions.Implementations.Deepreach.BasicEvent;
import Functions.Implementations.Deepreach.Media;
import Functions.Implementations.Deepreach.Space;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.EntityInterface;
import Functions.Interfaces.Deepreach.LocalReference;
import java.io.File;
import java.util.Dictionary;
import java.util.Vector;

/**
 *
 * @author HP User
 */
public class UploadButtonTouchEvent extends BasicEvent{
    LocalReference button = null;
    DeepreachSessionHandlerInterface dsh = null;
    LocalReference uploadedReference = null;
    Dictionary<LocalReference, ReferenceInfo> localReferenceTable;
    File dummyImageFile = new File("c:\\pic.jpg");
    float image_x = -0.2f;
    

    public UploadButtonTouchEvent(LocalReference btn, DeepreachSessionHandlerInterface _dsh){
        super(btn, _dsh);
        dsh = _dsh ;
        button = btn;
    }
    
    public UploadButtonTouchEvent(LocalReference btn, DeepreachSessionHandlerInterface _dsh, Dictionary<LocalReference, ReferenceInfo> dict){
        super(btn, _dsh);
        dsh = _dsh ;
        button = btn;
        localReferenceTable = dict;
    }
    
    public void eventOnBrowser(DeepreachThreadInterface onBrowserEventDeepreachThread)
    {
    }
    
    public void eventOnServer(Vector parameters, DeepreachThreadInterface onServerEventDeepreachThread){
        EntityInterface entity = dsh.getEntity();
        Media imageMedia = new Media(dummyImageFile);
        uploadedReference = entity.addMedia(onServerEventDeepreachThread , imageMedia, new Space(image_x, image_x, 0.3));
        ReferenceInfo imageInfo = new ReferenceInfo(image_x, image_x, 0.3f);
        image_x = image_x + 0.1f;
        localReferenceTable.put(uploadedReference, imageInfo);
        ReferenceInfo uploadedReference = imageInfo;
    }
    
    public LocalReference getUploadedReference(){
        return uploadedReference;
    }
}
