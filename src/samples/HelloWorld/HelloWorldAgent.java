package samples.HelloWorld;

import FieldenNode.Interfaces.IAgent;
import Functions.Implementations.Deepreach.Deepreach_WebAccessPoint_FunctionImplementation001;

public class HelloWorldAgent  extends IAgent
{
    public HelloWorldAgent()
    {
        super("Sample Hello World Agent - Version 1.1") ;
    }

    public void start()
    {
        System.out.println("Binding (HelloWorld) Deepreach Handler to Deepreach Function...");

        invokeFunction( Deepreach_WebAccessPoint_FunctionImplementation001.DEEPREACH_WEB_ACCESS_POINT_ID,   //1. Identify the Web Deepreach Access Point Function
                        new HelloWorld_DeepreachFunctionHandler()) ;                                        //2. The handler to manage incoming connections
    }
}
