package samples.HelloWorld;

import Functions.Implementations.Deepreach.* ; //.DeepreachFunctionHandler;
import Functions.Implementations.Deepreach.Entity;
import Functions.Implementations.Deepreach.Media;
import Functions.Implementations.Deepreach.SpaceImplementation001;
import Functions.Interfaces.DataService.DataState;
import Functions.Interfaces.Deepreach.*;

public class HelloWorld_DeepreachFunctionHandler extends DeepreachService 
{
    public void newSession  (   DeepreachSessionHandlerInterface deepreachSessionHandler, 
                                DataState dataState)
    {
        System.out.println("Hello World : Incoming session...") ;
        
        DeepreachThreadInterface mainDeepreachThread = deepreachSessionHandler.getDeepreachThread();
        EntityInterface mainEntity = deepreachSessionHandler.getEntity();

        String HTMLString = "<font size=\"3\" color=\"yellow\">Hello World!</font>" ;
        Media html = new Media(HTMLString, 100,100);
        
        LocalReference logoLF = mainEntity.addMedia(    mainDeepreachThread, 
                                                        html, 
                                                        new SpaceImplementation001(0, 0, 0.8));
    }
}
