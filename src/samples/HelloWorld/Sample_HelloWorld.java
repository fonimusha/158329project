package samples.HelloWorld;

import GridConfigurations.*;

public class Sample_HelloWorld
{
    public static void main(String[] args) 
    {
        new Sample_HelloWorld(new PathIPPort(args)) ;
    }
    
    public Sample_HelloWorld(PathIPPort pathIPPort)
    {
        DeepreachSingleNode deepreachSingleNode = new DeepreachSingleNode(pathIPPort) ;
        HelloWorldAgent helloWorldAgent = new HelloWorldAgent() ;
        deepreachSingleNode.addAgent(helloWorldAgent);
        deepreachSingleNode.start();
    }
}
