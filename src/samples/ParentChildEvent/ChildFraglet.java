package samples.ParentChildEvent;

import samples.BasicGridDeployment.*;
import Functions.Implementations.Deepreach.*;
import Functions.Interfaces.DataService.DataState;
import Functions.Interfaces.Deepreach.DeepreachEventInterface;
import Functions.Interfaces.Deepreach.FragletInterface;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.EntityInterface;
import Functions.Interfaces.Deepreach.LocalReference;

public class ChildFraglet extends BasicFraglet
{
    public void newSession(DeepreachSessionHandlerInterface dsh, DataState dataState) 
    {
        System.out.println("Fraglet has received Session event") ;
        
        DeepreachThreadInterface mainDeepreachThread = dsh.getDeepreachThread();
        EntityInterface mainEntity = dsh.getEntity();
        
        String HTMLString = "<font size=\"3\" color=\"red\">And this is just from the Child Fraglet.." ;
        Media html = new Media(HTMLString, 100,100);
        
        LocalReference localReference = mainEntity.addMedia(    mainDeepreachThread, 
                                                                html, 
                                                                new SpaceImplementation001(0, 0.5, 0.4));
        
        DeepreachThreadInterface escThread = mainDeepreachThread.splitThread() ;
        
        setEventPropagationFromChildOnBrowserEvent( mainDeepreachThread, localReference, escThread, 
                                                    DeepreachEventInterface.EVENT_TYPE_SWIPE_DOWN, false);
        
        SampleSwipeEvent sampleEvent1 = new SampleSwipeEvent(localReference, dsh) ;

        addSwipeEvent(mainDeepreachThread, sampleEvent1, SWIPE_DOWN, false) ;


//When working, push to BasicFraglet...
        
        
    }
}
