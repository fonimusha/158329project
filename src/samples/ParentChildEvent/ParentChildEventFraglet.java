package samples.ParentChildEvent;

import Functions.Implementations.Deepreach.BasicFraglet;
import Functions.Implementations.Deepreach.Media;
import Functions.Implementations.Deepreach.SpaceImplementation001;
import Functions.Interfaces.DataService.DataState;
import Functions.Interfaces.Deepreach.DeepreachEventInterface;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.EntityInterface;
import Functions.Interfaces.Deepreach.FragletInterface;
import Functions.Interfaces.Deepreach.LocalReference;

public class ParentChildEventFraglet extends BasicFraglet
{
    public void newSession(DeepreachSessionHandlerInterface deepreachSessionHandler, DataState dataState) 
    {
        System.out.println("Client Touch Event Fraglet has received Session event") ;
        
        DeepreachThreadInterface mainDeepreachThread = deepreachSessionHandler.getDeepreachThread();
        
        EntityInterface mainEntity = deepreachSessionHandler.getEntity();
        
        String HTMLString = "<font size=\"3\" color=\"red\">This is the Parent</font>" ;
        Media html = new Media(HTMLString, 100,100);
        
        LocalReference logoLF = mainEntity.addMedia(    mainDeepreachThread, 
                                                        html, 
                                                        new SpaceImplementation001(0, 0.5, 0.4)); 
        
        ChildFraglet childFraglet = new ChildFraglet() ;
        
        deepreachSessionHandler.launchApplication(null, mainDeepreachThread, mainEntity, new SpaceImplementation001(0, 200, 0.8), childFraglet) ;
        
        
    }
    
    public boolean receiveEventFromChildApplication(DeepreachThreadInterface onReceiveEventOnParent, FragletInterface childApplication, FragletInterface grandChildApplication) 
    {
        System.out.println("Parent received@!") ;
        return false;
    }
}
