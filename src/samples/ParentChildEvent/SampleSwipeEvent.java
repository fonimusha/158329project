package samples.ParentChildEvent;

import Functions.Implementations.Deepreach.BasicEvent;
import Functions.Implementations.Deepreach.SpaceImplementation001;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.LocalReference;

public class SampleSwipeEvent extends BasicEvent
{
    LocalReference localReference = null ;
    DeepreachSessionHandlerInterface dsh = null ;
    
    public SampleSwipeEvent(LocalReference lR, DeepreachSessionHandlerInterface _dsh) 
    {
        super(lR, _dsh) ;
        localReference = lR ;
        dsh = _dsh ;
    }
    
    public void eventOnBrowser(DeepreachThreadInterface onBrowserEventDeepreachThread)
    {
        System.out.println("This Swipe execution path was reached. ") ;
        
        localReference.moveLocalReference(  this.eventDeepreachThread, 
                                            new SpaceImplementation001(-0.3, 1, 0), 
                                            1);
        //
        //
        // Additional event stuff can be handled here
    }
    
}
