package samples.ParentChildEvent;

import Functions.Implementations.Deepreach.Space;
import samples.BasicGridDeployment.BasicGridDeployment;
import samples.BasicGridDeployment.Sample_LogonDeepreachService;

//Current Status: This sample has a defect - please do not use - update scheduled soon
public class Sample_ParentChildEvent 
{
    public static void main(String[] args) 
    {
        ParentChildEventFraglet parentChildEventFraglet = new ParentChildEventFraglet() ;
        
        BasicGridDeployment basicGridDeployment 
                = new BasicGridDeployment(  new Sample_LogonDeepreachService(  parentChildEventFraglet,
                                                                        new Space(0, 0.2, 0.8)), 
                                            args) ;
    }
}
