package samples.SwipeEvent;

import Functions.Implementations.Deepreach.BasicFraglet;
import Functions.Implementations.Deepreach.Media;
import Functions.Implementations.Deepreach.Space;
import Functions.Interfaces.DataService.DataState;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.EntityInterface;
import Functions.Interfaces.Deepreach.LocalReference;

public class ClientSwipeEventFraglet extends BasicFraglet
{
    public void newSession(DeepreachSessionHandlerInterface dsh, DataState dataState) 
    {
        System.out.println("Client Swipe Event Fraglet has received Session event") ;
        
        //////
        //#1 -  Acquire the foundation object
        
        //      Obtain the main thread - all instructions will be built on to this and pushed to the mobile
        DeepreachThreadInterface mainDeepreachThread = dsh.getDeepreachThread();
        
        //      Obtain the "universe" or "space" on which to work 
        EntityInterface fragletUniverseEntity = dsh.getEntity();
        
        //////
        //#2 -  Create the objects that will go onto the universe
        
        //Create some simple HTML to display
        String HTMLString = "<font size=\"3\" color=\"red\">Swipe DOWN #1 (Fixed)</font>" ;
        //Create it on a square Embedded HTML Browser of size 100x100 pixels
        Media html = new Media(HTMLString, 100,100);
        
        //Create another
        String HTMLString2 = "<font size=\"3\" color=\"red\">Swipe RIGHT #2 (Horizontal)</font>" ;
        Media html2 = new Media(HTMLString2, 100,100);
        
        //Add them both to the Universe
        LocalReference lf1 
                = fragletUniverseEntity.addMedia(       mainDeepreachThread, 
                                                        html, 
                                                        new Space(-0.3, -0.3, 0.3)); 
        
//This one is placed lower than the one above (see +0.3 vs -0.3 relates to x component in Space object)
        LocalReference lf2 
                = fragletUniverseEntity.addMedia(       mainDeepreachThread, 
                                                        html2, 
                                                        new Space(-0.3, 0.3, 0.3)); 
        
        //////
        //#3 -  Create interactive events that will move these objects
        
        SampleSwipeEvent1 sampleEvent1 = new SampleSwipeEvent1(lf1, dsh) ;
        
        SampleSwipeEvent2 sampleEvent2 = new SampleSwipeEvent2(lf2, dsh) ;
        
        addSwipeEvent(mainDeepreachThread, sampleEvent1, SWIPE_DOWN, false) ;
        
        addSwipeEvent(mainDeepreachThread, sampleEvent2, SWIPE_RIGHT, false) ;
    }
}
