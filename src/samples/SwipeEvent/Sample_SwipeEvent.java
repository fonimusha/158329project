package samples.SwipeEvent;

import Functions.Implementations.Deepreach.Space;
import samples.BasicGridDeployment.BasicGridDeployment;
import samples.BasicGridDeployment.Sample_LogonDeepreachService;

public class Sample_SwipeEvent 
{
    public static void main(String[] args) 
    {
        ClientSwipeEventFraglet clientSwipeEventFraglet = new ClientSwipeEventFraglet() ;
        
        BasicGridDeployment basicGridDeployment 
                = new BasicGridDeployment(  new Sample_LogonDeepreachService(  clientSwipeEventFraglet,
                                                                        new Space(0, 0.2, 0.8)), 
                                            args) ;
    }
}
