package samples.TouchEvent;

import Functions.Implementations.Deepreach.BasicFraglet;
import Functions.Implementations.Deepreach.Media;
import Functions.Implementations.Deepreach.SpaceImplementation001;
import Functions.Interfaces.DataService.DataState;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.EntityInterface;
import Functions.Interfaces.Deepreach.LocalReference;

public class ClientTouchEventFraglet extends BasicFraglet
{
    public void newSession(DeepreachSessionHandlerInterface dsh, DataState dataState) 
    {
        System.out.println("Client Touch Event Fraglet has received Session event") ;
        
        DeepreachThreadInterface mainDeepreachThread = dsh.getDeepreachThread();
        
        EntityInterface mainEntity = dsh.getEntity();
        
        String HTMLString = "<font size=\"3\" color=\"red\">Press this Text</font>" ;
        Media html = new Media(HTMLString, 100,100);
        
        LocalReference logoLF = mainEntity.addMedia(    mainDeepreachThread, 
                                                        html, 
                                                        new SpaceImplementation001(0, 0.5, 0.4)); 
        
        SampleTouchEvent sampleEvent = new SampleTouchEvent(logoLF, dsh) ;
        
        addTouchEvent(mainDeepreachThread, sampleEvent) ;
    }
}
