package samples.TouchEvent;

import Functions.Implementations.Deepreach.BasicEvent;
import Functions.Implementations.Deepreach.SpaceImplementation001;
import Functions.Interfaces.Deepreach.DeepreachSessionHandlerInterface;
import Functions.Interfaces.Deepreach.DeepreachThreadInterface;
import Functions.Interfaces.Deepreach.LocalReference;

public class SampleTouchEvent extends BasicEvent
{
    LocalReference localReference = null ;
    //BasicFraglet basicFraglet = null ;
    DeepreachSessionHandlerInterface dsh = null ;
    
    public SampleTouchEvent(LocalReference lR, DeepreachSessionHandlerInterface _dsh) //BasicFraglet bF)
    {
        super(lR, _dsh) ;
        localReference = lR ;
        //basicFraglet = bF ;
        dsh = _dsh ;
    }
    
    public void eventOnBrowser(DeepreachThreadInterface onBrowserEventDeepreachThread)
    {
        System.out.println("This Touch execution path was reached. You can do anything here") ;
        
        localReference.moveLocalReference(this.eventDeepreachThread, new SpaceImplementation001(0, 0.5, 3), 2); //3 means it is 3x bigger than screen when touched
        //
        //
        // Additional event stuff can be handled here
    }
    
}
