package samples.TouchEvent;

import Functions.Implementations.Deepreach.Space;
import samples.BasicGridDeployment.BasicGridDeployment;
import samples.BasicGridDeployment.Sample_LogonDeepreachService;

public class Sample_TouchEvent 
{
    public static void main(String[] args) 
    {
        ClientTouchEventFraglet clientTouchEventFraglet = new ClientTouchEventFraglet() ;
        
        BasicGridDeployment basicGridDeployment 
                = new BasicGridDeployment(  new Sample_LogonDeepreachService(  clientTouchEventFraglet,
                                                                        new Space(0, 200, 0.8)), 
                                            args) ;
    }
}
